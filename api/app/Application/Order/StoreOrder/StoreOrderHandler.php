<?php

declare(strict_types=1);

namespace App\Application\Order\StoreOrder;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\Cart\Cart;
use App\Domain\Cart\CartRepositoryInterface;
use App\Domain\Order\Order;
use App\Domain\Order\OrderRepositoryInterface;
use App\Domain\OrderItem\OrderItem;
use App\Domain\OrderItem\OrderItemRepositoryInterface;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

/**
 * Class StoreOrderHandler
 * @package App\Application\Order\StoreOrder
 */
class StoreOrderHandler implements HandlerInterface
{
    /** @var OrderRepositoryInterface $orderRepository */
    private OrderRepositoryInterface $orderRepository;

    /** @var OrderItemRepositoryInterface $orderItemRepository */
    private OrderItemRepositoryInterface $orderItemRepository;

    /** @var CartRepositoryInterface $cartRepository */
    private CartRepositoryInterface $cartRepository;

    /**
     * StoreOrderHandler constructor
     *
     * @param OrderRepositoryInterface $orderRepository
     * @param OrderItemRepositoryInterface $orderItemRepository
     * @param CartRepositoryInterface $cartRepository
     */
    public function __construct(
        OrderRepositoryInterface $orderRepository,
        OrderItemRepositoryInterface $orderItemRepository,
        CartRepositoryInterface $cartRepository
    ) {
        $this->orderRepository = $orderRepository;
        $this->orderItemRepository = $orderItemRepository;
        $this->cartRepository = $cartRepository;
    }

    /**
     * @param CommandInterface|StoreOrder $command
     *
     * @return Order|null
     */
    public function handle(CommandInterface $command): ?Order
    {
        $order = new Order();

        $order->name = $command->getName();
        $order->phone = $command->getPhone();
        $order->address = $command->getAddress();
        $order->sum = 0;
        $order->zip = $command->getZip();
        $order->department = $command->getDepartment();
        $order->delivery_id = $command->getDelivery();
        $order->pay_type_id = $command->getPayType();
        $order->status_id = Order::DEFAULT_STATUS_ID;

        $orderItems = [];

        foreach ($command->getCartItems() as $cartItem) {
            /** @var Cart $cartItem */
            $orderItem = new OrderItem();

            $orderItem->product_id = $cartItem->product->id;
            $orderItem->size_id = $cartItem->size->id;
            $orderItem->count = $cartItem->count;
            $orderItem->price = $cartItem->product->sale_price;

            $order->sum += ($orderItem->price * $cartItem->count);

            $orderItems[] = $orderItem;
        }

        DB::beginTransaction();
        try {
            $this->orderRepository->store($order);

            foreach ($orderItems as $orderItem) {
                $orderItem->order()->associate($order);
                $this->orderItemRepository->store($orderItem);
            }

            DB::commit();

            foreach ($command->getCartItems() as $cartItem) {
                $this->cartRepository->delete($cartItem);
            }
        } catch (\Exception $exception) {
            DB::rollBack();

            return null;
        }

        $this->sendEmail($order);

        return $order;
    }

    private function sendEmail(Order $order)
    {
        try {
            $orderLink = 'https://djelitop.com/desc/manage-orders/' . $order->id;
            Mail::raw($orderLink, function ($message) {
                $message->to('tatyana.djel@gmail.com')
                    ->subject("НОВЫЙ ЗАКАЗ НА САЙТЕ!!!!!");
            });
        }catch (\Exception $exception) {
            Log::error($exception->getMessage());
        }
    }
}