<?php

declare(strict_types=1);

namespace App\Http\Resourses\Season;

use App\Domain\Season\Season;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class SeasonResource
 * @package App\Http\Resourses\Season
 */
class SeasonResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        /** @var Season $this */
        return [
            'id' => $this->id,
            'type' => 'season',
            'attributes' => [
                'title' => $this->title,
                'slug' => $this->slug,
                'icon' => $this->icon,
                'createdAt' => $this->created_at ? $this->created_at->format('Y-m-d H:i:s') : '',
                'updatedAt' => $this->updated_at ? $this->updated_at->format('Y-m-d H:i:s') : '',
            ]
        ];
    }
}