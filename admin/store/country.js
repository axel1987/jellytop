import Country from "../dto/entities/Country";
import Pagination from "../dto/entities/Pagination";

/**
 * @returns {{
 * country: null,
 * countriesList: *[],
 * countriesSelectList: *[],
 * pagination: null,
 * }}
 */
export const state = () => ({
    country: null,
    countriesList: [],
    countriesSelectList: [],
    pagination: null
});

/**
 *
 * @type {{
 * getCountry(*): null,
 * getCountriesList(*): [],
 * getCountriesSelectList(*): [],
 * getCountryPagination(*): null
 * }}
 */
export const getters = {
    getCountry(state) {
        return state.country;
    },
    getCountriesList(state) {
        return state.countriesList;
    },
    getCountriesSelectList(state) {
        return state.countriesSelectList;
    },
    getCountryPagination(state) {
        return state.pagination;
    }
};

/**
 * @type {{
 * SET_COUNTRIES_LIST(*, *): void,
 * SET_COUNTRIES_SELECT_LIST(*, *): void,
 * SET_COUNTRY(*, *): void,
 * SET_COUNTRIES_PAGINATION(*, *): void,
 * }}
 */
export const mutations = {
    SET_COUNTRY(state, value) {
        state.country = value
    },
    SET_COUNTRIES_LIST(state, value) {
        state.countriesList = value
    },
    SET_COUNTRIES_SELECT_LIST(state, value) {
        state.countriesSelectList = value
    },
    SET_COUNTRIES_PAGINATION(state, value) {
        state.pagination = value
    },
};

/**
 * @type {{
 * registerCountry: ((function({commit: *}, *=): Promise<void>)|*),
 * updateCountry: ((function({commit: *}, *=): Promise<void>)|*),
 * fetchCountry: ((function({commit: *}, *=): Promise<void>)|*),
 * fetchCountriesList: ((function({commit: *}, *=): Promise<void>)|*),
 * fetchCountriesSelectList: ((function({commit: *}, *=): Promise<void>)|*)
 * }}
 */
export const actions = {
    fetchCountriesList: async function ({commit}, params = {}) {
        let {data} = await this.$axios.get('/countries', {params: params});

        const list = data.data.map((countryData) => {
            return new Country(countryData)
        });
        const pagination = data.pagination ? new Pagination(data.pagination) : null;

        commit('SET_COUNTRIES_LIST', list)
        commit('SET_COUNTRIES_PAGINATION', pagination)
    },
    fetchCountriesSelectList: async function ({commit}, params = {}) {
        let {data} = await this.$axios.get('/countries/list', {params: params});

        const list = data.data.map((countryData) => {
            return new Country(countryData)
        });

        commit('SET_COUNTRIES_SELECT_LIST', list)
    },
    fetchCountry: async function ({commit}, params = {}) {
        if (params.id) {
            let {data} = await this.$axios.get(`/countries/${params.id}`, params);
            commit('SET_COUNTRY', new Country(data))
        } else {
            commit('SET_COUNTRY', new Country(null))
        }
    },
    registerCountry: async function ({commit}, params = {}) {
        await this.$axios.post(`/countries`, params).then((res) => {
            if (res) {
                commit('SET_COUNTRY', new Country(res.data))
            }
        });
    },
    updateCountry: async function ({commit}, params = {}) {
        await this.$axios.put(`/countries/${params.id}`, params).then((res) => {
            if (res) {
                commit('SET_COUNTRY', new Country(res.data))
            }
        });
    },
    deleteCountry: async function ({commit}, params = {}) {
        await this.$axios.delete(`/countries/${params.id}`, params);
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations,
};
