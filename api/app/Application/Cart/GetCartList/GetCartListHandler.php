<?php

declare(strict_types=1);

namespace App\Application\Cart\GetCartList;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\Cart\CartRepositoryInterface;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class GetCartListHandler
 * @package App\Application\Cart\GetCartListByUser
 */
class GetCartListHandler implements HandlerInterface
{
    /** @var CartRepositoryInterface $cartRepository */
    private CartRepositoryInterface $cartRepository;

    /**
     * GetCartListByUserHandler constructor
     *
     * @param CartRepositoryInterface $cartRepository
     */
    public function __construct(CartRepositoryInterface $cartRepository)
    {
        $this->cartRepository = $cartRepository;
    }

    /**
     * @param CommandInterface|GetCartList $command
     *
     * @return LengthAwarePaginator|Collection
     */
    public function handle(CommandInterface $command): Collection
    {
        return $this->cartRepository->all(
            $command->getFilter()
        );
    }
}