<?php

declare(strict_types=1);

namespace App\Domain\Type;

use Exception;

/**
 * Class TypeException
 * @package App\Domain\Type
 */
class TypeException extends Exception
{
    /**
     * @param int $id
     *
     * @throws TypeException
     */
    public static function notFound(int $id)
    {
        throw new TypeException(sprintf('Color with id %d not found', $id));
    }
}