<?php

declare(strict_types=1);

namespace App\Http\Controllers\Site;

use App\Application\Cart\DeleteCartItem\DeleteCartItem;
use App\Application\Cart\GetCartByFilter\GetCartByFilter;
use App\Application\Cart\GetCartList\GetCartList;
use App\Application\Cart\StoreCartItem\StoreCartItem;
use App\Application\Color\GetColorByFilter\GetColorByFilter;
use App\Application\Product\GetProductByFilter\GetProductByFilter;
use App\Application\Size\GetSizeByFilter\GetSizeByFilter;
use App\Domain\Cart\CartFilter;
use App\Domain\Color\ColorFilter;
use App\Domain\Product\ProductFilter;
use App\Domain\Size\SizeFilter;
use App\Http\Controllers\Controller;
use App\Http\Requests\Site\Cart\CartRequest;
use App\Http\Resourses\Cart\CartListResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * Class CartController
 * @package App\Http\Controllers\Site
 */
class CartController extends Controller
{
    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $filter = CartFilter::fromRequest($request);
        $list = $this->execute(new GetCartList($filter));

        return response()->json(new CartListResource($list));
    }

    /**
     * @param CartRequest $request
     *
     * @return JsonResponse
     */
    public function store(CartRequest $request): JsonResponse
    {
        $productFilter = new ProductFilter();
        $productFilter->setId($request->get('productId'));
        $product = $this->execute(new GetProductByFilter($productFilter));

        $sizeFilter = new SizeFilter();
        $sizeFilter->setId($request->get('sizeId'));
        $size = $this->execute(new GetSizeByFilter($sizeFilter));

        $this->execute(new StoreCartItem(
            $request->header('access-session'),
            $product,
            $size,
            (int)$request->get('count')
        ));

        return response()->json(null, 201);
    }

    /**
     * @param int $id
     *
     * @return JsonResponse
     */
    public function delete(int $id): JsonResponse
    {
        $cartFilter = new CartFilter();
        $cartFilter->setId($id);
        $cart = $this->execute(new GetCartByFilter($cartFilter));

        $this->execute(new DeleteCartItem($cart));

        return response()->json(null, 204);
    }
}