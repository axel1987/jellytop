<?php

declare(strict_types=1);

namespace App\Console\Commands;

use App\Domain\Product\Product;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

/**
 * Class GenerateSitemap
 * @package App\Console\Commands
 */
class GenerateSitemap extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sitemap:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate a sitemap';

    /**
     * @return void
     */
    public function handle()
    {
        $products = Product::query()->where('in_stock', '=', true)->get();
        $date = Carbon::now()->format('Y-m-d');

        $sitemap = view('sitemap.sitemap', compact('products', 'date'))->render();
        Storage::disk('public')->put('sitemap/sitemap.xml', $sitemap);

        shell_exec('cp /var/www/project/api/storage/app/public/sitemap/sitemap.xml /var/www/project/api/public/sitemap.xml');
    }
}