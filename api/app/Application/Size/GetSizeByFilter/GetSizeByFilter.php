<?php

declare(strict_types=1);

namespace App\Application\Size\GetSizeByFilter;

use App\Contract\Core\CommandInterface;
use App\Domain\Size\SizeFilter;

/**
 * Class GetSizeByFilter
 * @package App\Application\Size\GetSizeByFilter
 */
class GetSizeByFilter implements CommandInterface
{
    /** @var SizeFilter $filter */
    private SizeFilter $filter;

    /**
     * GetSizeByFilter constructor
     *
     * @param SizeFilter $filter
     */
    public function __construct(SizeFilter $filter)
    {
        $this->filter = $filter;
    }

    /**
     * @return SizeFilter
     */
    public function getFilter(): SizeFilter
    {
        return $this->filter;
    }
}