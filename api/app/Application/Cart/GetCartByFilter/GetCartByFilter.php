<?php

declare(strict_types=1);

namespace App\Application\Cart\GetCartByFilter;

use App\Contract\Core\CommandInterface;
use App\Domain\Cart\CartFilter;

/**
 * Class GetCartByFilter
 * @package App\Application\Cart\GetCartByFilter
 */
class GetCartByFilter implements CommandInterface
{
    /** @var CartFilter $filter */
    private CartFilter $filter;

    /**
     * GetCartByFilter constructor
     *
     * @param CartFilter $filter
     */
    public function __construct(CartFilter $filter)
    {
        $this->filter = $filter;
    }

    /**
     * @return CartFilter
     */
    public function getFilter(): CartFilter
    {
        return $this->filter;
    }
}