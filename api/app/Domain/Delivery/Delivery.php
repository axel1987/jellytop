<?php

declare(strict_types=1);

namespace App\Domain\Delivery;

use Barryvdh\LaravelIdeHelper\Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class Delivery
 *
 * @package App\Domain\Delivery
 * @property int $id
 * @property array $title
 * @property array|null $description
 * @property string|null $icon
 * @property float|null $min_price
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|Delivery newModelQuery()
 * @method static Builder|Delivery newQuery()
 * @method static Builder|Delivery query()
 * @method static Builder|Delivery whereCreatedAt($value)
 * @method static Builder|Delivery whereMinPrice($value)
 * @method static Builder|Delivery whereIcon($value)
 * @method static Builder|Delivery whereId($value)
 * @method static Builder|Delivery whereTitle($value)
 * @method static Builder|Delivery whereDescription($value)
 * @method static Builder|Delivery whereUpdatedAt($value)
 * @mixin Eloquent
 */
class Delivery extends Model
{
    /** @var string $table */
    protected $table = 'delivery';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
        'icon',
        'min_price',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * @var string[]
     */
    protected $casts = [
        'title' => 'array',
        'description' => 'array',
        'min_price' => 'float',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];
}