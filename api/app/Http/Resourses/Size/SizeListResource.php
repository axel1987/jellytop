<?php

declare(strict_types=1);

namespace App\Http\Resourses\Size;

use App\Domain\Size\Size;
use App\Http\Resourses\BaseCollectionResource;

/**
 * Class SizeListResource
 * @package App\Http\Resourses\Size
 */
class SizeListResource extends BaseCollectionResource
{
    /**
     * @param Size $item
     *
     * @return array
     */
    protected function getItemData($item): array
    {
        return [
            'id' => $item->id,
            'type' => 'size',
            'attributes' => [
                'size' => round($item->size),
                'createdAt' => $item->created_at ? $item->created_at->format('Y-m-d H:i:s') : '',
                'updatedAt' => $item->updated_at ? $item->updated_at->format('Y-m-d H:i:s') : '',
            ]
        ];
    }
}