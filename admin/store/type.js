import Type from "../dto/entities/Type";
import Pagination from "../dto/entities/Pagination";

/**
 * @returns {{
 * type: null,
 * typesList: *[],
 * typesSelectList: *[],
 * pagination: null,
 * }}
 */
export const state = () => ({
    type: null,
    typesList: [],
    typesSelectList: [],
    pagination: null
});

/**
 *
 * @type {{
 * getType(*): null,
 * getTypesList(*): [],
 * getTypesSelectList(*): [],
 * getTypePagination(*): null
 * }}
 */
export const getters = {
    getType(state) {
        return state.type;
    },
    getTypesList(state) {
        return state.typesList;
    },
    getTypesSelectList(state) {
        return state.typesSelectList;
    },
    getTypePagination(state) {
        return state.pagination;
    }
};

/**
 * @type {{
 * SET_TYPES_LIST(*, *): void,
 * SET_TYPES_SELECT_LIST(*, *): void,
 * SET_TYPE(*, *): void,
 * SET_TYPES_PAGINATION(*, *): void,
 * }}
 */
export const mutations = {
    SET_TYPE(state, value) {
        state.type = value
    },
    SET_TYPES_LIST(state, value) {
        state.typesList = value
    },
    SET_TYPES_SELECT_LIST(state, value) {
        state.typesSelectList = value
    },
    SET_TYPES_PAGINATION(state, value) {
        state.pagination = value
    },
};

/**
 * @type {{
 * registerType: ((function({commit: *}, *=): Promise<void>)|*),
 * updateType: ((function({commit: *}, *=): Promise<void>)|*),
 * fetchType: ((function({commit: *}, *=): Promise<void>)|*),
 * fetchTypesList: ((function({commit: *}, *=): Promise<void>)|*),
 * fetchTypesSelectList: ((function({commit: *}, *=): Promise<void>)|*),
 * deleteType: ((function({commit: *}, *=): Promise<void>)|*)
 * }}
 */
export const actions = {
    fetchTypesList: async function ({commit}, params = {}) {
        let {data} = await this.$axios.get('/types', {params: params});

        const list = data.data.map((typeData) => {
            return new Type(typeData)
        });
        const pagination = data.pagination ? new Pagination(data.pagination) : null;

        commit('SET_TYPES_LIST', list)
        commit('SET_TYPES_PAGINATION', pagination)
    },
    fetchTypesSelectList: async function ({commit}, params = {}) {
        let {data} = await this.$axios.get('/types/list', params);

        const list = data.data.map((typeData) => {
            return new Type(typeData)
        });

        commit('SET_TYPES_SELECT_LIST', list)
    },
    fetchType: async function ({commit}, params = {}) {
        if (params.id) {
            let {data} = await this.$axios.get(`/types/${params.id}`, params);
            commit('SET_TYPE', new Type(data))
        } else {
            commit('SET_TYPE', new Type(null))
        }
    },
    registerType: async function ({commit}, params = {}) {
        await this.$axios.post(`/types`, params).then((res) => {
            if (res) {
                commit('SET_TYPE', new Type(res.data))
            }
        });
    },
    updateType: async function ({commit}, params = {}) {
        await this.$axios.put(`/types/${params.id}`, params).then((res) => {
            if (res) {
                commit('SET_TYPE', new Type(res.data))
            }
        });
    },
    deleteType: async function ({commit}, params = {}) {
        await this.$axios.delete(`/types/${params.id}`, params);
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations,
};
