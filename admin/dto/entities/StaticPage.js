export default class StaticPage {
    /**
     * @param data
     */
    constructor(data = {}) {
        this.id = data.id ? data.id : null;
        this.page = data.attributes && data.attributes.page ?
            data.attributes.page :
            null;
        this.pageName = data.attributes && data.attributes.pageName ?
            data.attributes.pageName :
            null;
        this.content = data.attributes && data.attributes.content ?
            data.attributes.content :
            null;
        this.createdAt = data.attributes && data.attributes.createdAt ?
            data.attributes.createdAt :
            null;
        this.updatedAt = data.attributes && data.attributes.updatedAt ?
            data.attributes.updatedAt :
            null;
    }
}