<?php

declare(strict_types=1);

namespace App\Application\PayType\GetPayTypeByFilter;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\PayType\PayTypeRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * Class GetPayTypeByFilterHandler
 * @package App\Application\PayType\GetPayTypeByFilter
 */
class GetPayTypeByFilterHandler implements HandlerInterface
{
    /** @var PayTypeRepositoryInterface $payTypeRepository */
    private PayTypeRepositoryInterface $payTypeRepository;

    /**
     * GetPayTypeByFilterHandler constructor
     *
     * @param PayTypeRepositoryInterface $payTypeRepository
     */
    public function __construct(PayTypeRepositoryInterface $payTypeRepository)
    {
        $this->payTypeRepository = $payTypeRepository;
    }

    /**
     * @param GetPayTypeByFilter $command
     *
     * @return Model|null
     */
    public function handle(CommandInterface $command): ?Model
    {
        return $this->payTypeRepository->one(
            $command->getFilter()
        );
    }
}