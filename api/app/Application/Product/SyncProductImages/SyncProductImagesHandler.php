<?php

declare(strict_types=1);

namespace App\Application\Product\SyncProductImages;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\ProductImage\ProductImage;
use App\Domain\ProductImage\ProductImageRepositoryInterface;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

/**
 * Class SyncProductImagesHandler
 * @package App\Application\Product\SyncProductImages
 */
class SyncProductImagesHandler implements HandlerInterface
{
    /** @var ProductImageRepositoryInterface $productImageRepository */
    private ProductImageRepositoryInterface $productImageRepository;

    /**
     * SyncProductImagesHandler constructor
     *
     * @param ProductImageRepositoryInterface $productImageRepository
     */
    public function __construct(ProductImageRepositoryInterface $productImageRepository)
    {
        $this->productImageRepository = $productImageRepository;
    }


    /**
     * @param SyncProductImages $command
     *
     * @return void
     */
    public function handle(CommandInterface $command): void
    {
        $product = $command->getProduct();
        $imagesArr = $command->getImages();
        $existImages = $product->images;
        $existImagesUrls = $existImages->pluck('url')->toArray();

        foreach ($imagesArr as $imageUrl) {
            if (!in_array($imageUrl, $existImagesUrls)) {
                $newImage = new ProductImage([
                    'url' => $imageUrl,
                    'is_main' => false
                ]);

                $newImage->product()->associate($product);
                $this->productImageRepository->store($newImage);

                $newImage->webp_url = $this->makeWebp($newImage->url);
                $this->productImageRepository->store($newImage);
            }
        }
    }

    /**
     * @param string $url
     *
     * @return string
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    protected function makeWebp(string $url): string
    {
        $img = Storage::disk('public')->get(str_replace('/storage', '', $url));
        $newImage = Image::make($img);
        $newImage = $newImage->encode('webp');

        $filepath = explode('.', $url)[0] . '.webp';

        Storage::disk('public')->put(str_replace('/storage', '', $filepath), $newImage);
        return Storage::disk('public')->url(str_replace('/storage', '', $filepath));
    }
}