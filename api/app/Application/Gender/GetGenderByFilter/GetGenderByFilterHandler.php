<?php

declare(strict_types=1);

namespace App\Application\Gender\GetGenderByFilter;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\Gender\Gender;
use App\Domain\Gender\GenderRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * Class GetGenderByFilterHandler
 * @package App\Application\Gender\GetGenderByFilter
 */
class GetGenderByFilterHandler implements HandlerInterface
{
    /** @var GenderRepositoryInterface $genderRepository */
    private GenderRepositoryInterface $genderRepository;

    /**
     * GetGenderByFilterHandler constructor
     *
     * @param GenderRepositoryInterface $genderRepository
     */
    public function __construct(GenderRepositoryInterface $genderRepository)
    {
        $this->genderRepository = $genderRepository;
    }

    /**
     * @param CommandInterface|GetGenderByFilter $command
     *
     * @return Gender|null
     */
    public function handle(CommandInterface $command): ?Model
    {
        return $this->genderRepository->one(
            $command->getFilter()
        );
    }
}