<?php

declare(strict_types=1);

namespace App\Application\Gender\UpdateGender;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\Gender\Gender;
use App\Domain\Gender\GenderRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * Class UpdateGenderHandler
 * @package App\Application\Gender\UpdateGender
 */
class UpdateGenderHandler implements HandlerInterface
{
    /** @var GenderRepositoryInterface $genderRepository */
    private GenderRepositoryInterface $genderRepository;

    /**
     * UpdateGenderHandler constructor
     *
     * @param GenderRepositoryInterface $genderRepository
     */
    public function __construct(GenderRepositoryInterface $genderRepository)
    {
        $this->genderRepository = $genderRepository;
    }

    /**
     * @param CommandInterface|UpdateGender $command
     *
     * @return Gender
     */
    public function handle(CommandInterface $command): Model
    {
        $gender = $command->getGender();

        $gender->title = $command->getTitle();
        $gender->icon = $command->getIcon();

        return $this->genderRepository->store($gender);
    }
}