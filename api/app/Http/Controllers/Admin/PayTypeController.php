<?php

declare(strict_types=1);

namespace App\Http\Controllers\Admin;

use App\Application\PayType\DeletePayType\DeletePayType;
use App\Application\PayType\GetPayTypeByFilter\GetPayTypeByFilter;
use App\Application\PayType\GetPayTypesList\GetPayTypesList;
use App\Application\PayType\StorePayType\StorePayType;
use App\Application\PayType\UpdatePayType\UpdatePayType;
use App\Domain\PayType\PayTypeFilter;
use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\PayType\PayTypeListRequest;
use App\Http\Requests\Dashboard\PayType\PayTypeRequest;
use App\Http\Resourses\PayType\PayTypeListResource;
use App\Http\Resourses\PayType\PayTypeResource;
use App\Infrastructure\Core\Pagination;
use App\Infrastructure\Core\Sorting;
use Illuminate\Http\JsonResponse;

/**
 * Class PayTypeController
 * @package App\Http\Controllers
 */
class PayTypeController extends Controller
{
    /**
     * @param PayTypeListRequest $request
     *
     * @return JsonResponse
     */
    public function index(PayTypeListRequest $request): JsonResponse
    {
        $filter = PayTypeFilter::fromRequest($request);
        $pagination = Pagination::fromRequest($request);
        $sorting = Sorting::fromRequest($request);

        $list = $this->execute(new GetPayTypesList(
            $filter,
            $pagination,
            $sorting
        ));

        return new JsonResponse(new PayTypeListResource($list));
    }

    /**
     * @param PayTypeListRequest $request
     *
     * @return JsonResponse
     */
    public function list(PayTypeListRequest $request): JsonResponse
    {
        $filter = PayTypeFilter::fromRequest($request);
        $sorting = Sorting::fromRequest($request);

        $list = $this->execute(new GetPayTypesList(
            $filter,
            null,
            $sorting
        ));

        return new JsonResponse(new PayTypeListResource($list));
    }

    /**
     * @param int $id
     *
     * @return JsonResponse
     */
    public function view(int $id): JsonResponse
    {
        $filter = new PayTypeFilter();
        $filter->setId($id);

        $type = $this->execute(new GetPayTypeByFilter($filter));

        return new JsonResponse(new PayTypeResource($type));
    }

    /**
     * @param PayTypeRequest $request
     *
     * @return JsonResponse
     */
    public function store(PayTypeRequest $request): JsonResponse
    {
        $type = $this->execute(new StorePayType(
            $request->get('title'),
            $request->get('description'),
            $request->get('icon'),
        ));

        return new JsonResponse(new PayTypeResource($type));
    }

    /**
     * @param PayTypeRequest $request
     * @param int $id
     *
     * @return JsonResponse
     */
    public function update(PayTypeRequest $request, int $id): JsonResponse
    {
        $filter = new PayTypeFilter();
        $filter->setId($id);

        $type = $this->execute(new GetPayTypeByFilter($filter));
        $type = $this->execute(new UpdatePayType(
            $type,
            $request->get('title'),
            $request->get('description'),
            $request->get('icon'),
        ));

        return new JsonResponse(new PayTypeResource($type));
    }

    /**
     * @param int $id
     *
     * @return JsonResponse
     */
    public function delete(int $id): JsonResponse
    {
        $filter = new PayTypeFilter();
        $filter->setId($id);

        $type = $this->execute(new GetPayTypeByFilter($filter));
        $this->execute(new DeletePayType($type));

        return new JsonResponse(null, 204);
    }
}