<?php

declare(strict_types=1);

namespace App\Application\Size\GetSizesList;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\PaginationInterface;
use App\Contract\Core\SortingInterface;
use App\Domain\Size\SizeFilter;

/**
 * Class GetSizesList
 * @package App\Application\Size\GetSizesList
 */
class GetSizesList implements CommandInterface
{
    /** @var SizeFilter $filter */
    private SizeFilter $filter;

    /** @var PaginationInterface|null $pagination */
    private ?PaginationInterface $pagination;

    /** @var SortingInterface|null $sorting */
    private ?SortingInterface $sorting;

    /**
     * GetSizesList constructor
     *
     * @param SizeFilter $filter
     * @param PaginationInterface|null $pagination
     * @param SortingInterface|null $sorting
     */
    public function __construct(
        SizeFilter $filter,
        ?PaginationInterface $pagination = null,
        ?SortingInterface $sorting = null
    ) {
        $this->filter = $filter;
        $this->pagination = $pagination;
        $this->sorting = $sorting;
    }

    /**
     * @return SizeFilter
     */
    public function getFilter(): SizeFilter
    {
        return $this->filter;
    }

    /**
     * @return PaginationInterface|null
     */
    public function getPagination(): ?PaginationInterface
    {
        return $this->pagination;
    }

    /**
     * @return SortingInterface|null
     */
    public function getSorting(): ?SortingInterface
    {
        return $this->sorting;
    }
}