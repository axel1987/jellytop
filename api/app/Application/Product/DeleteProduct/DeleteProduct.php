<?php

declare(strict_types=1);

namespace App\Application\Product\DeleteProduct;

use App\Contract\Core\CommandInterface;
use App\Domain\Product\Product;

/**
 * Class DeleteProduct
 * @package App\Application\Product\DeleteProduct
 */
class DeleteProduct implements CommandInterface
{
    /** @var Product $product */
    private Product $product;

    /**
     * DeleteProduct constructor
     *
     * @param Product $product
     */
    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    /**
     * @return Product
     */
    public function getProduct(): Product
    {
        return $this->product;
    }
}