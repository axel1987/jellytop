<?php

declare(strict_types=1);

namespace App\Application\Gender\GetGendersList;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\Gender\GenderRepositoryInterface;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

/**
 * Class GetGendersListHandler
 * @package App\Application\Gender\GetGendersList
 */
class GetGendersListHandler implements HandlerInterface
{
    /** @var GenderRepositoryInterface $genderRepository */
    private GenderRepositoryInterface $genderRepository;

    /**
     * GetGendersListHandler constructor
     *
     * @param GenderRepositoryInterface $genderRepository
     */
    public function __construct(GenderRepositoryInterface $genderRepository)
    {
        $this->genderRepository = $genderRepository;
    }

    /**
     * @param CommandInterface|GetGendersList $command
     *
     * @return mixed|LengthAwarePaginator|Collection
     */
    public function handle(CommandInterface $command): mixed
    {
        return $this->genderRepository->all(
            $command->getFilter(),
            $command->getPagination(),
            $command->getSorting()
        );
    }
}