<?php

declare(strict_types=1);

namespace App\Domain\Size;

use Barryvdh\LaravelIdeHelper\Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class Size
 *
 * @package App\Domain\Size
 * @property int $id
 * @property float $size
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|Size newModelQuery()
 * @method static Builder|Size newQuery()
 * @method static Builder|Size query()
 * @method static Builder|Size whereCreatedAt($value)
 * @method static Builder|Size whereId($value)
 * @method static Builder|Size whereSize($value)
 * @method static Builder|Size whereUpdatedAt($value)
 * @mixin Eloquent
 */
class Size extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'size',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * @var string[]
     */
    protected $casts = [
        'size' => 'float',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];
}