<?php

declare(strict_types=1);

namespace App\Domain\PayType;

use App\Contract\Core\DatabaseRepositoryInterface;

/**
 * Interface PayTypeRepositoryInterface
 * @package App\Domain\PayType
 */
interface PayTypeRepositoryInterface extends DatabaseRepositoryInterface
{

}