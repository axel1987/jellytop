<?php

declare(strict_types=1);

namespace App\Domain\Color;

use App\Contract\Core\FilterInterface;
use Illuminate\Http\Request;

/**
 * Class ColorFilter
 * @package App\Domain\Color
 */
class ColorFilter implements FilterInterface
{
    /** @var int|null $id */
    private ?int $id = null;

    /** @var array|null $ids */
    private ?array $ids = null;

    /**
     * @param Request $request
     *
     * @return FilterInterface|ColorFilter
     */
    public static function fromRequest(Request $request): FilterInterface
    {
        $filter = new self();

        return $filter;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return array|null
     */
    public function getIds(): ?array
    {
        return $this->ids;
    }

    /**
     * @param array|null $ids
     */
    public function setIds(?array $ids): void
    {
        $this->ids = $ids;
    }
}