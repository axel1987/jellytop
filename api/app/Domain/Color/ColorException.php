<?php

declare(strict_types=1);

namespace App\Domain\Color;

use Exception;

/**
 * Class ColorException
 * @package App\Domain\Color
 */
class ColorException extends Exception
{
    /**
     * @param int $id
     * @throws ColorException
     */
    public static function notFound(int $id)
    {
        throw new ColorException(sprintf('Color with id %d not found', $id));
    }
}