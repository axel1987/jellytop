<?php

declare(strict_types=1);

namespace App\Application\PayType\DeletePayType;

use App\Contract\Core\CommandInterface;
use App\Domain\PayType\PayType;

/**
 * Class DeletePayType
 * @package App\Application\PayType\DeletePayType
 */
class DeletePayType implements CommandInterface
{
    /** @var PayType $payType */
    private PayType $payType;

    /**
     * DeletePayType constructor
     *
     * @param PayType $payType
     */
    public function __construct(PayType $payType)
    {
        $this->payType = $payType;
    }

    /**
     * @return PayType
     */
    public function getPayType(): PayType
    {
        return $this->payType;
    }
}