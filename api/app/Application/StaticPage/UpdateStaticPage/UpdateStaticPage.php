<?php

declare(strict_types=1);

namespace App\Application\StaticPage\UpdateStaticPage;

use App\Contract\Core\CommandInterface;
use App\Domain\StaticPage\StaticPage;

/**
 * Class UpdateStaticPage
 * @package App\Application\StaticPage\UpdateStaticPage
 */
class UpdateStaticPage implements CommandInterface
{
    /** @var StaticPage $staticPage */
    private StaticPage $staticPage;

    /** @var array $content */
    private array $content;

    /**
     * UpdateStaticPage constructor
     *
     * @param StaticPage $staticPage
     * @param array $content
     */
    public function __construct(
        StaticPage $staticPage,
        array $content
    ) {
        $this->staticPage = $staticPage;
        $this->content = $content;
    }

    /**
     * @return StaticPage
     */
    public function getStaticPage(): StaticPage
    {
        return $this->staticPage;
    }

    /**
     * @return array
     */
    public function getContent(): array
    {
        return $this->content;
    }
}