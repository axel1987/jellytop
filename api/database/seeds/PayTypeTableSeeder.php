<?php

declare(strict_types=1);

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

/**
 * Class GendersTableSeeder
 */
class PayTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now();

        DB::table('pay_types')->insert([
            [
                'title' => json_encode([
                    'ua' => 'Готівкою в магазині',
                    'ru' => 'Наличными в магазине'
                ]),
                'created_at' => $now,
                'updated_at' => $now,
            ],
            [
                'title' => json_encode([
                    'ua' => 'Оплата картою',
                    'ru' => 'Оплата картой'
                ]),
                'created_at' => $now,
                'updated_at' => $now,
            ],
            [
                'title' => json_encode([
                    'ua' => 'Накладений платіж',
                    'ru' => 'Наложенный платеж'
                ]),
                'created_at' => $now,
                'updated_at' => $now,
            ],
        ]);
    }
}