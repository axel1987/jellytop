<?php

declare(strict_types=1);

namespace App\Http\Resourses\OrderStatuses;

use App\Domain\OrderStatus\OrderStatus;
use App\Http\Resourses\BaseCollectionResource;

/**
 * Class OrderStatusesListResource
 * @package App\Http\Resourses\OrderStatuses
 */
class OrderStatusesListResource extends BaseCollectionResource
{
    /**
     * @param OrderStatus $item
     *
     * @return array
     */
    protected function getItemData($item): array
    {
        return [
            'id' => $item->id,
            'type' => 'order-status',
            'attributes' => [
                'title' => $item->title,
                'createdAt' => $item->created_at ? $item->created_at->format('Y-m-d H:i:s') : '',
                'updatedAt' => $item->updated_at ? $item->updated_at->format('Y-m-d H:i:s') : '',
            ]
        ];
    }
}