import StaticPage from "../dto/entities/StaticPage";

/**
 * @returns {{
 * staticPage: null,
 * }}
 */
export const state = () => ({
    staticPage: null,
});

/**
 *
 * @type {{
 * getStaticPage(*): null,
 * }}
 */
export const getters = {
    getStaticPage(state) {
        return state.staticPage;
    },
};

/**
 * @type {{
 * SET_STATIC_PAGE(*, *): void,
 * }}
 */
export const mutations = {
    SET_STATIC_PAGE(state, value) {
        state.staticPage = value
    },
};

/**
 * @type {{
 * fetchStaticPage: ((function({commit: *}, *=): Promise<void>)|*),
 * }}
 */
export const actions = {
    fetchStaticPage: async function ({commit}, name = '') {
        let {data} = await this.$axios.get(`/static-pages/${name}`);
        commit('SET_STATIC_PAGE', new StaticPage(data))
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations,
};
