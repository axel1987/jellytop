<?php

declare(strict_types=1);

namespace App\Application\Product\UpdateProductStock;

use App\Contract\Core\CommandInterface;
use App\Domain\Product\Product;

/**
 * Class UpdateProductStock
 * @package App\Application\Product\UpdateProductStock
 */
class UpdateProductStock implements CommandInterface
{
    /** @var Product $product */
    private Product $product;

    /** @var bool $inStock*/
    private bool $inStock;

    /**
     * UpdateProductStock constructor
     *
     * @param Product $product
     * @param bool $inStock
     */
    public function __construct(Product $product, bool $inStock)
    {
        $this->product = $product;
        $this->inStock = $inStock;
    }

    /**
     * @return Product
     */
    public function getProduct(): Product
    {
        return $this->product;
    }

    /**
     * @return bool
     */
    public function isInStock(): bool
    {
        return $this->inStock;
    }
}