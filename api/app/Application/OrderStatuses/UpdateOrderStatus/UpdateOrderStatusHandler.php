<?php

declare(strict_types=1);

namespace App\Application\OrderStatuses\UpdateOrderStatus;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\OrderStatus\OrderStatus;
use App\Infrastructure\DatabaseRepository\OrderStatusRepository;
use Illuminate\Database\Eloquent\Model;

/**
 * Class UpdateOrderStatusHandler
 * @package App\Application\OrderStatuses\UpdateOrderStatus
 */
class UpdateOrderStatusHandler implements HandlerInterface
{
    /** @var OrderStatusRepository $orderStatusRepository */
    private OrderStatusRepository $orderStatusRepository;

    /**
     * UpdateOrderStatusHandler constructor
     *
     * @param OrderStatusRepository $orderStatusRepository
     */
    public function __construct(OrderStatusRepository $orderStatusRepository)
    {
        $this->orderStatusRepository = $orderStatusRepository;
    }

    /**
     * @param CommandInterface|UpdateOrderStatus $command
     *
     * @return OrderStatus
     */
    public function handle(CommandInterface $command): Model
    {
        $orderStatus = $command->getOrderStatus();
        $orderStatus->title = $command->getTitle();

        $this->orderStatusRepository->store($orderStatus);

        return $orderStatus;
    }
}