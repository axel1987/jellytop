<?php

declare(strict_types=1);

namespace App\Application\Season\GetSeasonByFilter;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\Season\Season;
use App\Domain\Season\SeasonRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * Class GetSeasonByFilterHandler
 * @package App\Application\Season\GetSeasonByFilter
 */
class GetSeasonByFilterHandler implements HandlerInterface
{
    /** @var SeasonRepositoryInterface $seasonRepository */
    private SeasonRepositoryInterface $seasonRepository;

    /**
     * GetSeasonByFilterHandler constructor
     *
     * @param SeasonRepositoryInterface $seasonRepository
     */
    public function __construct(SeasonRepositoryInterface $seasonRepository)
    {
        $this->seasonRepository = $seasonRepository;
    }

    /**
     * @param CommandInterface|GetSeasonByFilter $command
     *
     * @return Season|null
     */
    public function handle(CommandInterface $command): ?Model
    {
        return $this->seasonRepository->one(
            $command->getFilter()
        );
    }
}