<?php

declare(strict_types=1);

namespace App\Http\Resourses\Order;

use App\Domain\Order\Order;
use App\Http\Resourses\Delivery\DeliveryResource;
use App\Http\Resourses\OrderItem\OrderItemListResource;
use App\Http\Resourses\OrderStatuses\OrderStatusResource;
use App\Http\Resourses\PayType\PayTypeResource;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class OrderResource
 * @package App\Http\Resourses\Order
 */
class OrderResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        /** @var Order $this */
        return [
            'id' => $this->id,
            'type' => 'order',
            'attributes' => [
                'name' => $this->name,
                'address' => $this->address,
                'sum' => $this->sum,
                'phone' => $this->phone,
                'createdAt' => $this->created_at ? $this->created_at->format('Y-m-d H:i:s') : '',
                'updatedAt' => $this->updated_at ? $this->updated_at->format('Y-m-d H:i:s') : '',
            ],
            'relationships' => [
                'status' => new OrderStatusResource($this->status),
                'delivery' => new DeliveryResource($this->delivery),
                'payType' => new PayTypeResource($this->payType),
                'items' => new OrderItemListResource($this->items)
            ]
        ];
    }
}