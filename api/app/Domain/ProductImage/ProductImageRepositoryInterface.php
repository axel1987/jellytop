<?php

declare(strict_types=1);

namespace App\Domain\ProductImage;

use App\Contract\Core\DatabaseRepositoryInterface;

/**
 * Interface ProductImageRepositoryInterface
 * @package App\Domain\ProductImage
 */
interface ProductImageRepositoryInterface extends DatabaseRepositoryInterface
{

}