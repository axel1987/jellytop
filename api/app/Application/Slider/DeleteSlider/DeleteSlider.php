<?php

declare(strict_types=1);

namespace App\Application\Slider\DeleteSlider;

use App\Contract\Core\CommandInterface;
use App\Domain\Slider\Slider;

/**
 * Class DeleteSlider
 * @package App\Application\Slider\DeleteSlider
 */
class DeleteSlider implements CommandInterface
{
    /** @var Slider $slider */
    private Slider $slider;

    /**
     * DeleteSlider constructor
     *
     * @param Slider $slider
     */
    public function __construct(Slider $slider)
    {
        $this->slider = $slider;
    }

    /**
     * @return Slider
     */
    public function getSlider(): Slider
    {
        return $this->slider;
    }
}