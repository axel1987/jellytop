<?php

declare(strict_types=1);

namespace App\Infrastructure\DatabaseRepository;

use App\Contract\Core\DatabaseRepositoryInterface;
use App\Contract\Core\FilterInterface;
use App\Contract\Core\SortingInterface;
use App\Domain\Color\Color;
use App\Domain\Color\ColorFilter;
use App\Domain\Color\ColorRepositoryInterface;

/**
 * Class ColorRepository
 * @package App\Infrastructure\DatabaseRepository
 */
class ColorRepository extends DatabaseRepository implements ColorRepositoryInterface
{
    /**
     * ColorRepository constructor
     */
    public function __construct()
    {
        $this->model = new Color();
    }

    /**
     * @param FilterInterface|ColorFilter $filter
     *
     * @return $this
     */
    protected function filter(FilterInterface $filter): DatabaseRepositoryInterface
    {
        $this->builder = $this->builder ?? $this->model->newQuery();

        if ($filter->getId()) {
            $this->builder->where('id', '=', $filter->getId());
        }

        if ($filter->getIds()) {
            $this->builder->whereIn('id', $filter->getIds());
        }

        return $this;
    }

    /**
     * @param SortingInterface $sorting
     *
     * @return $this
     */
    protected function sorting(SortingInterface $sorting): DatabaseRepositoryInterface
    {
        return $this;
    }
}