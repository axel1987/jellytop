/**
 * @returns {{currentUser: {}}}
 */
import User from '../dto/entities/User';

export const state = () => ({
    currentUser: null,
    user: null,
    usersList: [],
});

/**
 * @type {{getCurrentUser(*): []}}
 */
export const getters = {
    getCurrentUser(state) {
        return state.currentUser;
    },
    getUser(state) {
        return state.user;
    },
    getUsersList(state) {
        return state.usersList;
    }
};

/**
 * @type {{SET_CURRENT_USER(*, *): void}}
 */
export const mutations = {
    SET_CURRENT_USER(state, value) {
        state.currentUser = value
    },
    SET_USER(state, value) {
        state.user = value
    },
    SET_USERS_LIST(state, value) {
        state.usersList = value
    },
};

/**
 * @type {{
 * fetchUsersList: ((function({commit: *}, *=): Promise<void>)|*),
 * registerUser: ((function({commit: *}, *=): Promise<void>)|*),
 * updateUser: ((function({commit: *}, *=): Promise<void>)|*),
 * fetchUser: ((function({commit: *}, *=): Promise<void>)|*),
 * fetchCurrentUser: ((function({commit: *}, *=): Promise<void>)|*)
 * }}
 */
export const actions = {
    fetchUsersList: async function ({commit}, params = {}) {
        let {data} = await this.$axios.get('/users', {params: params});

        const list = data.data.map((userData) => {
            return new User(userData)
        });

        commit('SET_USERS_LIST', list)
    },
    fetchUser: async function ({commit}, params = {}) {
        if (params.id) {
            let {data} = await this.$axios.get(`/users/${params.id}`, params);
            commit('SET_USER', new User(data))
        } else {
            commit('SET_USER', new User({}))
        }
    },
    fetchCurrentUser: async function ({commit}) {
        const res = await this.$axios.get(`/users/current`);

        if (res && res.data) {
            commit('SET_CURRENT_USER', new User(res.data))
        }
    },
    registerUser: async function ({commit}, params = {}) {
        await this.$axios.post(`/users`, params).then((res) => {
            if (res) {
                commit('SET_USER', new User(res.data))
            }
        });
    },
    updateUser: async function ({commit}, params = {}) {
        await this.$axios.put(`/users/${params.id}`, params).then((res) => {
            if (res) {
                commit('SET_USER', new User(res.data))
            }
        });
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations,
};
