<?php

declare(strict_types=1);

namespace App\Http\Middleware;

use Closure;
use FastRoute\BadRouteException;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

/**
 * Class CheckServiceMiddleware
 * @package App\Http\Middleware
 */
class CheckServiceMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param  Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        /** @var Collection $accessedServices */
        $accessedServices = $request->user()->accessedServices;
        $fullRoute = last((array)$request->route());
        $route = head(explode('/', $fullRoute['route']));

        $service = $accessedServices->filter(function ($service) use ($route) {
            return $service->route == $route;
        });

        if ($service->count()) {
            $request->service = $service->first();

            return $next($request);
        }

        throw new BadRouteException();
    }
}