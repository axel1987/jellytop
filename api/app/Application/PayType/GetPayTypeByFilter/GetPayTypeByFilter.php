<?php

declare(strict_types=1);

namespace App\Application\PayType\GetPayTypeByFilter;

use App\Contract\Core\CommandInterface;
use App\Domain\PayType\PayTypeFilter;

/**
 * Class GetPayTypeByFilter
 * @package App\Application\PayType\GetPayTypeByFilter
 */
class GetPayTypeByFilter implements CommandInterface
{
    /** @var PayTypeFilter $filter */
    private PayTypeFilter $filter;

    /**
     * GetPayTypeByFilter constructor
     *
     * @param PayTypeFilter $filter
     */
    public function __construct(PayTypeFilter $filter)
    {
        $this->filter = $filter;
    }

    /**
     * @return PayTypeFilter
     */
    public function getFilter(): PayTypeFilter
    {
        return $this->filter;
    }
}