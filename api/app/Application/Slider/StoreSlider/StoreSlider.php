<?php

declare(strict_types=1);

namespace App\Application\Slider\StoreSlider;

use App\Contract\Core\CommandInterface;

/**
 * Class StoreSlider
 * @package App\Application\Slider\StoreSlider
 */
class StoreSlider implements CommandInterface
{
    /** @var string $link */
    private string $link;

    /** @var array $text */
    private array $text;

    /**
     * StoreSlider constructor
     *
     * @param string $link
     * @param array $text
     */
    public function __construct(
        string $link,
        array $text
    ) {
        $this->link = $link;
        $this->text = $text;
    }

    /**
     * @return string
     */
    public function getLink(): string
    {
        return $this->link;
    }

    /**
     * @return array
     */
    public function getText(): array
    {
        return $this->text;
    }
}