import Pagination from "../dto/entities/Pagination";
import Order from "../dto/entities/Order";
import Product from "@/dto/entities/Product";
import Vue from "vue";

/**
 * @returns {{
 * pagination: null,
 * ordersList: *[],
 * order: null
 * }}
 */
export const state = () => ({
    order: null,
    ordersList: [],
    pagination: null
});

/**
 * @type {{
 * getOrdersList(*): [],
 * getOrder(*): null,
 * getOrdersPagination(*): null
 * }}
 */
export const getters = {
    getOrder(state) {
        return state.order;
    },
    getOrdersList(state) {
        return state.ordersList;
    },
    getOrdersPagination(state) {
        return state.pagination;
    }
};

/**
 * @type {{
 * SET_ORDERS_LIST(*, *): void,
 * SET_ORDER(*, *): void
 * }}
 */
export const mutations = {
    SET_ORDER(state, value) {
        state.order = value
    },
    SET_ORDERS_LIST(state, value) {
        state.ordersList = value
    },
    SET_ORDERS_PAGINATION(state, value) {
        state.pagination = value
    },
};

/**
 * @type {{
 * updateOrder: ((function({commit: *}, *=): Promise<void>)|*),
 * fetchOrder: ((function({commit: *}, *=): Promise<void>)|*),
 * fetchOrdersList: ((function({commit: *}, *=): Promise<void>)|*),
 * }}
 */
export const actions = {
    fetchOrdersList: async function ({commit}, params = {}) {
        let {data} = await this.$axios.get('/orders', {params: params});
        const list = data.data.map((orderData) => {
            return new Order(orderData)
        });
        const pagination = data.pagination ? new Pagination(data.pagination) : null;

        commit('SET_ORDERS_LIST', list)
        commit('SET_ORDERS_PAGINATION', pagination)
    },
    fetchOrder: async function ({commit}, params = {}) {
        if (params.id) {
            let {data} = await this.$axios.get(`/orders/${params.id}`, params);
            commit('SET_ORDER', new Order(data))
        } else {
            commit('SET_ORDER', new Order(null))
        }
    },
    updateOrder: async function ({commit}, params = {}) {
        await this.$axios.put(`/orders/${params.id}`, params).then((res) => {
            if (res) {
                commit('SET_ORDER', new Order(res.data))

                Vue.notify({
                    group: 'dashboard',
                    title: 'Успешно',
                    type: 'success',
                    text: `Заказ обновлен`,
                })
            } else {
                Vue.notify({
                    group: 'dashboard',
                    title: 'Ошибка',
                    type: 'error',
                    text: `Неверные данные`,
                });
            }
        });
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations,
};
