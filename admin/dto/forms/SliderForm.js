export default class SliderForm {

    /**
     * @return <void>
     */
    constructor(data) {
        this.id = data.id ? data.id : null;
        this.link = data.link ? data.link : null;
        this.text = {}
        this.text.ua = data.text && data.text.ua ? data.text.ua : null;
        this.text.ru = data.text && data.text.ru ? data.text.ru : null;
    }

    /**
     * @returns {{required: (function(*=))}}
     */
    rules() {
        return {
            required: value => !!value || 'Это поле обязательно',
        }
    }
}
