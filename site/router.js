import Vue from 'vue'
import Router from 'vue-router'

import Index from './pages/index'
import ProductCard from './pages/ProductCard'
import Cart from './pages/Cart'
import StaticPage from './pages/StaticPage'

Vue.use(Router)

/**
 * @returns {Router}
 */
export function createRouter() {
  return new Router({
    mode: 'history',
    linkActiveClass: 'nuxt-link-active',
    linkExactActiveClass: 'nuxt-link-exact-active',
    routes: [
      {
        name: 'home',
        path: '/',
        component: Index
      },
      {
        name: 'cart',
        path: '/cart',
        component: Cart
      },
      {
        name: 'delivery',
        path: '/delivery',
        component: StaticPage
      },
      {
        name: 'payments',
        path: '/payments',
        component: StaticPage
      },
      {
        name: 'contacts',
        path: '/contacts',
        component: StaticPage
      },
      {
        name: 'actions',
        path: '/actions',
        component: StaticPage
      },
      {
        name: 'product-card',
        path: '/:slug',
        component: ProductCard
      },
    ]
  })
}
