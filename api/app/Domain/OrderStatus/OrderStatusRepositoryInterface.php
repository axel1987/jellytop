<?php

declare(strict_types=1);

namespace App\Domain\OrderStatus;

use App\Contract\Core\DatabaseRepositoryInterface;

/**
 * Interface OrderStatusRepositoryInterface
 * @package App\Domain\OrderStatus
 */
interface OrderStatusRepositoryInterface extends DatabaseRepositoryInterface
{

}