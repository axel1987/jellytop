<?php

declare(strict_types=1);

namespace App\Application\StaticPage\GetStaticPagesList;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\StaticPage\StaticPageRepositoryInterface;

/**
 * Class GetStaticPagesListHandler
 * @package App\Application\StaticPage\GetStaticPagesList
 */
class GetStaticPagesListHandler implements HandlerInterface
{
    /** @var StaticPageRepositoryInterface $staticPageRepository */
    private StaticPageRepositoryInterface $staticPageRepository;

    /**
     * GetStaticPagesListHandler constructor
     *
     * @param StaticPageRepositoryInterface $staticPageRepository
     */
    public function __construct(StaticPageRepositoryInterface $staticPageRepository)
    {
        $this->staticPageRepository = $staticPageRepository;
    }

    /**
     * @param CommandInterface|GetStaticPagesList $command
     *
     * @return mixed
     */
    public function handle(CommandInterface $command)
    {
        return $this->staticPageRepository->all(
            $command->getFilter(),
            $command->getPagination(),
            $command->getSorting()
        );
    }
}