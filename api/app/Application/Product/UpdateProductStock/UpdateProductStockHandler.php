<?php

declare(strict_types=1);

namespace App\Application\Product\UpdateProductStock;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\Product\ProductRepositoryInterface;

/**
 * Class UpdateProductStockHandler
 * @package App\Application\Product\UpdateProductStock
 */
class UpdateProductStockHandler implements HandlerInterface
{
    /** @var ProductRepositoryInterface $productRepository */
    private ProductRepositoryInterface $productRepository;

    /**
     * UpdateProductStockHandler constructor
     *
     * @param ProductRepositoryInterface $productRepository
     */
    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    /**
     * @param CommandInterface|UpdateProductStock $command
     *
     * @return mixed|void
     */
    public function handle(CommandInterface $command)
    {
        $product = $command->getProduct();

        $product->in_stock = $command->isInStock();

        $this->productRepository->store($product);

        return $product;
    }
}