<?php

declare(strict_types=1);

namespace App\Application\Country\UpdateCountry;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\Country\Country;
use App\Domain\Country\CountryRepositoryInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

/**
 * Class UpdateCountryHandler
 * @package App\Application\Country\UpdateCountry
 */
class UpdateCountryHandler implements HandlerInterface
{
    /** @var CountryRepositoryInterface $countryRepository */
    private CountryRepositoryInterface $countryRepository;

    /**
     * UpdateCountryHandler constructor
     *
     * @param CountryRepositoryInterface $countryRepository
     */
    public function __construct(CountryRepositoryInterface $countryRepository)
    {
        $this->countryRepository = $countryRepository;
    }

    /**
     * @param CommandInterface|UpdateCountry $command
     *
     * @return Country
     */
    public function handle(CommandInterface $command): Model
    {
        $oldIcon = null;
        $country = $command->getCountry();

        if ($country->icon != $command->getIcon()) {
            $oldIcon = $country->icon;
        }

        $country->title = $command->getTitle();
        $country->icon = $command->getIcon();
        $country->slug = $command->getSlug();
        $this->countryRepository->store($country);

        if ($oldIcon) {
            Storage::disk('public')
                ->delete(str_replace('/storage', '', $oldIcon));
        }

        return $country;
    }
}