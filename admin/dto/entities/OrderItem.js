import Product from "./Product";
import Size from "./Size";

export default class OrderItem {
    /**
     * @param data
     */
    constructor(data = {}) {
        this.id = data.id ? data.id : null;

        this.count = data.attributes && data.attributes.count ?
            data.attributes.count :
            null;
        this.price = data.attributes && data.attributes.price ?
            data.attributes.price :
            null;
        this.product = data.relationships && data.relationships.product ?
            new Product(data.relationships.product) :
            null;
        this.size = data.relationships && data.relationships.size ?
            new Size(data.relationships.size) :
            null;

        this.createdAt = data.attributes && data.attributes.createdAt ?
            data.attributes.createdAt :
            null;
        this.updatedAt = data.attributes && data.attributes.updatedAt ?
            data.attributes.updatedAt :
            null;
    }
}