<?php

declare(strict_types=1);

namespace App\Application\Gender\GetGendersList;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\PaginationInterface;
use App\Contract\Core\SortingInterface;
use App\Domain\Gender\GenderFilter;

/**
 * Class GetGendersList
 * @package App\Application\Gender\GetGendersList
 */
class GetGendersList implements CommandInterface
{
    /** @var GenderFilter $filter */
    private GenderFilter $filter;

    /** @var PaginationInterface|null $pagination */
    private ?PaginationInterface $pagination;

    /** @var SortingInterface|null $sorting */
    private ?SortingInterface $sorting;

    /**
     * GetGendersList constructor
     *
     * @param GenderFilter $filter
     * @param PaginationInterface|null $pagination
     * @param SortingInterface|null $sorting
     */
    public function __construct(
        GenderFilter $filter,
        ?PaginationInterface $pagination,
        ?SortingInterface $sorting
    ) {
        $this->filter = $filter;
        $this->pagination = $pagination;
        $this->sorting = $sorting;
    }

    /**
     * @return GenderFilter
     */
    public function getFilter(): GenderFilter
    {
        return $this->filter;
    }

    /**
     * @return PaginationInterface|null
     */
    public function getPagination(): ?PaginationInterface
    {
        return $this->pagination;
    }

    /**
     * @return SortingInterface|null
     */
    public function getSorting(): ?SortingInterface
    {
        return $this->sorting;
    }
}