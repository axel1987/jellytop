<?php

declare(strict_types=1);

namespace App\Application\Country\DeleteCountry;

use App\Contract\Core\CommandInterface;
use App\Domain\Country\Country;

/**
 * Class DeleteCountry
 * @package App\Application\Country\DeleteCountry
 */
class DeleteCountry implements CommandInterface
{
    /** @var Country $color */
    private Country $color;

    /**
     * DeleteCountry constructor
     *
     * @param Country $color
     */
    public function __construct(Country $color)
    {
        $this->color = $color;
    }

    /**
     * @return Country
     */
    public function getCountry(): Country
    {
        return $this->color;
    }
}