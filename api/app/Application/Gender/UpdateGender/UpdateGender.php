<?php

declare(strict_types=1);

namespace App\Application\Gender\UpdateGender;

use App\Contract\Core\CommandInterface;
use App\Domain\Gender\Gender;

/**
 * Class UpdateGender
 * @package App\Application\Gender\UpdateGender
 */
class UpdateGender implements CommandInterface
{
    /** @var Gender $gender */
    private Gender $gender;

    /** @var array $title */
    private array $title;

    /** @var string|null $icon */
    private ?string $icon;

    /**
     * UpdateGender constructor
     *
     * @param Gender $gender
     * @param array $title
     * @param string|null $icon
     */
    public function __construct(
        Gender $gender,
        array $title,
        ?string $icon = null
    ) {
        $this->gender = $gender;
        $this->title = $title;
        $this->icon = $icon;
    }

    /**
     * @return Gender
     */
    public function getGender(): Gender
    {
        return $this->gender;
    }

    /**
     * @return array
     */
    public function getTitle(): array
    {
        return $this->title;
    }

    /**
     * @return string|null
     */
    public function getIcon(): ?string
    {
        return $this->icon;
    }
}