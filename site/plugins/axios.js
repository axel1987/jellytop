export default function ({$axios, $cookies, store, redirect, route}) {
    $axios.onError(async err => {
        const code = parseInt(err.response && err.response.status)

        if (code === 422) {
            const errs = prepareErrors(err.response)
            store.dispatch('validation/setErrors', errs)
        }

        if (code === 404 || code >= 500) {
            store.dispatch('validation/setErrors', {
                message: err.response.data.detail,
                fields: null,
                statusCode: code
            })
        }

        if (code === 403) {
            store.dispatch('validation/setErrors', {
                message: "Forbidden! You can't do this action",
                fields: null,
                statusCode: 403
            })
        }

        if (code === 440 || code === 401) {
            const originRequest = err.config;

            if (sessionStorage.getItem('token')) {
                let response = await store.dispatch('authorization/restoreToken');

                if (response.status === 200) {
                    return $axios(originRequest);
                } else {
                    sessionStorage.removeItem('token');
                    store.dispatch('validation/clearErrors')

                    redirect('/');
                }
            } else {
                if (route.name !== '/') {
                    store.dispatch('validation/clearErrors')
                    redirect('/');
                } else {
                    const errs = prepareErrors(err.response)
                    store.dispatch('validation/setErrors', errs)
                }
            }
        }
    })

    $axios.onRequest((config) => {
        store.dispatch('validation/clearErrors')

        /** Set temporary user's session */
        store.dispatch('authorization/fetchSession')
        $axios.setHeader('access-session', $cookies.get('access-session'))
    })

    const prepareErrors = function (response) {
        const fields = {}
        let msg = ''
        let errData = response.data;

        if (errData.detail) {
            msg += errData.detail
        } else {
            for (let i in errData) {
                if (errData[i].source) {
                    fields[errData[i].source.parameter] = errData[i].detail
                } else {
                    msg += errData[i].detail
                }
            }
        }

        return {
            message: msg,
            fields,
            statusCode: response.status
        }
    }
}
