<?php

declare(strict_types=1);

namespace App\Domain\Season;

use Exception;

/**
 * Class SeasonException
 * @package App\Domain\Season
 */
class SeasonException extends Exception
{
    /**
     * @param int $id
     *
     * @throws SeasonException
     */
    public static function notFound(int $id)
    {
        throw new SeasonException(sprintf('Color with id %d not found', $id));
    }
}