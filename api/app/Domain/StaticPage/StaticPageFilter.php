<?php

declare(strict_types=1);

namespace App\Domain\StaticPage;

use App\Contract\Core\FilterInterface;
use Illuminate\Http\Request;

/**
 * Class StaticPageFilter
 * @package App\Domain\StaticPage
 */
class StaticPageFilter implements FilterInterface
{
    /** @var int|null $id */
    private ?int $id = null;

    /** @var string|null $name */
    private ?string $name = null;

    /**
     * @param Request $request
     *
     * @return FilterInterface|StaticPageFilter
     */
    public static function fromRequest(Request $request): FilterInterface
    {
        $filter = new self();

        return $filter;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName(?string $name): void
    {
        $this->name = $name;
    }
}