<?php

declare(strict_types=1);

namespace App\Application\Order\GetOrderByFilter;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\PaginationInterface;
use App\Contract\Core\SortingInterface;
use App\Domain\Order\OrderFilter;

/**
 * Class GetOrderByFilter
 * @package App\Application\Order\GetOrderByFilter
 */
class GetOrderByFilter implements CommandInterface
{
    /** @var OrderFilter $filter */
    private OrderFilter $filter;

    /**
     * GetOrderByFilter constructor
     *
     * @param OrderFilter $filter
     */
    public function __construct(OrderFilter $filter)
    {
        $this->filter = $filter;
    }

    /**
     * @return OrderFilter
     */
    public function getFilter(): OrderFilter
    {
        return $this->filter;
    }
}