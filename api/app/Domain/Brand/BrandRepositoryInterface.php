<?php

declare(strict_types=1);

namespace App\Domain\Brand;

use App\Contract\Core\DatabaseRepositoryInterface;

/**
 * Interface BrandRepositoryInterface
 * @package App\Domain\Brand
 */
interface BrandRepositoryInterface extends DatabaseRepositoryInterface
{

}