import Season from "../dto/entities/Season";
import Pagination from "../dto/entities/Pagination";

/**
 * @returns {{
 * season: null,
 * seasonsList: *[],
 * seasonsSelectList: *[],
 * pagination: null,
 * }}
 */
export const state = () => ({
    season: null,
    seasonsList: [],
    seasonsSelectList: [],
    pagination: null
});

/**
 *
 * @type {{
 * getSeason(*): null,
 * getSeasonsList(*): [],
 * getSeasonsSelectList(*): [],
 * getSeasonPagination(*): null
 * }}
 */
export const getters = {
    getSeason(state) {
        return state.season;
    },
    getSeasonsList(state) {
        return state.seasonsList;
    },
    getSeasonsSelectList(state) {
        return state.seasonsSelectList;
    },
    getSeasonPagination(state) {
        return state.pagination;
    }
};

/**
 * @type {{
 * SET_SEASONS_LIST(*, *): void,
 * SET_SEASONS_SELECT_LIST(*, *): void,
 * SET_SEASON(*, *): void,
 * SET_SEASONS_PAGINATION(*, *): void,
 * }}
 */
export const mutations = {
    SET_SEASON(state, value) {
        state.season = value
    },
    SET_SEASONS_LIST(state, value) {
        state.seasonsList = value
    },
    SET_SEASONS_SELECT_LIST(state, value) {
        state.seasonsSelectList = value
    },
    SET_SEASONS_PAGINATION(state, value) {
        state.pagination = value
    },
};

/**
 * @type {{
 * registerSeason: ((function({commit: *}, *=): Promise<void>)|*),
 * updateSeason: ((function({commit: *}, *=): Promise<void>)|*),
 * fetchSeason: ((function({commit: *}, *=): Promise<void>)|*),
 * fetchSeasonsList: ((function({commit: *}, *=): Promise<void>)|*),
 * fetchSeasonsSelectList: ((function({commit: *}, *=): Promise<void>)|*),
 * deleteSeason: ((function({commit: *}, *=): Promise<void>)|*)
 * }}
 */
export const actions = {
    fetchSeasonsList: async function ({commit}, params = {}) {
        let {data} = await this.$axios.get('/seasons', {params: params});

        const list = data.data.map((seasonData) => {
            return new Season(seasonData)
        });
        const pagination = data.pagination ? new Pagination(data.pagination) : null;

        commit('SET_SEASONS_LIST', list)
        commit('SET_SEASONS_PAGINATION', pagination)
    },
    fetchSeasonsSelectList: async function ({commit}, params = {}) {
        let {data} = await this.$axios.get('/seasons/list', params);

        const list = data.data.map((seasonData) => {
            return new Season(seasonData)
        });

        commit('SET_SEASONS_SELECT_LIST', list)
    },
    fetchSeason: async function ({commit}, params = {}) {
        if (params.id) {
            let {data} = await this.$axios.get(`/seasons/${params.id}`, params);
            commit('SET_SEASON', new Season(data))
        } else {
            commit('SET_SEASON', new Season(null))
        }
    },
    registerSeason: async function ({commit}, params = {}) {
        await this.$axios.post(`/seasons`, params).then((res) => {
            if (res) {
                commit('SET_SEASON', new Season(res.data))
            }
        });
    },
    updateSeason: async function ({commit}, params = {}) {
        await this.$axios.put(`/seasons/${params.id}`, params).then((res) => {
            if (res) {
                commit('SET_SEASON', new Season(res.data))
            }
        });
    },
    deleteSeason: async function ({commit}, params = {}) {
        await this.$axios.delete(`/seasons/${params.id}`, params);
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations,
};
