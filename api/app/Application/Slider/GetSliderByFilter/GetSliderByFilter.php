<?php

declare(strict_types=1);

namespace App\Application\Slider\GetSliderByFilter;

use App\Contract\Core\CommandInterface;
use App\Domain\Slider\SliderFilter;

/**
 * Class GetSliderByFilter
 * @package App\Application\Slider\GetSliderByFilter
 */
class GetSliderByFilter implements CommandInterface
{
    /** @var SliderFilter $filter */
    private SliderFilter $filter;

    /**
     * GetSliderByFilter constructor
     *
     * @param SliderFilter $filter
     */
    public function __construct(SliderFilter $filter)
    {
        $this->filter = $filter;
    }

    /**
     * @return SliderFilter
     */
    public function getFilter(): SliderFilter
    {
        return $this->filter;
    }
}