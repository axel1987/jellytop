<?php

declare(strict_types=1);

namespace App\Application\Country\GetCountryByFilter;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\Country\Country;
use App\Domain\Country\CountryRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * Class GetCountryByFilterHandler
 * @package App\Application\Country\GetCountryByFilter
 */
class GetCountryByFilterHandler implements HandlerInterface
{
    /** @var CountryRepositoryInterface $countryRepository */
    private CountryRepositoryInterface $countryRepository;

    /**
     * GetCountryByFilterHandler constructor
     *
     * @param CountryRepositoryInterface $countryRepository
     */
    public function __construct(CountryRepositoryInterface $countryRepository)
    {
        $this->countryRepository = $countryRepository;
    }

    /**
     * @param CommandInterface|GetCountryByFilter $command
     *
     * @return Country|null
     */
    public function handle(CommandInterface $command): ?Model
    {
        return $this->countryRepository->one(
            $command->getFilter()
        );
    }
}