<?php

declare(strict_types=1);

namespace App\Domain\Cart;

use App\Contract\Core\DatabaseRepositoryInterface;

/**
 * Interface CartRepositoryInterface
 * @package App\Domain\Cart
 */
interface CartRepositoryInterface extends DatabaseRepositoryInterface
{

}