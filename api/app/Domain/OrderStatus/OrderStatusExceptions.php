<?php

declare(strict_types=1);

namespace App\Domain\OrderStatus;

use Exception;

/**
 * Class OrderStatusExceptions
 * @package App\Domain\OrderStatus
 */
class OrderStatusExceptions extends Exception
{
    /**
     * @param int $id
     *
     * @throws OrderStatusExceptions
     */
    public static function notFound(int $id)
    {
        throw new OrderStatusExceptions(sprintf('Order status item with id %d not found', $id));
    }
}