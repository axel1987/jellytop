<?php

declare(strict_types=1);

namespace App\Domain\Product;

use Exception;

/**
 * Class ProductException
 * @package App\Domain\Product
 */
class ProductException extends Exception
{
    /**
     * @param int $id
     *
     * @throws ProductException
     */
    public static function notFound(int $id)
    {
        throw new ProductException(sprintf('Product with id %d not found', $id));
    }
}