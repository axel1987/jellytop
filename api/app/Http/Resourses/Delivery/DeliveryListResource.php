<?php

declare(strict_types=1);

namespace App\Http\Resourses\Delivery;

use App\Domain\Delivery\Delivery;
use App\Http\Resourses\BaseCollectionResource;

/**
 * Class DeliveryListResource
 * @package App\Http\Resourses\Delivery
 */
class DeliveryListResource extends BaseCollectionResource
{
    /**
     * @param Delivery $item
     *
     * @return array
     */
    protected function getItemData($item): array
    {
        return [
            'id' => $item->id,
            'type' => 'type',
            'attributes' => [
                'title' => $item->title,
                'slug' => $item->slug,
                'description' => $item->description,
                'icon' => $item->icon,
                'minPrice' => $item->min_price,
                'createdAt' => $item->created_at ? $item->created_at->format('Y-m-d H:i:s') : '',
                'updatedAt' => $item->updated_at ? $item->updated_at->format('Y-m-d H:i:s') : '',
            ]
        ];
    }
}