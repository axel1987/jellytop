<?php

declare(strict_types=1);

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

/**
 * Class BrandsTableSeeder
 */
class BrandsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now();

        DB::table('brands')->insert([
            [
                'title' => json_encode([
                    'ua' => 'Виробник 1',
                    'ru' => 'Производитель 1'
                ]),
                'description' => json_encode([
                    'ua' => 'Тут якийсь текст по укр',
                    'ru' => 'Тут какой-то текст по рус'
                ]),
                'slug' => 'manufacturer-1',
                'country_id' => 1,
                'created_at' => $now,
                'updated_at' => $now,
            ],
            [
                'title' => json_encode([
                    'ua' => 'Виробник 2',
                    'ru' => 'Производитель 2'
                ]),
                'description' => json_encode([
                    'ua' => 'Тут якийсь текст по укр',
                    'ru' => 'Тут какой-то текст по рус'
                ]),
                'slug' => 'manufacturer-2',
                'country_id' => 2,
                'created_at' => $now,
                'updated_at' => $now,
            ]
        ]);
    }
}