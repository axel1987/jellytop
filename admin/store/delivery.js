import Delivery from "../dto/entities/Delivery";
import Pagination from "../dto/entities/Pagination";

/**
 * @returns {{
 * Delivery: null,
 * DeliveriesList: *[],
 * DeliveriesSelectList: *[],
 * pagination: null,
 * }}
 */
export const state = () => ({
    delivery: null,
    deliveriesList: [],
    deliveriesSelectList: [],
    pagination: null
});

/**
 * @Delivery {{
 * getDelivery(*): null,
 * getDeliveriesList(*): [],
 * getDeliveriesSelectList(*): [],
 * getDeliveryPagination(*): null
 * }}
 */
export const getters = {
    getDelivery(state) {
        return state.delivery;
    },
    getDeliveriesList(state) {
        return state.deliveriesList;
    },
    getDeliveriesSelectList(state) {
        return state.deliveriesSelectList;
    },
    getDeliveryPagination(state) {
        return state.pagination;
    }
};

/**
 * @Delivery {{
 * SET_DELIVERIES_LIST(*, *): void,
 * SET_DELIVERIES_SELECT_LIST(*, *): void,
 * SET_DELIVERY(*, *): void,
 * SET_DELIVERIES_PAGINATION(*, *): void,
 * }}
 */
export const mutations = {
    SET_DELIVERY(state, value) {
        state.delivery = value
    },
    SET_DELIVERIES_LIST(state, value) {
        state.deliveriesList = value
    },
    SET_DELIVERIES_SELECT_LIST(state, value) {
        state.deliveriesSelectList = value
    },
    SET_DELIVERIES_PAGINATION(state, value) {
        state.pagination = value
    },
};

/**
 * @Delivery {{
 * registerDelivery: ((function({commit: *}, *=): Promise<void>)|*),
 * updateDelivery: ((function({commit: *}, *=): Promise<void>)|*),
 * fetchDelivery: ((function({commit: *}, *=): Promise<void>)|*),
 * fetchDeliveriesList: ((function({commit: *}, *=): Promise<void>)|*),
 * fetchDeliveriesSelectList: ((function({commit: *}, *=): Promise<void>)|*),
 * deleteDelivery: ((function({commit: *}, *=): Promise<void>)|*)
 * }}
 */
export const actions = {
    fetchDeliveriesList: async function ({commit}, params = {}) {
        let {data} = await this.$axios.get('/delivery', {params: params});

        const list = data.data.map((DeliveryData) => {
            return new Delivery(DeliveryData)
        });
        const pagination = data.pagination ? new Pagination(data.pagination) : null;

        commit('SET_DELIVERIES_LIST', list)
        commit('SET_DELIVERIES_PAGINATION', pagination)
    },
    fetchDeliveriesSelectList: async function ({commit}, params = {}) {
        let {data} = await this.$axios.get('/delivery/list', params);

        const list = data.data.map((DeliveryData) => {
            return new Delivery(DeliveryData)
        });

        commit('SET_DELIVERIES_SELECT_LIST', list)
    },
    fetchDelivery: async function ({commit}, params = {}) {
        if (params.id) {
            let {data} = await this.$axios.get(`/delivery/${params.id}`, params);
            commit('SET_DELIVERY', new Delivery(data))
        } else {
            commit('SET_DELIVERY', new Delivery(null))
        }
    },
    registerDelivery: async function ({commit}, params = {}) {
        await this.$axios.post(`/delivery`, params).then((res) => {
            if (res) {
                commit('SET_DELIVERY', new Delivery(res.data))
            }
        });
    },
    updateDelivery: async function ({commit}, params = {}) {
        await this.$axios.put(`/delivery/${params.id}`, params).then((res) => {
            if (res) {
                commit('SET_DELIVERY', new Delivery(res.data))
            }
        });
    },
    deleteDelivery: async function ({commit}, params = {}) {
        await this.$axios.delete(`/delivery/${params.id}`, params);
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations,
};
