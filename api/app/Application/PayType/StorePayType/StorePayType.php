<?php

declare(strict_types=1);

namespace App\Application\PayType\StorePayType;

use App\Contract\Core\CommandInterface;

/**
 * Class StorePayType
 * @package App\Application\PayType\StorePayType
 */
class StorePayType implements CommandInterface
{
    /** @var array $title */
    private array $title;

    /** @var array $description */
    private array $description;

    /** @var string|null $icon */
    private ?string $icon;

    /**
     * StorePayType constructor
     *
     * @param array $title
     * @param array $description
     * @param string|null $icon
     */
    public function __construct(
        array $title,
        array $description,
        ?string $icon
    ) {
        $this->title = $title;
        $this->description = $description;
        $this->icon = $icon;
    }

    /**
     * @return array
     */
    public function getTitle(): array
    {
        return $this->title;
    }

    /**
     * @return array
     */
    public function getDescription(): array
    {
        return $this->description;
    }

    /**
     * @return string|null
     */
    public function getIcon(): ?string
    {
        return $this->icon;
    }
}