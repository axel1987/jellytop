module.exports = {
    /*
    ** Headers of the page
    */
    ssr: true,
    server: {
        host: '0' // default: localhost
    },
    head: {
        script: [
            { hid: 'mdi', src: 'https://cdn.jsdelivr.net/npm/@mdi/font@latest/fonts/materialdesignicons-webfont.woff2?v=6.5.95', defer: true },
            { hid: 'analytics', src: 'https://www.google-analytics.com/analytics.js', defer: true }
        ],
        title: 'DjeliTop',
        meta: [
            {charset: 'utf-8'},
            {name: 'viewport', content: 'width=device-width, initial-scale=1'},
            {
                hid: 'description', name: 'description', content: 'Детская обувь, дитяче взуття\n' +
                    'Товар/услуга, Отправки по Украине, Мы находимся: Харьков метро  Рынок "Платформа"\n' +
                    'Магазин 3-13  Тел 0672977918'
            },
            {charset: 'utf-8'},
            {property: 'og:site_name', content: 'DjeliTop'},
            {property: 'og:title', content: 'Детская и подростковая обувь по доступным ценам'},
            {
                property: 'og:description', content: 'Детская обувь, дитяче взуття\n' +
                    'Товар/услуга, Отправки по Украине, Мы находимся: Харьков метро  Рынок "Платформа"\n' +
                    'Магазин 3-13  Тел 0672977918'
            },
            {
                property: 'og:image', content: (process.env.API_HTTPS.toLowerCase() === 'true' ? 'https://' : 'http://')
                    + process.env.API_HOST + '/images/logo.png'
            },
            {property: 'og:type', content: 'product'},
            {
                property: 'og:url', content: (process.env.API_HTTPS.toLowerCase() === 'true' ? 'https://' : 'http://')
                    + process.env.API_HOST
            },
            {name: 'google-site-verification', content: 'z2ZbeG0oee5DYHsokUZPdhSVC7RZ3RysY6r2M6Oe0Ww'}
        ],
        link: [
            {rel: 'icon', type: 'image/x-icon', href: '/favicon.ico'},
        ],
    },
    publicRuntimeConfig: {
        // apiUrl: 'https://ad69-193-30-243-34.ngrok.io:8881',
        apiUrl: (process.env.API_HTTPS.toLowerCase() === 'true' ? 'https://' : 'http://')
            + process.env.API_HOST
            + ':'
            + process.env.API_PORT,
        appUrl: (process.env.API_HTTPS.toLowerCase() === 'true' ? 'https://' : 'http://')
            + process.env.API_HOST,
        appKey: process.env.APP_KEY
    },
    /*
    ** Customize the progress bar color
    */
    loading: {color: '#3B8070'},
    css: [
        '~assets/styles/global.scss',
    ],
    plugins: [
        '~/plugins/axios',
        '~/plugins/vuetify',
        '~/plugins/jsonld'
    ],
    modules: [
        '@nuxtjs/axios',
        '@nuxtjs/router',
        '@nuxtjs/proxy',
        'cookie-universal-nuxt',
    ],
    buildModules: ['@nuxtjs/dotenv', '@nuxtjs/vuetify', '@nuxtjs/google-analytics'],
    googleAnalytics: {
        asyncID: async (context) => {
            return 'UA-219617546-1'
        }
    },
    components: true,
    router: {
        middleware: ['auth'],
    },
    axios: {
        proxy: process.env.AXIOS_PROXY.toLowerCase() === 'true',
        proxyHeaders: true,
        https: process.env.API_HTTPS.toLowerCase() === 'true',
        progress: true,
        debug: process.env.AXIOS_DEBUG.toLowerCase() === 'true',
    },
    proxy: [
        // 'https://ad69-193-30-243-34.ngrok.io:8881'
        (process.env.API_HTTPS.toLowerCase() === 'true' ? 'https://' : 'http://') +
        process.env.API_HOST +
        ':' +
        process.env.API_PORT +
        process.env.API_PREFIX,
    ],
    /*
    ** Build configuration
    */
    build: {
        /*
        ** Run ESLint on save
        */
        extend(config, {isDev, isClient}) {
            if (isDev && isClient) {
                config.module.rules.push({
                    enforce: 'pre',
                    test: /\.(js|vue)$/,
                    loader: 'eslint-loader',
                    exclude: /(node_modules)/,
                });
            }
        },
    },
};

