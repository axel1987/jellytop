export default class LoginForm {

    /**
     * @return <void>
     */
    constructor() {
        this.email = null;
        this.password = null;
    }

    /**
     * @returns {{
     * emailRules: (function(*=)),
     * required: (function(*=))
     * }}
     */
    rules() {
        return {
            required: value => !!value || 'Это поле обязательно',
            emailRules: v => /.+@.+\..+/.test(v) || 'E-mail введен неверно',
        }
    }
}
