<?php

declare(strict_types=1);

namespace App\Domain\PayType;

use Exception;

/**
 * Class PayTypeExceptions
 * @package App\Domain\PayType
 */
class PayTypeExceptions extends Exception
{
    /**
     * @param int $id
     *
     * @throws PayTypeExceptions
     */
    public static function notFound(int $id)
    {
        throw new PayTypeExceptions(sprintf('Color with id %d not found', $id));
    }
}