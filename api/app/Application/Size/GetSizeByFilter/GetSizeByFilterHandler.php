<?php

declare(strict_types=1);

namespace App\Application\Size\GetSizeByFilter;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\Size\Size;
use App\Domain\Size\SizeRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * Class GetSizeByFilterHandler
 * @package App\Application\Size\GetSizeByFilter
 */
class GetSizeByFilterHandler implements HandlerInterface
{
    /** @var SizeRepositoryInterface $sizeRepository */
    private SizeRepositoryInterface $sizeRepository;

    /**
     * GetSizeByFilterHandler constructor
     *
     * @param SizeRepositoryInterface $sizeRepository
     */
    public function __construct(SizeRepositoryInterface $sizeRepository)
    {
        $this->sizeRepository = $sizeRepository;
    }

    /**
     * @param CommandInterface|GetSizeByFilter $command
     *
     * @return Size|null
     */
    public function handle(CommandInterface $command): ?Model
    {
        return $this->sizeRepository->one(
            $command->getFilter()
        );
    }
}