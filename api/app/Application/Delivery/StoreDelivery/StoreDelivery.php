<?php

declare(strict_types=1);

namespace App\Application\Delivery\StoreDelivery;

use App\Contract\Core\CommandInterface;

/**
 * Class StoreDelivery
 * @package App\Application\Delivery\StoreDelivery
 */
class StoreDelivery implements CommandInterface
{
    /** @var array $title */
    private array $title;

    /** @var array $description */
    private array $description;

    /** @var string|null $icon */
    private ?string $icon;

    /** @var float|null $icon */
    private ?float $minPrice;

    /**
     * StoreDelivery constructor
     *
     * @param array $title
     * @param array $description
     * @param string|null $icon
     * @param float|null $minPrice
     */
    public function __construct(
        array $title,
        array $description,
        ?string $icon = null,
        ?float $minPrice = null
    ) {
        $this->title = $title;
        $this->description = $description;
        $this->icon = $icon;
        $this->minPrice = $minPrice;
    }


    /**
     * @return array
     */
    public function getTitle(): array
    {
        return $this->title;
    }

    /**
     * @return array
     */
    public function getDescription(): array
    {
        return $this->description;
    }

    /**
     * @return string|null
     */
    public function getIcon(): ?string
    {
        return $this->icon;
    }

    /**
     * @return float|null
     */
    public function getMinPrice(): ?float
    {
        return $this->minPrice;
    }
}