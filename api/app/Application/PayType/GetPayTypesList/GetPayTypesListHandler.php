<?php

declare(strict_types=1);

namespace App\Application\PayType\GetPayTypesList;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\PayType\PayTypeRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Class GetPayTypesListHandler
 * @package App\Application\PayType\GetPayTypesList
 */
class GetPayTypesListHandler implements HandlerInterface
{
    /** @var PayTypeRepositoryInterface $payTypeRepository */
    private PayTypeRepositoryInterface $payTypeRepository;

    /**
     * GetPayTypesListHandler constructor
     *
     * @param PayTypeRepositoryInterface $payTypeRepository
     */
    public function __construct(PayTypeRepositoryInterface $payTypeRepository)
    {
        $this->payTypeRepository = $payTypeRepository;
    }

    /**
     * @param  GetPayTypesList $command
     *
     * @return LengthAwarePaginator|Collection
     */
    public function handle(CommandInterface $command)
    {
        return $this->payTypeRepository->all(
            $command->getFilter(),
            $command->getPagination(),
            $command->getSorting()
        );
    }
}