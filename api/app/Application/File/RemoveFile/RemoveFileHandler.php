<?php

declare(strict_types=1);

namespace App\Application\File\RemoveFile;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\Product\Product;
use App\Domain\Product\ProductFilter;
use App\Domain\Product\ProductRepositoryInterface;
use App\Domain\ProductImage\ProductImage;
use Illuminate\Support\Facades\Storage;

/**
 * Class RemoveFileHandler
 * @package App\Application\File\RemoveFile
 */
class RemoveFileHandler implements HandlerInterface
{
    /** @var ProductRepositoryInterface $productRepository */
    private ProductRepositoryInterface $productRepository;

    /**
     * RemoveFileHandler constructor
     *
     * @param ProductRepositoryInterface $productRepository
     */
    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    /**
     * @param RemoveFile $command
     *
     * @return void
     */
    public function handle(CommandInterface $command): void
    {
        if ($command->getGroup() == 'product' && $command->getId()) {
            $filter = new ProductFilter();
            $filter->setId($command->getId());
            /** @var Product $product */
            $product = $this->productRepository->one($filter);

            if ($product) {
                /** @var ProductImage $image */
                $image = $product->images()->where('url', '=', $command->getUrl())->first();

                Storage::disk('public')
                    ->delete(str_replace('/storage', '', $image->url));

                Storage::disk('public')
                    ->delete(str_replace('/storage', '', $image->webp_url));
                $image->delete();
            }
        } else {
            Storage::disk('public')
                ->delete(str_replace('/storage', '', $command->getUrl()));
        }
    }
}