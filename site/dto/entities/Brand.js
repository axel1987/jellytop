import Country from "./Country";

export default class Brand {
    /**
     * @param data
     */
    constructor(data = {}) {
        this.id = data.id ? data.id : null;
        this.title = data.attributes && data.attributes.title ?
            data.attributes.title :
            null;
        this.description = data.attributes && data.attributes.description ?
            data.attributes.description :
            null;
        this.slug = data.attributes && data.attributes.slug ?
            data.attributes.slug :
            null;
        this.logo = data.attributes && data.attributes.logo
            ? data.attributes.logo
            : null;
        this.country = data.relationships && data.relationships.country
            ? new Country(data.relationships.country)
            : null;
        this.createdAt = data.attributes && data.attributes.createdAt ?
            data.attributes.createdAt :
            null;
        this.updatedAt = data.attributes && data.attributes.updatedAt ?
            data.attributes.updatedAt :
            null;
    }
}