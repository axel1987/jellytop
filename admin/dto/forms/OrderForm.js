export default class OrderForm {

    /**
     * @return <void>
     */
    constructor(data) {
        this.id = data.id;
        this.statusId = data.status.id;
        this.note = data.note;
    }

    /**
     * @returns {{required: (function(*=))}}
     */
    rules() {
        return {
            required: value => !!value || 'Это поле обязательно',
        }
    }
}
