<?php

declare(strict_types=1);

namespace App\Http\Controllers\Site;

use App\Application\StaticPage\DeleteStatiPage\DeleteStaticPage;
use App\Application\StaticPage\GetStaticPageByFilter\GetStaticPageByFilter;
use App\Application\StaticPage\GetStaticPagesList\GetStaticPagesList;
use App\Application\StaticPage\StoreStaticPage\StoreStaticPage;
use App\Application\StaticPage\UpdateStaticPage\UpdateStaticPage;
use App\Domain\StaticPage\StaticPageFilter;
use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\StatisPage\StaticPageIndexRequest;
use App\Http\Requests\Dashboard\StatisPage\StaticPageRequest;
use App\Http\Resourses\StaticPage\StaticPageListResource;
use App\Http\Resourses\StaticPage\StaticPageResource;
use App\Infrastructure\Core\Pagination;
use App\Infrastructure\Core\Sorting;
use Illuminate\Http\JsonResponse;

/**
 * Class StaticPageController
 * @package App\Http\Controllers\Admin
 */
class StaticPageController extends Controller
{
    /**
     * @param string $name
     *
     * @return JsonResponse
     */
    public function view(string $name): JsonResponse
    {
        $filter = new StaticPageFilter();
        $filter->setName($name);
        $staticPage = $this->execute(new GetStaticPageByFilter($filter));

        return new JsonResponse(new StaticPageResource($staticPage));
    }
}