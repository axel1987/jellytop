<?php

declare(strict_types=1);

namespace App\Domain\Color;

use App\Contract\Core\DatabaseRepositoryInterface;

/**
 * Interface ColorRepositoryInterface
 * @package App\Domain\Color
 */
interface ColorRepositoryInterface extends DatabaseRepositoryInterface
{

}