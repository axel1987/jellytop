export default class FileUploadForm {

    /**
     * @return <void>
     */
    constructor(data) {
        this.file = null;
        this.group = data.group ? data.group : '';
        this.id = data.id ? data.id  : '';
    }

    /**
     * @returns {{required: (function(*=))}}
     */
    rules() {
        return {
            required: value => !!value || 'Это поле обязательно',
        }
    }
}
