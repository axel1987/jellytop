<?php

declare(strict_types=1);

namespace App\Application\Product\GetProductByFilter;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\Product\Product;
use App\Domain\Product\ProductRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * Class GetProductByFilterHandler
 * @package App\Application\Product\GetProductByFilter
 */
class GetProductByFilterHandler implements HandlerInterface
{
    /** @var ProductRepositoryInterface $productRepository */
    private ProductRepositoryInterface $productRepository;

    /**
     * GetProductByFilterHandler constructor
     *
     * @param ProductRepositoryInterface $productRepository
     */
    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    /**
     * @param GetProductByFilter $command
     *
     * @return Product
     */
    public function handle(CommandInterface $command): Model
    {
        return $this->productRepository->one(
            $command->getProductFilter()
        );
    }
}