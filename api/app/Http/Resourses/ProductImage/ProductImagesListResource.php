<?php

declare(strict_types=1);

namespace App\Http\Resourses\ProductImage;

use App\Domain\ProductImage\ProductImage;
use App\Http\Resourses\BaseCollectionResource;

/**
 * Class ProductImagesListResource
 * @package App\Http\Resourses\ProductImage
 */
class ProductImagesListResource extends BaseCollectionResource
{
    /**
     * @param ProductImage $item
     *
     * @return array
     */
    protected function getItemData($item): array
    {
        $webp = (bool)strpos(strtolower(request()->server('HTTP_ACCEPT')), 'webp');
        /** @var ProductImage $item */
        return [
            'type' => 'product-image',
            'attributes' => [
                'url' => $webp && !empty($item->webp_url) ? $item->webp_url : $item->url,
                'isMain' => $item->is_main,
            ]
        ];
    }
}