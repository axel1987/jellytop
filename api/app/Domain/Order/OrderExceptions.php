<?php

declare(strict_types=1);

namespace App\Domain\Order;

use Exception;

/**
 * Class OrderExceptions
 * @package App\Domain\Order
 */
class OrderExceptions extends Exception
{
    /**
     * @param int $id
     *
     * @throws OrderExceptions
     */
    public static function notFound(int $id)
    {
        throw new OrderExceptions(sprintf('Order with id %d not found', $id));
    }
}