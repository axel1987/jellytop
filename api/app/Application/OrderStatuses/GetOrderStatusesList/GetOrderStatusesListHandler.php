<?php

declare(strict_types=1);

namespace App\Application\OrderStatuses\GetOrderStatusesList;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Infrastructure\DatabaseRepository\OrderStatusRepository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Class GetOrderStatusesListHandler
 * @package App\Application\OrderStatuses\GetOrderStatusesList
 */
class GetOrderStatusesListHandler implements HandlerInterface
{
    /** @var OrderStatusRepository $orderStatusRepository */
    private OrderStatusRepository $orderStatusRepository;

    /**
     * UpdateOrderStatusHandler constructor
     *
     * @param OrderStatusRepository $orderStatusRepository
     */
    public function __construct(OrderStatusRepository $orderStatusRepository)
    {
        $this->orderStatusRepository = $orderStatusRepository;
    }

    /**
     * @param CommandInterface|GetOrderStatusesList $command
     *
     * @return Collection|LengthAwarePaginator
     */
    public function handle(CommandInterface $command)
    {
        return $this->orderStatusRepository->all(
            $command->getFilter(),
            $command->getPagination(),
            $command->getSorting()
        );
    }
}