<?php

declare(strict_types=1);

namespace App\Application\StaticPage\DeleteStatiPage;

use App\Contract\Core\CommandInterface;
use App\Domain\StaticPage\StaticPage;

/**
 * Class DeleteStaticPage
 * @package App\Application\StaticPage\DeleteStatiPage
 */
class DeleteStaticPage implements CommandInterface
{
    /** @var StaticPage $staticPage */
    private StaticPage $staticPage;

    /**
     * DeleteStaticPage constructor
     *
     * @param StaticPage $staticPage
     */
    public function __construct(StaticPage $staticPage)
    {
        $this->staticPage = $staticPage;
    }

    /**
     * @return StaticPage
     */
    public function getStaticPage(): StaticPage
    {
        return $this->staticPage;
    }
}