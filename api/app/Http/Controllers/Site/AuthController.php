<?php

declare(strict_types=1);

namespace App\Http\Controllers\Site;

use App\Application\Authentication\GenerateSession\GenerateSession;
use App\Application\Authentication\RestoreToken\RestoreToken;
use App\Application\Authentication\SignIn\SignIn;
use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Auth\LoginRequest;
use App\Http\Resourses\Auth\AuthDataResource;
use App\Http\Resourses\Auth\SessionDataResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * Class AuthController
 * @package App\Http\Controllers\Site
 */
class AuthController extends Controller
{
    /**
     * @return JsonResponse
     */
    public function session(): JsonResponse
    {
        $token = $this->execute(new GenerateSession());

        return new JsonResponse(new SessionDataResource($token));
    }

    /**
     * @param LoginRequest $request
     *
     * @return JsonResponse
     */
    public function signIn(LoginRequest $request): JsonResponse
    {
        $token = $this->execute(new SignIn(
                $request->get('email'),
                $request->get('password')
            )
        );

        return new JsonResponse(new AuthDataResource($token));
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function refreshToken(Request $request): JsonResponse
    {
        $tokenData = $this->execute(new RestoreToken($request->header('Authorization', '')));

        return new JsonResponse(new AuthDataResource($tokenData));
    }
}
