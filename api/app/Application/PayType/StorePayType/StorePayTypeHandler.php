<?php

declare(strict_types=1);

namespace App\Application\PayType\StorePayType;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\PayType\PayType;
use App\Domain\PayType\PayTypeRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * Class StorePayTypeHandler
 * @package App\Application\PayType\StorePayType
 */
class StorePayTypeHandler implements HandlerInterface
{
    /** @var PayTypeRepositoryInterface $payTypeRepository */
    private PayTypeRepositoryInterface $payTypeRepository;

    /**
     * StorePayTypeHandler constructor
     *
     * @param PayTypeRepositoryInterface $payTypeRepository
     */
    public function __construct(PayTypeRepositoryInterface $payTypeRepository)
    {
        $this->payTypeRepository = $payTypeRepository;
    }

    /**
     * @param StorePayType $command
     *
     * @return Model
     */
    public function handle(CommandInterface $command): Model
    {
        $payType = new PayType();

        $payType->title = $command->getTitle();
        $payType->description = $command->getDescription();
        $payType->icon = $command->getIcon();

        return $this->payTypeRepository->store($payType);
    }
}