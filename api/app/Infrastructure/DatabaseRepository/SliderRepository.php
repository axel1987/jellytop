<?php

declare(strict_types=1);

namespace App\Infrastructure\DatabaseRepository;

use App\Contract\Core\DatabaseRepositoryInterface;
use App\Contract\Core\FilterInterface;
use App\Contract\Core\SortingInterface;
use App\Domain\Slider\Slider;
use App\Domain\Slider\SliderFilter;
use App\Domain\Slider\SliderRepositoryInterface;

/**
 * Class SliderRepository
 * @package App\Infrastructure\DatabaseRepository
 */
class SliderRepository extends DatabaseRepository implements SliderRepositoryInterface
{
    /**
     * SliderRepository constructor
     */
    public function __construct()
    {
        $this->model = new Slider();
    }

    /**
     * @param FilterInterface|SliderFilter $filter
     *
     * @return $this
     */
    protected function filter(FilterInterface $filter): DatabaseRepositoryInterface
    {
        $this->builder = $this->builder ?? $this->model->newQuery();

        if ($filter->getId()) {
            $this->builder->where('id', '=', $filter->getId());
        }

        return $this;
    }

    /**
     * @param SortingInterface $sorting
     *
     * @return $this
     */
    protected function sorting(SortingInterface $sorting): DatabaseRepositoryInterface
    {
        return $this;
    }
}