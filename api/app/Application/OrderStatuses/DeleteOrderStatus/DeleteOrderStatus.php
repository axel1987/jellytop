<?php

declare(strict_types=1);

namespace App\Application\OrderStatuses\DeleteOrderStatus;

use App\Contract\Core\CommandInterface;
use App\Domain\OrderStatus\OrderStatus;

/**
 * Class DeleteOrderStatus
 * @package App\Application\OrderStatuses\DeleteOrderStatus
 */
class DeleteOrderStatus implements CommandInterface
{
    /** @var OrderStatus $orderStatus */
    private OrderStatus $orderStatus;

    /**
     * DeleteOrderStatus constructor
     *
     * @param OrderStatus $orderStatus
     */
    public function __construct(OrderStatus $orderStatus)
    {
        $this->orderStatus = $orderStatus;
    }

    /**
     * @return OrderStatus
     */
    public function getOrderStatus(): OrderStatus
    {
        return $this->orderStatus;
    }
}