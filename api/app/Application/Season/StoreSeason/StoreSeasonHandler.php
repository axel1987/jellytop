<?php

declare(strict_types=1);

namespace App\Application\Season\StoreSeason;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\Season\Season;
use App\Domain\Season\SeasonRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * Class StoreSeasonHandler
 * @package App\Application\Season\StoreSeason
 */
class StoreSeasonHandler implements HandlerInterface
{
    /** @var SeasonRepositoryInterface $seasonRepository */
    private SeasonRepositoryInterface $seasonRepository;

    /**
     * StoreSeasonHandler constructor
     *
     * @param SeasonRepositoryInterface $seasonRepository
     */
    public function __construct(SeasonRepositoryInterface $seasonRepository)
    {
        $this->seasonRepository = $seasonRepository;
    }

    /**
     * @param CommandInterface| StoreSeason $command
     *
     * @return Season
     */
    public function handle(CommandInterface $command): Model
    {
        $season = new Season();

        $season->title = $command->getTitle();
        $season->slug = $command->getSlug();
        $season->icon = $command->getIcon();

        return $this->seasonRepository->store($season);
    }
}