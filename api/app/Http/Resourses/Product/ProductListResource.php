<?php

declare(strict_types=1);

namespace App\Http\Resourses\Product;

use App\Domain\Product\Product;
use App\Http\Resourses\BaseCollectionResource;
use App\Http\Resourses\ProductImage\ProductImagesListResource;

/**
 * Class ProductListResource
 * @package App\Http\Resourses\Product
 */
class ProductListResource extends BaseCollectionResource
{
    /**
     * @param Product $item
     *
     * @return array
     */
    protected function getItemData($item): array
    {
        return [
            'id' => $item->id,
            'type' => 'product',
            'attributes' => [
                'title' => $item->title,
                'slug' => $item->slug,
                'priority' => $item->priority,
                'inStock' => $item->in_stock,
                'createdAt' => $item->created_at ? $item->created_at->format('Y-m-d H:i:s') : '',
                'updatedAt' => $item->updated_at ? $item->updated_at->format('Y-m-d H:i:s') : '',
            ],
            'relationships' => [
                'images' => new ProductImagesListResource($item->images),
            ]
        ];
    }
}