import Slider from "../dto/entities/Slider";
import Pagination from "../dto/entities/Pagination";

/**
 * @returns {{
 * slider: null,
 * slidersList: *[],
 * slidersSelectList: *[],
 * pagination: null,
 * }}
 */
export const state = () => ({
    slider: null,
    slidersList: [],
    slidersSelectList: [],
    pagination: null
});

/**
 *
 * @type {{
 * getSlider(*): null,
 * getSlidersList(*): [],
 * getSlidersSelectList(*): [],
 * getSliderPagination(*): null
 * }}
 */
export const getters = {
    getSlider(state) {
        return state.slider;
    },
    getSlidersList(state) {
        return state.slidersList;
    },
    getSlidersSelectList(state) {
        return state.slidersSelectList;
    },
    getSliderPagination(state) {
        return state.pagination;
    }
};

/**
 * @type {{
 * SET_SLIDER_LIST(*, *): void,
 * SET_SLIDER_SELECT_LIST(*, *): void,
 * SET_SLIDER(*, *): void,
 * SET_SLIDER_PAGINATION(*, *): void,
 * }}
 */
export const mutations = {
    SET_SLIDER(state, value) {
        state.slider = value
    },
    SET_SLIDER_LIST(state, value) {
        state.slidersList = value
    },
    SET_SLIDER_SELECT_LIST(state, value) {
        state.slidersSelectList = value
    },
    SET_SLIDER_PAGINATION(state, value) {
        state.pagination = value
    },
};

/**
 * @type {{
 * registerSlider: ((function({commit: *}, *=): Promise<void>)|*),
 * updateSlider: ((function({commit: *}, *=): Promise<void>)|*),
 * fetchSlider: ((function({commit: *}, *=): Promise<void>)|*),
 * fetchSlidersList: ((function({commit: *}, *=): Promise<void>)|*),
 * fetchSlidersSelectList: ((function({commit: *}, *=): Promise<void>)|*),
 * deleteSlider: ((function({commit: *}, *=): Promise<void>)|*)
 * }}
 */
export const actions = {
    fetchSlidersList: async function ({commit}, params = {}) {
        let {data} = await this.$axios.get('/sliders', {params: params});

        const list = data.data.map((sliderData) => {
            return new Slider(sliderData)
        });
        const pagination = data.pagination ? new Pagination(data.pagination) : null;

        commit('SET_SLIDER_LIST', list)
        commit('SET_SLIDER_PAGINATION', pagination)
    },
    fetchSlidersSelectList: async function ({commit}, params = {}) {
        let {data} = await this.$axios.get('/sliders/list', params);

        const list = data.data.map((sliderData) => {
            return new Slider(sliderData)
        });

        commit('SET_SLIDER_SELECT_LIST', list)
    },
    fetchSlider: async function ({commit}, params = {}) {
        let {data} = await this.$axios.get(`/sliders/${params.id}`, params);
        commit('SET_SLIDER', new Slider(data))
    },
    registerSlider: async function ({commit}, params = {}) {
        await this.$axios.post(`/sliders`, params).then((res) => {
            if (res) {
                commit('SET_SLIDER', new Slider(res.data))
            }
        });
    },
    updateSlider: async function ({commit}, params = {}) {
        await this.$axios.put(`/sliders/${params.id}`, params).then((res) => {
            if (res) {
                commit('SET_SLIDER', new Slider(res.data))
            }
        });
    },
    deleteSlider: async function ({commit}, params = {}) {
        await this.$axios.delete(`/sliders/${params.id}`, params);
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations,
};
