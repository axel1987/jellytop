export default class BrandForm {

    /**
     * @return <void>
     */
    constructor(data) {
        this.id = data.id;
        this.title = {
            ua: data.title && data.title.ua ? data.title.ua : null,
            ru: data.title && data.title.ru ? data.title.ru : null
        };
        this.description = {
            ua: data.description && data.description.ua ? data.description.ua : null,
            ru: data.description && data.description.ru ? data.description.ru : null
        };
        this.slug = data.slug;
        this.logo = data.logo;
        this.countryId = data.country && data.country.id ? data.country.id : null
    }

    /**
     * @returns {{required: (function(*=))}}
     */
    rules() {
        return {
            required: value => !!value || 'Это поле обязательно',
        }
    }
}
