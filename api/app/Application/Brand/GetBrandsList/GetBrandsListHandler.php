<?php

declare(strict_types=1);

namespace App\Application\Brand\GetBrandsList;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\Brand\BrandRepositoryInterface;

/**
 * Class GetBrandsListHandler
 * @package App\Application\Brand\GetBrandsList
 */
class GetBrandsListHandler implements HandlerInterface
{
    /** @var BrandRepositoryInterface $brandRepository */
    private BrandRepositoryInterface $brandRepository;

    /**
     * GetBrandsListHandler constructor
     *
     * @param BrandRepositoryInterface $brandRepository
     */
    public function __construct(BrandRepositoryInterface $brandRepository)
    {
        $this->brandRepository = $brandRepository;
    }

    /**
     * @param CommandInterface|GetBrandsList $command
     *
     * @return mixed|void
     */
    public function handle(CommandInterface $command)
    {
        return $this->brandRepository->all(
            $command->getBrandFilter(),
            $command->getPagination(),
            $command->getSorting()
        );
    }
}