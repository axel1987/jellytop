<?php

declare(strict_types=1);

namespace App\Application\Color\GetColorByFilter;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\Color\Color;
use App\Domain\Color\ColorRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * Class GetColorByFilterHandler
 * @package App\Application\Color\GetColorByFilter
 */
class GetColorByFilterHandler implements HandlerInterface
{
    /** @var ColorRepositoryInterface $colorRepository */
    private ColorRepositoryInterface $colorRepository;

    /**
     * GetColorByFilterHandler constructor
     *
     * @param ColorRepositoryInterface $colorRepository
     */
    public function __construct(ColorRepositoryInterface $colorRepository)
    {
        $this->colorRepository = $colorRepository;
    }

    /**
     * @param CommandInterface|GetColorByFilter $command
     *
     * @return Color|null
     */
    public function handle(CommandInterface $command): ?Model
    {
        return $this->colorRepository->one(
            $command->getFilter()
        );
    }
}