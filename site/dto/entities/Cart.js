import Product from "./Product";
import Size from "./Size";
import ProductImage from "./ProductImage";

export default class Cart {
    /**
     * @param data
     */
    constructor(data = {}) {
        this.id = data.id ? data.id : null;
        this.count = data.attributes && data.attributes.count ?
            data.attributes.count :
            null;
        this.sum = data.attributes && data.attributes.sum ?
            data.attributes.sum :
            null;

        this.product = data.relationships && data.relationships.product
            ? new Product(data.relationships.product)
            : null;
        this.productImages = data.relationships
                && data.relationships.product
                && data.relationships.product.relationships
                && data.relationships.product.relationships.images
            ? data.relationships.product.relationships.images.data.map((item) => {return new ProductImage(item)})
            : null;
        this.size = data.relationships && data.relationships.size
            ? new Size(data.relationships.size)
            : null;

        this.createdAt = data.attributes && data.attributes.createdAt ?
            data.attributes.createdAt :
            null;
        this.updatedAt = data.attributes && data.attributes.updatedAt ?
            data.attributes.updatedAt :
            null;
    }
}