<?php

declare(strict_types=1);

namespace App\Application\StaticPage\StoreStaticPage;

use App\Contract\Core\CommandInterface;

/**
 * Class StoreStaticPage
 * @package App\Application\StaticPage\StoreStaticPage
 */
class StoreStaticPage implements CommandInterface
{
    /** @var string $pageName */
    private string $pageName;

    /** @var array $content */
    private array $content;

    /**
     * StoreStaticPage constructor
     *
     * @param string $pageName
     * @param array $content
     */
    public function __construct(
        string $pageName,
        array $content
    ) {
        $this->pageName = $pageName;
        $this->content = $content;
    }

    /**
     * @return string
     */
    public function getPageName(): string
    {
        return $this->pageName;
    }

    /**
     * @return array
     */
    public function getContent(): array
    {
        return $this->content;
    }
}