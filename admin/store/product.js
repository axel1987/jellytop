import Pagination from "../dto/entities/Pagination";
import Product from "../dto/entities/Product";
import Vue from 'vue';

/**
 * @returns {{
 * pagination: null,
 * productsList: *[],
 * product: null
 * }}
 */
export const state = () => ({
  product: null,
  productsList: [],
  pagination: null
});

/**
 * @type {{
 * getProductsList(*): [],
 * getProduct(*): null,
 * getProductsPagination(*): null
 * }}
 */
export const getters = {
  getProduct(state) {
    return state.product;
  },
  getProductsList(state) {
    return state.productsList;
  },
  getProductsPagination(state) {
    return state.pagination;
  }
};

/**
 * @type {{
 * SET_PRODUCTS_LIST(*, *): void,
 * SET_PRODUCT(*, *): void
 * }}
 */
export const mutations = {
  SET_PRODUCT(state, value) {
    state.product = value
  },
  SET_PRODUCTS_LIST(state, value) {
    state.productsList = value
  },
  SET_PRODUCTS_PAGINATION(state, value) {
    state.pagination = value
  },
};

/**
 * @type {{
 * registerProduct: ((function({commit: *}, *=): Promise<void>)|*),
 * updateProduct: ((function({commit: *}, *=): Promise<void>)|*),
 * fetchProduct: ((function({commit: *}, *=): Promise<void>)|*),
 * fetchProductsList: ((function({commit: *}, *=): Promise<void>)|*),
 * deleteProduct: ((function({commit: *}, *=): Promise<void>)|*)
 * }}
 */
export const actions = {
  fetchProductsList: async function ({commit}, params = {}) {
    let {data} = await this.$axios.get('/products', {params: params});
    
    const list = data.data.map((productData) => {
      return new Product(productData)
    });
    const pagination = data.pagination ? new Pagination(data.pagination) : null;
    
    commit('SET_PRODUCTS_LIST', list)
    commit('SET_PRODUCTS_PAGINATION', pagination)
  },
  fetchProduct: async function ({commit}, params = {}) {
    if (params.id) {
      let {data} = await this.$axios.get(`/products/${params.id}`, params);
      commit('SET_PRODUCT', new Product(data))
    } else {
      commit('SET_PRODUCT', new Product({}))
    }
  },
  registerProduct: async function ({commit}, params = {}) {
    await this.$axios.post(`/products`, params).then(
      (res) => {
        if (res) {
          commit('SET_PRODUCT', new Product(res.data))

          Vue.notify({
            group: 'dashboard',
            title: 'Успешно',
            type: 'success',
            text: `Продукт добавлен`,
          })
          this.$router.push(`/products/${res.data.id}`)
        } else {
          Vue.notify({
            group: 'dashboard',
            title: 'Ошибка',
            type: 'error',
            text: `неверные данные`,
          });
        }
      }, (error)=> {
        console.log('error', error)
      });
  },
  updateProduct: async function ({commit}, params = {}) {
    await this.$axios.put(`/products/${params.id}`, params).then((res) => {
      if (res) {
        commit('SET_PRODUCT', new Product(res.data))
        
        Vue.notify({
          group: 'dashboard',
          title: 'Успешно',
          type: 'success',
          text: `Продукт обновлен`,
        })
      } else {
        Vue.notify({
          group: 'dashboard',
          title: 'Ошибка',
          type: 'error',
          text: `неверные данные`,
        });
      }
      
      return res;
    });
  },
  updateProductStock: async function ({commit}, params = {}) {
    await this.$axios.put(`/products/${params.id}/update-stock`, params).then((res) => {
      if (res) {
        commit('SET_PRODUCT', new Product(res.data))
        
        Vue.notify({
          group: 'dashboard',
          title: 'Успешно',
          type: 'success',
          text: `Продукт обновлен`,
        })
      } else {
        Vue.notify({
          group: 'dashboard',
          title: 'Ошибка',
          type: 'error',
          text: `неверные данные`,
        });
      }
      
      return res;
    });
  },
  updateProductPriority: async function ({commit}, params = {}) {
    await this.$axios.put(`/products/${params.id}/update-priority`, params).then((res) => {
      if (res) {
        commit('SET_PRODUCT', new Product(res.data))
        
        Vue.notify({
          group: 'dashboard',
          title: 'Успешно',
          type: 'success',
          text: `Продукт обновлен`,
        })
      } else {
        Vue.notify({
          group: 'dashboard',
          title: 'Ошибка',
          type: 'error',
          text: `неверные данные`,
        });
      }
      
      return res;
    });
  },
  deleteProduct: async function ({commit}, params = {}) {
    await this.$axios.delete(`/products/${params.id}`, params);
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
