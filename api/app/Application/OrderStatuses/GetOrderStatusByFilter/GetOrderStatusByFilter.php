<?php

declare(strict_types=1);

namespace App\Application\OrderStatuses\GetOrderStatusByFilter;

use App\Contract\Core\CommandInterface;
use App\Domain\OrderStatus\OrderStatusFilter;

/**
 * Class GetOrderStatusByFilter
 * @package App\Application\OrderStatuses\GetOrderStatusByFilter
 */
class GetOrderStatusByFilter implements CommandInterface
{
    /** @var OrderStatusFilter $filter */
    private OrderStatusFilter $filter;

    /**
     * GetOrderStatusByFilter constructor
     *
     * @param OrderStatusFilter $filter
     */
    public function __construct(OrderStatusFilter $filter)
    {
        $this->filter = $filter;
    }

    /**
     * @return OrderStatusFilter
     */
    public function getFilter(): OrderStatusFilter
    {
        return $this->filter;
    }
}