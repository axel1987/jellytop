<?php

declare(strict_types=1);

namespace App\Http\Controllers\Site;

use App\Application\Gender\GetGendersList\GetGendersList;
use App\Domain\Gender\GenderFilter;
use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Gender\GenderIndexRequest;
use App\Http\Resourses\Gender\GenderListResource;
use App\Infrastructure\Core\Pagination;
use App\Infrastructure\Core\Sorting;
use Illuminate\Http\JsonResponse;

/**
 * Class GenderController
 * @package App\Http\Controllers
 */
class GenderController extends Controller
{
    /**
     * @param GenderIndexRequest $request
     *
     * @return JsonResponse
     */
    public function index(GenderIndexRequest $request): JsonResponse
    {
        $filter = GenderFilter::fromRequest($request);
        $pagination = Pagination::fromRequest($request);
        $sorting = Sorting::fromRequest($request);

        $list = $this->execute(new GetGendersList(
            $filter,
            $pagination,
            $sorting
        ));

        return new JsonResponse(new GenderListResource($list));
    }

    /**
     * @param GenderIndexRequest $request
     *
     * @return JsonResponse
     */
    public function list(GenderIndexRequest $request): JsonResponse
    {
        $filter = GenderFilter::fromRequest($request);
        $sorting = Sorting::fromRequest($request);

        $list = $this->execute(new GetGendersList(
            $filter,
            null,
            $sorting
        ));

        return new JsonResponse(new GenderListResource($list));
    }
}