<?php

declare(strict_types=1);

namespace App\Domain\Gender;

use Exception;

/**
 * Class GenderException
 * @package App\Domain\Gender
 */
class GenderException extends Exception
{
    /**
     * @param int $id
     *
     * @throws GenderException
     */
    public static function notFound(int $id)
    {
        throw new GenderException(sprintf('Color with id %d not found', $id));
    }
}