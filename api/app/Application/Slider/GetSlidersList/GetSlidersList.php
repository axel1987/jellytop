<?php

declare(strict_types=1);

namespace App\Application\Slider\GetSlidersList;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\PaginationInterface;
use App\Contract\Core\SortingInterface;
use App\Domain\Slider\SliderFilter;

/**
 * Class GetSlidersList
 * @package App\Application\Slider\GetSlidersList
 */
class GetSlidersList implements CommandInterface
{
    /** @var SliderFilter $filter */
    private SliderFilter $filter;

    /** @var PaginationInterface|null $pagination */
    private ?PaginationInterface $pagination;

    /** @var SortingInterface|null $sorting */
    private ?SortingInterface $sorting;

    /**
     * GetSlidersList constructor
     *
     * @param SliderFilter $filter
     * @param PaginationInterface|null $pagination
     * @param SortingInterface|null $sorting
     */
    public function __construct(
        SliderFilter $filter,
        ?PaginationInterface $pagination = null,
        ?SortingInterface $sorting = null
    ) {
        $this->filter = $filter;
        $this->pagination = $pagination;
        $this->sorting = $sorting;
    }

    /**
     * @return SliderFilter
     */
    public function getFilter(): SliderFilter
    {
        return $this->filter;
    }

    /**
     * @return PaginationInterface
     */
    public function getPagination(): ?PaginationInterface
    {
        return $this->pagination;
    }

    /**
     * @return SortingInterface
     */
    public function getSorting(): ?SortingInterface
    {
        return $this->sorting;
    }
}