<?php

declare(strict_types=1);

namespace App\Application\Delivery\DeleteDelivery;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\Delivery\DeliveryRepositoryInterface;
use Illuminate\Support\Facades\Storage;

/**
 * Class DeleteDeliveryHandler
 * @package App\Application\Delivery\DeleteDelivery
 */
class DeleteDeliveryHandler implements HandlerInterface
{
    /** @var DeliveryRepositoryInterface $deliveryRepository */
    private DeliveryRepositoryInterface $deliveryRepository;

    /**
     * DeleteDeliveryHandler constructor
     *
     * @param DeliveryRepositoryInterface $deliveryRepository
     */
    public function __construct(DeliveryRepositoryInterface $deliveryRepository)
    {
        $this->deliveryRepository = $deliveryRepository;
    }

    /**
     * @param DeleteDelivery $command
     *
     * @return void
     */
    public function handle(CommandInterface $command): void
    {
        $delivery = $command->getDelivery();

        if ($delivery->icon) {
            Storage::disk('public')
                ->delete(str_replace('/storage', '', $delivery->icon));
        }

        $this->deliveryRepository->delete($delivery);
    }
}