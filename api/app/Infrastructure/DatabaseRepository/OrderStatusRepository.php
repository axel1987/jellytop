<?php

declare(strict_types=1);

namespace App\Infrastructure\DatabaseRepository;

use App\Contract\Core\DatabaseRepositoryInterface;
use App\Contract\Core\FilterInterface;
use App\Contract\Core\SortingInterface;
use App\Domain\OrderStatus\OrderStatus;
use App\Domain\OrderStatus\OrderStatusRepositoryInterface;

/**
 * Class OrderStatusRepository
 * @package App\Infrastructure\DatabaseRepository
 */
class OrderStatusRepository extends DatabaseRepository implements OrderStatusRepositoryInterface
{
    /**
     * OrderStatusRepository constructor
     */
    public function __construct()
    {
        $this->model = new OrderStatus();
    }

    /**
     * @param FilterInterface $filter
     *
     * @return $this
     */
    protected function filter(FilterInterface $filter): DatabaseRepositoryInterface
    {
        $this->builder = $this->builder ?? $this->model->newQuery();

        if ($filter->getId()) {
            $this->builder->where('id', '=', $filter->getId());
        }

        return $this;
    }

    /**
     * @param SortingInterface $sorting
     *
     * @return $this
     */
    protected function sorting(SortingInterface $sorting): DatabaseRepositoryInterface
    {
        return $this;
    }
}