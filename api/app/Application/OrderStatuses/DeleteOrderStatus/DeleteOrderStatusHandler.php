<?php

declare(strict_types=1);

namespace App\Application\OrderStatuses\DeleteOrderStatus;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Infrastructure\DatabaseRepository\OrderStatusRepository;

/**
 * Class DeleteOrderStatusHandler
 * @package App\Application\OrderStatuses\DeleteOrderStatus
 */
class DeleteOrderStatusHandler implements HandlerInterface
{
    /** @var OrderStatusRepository $orderStatusRepository */
    private OrderStatusRepository $orderStatusRepository;

    /**
     * UpdateOrderStatusHandler constructor
     *
     * @param OrderStatusRepository $orderStatusRepository
     */
    public function __construct(OrderStatusRepository $orderStatusRepository)
    {
        $this->orderStatusRepository = $orderStatusRepository;
    }

    /**
     * @param CommandInterface|DeleteOrderStatus $command
     *
     * @return void
     */
    public function handle(CommandInterface $command): void
    {
        $this->orderStatusRepository->delete(
            $command->getOrderStatus()
        );
    }
}