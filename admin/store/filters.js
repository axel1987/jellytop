/**
 * @returns {{
 * orderFilter: {search: null, deliveryId: null, statusId: null, payTypeId: null},
 * productFilter: {sizeId: null, search: null, seasonId: null, colorId: null, brandId: null, genderId: null, typeId: null}
 * }}
 */
export const state = () => ({
    orderFilter : {
        search: null,
        statusId: null,
        deliveryId: null,
        payTypeId: null,
    },
    productFilter : {
        search: null,
        brandId: null,
        typeId: null,
        colorId: null,
        sizeId: null,
        seasonId: null,
        genderId: null,
        inStock: 1,
    }
});

/**
 * @type {{
 * getProductFilter(*): {sizeId: null, search: null, seasonId: null, colorId: null, brandId: null, genderId: null, typeId: null},
 * getOrderFilter(*): {search: null, deliveryId: null, statusId: null, payTypeId: null}
 * }}
 */
export const getters = {
    getOrderFilter(state) {
        return state.orderFilter;
    },
    getProductFilter(state) {
        return state.productFilter;
    },
};

/**
 * @type {{
 * SET_ORDER_FILTER(*, *): void,
 * SET_PRODUCT_FILTER(*, *): void
 * }}
 */
export const mutations = {
    SET_ORDER_FILTER(state, value) {
        state.orderFilter = value
    },
    SET_PRODUCT_FILTER(state, value) {
        state.productFilter = value
    },
};