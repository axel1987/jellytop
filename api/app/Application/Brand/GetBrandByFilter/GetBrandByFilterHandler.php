<?php

declare(strict_types=1);

namespace App\Application\Brand\GetBrandByFilter;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\Brand\Brand;
use App\Domain\Brand\BrandRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * Class GetBrandByFilterHandler
 * @package App\Application\Brand\GetBrandByFilter
 */
class GetBrandByFilterHandler implements HandlerInterface
{
    /** @var BrandRepositoryInterface $brandRepository */
    private BrandRepositoryInterface $brandRepository;

    /**
     * GetBrandByFilterHandler constructor
     *
     * @param BrandRepositoryInterface $brandRepository
     */
    public function __construct(BrandRepositoryInterface $brandRepository)
    {
        $this->brandRepository = $brandRepository;
    }

    /**
     * @param CommandInterface|GetBrandByFilter $command
     *
     * @return Brand|null
     */
    public function handle(CommandInterface $command): ?Model
    {
        return $this->brandRepository->one(
            $command->getFilter()
        );
    }
}