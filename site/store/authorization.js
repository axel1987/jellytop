import {v4 as uuid} from 'uuid';

/**
 * @returns {{isAuthorized: boolean}}
 */
export const state = () => ({
    isAuthorized: false,
    session: false,
});

/**
 * @type {{isAuthorized(*): boolean}}
 */
export const getters = {
    isAuthorized(state) {
        return state.isAuthorized;
    },
    session(state) {
        return state.session;
    },
};

/**
 * @type {{SET_IS_AUTHORIZED(*, *): void}}
 */
export const mutations = {
    SET_IS_AUTHORIZED(state, value) {
        state.isAuthorized = value;
    },
    SET_SESSION(state, value) {
        state.session = value;
    },
};

/**
 * @type {{
 * restorePassword: ((function({commit: *}, *=): Promise<void>)|*),
 * login: ((function({commit: *}, *=): Promise<any|undefined>)|*),
 * confirmEmail: (function({commit: *}, *=): any),
 * register: ((function({commit: *}, *=): Promise<void>)|*)}}
 */
export const actions = {
    fetchSession: async function ({commit}, params = {}) {
        let token = this.$cookies.get('access-session')
            ? this.$cookies.get('access-session')
            : false

        if (!token) {
            token = state().session ? state().session : uuid()
        }
        commit('SET_SESSION', token);

        if (process.browser) {
            this.$cookies.set('access-session', token, {maxAge: 2592000})
        }
    },
    login: async function ({commit}, params = {}) {
        let res = await this.$axios.post('/auth/sign-in', params);

        if (res && res.data) {
            const data = res.data;
            this.$cookies.set(
                'token',
                `${data.data.attributes.tokenType} ${data.data.attributes.accessToken}`,
            );
            commit('SET_IS_AUTHORIZED', true);

            return data;
        }
    },
    register: async function ({commit}, params = {}) {
        await this.$axios.post('/auth/register', params);
    },
    restorePassword: async function ({commit}, params = {}) {
        await this.$axios.post('/auth/restore-password', params);
    },
    restoreToken: async function ({commit}, params = {}) {
        let res = await this.$axios.post('/auth/refresh-token', params);
        if (res.data) {
            const data = res.data;
            sessionStorage.setItem(
                'token',
                `${data.data.attributes.tokenType} ${data.data.attributes.accessToken}`,
            );
            commit('SET_IS_AUTHORIZED', true);
        }

        return res;
    },
    confirmEmail: async function ({commit}, params = {}) {
        let {data} = await this.$axios.post('/auth/confirm-email', params);
        localStorage.setItem('token', `${data.tokenType} ${data.accessToken}`);
        commit('SET_IS_AUTHORIZED', true);

        return data;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations,
};
