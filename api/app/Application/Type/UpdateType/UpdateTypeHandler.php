<?php

declare(strict_types=1);

namespace App\Application\Type\UpdateType;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\Type\Type;
use App\Domain\Type\TypeRepositoryInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

/**
 * Class UpdateTypeHandler
 * @package App\Application\Type\UpdateType
 */
class UpdateTypeHandler implements HandlerInterface
{
    /** @var TypeRepositoryInterface $typeRepository */
    private TypeRepositoryInterface $typeRepository;

    /**
     * UpdateTypeHandler constructor
     *
     * @param TypeRepositoryInterface $typeRepository
     */
    public function __construct(TypeRepositoryInterface $typeRepository)
    {
        $this->typeRepository = $typeRepository;
    }

    /**
     * @param CommandInterface|UpdateType $command
     *
     * @return Type
     */
    public function handle(CommandInterface $command): Model
    {
        $oldIcon = null;
        $type = $command->getType();

        if ($type->icon != $command->getIcon()) {
            $oldIcon = $type->icon;
        }

        $type->title = $command->getTitle();
        $type->slug = $command->getSlug();
        $type->description = $command->getDescription();
        $type->icon = $command->getIcon();
        $this->typeRepository->store($type);

        if ($oldIcon) {
            Storage::disk('public')
                ->delete(str_replace('/storage', '', $oldIcon));
        }

        return $type;
    }
}