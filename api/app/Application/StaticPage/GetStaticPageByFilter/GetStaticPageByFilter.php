<?php

declare(strict_types=1);

namespace App\Application\StaticPage\GetStaticPageByFilter;

use App\Contract\Core\CommandInterface;
use App\Domain\StaticPage\StaticPageFilter;

/**
 * Class GetStaticPageByFilter
 * @package App\Application\StaticPage\GetStaticPageByFilter
 */
class GetStaticPageByFilter implements CommandInterface
{
    /** @var StaticPageFilter $filter */
    private StaticPageFilter $filter;

    /**
     * GetStaticPageByFilter constructor
     *
     * @param StaticPageFilter $filter
     */
    public function __construct(StaticPageFilter $filter)
    {
        $this->filter = $filter;
    }

    /**
     * @return StaticPageFilter
     */
    public function getFilter(): StaticPageFilter
    {
        return $this->filter;
    }
}