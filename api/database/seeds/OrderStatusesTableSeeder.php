<?php

declare(strict_types=1);

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

/**
 * Class GendersTableSeeder
 */
class OrderStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now();

        DB::table('order_statuses')->insert([
            [
                'title' => 'Новый',
                'created_at' => $now,
                'updated_at' => $now,
            ],
            [
                'title' => 'Прозвонен',
                'created_at' => $now,
                'updated_at' => $now,
            ],
            [
                'title' => 'Резерв',
                'created_at' => $now,
                'updated_at' => $now,
            ],
            [
                'title' => 'Оплачен/Не отправлен',
                'created_at' => $now,
                'updated_at' => $now,
            ],
            [
                'title' => 'Оплачен/Отправлен',
                'created_at' => $now,
                'updated_at' => $now,
            ],
            [
                'title' => 'Отправлен/Наложенный платеж',
                'created_at' => $now,
                'updated_at' => $now,
            ],
            [
                'title' => 'Отказ',
                'created_at' => $now,
                'updated_at' => $now,
            ],
            [
                'title' => 'Возврат',
                'created_at' => $now,
                'updated_at' => $now,
            ],
            [
                'title' => 'Завершен',
                'created_at' => $now,
                'updated_at' => $now,
            ],
        ]);
    }
}