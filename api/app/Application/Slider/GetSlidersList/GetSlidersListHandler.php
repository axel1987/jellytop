<?php

declare(strict_types=1);

namespace App\Application\Slider\GetSlidersList;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\Slider\SliderRepositoryInterface;

/**
 * Class GetSlidersListHandler
 * @package App\Application\Slider\GetSlidersList
 */
class GetSlidersListHandler implements HandlerInterface
{
    /** @var SliderRepositoryInterface $sliderRepository */
    private SliderRepositoryInterface $sliderRepository;

    /**
     * GetSlidersListHandler constructor
     *
     * @param SliderRepositoryInterface $sliderRepository
     */
    public function __construct(SliderRepositoryInterface $sliderRepository)
    {
        $this->sliderRepository = $sliderRepository;
    }

    /**
     * @param CommandInterface|GetSlidersList $command
     *
     * @return mixed
     */
    public function handle(CommandInterface $command)
    {
        return $this->sliderRepository->all(
            $command->getFilter(),
            $command->getPagination(),
            $command->getSorting()
        );
    }
}