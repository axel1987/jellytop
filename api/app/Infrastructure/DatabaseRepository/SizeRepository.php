<?php

declare(strict_types=1);

namespace App\Infrastructure\DatabaseRepository;

use App\Contract\Core\DatabaseRepositoryInterface;
use App\Contract\Core\FilterInterface;
use App\Contract\Core\SortingInterface;
use App\Domain\Size\Size;
use App\Domain\Size\SizeFilter;
use App\Domain\Size\SizeRepositoryInterface;

/**
 * Class SizeRepository
 * @package App\Infrastructure\DatabaseRepository
 */
class SizeRepository extends DatabaseRepository implements SizeRepositoryInterface
{
    /**
     * SizeRepository constructor
     */
    public function __construct()
    {
        $this->model = new Size();
    }

    /**
     * @param FilterInterface|SizeFilter $filter
     *
     * @return $this
     */
    protected function filter(FilterInterface $filter): DatabaseRepositoryInterface
    {
        $this->builder = $this->builder ?? $this->model->newQuery();

        if ($filter->getId()) {
            $this->builder->where('id', '=', $filter->getId());
        }

        if ($filter->getIds()) {
            $this->builder->whereIn('id', $filter->getIds());
        }

        return $this;
    }

    /**
     * @param SortingInterface $sorting
     *
     * @return $this
     */
    protected function sorting(SortingInterface $sorting): DatabaseRepositoryInterface
    {
        return $this;
    }
}