<?php

declare(strict_types=1);

namespace App\Application\Size\StoreSize;

use App\Contract\Core\CommandInterface;

/**
 * Class StoreSize
 * @package App\Application\Size\StoreSize
 */
class StoreSize implements CommandInterface
{
    /** @var float $size */
    private float $size;

    /**
     * StoreSize constructor
     *
     * @param float $size
     */
    public function __construct(float $size)
    {
        $this->size = $size;
    }

    /**
     * @return float
     */
    public function getSize(): float
    {
        return $this->size;
    }
}