<?php

declare(strict_types=1);

namespace App\Domain\Slider;

use Exception;
/**
 * Class SliderExceptions
 * @package App\Domain\Slider
 */
class SliderExceptions extends Exception
{
    /**
     * @param int $id
     *
     * @throws SliderExceptions
     */
    public static function notFound(int $id)
    {
        throw new SliderExceptions(sprintf('Slider with id %d not found', $id));
    }
}