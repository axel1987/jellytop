<?php

use Laravel\Lumen\Routing\Router;

/**
 * @var Router $router
 */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->group(['prefix' => 'site'], function (Router $router) {
    $router->group(['prefix' => 'auth'], function (Router $router) {
        $router->get('/session', 'AuthController@session');
    });

    $router->group(['prefix' => 'products'], function (Router $router) {
        $router->get('/', 'ProductController@index');
        $router->get('/list', 'ProductController@list');
        $router->get('/{id:[0-9]+}', 'ProductController@view');
        $router->get('/{slug}', 'ProductController@slug');
    });

    $router->group(['prefix' => 'brands'], function (Router $router) {
        $router->get('/', 'BrandController@index');
        $router->get('/list', 'BrandController@list');
        $router->get('/{id}', 'BrandController@view');
    });

    $router->group(['prefix' => 'types'], function (Router $router) {
        $router->get('/', 'TypeController@index');
        $router->get('/list', 'TypeController@list');
        $router->get('/{id}', 'TypeController@view');
    });

    $router->group(['prefix' => 'seasons'], function (Router $router) {
        $router->get('/', 'SeasonController@index');
        $router->get('/list', 'SeasonController@list');
        $router->get('/{id}', 'SeasonController@view');
    });

    $router->group(['prefix' => 'sizes'], function (Router $router) {
        $router->get('/', 'SizeController@index');
        $router->get('/list', 'SizeController@list');
        $router->get('/{id}', 'SizeController@view');
    });

    $router->group(['prefix' => 'colors'], function (Router $router) {
        $router->get('/', 'ColorController@index');
        $router->get('/list', 'ColorController@list');
        $router->get('/{id}', 'ColorController@view');
    });

    $router->group(['prefix' => 'genders'], function (Router $router) {
        $router->get('/', 'GenderController@index');
        $router->get('/list', 'GenderController@list');
    });

    $router->group(['prefix' => 'static-pages'], function (Router $router) {
        $router->get('/{name}', 'StaticPageController@view');
    });

    $router->group(['prefix' => 'delivery'], function (Router $router) {
        $router->get('/', 'DeliveryController@index');
        $router->get('/list', 'DeliveryController@list');
        $router->get('/{id}', 'DeliveryController@view');
        $router->put('/{id}', 'DeliveryController@update');
        $router->post('/', 'DeliveryController@store');
        $router->delete('/{id}', 'DeliveryController@delete');
    });

    $router->group(['prefix' => 'pay-type'], function (Router $router) {
        $router->get('/', 'PayTypeController@index');
        $router->get('/list', 'PayTypeController@list');
        $router->get('/{id}', 'PayTypeController@view');
        $router->put('/{id}', 'PayTypeController@update');
        $router->post('/', 'PayTypeController@store');
        $router->delete('/{id}', 'PayTypeController@delete');
    });

    $router->group(['prefix' => 'cart'], function (Router $router) {
        $router->get('/', 'CartController@index');
        $router->post('/', 'CartController@store');
        $router->delete('/{id}', 'CartController@delete');
    });

    $router->group(['prefix' => 'orders'], function (Router $router) {
        $router->post('/', 'OrderController@store');
    });

    $router->group(['prefix' => 'sliders'], function (Router $router) {
        $router->get('/', 'SliderController@index');
        $router->get('/list', 'SliderController@list');
    });

    $router->group(['prefix' => 'file'], function (Router $router) {
        $router->post('/', 'FileController@upload');
        $router->delete('/', 'FileController@remove');
    });
});
