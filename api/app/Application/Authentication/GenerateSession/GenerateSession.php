<?php

declare(strict_types=1);

namespace App\Application\Authentication\GenerateSession;

use App\Contract\Core\CommandInterface;

/**
 * Class GenerateSession
 * @package App\Application\Authentication\GenerateSession
 */
class GenerateSession implements CommandInterface
{

}