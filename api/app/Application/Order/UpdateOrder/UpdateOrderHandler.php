<?php

declare(strict_types=1);

namespace App\Application\Order\UpdateOrder;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\Order\Order;
use App\Domain\Order\OrderRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * Class UpdateOrderHandler
 * @package App\Application\Order\UpdateOrder
 */
class UpdateOrderHandler implements HandlerInterface
{
    /** @var OrderRepositoryInterface $orderRepository */
    private OrderRepositoryInterface $orderRepository;

    /**
     * UpdateOrderHandler constructor
     *
     * @param OrderRepositoryInterface $orderRepository
     */
    public function __construct(OrderRepositoryInterface $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    /**
     * @param CommandInterface|UpdateOrder $command
     *
     * @return Order
     */
    public function handle(CommandInterface $command): Model
    {
        $order = $command->getOrder();
        $order->status()->associate($command->getOrderStatus());
        $order->note = $command->getNote();

        $this->orderRepository->store($order);

        return $order;
    }
}