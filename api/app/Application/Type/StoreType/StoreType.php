<?php

declare(strict_types=1);

namespace App\Application\Type\StoreType;

use App\Contract\Core\CommandInterface;

/**
 * Class StoreType
 * @package App\Application\Type\StoreType
 */
class StoreType implements CommandInterface
{
    /** @var array $title */
    private array $title;

    /** @var string $slug */
    private string $slug;

    /** @var array|null $description */
    private ?array $description;

    /** @var string|null $icon */
    private ?string $icon;

    /**
     * StoreType constructor
     *
     * @param array $title
     * @param string $slug
     * @param array|null $description
     * @param string|null $icon
     */
    public function __construct(
        array $title,
        string $slug,
        ?array $description = null,
        ?string $icon = null
    ) {
        $this->title = $title;
        $this->slug = $slug;
        $this->description = $description;
        $this->icon = $icon;
    }

    /**
     * @return array
     */
    public function getTitle(): array
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @return array|null
     */
    public function getDescription(): ?array
    {
        return $this->description;
    }

    /**
     * @return string|null
     */
    public function getIcon(): ?string
    {
        return $this->icon;
    }
}