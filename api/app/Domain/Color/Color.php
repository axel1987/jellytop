<?php

declare(strict_types=1);

namespace App\Domain\Color;

use Barryvdh\LaravelIdeHelper\Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class Color
 *
 * @package App\Domain\Color
 * @property int $id
 * @property array $title
 * @property string|null $hex
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|Color newModelQuery()
 * @method static Builder|Color newQuery()
 * @method static Builder|Color query()
 * @method static Builder|Color whereCreatedAt($value)
 * @method static Builder|Color whereIcon($value)
 * @method static Builder|Color whereId($value)
 * @method static Builder|Color whereTitle($value)
 * @method static Builder|Color whereUpdatedAt($value)
 * @mixin Eloquent
 * @method static Builder|Color whereHex($value)
 * @property string $slug
 * @method static Builder|Color whereSlug($value)
 */
class Color extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'hex',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * @var string[]
     */
    protected $casts = [
        'title' => 'array',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];
}