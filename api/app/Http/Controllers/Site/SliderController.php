<?php

declare(strict_types=1);

namespace App\Http\Controllers\Site;

use App\Application\Slider\DeleteSlider\DeleteSlider;
use App\Application\Slider\GetSliderByFilter\GetSliderByFilter;
use App\Application\Slider\GetSlidersList\GetSlidersList;
use App\Application\Slider\StoreSlider\StoreSlider;
use App\Application\Slider\UpdateSlider\UpdateSlider;
use App\Domain\Slider\SliderFilter;
use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Slider\SliderIndexRequest;
use App\Http\Requests\Dashboard\Slider\SliderRequest;
use App\Http\Resourses\Slider\SliderListResource;
use App\Http\Resourses\Slider\SliderResource;
use App\Infrastructure\Core\Pagination;
use App\Infrastructure\Core\Sorting;
use Illuminate\Http\JsonResponse;

/**
 * Class SliderController
 * @package App\Http\Controllers\Admin
 */
class SliderController extends Controller
{
    /**
     * @param SliderIndexRequest $request
     *
     * @return JsonResponse
     */
    public function index(SliderIndexRequest $request): JsonResponse
    {
        $filter = SliderFilter::fromRequest($request);
        $pagination = Pagination::fromRequest($request);
        $sorting = Sorting::fromRequest($request);

        $list = $this->execute(new GetSlidersList(
            $filter,
            $pagination,
            $sorting
        ));

        return new JsonResponse(new SliderListResource($list));
    }
    /**
     * @param SliderIndexRequest $request
     *
     * @return JsonResponse
     */
    public function list(SliderIndexRequest $request): JsonResponse
    {
        $filter = SliderFilter::fromRequest($request);
        $sorting = Sorting::fromRequest($request);

        $list = $this->execute(new GetSlidersList(
            $filter,
            null,
            $sorting
        ));

        return new JsonResponse(new SliderListResource($list));
    }
}