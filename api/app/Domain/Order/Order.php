<?php

declare(strict_types=1);

namespace App\Domain\Order;

use App\Domain\Delivery\Delivery;
use App\Domain\OrderItem\OrderItem;
use App\Domain\OrderStatus\OrderStatus;
use App\Domain\PayType\PayType;
use Barryvdh\LaravelIdeHelper\Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;

/**
 * Class Order
 *
 * @package App\Domain\Order
 * @property int $id
 * @property string $name
 * @property string $phone
 * @property string $sum
 * @property int $status_id
 * @property int $delivery_id
 * @property int $pay_type_id
 * @property string|null $address
 * @property string|null $zip
 * @property string|null $department
 * @property string|null $note
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Delivery $delivery
 * @property-read Collection|OrderItem[] $items
 * @property-read int|null $items_count
 * @property-read PayType $payType
 * @property-read OrderStatus $status
 * @method static Builder|Order newModelQuery()
 * @method static Builder|Order newQuery()
 * @method static Builder|Order query()
 * @method static Builder|Order whereAddress($value)
 * @method static Builder|Order whereCreatedAt($value)
 * @method static Builder|Order whereStatusId($value)
 * @method static Builder|Order whereDeliveryId($value)
 * @method static Builder|Order whereDepartment($value)
 * @method static Builder|Order whereNote($value)
 * @method static Builder|Order whereId($value)
 * @method static Builder|Order whereName($value)
 * @method static Builder|Order wherePayTypeId($value)
 * @method static Builder|Order wherePhone($value)
 * @method static Builder|Order whereSum($value)
 * @method static Builder|Order whereUpdatedAt($value)
 * @method static Builder|Order whereZip($value)
 * @mixin Eloquent
 */
class Order extends Model
{
    /** @type int  */
    const DEFAULT_STATUS_ID = 1;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'phone',
        'sum',
        'address',
        'zip',
        'department',
        'note'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * @var string[]
     */
    protected $casts = [
        'sum' => 'float',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];

    /**
     * @return HasMany
     */
    public function items(): HasMany
    {
        return $this->hasMany(OrderItem::class);
    }

    /**
     * @return BelongsTo
     */
    public function status(): BelongsTo
    {
        return $this->belongsTo(OrderStatus::class);
    }

    /**
     * @return BelongsTo
     */
    public function payType(): BelongsTo
    {
        return $this->belongsTo(PayType::class);
    }

    /**
     * @return BelongsTo
     */
    public function delivery(): BelongsTo
    {
        return $this->belongsTo(Delivery::class);
    }
}