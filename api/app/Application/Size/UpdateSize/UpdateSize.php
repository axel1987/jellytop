<?php

declare(strict_types=1);

namespace App\Application\Size\UpdateSize;

use App\Contract\Core\CommandInterface;
use App\Domain\Size\Size;

/**
 * Class UpdateSize
 * @package App\Application\Size\UpdateSize
 */
class UpdateSize implements CommandInterface
{
    /** @var Size $sizeModel */
    private Size $sizeModel;

    /** @var float $size */
    private float $size;

    /**
     * UpdateSize constructor
     *
     * @param Size $sizeModel
     * @param float $size
     */
    public function __construct(
        Size $sizeModel,
        float $size
    ) {
        $this->sizeModel = $sizeModel;
        $this->size = $size;
    }

    /**
     * @return Size
     */
    public function getSizeModel(): Size
    {
        return $this->sizeModel;
    }

    /**
     * @return float
     */
    public function getSize(): float
    {
        return $this->size;
    }
}