<?php

declare(strict_types=1);

namespace App\Application\Product\GetProductsList;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\PaginationInterface;
use App\Contract\Core\SortingInterface;
use App\Domain\Product\ProductFilter;

/**
 * Class GetProductsList
 * @package App\Application\Product\GetProductsList
 */
class GetProductsList implements CommandInterface
{
    /** @var ProductFilter $productFilter */
    private ProductFilter $productFilter;

    /** @var PaginationInterface|null $pagination */
    private ?PaginationInterface $pagination;

    /** @var SortingInterface|null $sorting */
    protected ?SortingInterface $sorting;

    /**
     * GetProductsList constructor
     *
     * @param ProductFilter $productFilter
     * @param PaginationInterface|null $pagination
     * @param SortingInterface|null $sorting
     */
    public function __construct(
        ProductFilter $productFilter,
        ?PaginationInterface $pagination,
        ?SortingInterface $sorting
    ) {
        $this->productFilter = $productFilter;
        $this->pagination = $pagination;
        $this->sorting = $sorting;
    }

    /**
     * @return ProductFilter
     */
    public function getProductFilter(): ProductFilter
    {
        return $this->productFilter;
    }

    /**
     * @return PaginationInterface|null
     */
    public function getPagination(): ?PaginationInterface
    {
        return $this->pagination;
    }

    /**
     * @return SortingInterface|null
     */
    public function getSorting(): ?SortingInterface
    {
        return $this->sorting;
    }
}