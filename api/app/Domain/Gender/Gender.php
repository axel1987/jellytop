<?php

declare(strict_types=1);

namespace App\Domain\Gender;

use Barryvdh\LaravelIdeHelper\Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class Gender
 *
 * @package App\Domain\Gender
 * @property int $id
 * @property array $title
 * @property string|null $icon
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|Gender newModelQuery()
 * @method static Builder|Gender newQuery()
 * @method static Builder|Gender query()
 * @method static Builder|Gender whereCreatedAt($value)
 * @method static Builder|Gender whereIcon($value)
 * @method static Builder|Gender whereId($value)
 * @method static Builder|Gender whereTitle($value)
 * @method static Builder|Gender whereUpdatedAt($value)
 * @mixin Eloquent
 * @property string $slug
 * @method static Builder|Gender whereSlug($value)
 */
class Gender extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'icon',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * @var string[]
     */
    protected $casts = [
        'title' => 'array',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];
}