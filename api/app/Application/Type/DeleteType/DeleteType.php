<?php

declare(strict_types=1);

namespace App\Application\Type\DeleteType;

use App\Contract\Core\CommandInterface;
use App\Domain\Type\Type;

/**
 * Class DeleteType
 * @package App\Application\Type\DeleteType
 */
class DeleteType implements CommandInterface
{
    /** @var Type $type */
    private Type $type;

    /**
     * DeleteType constructor
     *
     * @param Type $type
     */
    public function __construct(Type $type)
    {
        $this->type = $type;
    }

    /**
     * @return Type
     */
    public function getType(): Type
    {
        return $this->type;
    }
}