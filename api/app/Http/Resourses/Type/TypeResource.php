<?php

declare(strict_types=1);

namespace App\Http\Resourses\Type;

use App\Domain\Brand\Brand;
use App\Domain\Type\Type;
use App\Http\Resourses\Country\CountryResource;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class TypeResource
 * @package App\Http\Resourses\Type
 */
class TypeResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        /** @var Type $this */
        return [
            'id' => $this->id,
            'type' => 'type',
            'attributes' => [
                'title' => $this->title,
                'slug' => $this->slug,
                'description' => $this->description,
                'icon' => $this->icon,
                'createdAt' => $this->created_at ? $this->created_at->format('Y-m-d H:i:s') : '',
                'updatedAt' => $this->updated_at ? $this->updated_at->format('Y-m-d H:i:s') : '',
            ]
        ];
    }
}