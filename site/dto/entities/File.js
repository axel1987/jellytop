export default class File {
    /**
     * @param data
     */
    constructor(data = {}) {
        this.url = data.attributes && data.attributes.url ?
            data.attributes.url :
            null;
    }
}