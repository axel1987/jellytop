<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <url>
        <loc>https://djelitop.com</loc>
        <lastmod>{{ $date }}</lastmod>
        <changefreq>weekly</changefreq>
        <priority>1.0</priority>
    </url>

    @foreach($products as $product)
        <url>
            <loc>https://djelitop.com/{{ $product->slug }}</loc>
            <lastmod>{{ $date }}</lastmod>
            <changefreq>weekly</changefreq>
            <priority>0.7</priority>
        </url>
    @endforeach
</urlset>