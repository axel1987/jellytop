<?php

declare(strict_types=1);

namespace App\Application\Cart\StoreCartItem;

use App\Contract\Core\CommandInterface;
use App\Domain\Product\Product;
use App\Domain\Size\Size;

/**
 * Class StoreCartItem
 * @package App\Application\Cart\StoreCartItem
 */
class StoreCartItem implements CommandInterface
{
    /** @var string $sessionId */
    private string $sessionId;

    /** @var Product $product */
    private Product $product;

    /** @var Size $size */
    private Size $size;

    /** @var int $count */
    private int $count;

    /**
     * StoreCartItem constructor
     *
     * @param string $sessionId
     * @param Product $product
     * @param Size $size
     * @param int $count
     */
    public function __construct(
        string $sessionId,
        Product $product,
        Size $size,
        int $count
    ) {
        $this->sessionId = $sessionId;
        $this->product = $product;
        $this->size = $size;
        $this->count = $count;
    }

    /**
     * @return string
     */
    public function getSessionId(): string
    {
        return $this->sessionId;
    }

    /**
     * @return Product
     */
    public function getProduct(): Product
    {
        return $this->product;
    }

    /**
     * @return Size
     */
    public function getSize(): Size
    {
        return $this->size;
    }

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }
}