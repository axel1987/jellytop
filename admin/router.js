import Vue from 'vue'
import Router from 'vue-router'

import Index from './pages/index'
import SignIn from './pages/autorization/SignIn'

import Users from './pages/user/UsersList'
import ManageUsers from './pages/user/ManageUsers'

import ProductList from './pages/product/ProductList'
import ManageProduct from './pages/product/ManageProduct'

import OrdersList from './pages/order/OrderList'
import ManageOrder from './pages/order/ManageOrder'

import BrandsList from './pages/catalog/BrandsList'
import CountriesList from './pages/catalog/CountriesList'
import ColorsList from './pages/catalog/ColorsList'
import SeasonsList from './pages/catalog/SeasonsList'
import TypesList from './pages/catalog/TypesList'
import SizesList from './pages/catalog/SizesList'
import DeliveryList from './pages/catalog/DeliveryList'
import PayTypesList from './pages/catalog/PayTypesList'
import OrderStatusesList from './pages/catalog/OrderStatusesList'

import StaticPages from "./pages/settings/StaticPages";
import EditStaticPage from "./pages/settings/EditStaticPage";
import Slider from "./pages/settings/Slider";
import EditSlider from "./pages/settings/EditSlider";

Vue.use(Router)

/**
 * @returns {Router}
 */
export function createRouter() {
  return new Router({
    mode: 'history',
    linkActiveClass: 'nuxt-link-active',
    linkExactActiveClass: 'nuxt-link-exact-active',
    routes: [
      {
        name: 'home',
        path: '/',
        component: Index
      },
      {
        name: 'sign-in',
        path: '/sign-in',
        component: SignIn
      },
      {
        name: 'users',
        path: '/users',
        component: Users
      },
      {
        name: 'manage-users',
        path: '/users/:id?',
        component: ManageUsers
      },
      {
        name: 'products',
        path: '/products',
        component: ProductList
      },
      {
        name: 'manage-products',
        path: '/products/:id?',
        component: ManageProduct
      },
      {
        name: 'orders',
        path: '/orders',
        component: OrdersList
      },
      {
        name: 'manage-orders',
        path: '/manage-orders/:id?',
        component: ManageOrder
      },
      {
        name: 'brands',
        path: '/brands',
        component: BrandsList
      },
      {
        name: 'countries',
        path: '/countries',
        component: CountriesList
      },
      {
        name: 'colors',
        path: '/colors',
        component: ColorsList
      },
      {
        name: 'seasons',
        path: '/seasons',
        component: SeasonsList
      },
      {
        name: 'types',
        path: '/types',
        component: TypesList
      },
      {
        name: 'sizes',
        path: '/sizes',
        component: SizesList
      },
      {
        name: 'delivery',
        path: '/delivery',
        component: DeliveryList
      },
      {
        name: 'pay-types',
        path: '/pay-types',
        component: PayTypesList
      },
      {
        name: 'order-statuses',
        path: '/order-statuses',
        component: OrderStatusesList
      },
      {
        name: 'static-pages',
        path: '/static-pages',
        component: StaticPages
      },
      {
        name: 'static-pages-edit',
        path: '/static-pages/:id',
        component: EditStaticPage
      },
      {
        name: 'slider',
        path: '/slider',
        component: Slider
      },
      {
        name: 'slider-edit',
        path: '/slider-edit/:id?',
        component: EditSlider
      },
    ]
  })
}
