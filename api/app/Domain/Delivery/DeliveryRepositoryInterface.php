<?php

declare(strict_types=1);

namespace App\Domain\Delivery;

use App\Contract\Core\DatabaseRepositoryInterface;

/**
 * Interface DeliveryRepositoryInterface
 * @package App\Domain\Delivery
 */
interface DeliveryRepositoryInterface extends DatabaseRepositoryInterface
{

}