<?php

declare(strict_types=1);

namespace App\Http\Resourses\Country;

use App\Domain\Country\Country;
use App\Http\Resourses\BaseCollectionResource;

/**
 * Class CountriesListResource
 * @package App\Http\Resourses\Country
 */
class CountriesListResource extends BaseCollectionResource
{
    /**
     * @param Country $item
     *
     * @return array
     */
    protected function getItemData($item): array
    {
        return [
            'id' => $item->id,
            'type' => 'country',
            'attributes' => [
                'title' => $item->title,
                'slug' => $item->slug,
                'icon' => $item->icon,
                'createdAt' => $item->created_at ? $item->created_at->format('Y-m-d H:i:s') : '',
                'updatedAt' => $item->updated_at ? $item->updated_at->format('Y-m-d H:i:s') : '',
            ]
        ];
    }
}