<?php

declare(strict_types=1);

namespace App\Application\Cart\GetCartList;

use App\Contract\Core\CommandInterface;
use App\Domain\Cart\CartFilter;

/**
 * Class GetCartList
 * @package App\Application\Cart\GetCartListByUser
 */
class GetCartList implements CommandInterface
{
    /** @var CartFilter $filter */
    private CartFilter $filter;

    /**
     * GetCartListByUser constructor
     *
     * @param CartFilter $filter
     */
    public function __construct(CartFilter $filter)
    {
        $this->filter = $filter;
    }

    /**
     * @return CartFilter
     */
    public function getFilter(): CartFilter
    {
        return $this->filter;
    }
}