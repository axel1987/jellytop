<?php

declare(strict_types=1);

namespace App\Domain\Order;

use App\Contract\Core\FilterInterface;
use Illuminate\Http\Request;

/**
 * Class OrderFilter
 * @package App\Domain\Order
 */
class OrderFilter implements FilterInterface
{
    /** @var int|null $id */
    private ?int $id = null;

    /** @var string|null $search */
    private ?string $search = null;

    /** @var int|null $statusId */
    private ?int $statusId = null;

    /** @var int|null $deliveryId */
    private ?int $deliveryId = null;

    /** @var int|null $payTypeId */
    private ?int $payTypeId = null;

    /**
     * @param Request $request
     *
     * @return FilterInterface|OrderFilter
     */
    public static function fromRequest(Request $request): FilterInterface
    {
        $filter = new self();
        $filter->setSearch($request->get('search'));
        $filter->setDeliveryId((int)$request->get('deliveryId'));
        $filter->setPayTypeId((int)$request->get('payTypeId'));
        $filter->setStatusId((int)$request->get('statusId'));

        return $filter;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string|null
     */
    public function getSearch(): ?string
    {
        return $this->search;
    }

    /**
     * @param string|null $search
     */
    public function setSearch(?string $search): void
    {
        $this->search = $search;
    }

    /**
     * @return int|null
     */
    public function getStatusId(): ?int
    {
        return $this->statusId;
    }

    /**
     * @param int|null $statusId
     */
    public function setStatusId(?int $statusId): void
    {
        $this->statusId = $statusId;
    }

    /**
     * @return int|null
     */
    public function getDeliveryId(): ?int
    {
        return $this->deliveryId;
    }

    /**
     * @param int|null $deliveryId
     */
    public function setDeliveryId(?int $deliveryId): void
    {
        $this->deliveryId = $deliveryId;
    }

    /**
     * @return int|null
     */
    public function getPayTypeId(): ?int
    {
        return $this->payTypeId;
    }

    /**
     * @param int|null $payTypeId
     */
    public function setPayTypeId(?int $payTypeId): void
    {
        $this->payTypeId = $payTypeId;
    }
}