import PayType from "./PayType";
import Delivery from "./Delivery";
import OrderStatus from "./OrderStatus";
import OrderItem from "./OrderItem";

export default class Order {
    /**
     * @param data
     */
    constructor(data = {}) {
        this.id = data.id ? data.id : null;

        this.name = data.attributes && data.attributes.name ?
            data.attributes.name :
            null;
        this.address = data.attributes && data.attributes.address ?
            data.attributes.address :
            null;
        this.phone = data.attributes && data.attributes.phone ?
            data.attributes.phone :
            null;
        this.sum = data.attributes && data.attributes.sum ?
            data.attributes.sum :
            null;
        this.note = data.attributes && data.attributes.note ?
            data.attributes.note :
            null;

        this.payType = data.relationships && data.relationships.payType
            ? new PayType(data.relationships.payType)
            : null
        this.delivery = data.relationships && data.relationships.delivery
            ? new Delivery(data.relationships.delivery)
            : null
        this.status = data.relationships && data.relationships.status
            ? new OrderStatus(data.relationships.status)
            : null

        this.items = data.relationships && data.relationships.items  && data.relationships.items.data
            ? data.relationships.items.data.map(item => {
                return new OrderItem(item)
            })
            : []

        this.createdAt = data.attributes && data.attributes.createdAt ?
            data.attributes.createdAt :
            null;
        this.updatedAt = data.attributes && data.attributes.updatedAt ?
            data.attributes.updatedAt :
            null;
    }
}