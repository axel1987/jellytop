<?php

declare(strict_types=1);

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

/**
 * Class GendersTableSeeder
 */
class SliderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now();

        DB::table('slider')->insert([
            [
                'text' => json_encode([
                    'ua' => 'Акция №1',
                    'ru' => 'Акцiя №1'
                ]),
                'link' => 'https://cdn.vuetifyjs.com/images/carousel/planet.jpg',
                'created_at' => $now,
                'updated_at' => $now,
            ],
            [
                'text' => json_encode([
                    'ua' => 'Акция №2',
                    'ru' => 'Акцiя №2'
                ]),
                'link' => 'https://cdn.vuetifyjs.com/images/carousel/bird.jpg',
                'created_at' => $now,
                'updated_at' => $now,
            ],
        ]);
    }
}