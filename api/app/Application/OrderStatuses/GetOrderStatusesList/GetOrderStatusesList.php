<?php

declare(strict_types=1);

namespace App\Application\OrderStatuses\GetOrderStatusesList;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\PaginationInterface;
use App\Contract\Core\SortingInterface;
use App\Domain\OrderStatus\OrderStatusFilter;

/**
 * Class GetOrderStatusesList
 * @package App\Application\OrderStatuses\GetOrderStatusesList
 */
class GetOrderStatusesList implements CommandInterface
{
    /** @var OrderStatusFilter $filter */
    private OrderStatusFilter $filter;

    /** @var PaginationInterface|null $pagination */
    private ?PaginationInterface $pagination;

    /** @var SortingInterface|null $sorting */
    private ?SortingInterface $sorting;

    /**
     * GetOrderStatusesList constructor
     *
     * @param OrderStatusFilter $filter
     * @param PaginationInterface|null $pagination
     * @param SortingInterface|null $sorting
     */
    public function __construct(
        OrderStatusFilter $filter,
        PaginationInterface $pagination = null,
        SortingInterface $sorting = null
    ) {
        $this->filter = $filter;
        $this->pagination = $pagination;
        $this->sorting = $sorting;
    }

    /**
     * @return OrderStatusFilter
     */
    public function getFilter(): OrderStatusFilter
    {
        return $this->filter;
    }

    /**
     * @return PaginationInterface|null
     */
    public function getPagination(): ?PaginationInterface
    {
        return $this->pagination;
    }

    /**
     * @return SortingInterface|null
     */
    public function getSorting(): ?SortingInterface
    {
        return $this->sorting;
    }
}