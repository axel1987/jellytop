<?php

declare(strict_types=1);

namespace App\Application\Cart\StoreCartItem;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\Cart\Cart;
use App\Domain\Cart\CartRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class StoreCartItemHandler
 * @package App\Application\Cart\StoreCartItem
 */
class StoreCartItemHandler implements HandlerInterface
{
    /** @var CartRepositoryInterface $cartRepository */
    private CartRepositoryInterface $cartRepository;

    /**
     * StoreCartItemHandler constructor
     *
     * @param CartRepositoryInterface $cartRepository
     */
    public function __construct(CartRepositoryInterface $cartRepository)
    {
        $this->cartRepository = $cartRepository;
    }

    /**
     * @param CommandInterface|StoreCartItem $command
     *
     * @return Model|Cart
     */
    public function handle(CommandInterface $command): Model
    {
        $cart = new Cart();

        $cart->session_id = $command->getSessionId();
        $cart->count = $command->getCount();
        $cart->storage_until = Carbon::now()->addMonth();

        $cart->product()->associate($command->getProduct());
        $cart->size()->associate($command->getSize());

        $this->cartRepository->store($cart);

        return $cart;
    }
}