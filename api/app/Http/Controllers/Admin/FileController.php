<?php

declare(strict_types=1);

namespace App\Http\Controllers\Admin;

use App\Application\File\RemoveFile\RemoveFile;
use App\Application\File\UploadFile\UploadFile;
use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\File\RemoveFileRequest;
use App\Http\Requests\Dashboard\File\UploadFileRequest;
use App\Http\Resourses\File\FileResource;
use Illuminate\Http\JsonResponse;

/**
 * Class FileController
 * @package App\Http\Controllers
 */
class FileController extends Controller
{
    /**
     * @param UploadFileRequest $request
     *
     * @return JsonResponse
     */
    public function upload(UploadFileRequest $request): JsonResponse
    {
        $url = $this->execute(new UploadFile(
            $request->file('file'),
            $request->get('group'),
            $request->get('id')
        ));

        return new JsonResponse(new FileResource($url));
    }

    /**
     * @param RemoveFileRequest $request
     *
     * @return JsonResponse
     */
    public function remove(RemoveFileRequest $request): JsonResponse
    {
        $this->execute(new RemoveFile(
            $request->get('url'),
            $request->get('group'),
            $request->get('id'),
        ));

        return new JsonResponse(null, 204);
    }
}