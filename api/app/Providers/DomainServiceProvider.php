<?php

declare(strict_types=1);

namespace App\Providers;

use App\Domain\Brand\BrandRepositoryInterface;
use App\Domain\Cart\CartRepositoryInterface;
use App\Domain\Color\ColorRepositoryInterface;
use App\Domain\Country\CountryRepositoryInterface;
use App\Domain\Delivery\DeliveryRepositoryInterface;
use App\Domain\Gender\GenderRepositoryInterface;
use App\Domain\Order\OrderRepositoryInterface;
use App\Domain\OrderItem\OrderItemRepositoryInterface;
use App\Domain\OrderStatus\OrderStatusRepositoryInterface;
use App\Domain\PayType\PayTypeRepositoryInterface;
use App\Domain\Product\ProductRepositoryInterface;
use App\Domain\ProductImage\ProductImageRepositoryInterface;
use App\Domain\Season\SeasonRepositoryInterface;
use App\Domain\Size\SizeRepositoryInterface;
use App\Domain\Slider\SliderRepositoryInterface;
use App\Domain\StaticPage\StaticPageRepositoryInterface;
use App\Domain\Type\TypeRepositoryInterface;
use App\Domain\User\UserRepositoryInterface;
use App\Infrastructure\DatabaseRepository\BrandRepository;
use App\Infrastructure\DatabaseRepository\CartRepository;
use App\Infrastructure\DatabaseRepository\ColorRepository;
use App\Infrastructure\DatabaseRepository\CountryRepository;
use App\Infrastructure\DatabaseRepository\DeliveryRepository;
use App\Infrastructure\DatabaseRepository\GenderRepository;
use App\Infrastructure\DatabaseRepository\OrderItemRepository;
use App\Infrastructure\DatabaseRepository\OrderRepository;
use App\Infrastructure\DatabaseRepository\OrderStatusRepository;
use App\Infrastructure\DatabaseRepository\PayTypeRepository;
use App\Infrastructure\DatabaseRepository\ProductImageRepository;
use App\Infrastructure\DatabaseRepository\ProductRepository;
use App\Infrastructure\DatabaseRepository\SeasonRepository;
use App\Infrastructure\DatabaseRepository\SizeRepository;
use App\Infrastructure\DatabaseRepository\SliderRepository;
use App\Infrastructure\DatabaseRepository\StaticPageRepository;
use App\Infrastructure\DatabaseRepository\TypeRepository;
use App\Infrastructure\DatabaseRepository\UserRepository;
use Illuminate\Support\ServiceProvider;

/**
 * Class DomainServiceProvider
 * @package App\Providers
 */
class DomainServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->singleton(
            UserRepositoryInterface::class,
            UserRepository::class
        );
        $this->app->singleton(
            BrandRepositoryInterface::class,
            BrandRepository::class
        );
        $this->app->singleton(
            ColorRepositoryInterface::class,
            ColorRepository::class
        );
        $this->app->singleton(
            CountryRepositoryInterface::class,
            CountryRepository::class
        );
        $this->app->singleton(
            GenderRepositoryInterface::class,
            GenderRepository::class
        );
        $this->app->singleton(
            SeasonRepositoryInterface::class,
            SeasonRepository::class
        );
        $this->app->singleton(
            SizeRepositoryInterface::class,
            SizeRepository::class
        );
        $this->app->singleton(
            TypeRepositoryInterface::class,
            TypeRepository::class
        );
        $this->app->singleton(
            DeliveryRepositoryInterface::class,
            DeliveryRepository::class
        );
        $this->app->singleton(
            PayTypeRepositoryInterface::class,
            PayTypeRepository::class
        );
        $this->app->singleton(
            ProductRepositoryInterface::class,
            ProductRepository::class
        );
        $this->app->singleton(
            ProductImageRepositoryInterface::class,
            ProductImageRepository::class
        );
        $this->app->singleton(
            CartRepositoryInterface::class,
            CartRepository::class
        );
        $this->app->singleton(
            OrderRepositoryInterface::class,
            OrderRepository::class
        );
        $this->app->singleton(
            OrderItemRepositoryInterface::class,
            OrderItemRepository::class
        );
        $this->app->singleton(
            OrderStatusRepositoryInterface::class,
            OrderStatusRepository::class
        );
        $this->app->singleton(
            SliderRepositoryInterface::class,
            SliderRepository::class
        );
        $this->app->singleton(
            StaticPageRepositoryInterface::class,
            StaticPageRepository::class
        );
    }
}
