<?php

declare(strict_types=1);

namespace App\Domain\OrderItem;

use App\Contract\Core\DatabaseRepositoryInterface;

/**
 * Interface OrderItemRepositoryInterface
 * @package App\Domain\OrderItem
 */
interface OrderItemRepositoryInterface extends DatabaseRepositoryInterface
{

}