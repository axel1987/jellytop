<?php

declare(strict_types=1);

namespace App\Domain\Product;

use App\Contract\Core\FilterInterface;
use Illuminate\Http\Request;

/**
 * Class ProductFilter
 * @package App\Domain\Product
 */
class ProductFilter implements FilterInterface
{
    /** @var int|null $id */
    private ?int $id = null;

    /** @var string|null $slug */
    private ?string $slug = null;

    /** @var array|null $brandIds */
    private ?array $brandIds = null;

    /** @var array|null $typeIds */
    private ?array $typeIds = null;

    /** @var array|null $colorIds */
    private ?array $colorIds = null;

    /** @var array|null $sizeIds */
    private ?array $sizeIds = null;

    /** @var array|null $seasonIds */
    private ?array $seasonIds = null;

    /** @var array|null $genderIds */
    private ?array $genderIds = null;

    /** @var array|null $brandsSlug */
    private ?array $brandsSlug = null;

    /** @var array|null $typesSlug */
    private ?array $typesSlug = null;

    /** @var array|null $seasonsSlug */
    private ?array $seasonsSlug = null;

    /** @var array|null $sizesSlug */
    private ?array $sizesSlug = null;

    /** @var array|null $colorsSlug */
    private ?array $colorsSlug = null;

    /** @var array|null $gendersSlug */
    private ?array $gendersSlug = null;

    /** @var int|null $minPrice */
    private ?int $minPrice = null;

    /** @var int|null $maxPrice */
    private ?int $maxPrice = null;

    /** @var bool|null $inStock */
    private ?bool $inStock = null;

    /**
     * @param Request $request
     *
     * @return FilterInterface|ProductFilter
     */
    public static function fromRequest(Request $request): FilterInterface
    {
        $filter = new self();

        $filter->setBrandIds($request->get('brandId'));
        $filter->setTypeIds($request->get('typeId'));
        $filter->setColorIds($request->get('colorId'));
        $filter->setSizeIds($request->get('sizeId'));
        $filter->setSeasonIds($request->get('seasonId'));
        $filter->setGenderIds($request->get('genderId'));

        $filter->setBrandsSlug($request->get('brands'));
        $filter->setTypesSlug($request->get('types'));
        $filter->setSeasonsSlug($request->get('seasons'));
        $filter->setSizesSlug($request->get('sizes'));
        $filter->setColorsSlug($request->get('colors'));
        $filter->setGendersSlug($request->get('genders'));

        $filter->setMinPrice((int)$request->get('minPrice'));
        $filter->setMaxPrice((int)$request->get('maxPrice'));
        $filter->setInStock(!is_null($request->get('inStock')) ? (bool)$request->get('inStock') : null);

        return $filter;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string|null
     */
    public function getSlug(): ?string
    {
        return $this->slug;
    }

    /**
     * @param string|null $slug
     */
    public function setSlug(?string $slug): void
    {
        $this->slug = $slug;
    }

    /**
     * @return array|null
     */
    public function getBrandIds(): ?array
    {
        return $this->brandIds;
    }

    /**
     * @param array|null $brandIds
     */
    public function setBrandIds(?array $brandIds): void
    {
        $this->brandIds = $brandIds;
    }

    /**
     * @return array|null
     */
    public function getTypeIds(): ?array
    {
        return $this->typeIds;
    }

    /**
     * @param array|null $typeIds
     */
    public function setTypeIds(?array $typeIds): void
    {
        $this->typeIds = $typeIds;
    }

    /**
     * @return array|null
     */
    public function getColorIds(): ?array
    {
        return $this->colorIds;
    }

    /**
     * @param array|null $colorIds
     */
    public function setColorIds(?array $colorIds): void
    {
        $this->colorIds = $colorIds;
    }

    /**
     * @return array|null
     */
    public function getSizeIds(): ?array
    {
        return $this->sizeIds;
    }

    /**
     * @param array|null $sizeIds
     */
    public function setSizeIds(?array $sizeIds): void
    {
        $this->sizeIds = $sizeIds;
    }

    /**
     * @return array|null
     */
    public function getSeasonIds(): ?array
    {
        return $this->seasonIds;
    }

    /**
     * @param array|null $seasonIds
     */
    public function setSeasonIds(?array $seasonIds): void
    {
        $this->seasonIds = $seasonIds;
    }

    /**
     * @return array|null
     */
    public function getGenderIds(): ?array
    {
        return $this->genderIds;
    }

    /**
     * @param array|null $genderIds
     */
    public function setGenderIds(?array $genderIds): void
    {
        $this->genderIds = $genderIds;
    }

    /**
     * @return array|null
     */
    public function getBrandsSlug(): ?array
    {
        return $this->brandsSlug;
    }

    /**
     * @param array|null $brandsSlug
     */
    public function setBrandsSlug(?array $brandsSlug): void
    {
        $this->brandsSlug = $brandsSlug;
    }

    /**
     * @return array|null
     */
    public function getTypesSlug(): ?array
    {
        return $this->typesSlug;
    }

    /**
     * @param array|null $typesSlug
     */
    public function setTypesSlug(?array $typesSlug): void
    {
        $this->typesSlug = $typesSlug;
    }

    /**
     * @return array|null
     */
    public function getSeasonsSlug(): ?array
    {
        return $this->seasonsSlug;
    }

    /**
     * @param array|null $seasonsSlug
     */
    public function setSeasonsSlug(?array $seasonsSlug): void
    {
        $this->seasonsSlug = $seasonsSlug;
    }

    /**
     * @return array|null
     */
    public function getSizesSlug(): ?array
    {
        return $this->sizesSlug;
    }

    /**
     * @param array|null $sizesSlug
     */
    public function setSizesSlug(?array $sizesSlug): void
    {
        $this->sizesSlug = $sizesSlug;
    }

    /**
     * @return array|null
     */
    public function getColorsSlug(): ?array
    {
        return $this->colorsSlug;
    }

    /**
     * @param array|null $colorsSlug
     */
    public function setColorsSlug(?array $colorsSlug): void
    {
        $this->colorsSlug = $colorsSlug;
    }

    /**
     * @return array|null
     */
    public function getGendersSlug(): ?array
    {
        return $this->gendersSlug;
    }

    /**
     * @param array|null $gendersSlug
     */
    public function setGendersSlug(?array $gendersSlug): void
    {
        $this->gendersSlug = $gendersSlug;
    }

    /**
     * @return int|null
     */
    public function getMinPrice(): ?int
    {
        return $this->minPrice;
    }

    /**
     * @param int|null $minPrice
     */
    public function setMinPrice(?int $minPrice): void
    {
        $this->minPrice = $minPrice;
    }

    /**
     * @return int|null
     */
    public function getMaxPrice(): ?int
    {
        return $this->maxPrice;
    }

    /**
     * @param int|null $maxPrice
     */
    public function setMaxPrice(?int $maxPrice): void
    {
        $this->maxPrice = $maxPrice;
    }

    /**
     * @return bool|null
     */
    public function getInStock(): ?bool
    {
        return $this->inStock;
    }

    /**
     * @param bool|null $inStock
     */
    public function setInStock(?bool $inStock): void
    {
        $this->inStock = $inStock;
    }
}