<?php

declare(strict_types=1);

namespace App\Application\Size\StoreSize;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\Size\Size;
use App\Domain\Size\SizeRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * Class StoreSizeHandler
 * @package App\Application\Size\StoreSize
 */
class StoreSizeHandler implements HandlerInterface
{
    /** @var SizeRepositoryInterface $sizeRepository */
    private SizeRepositoryInterface $sizeRepository;

    /**
     * StoreSizeHandler constructor
     *
     * @param SizeRepositoryInterface $sizeRepository
     */
    public function __construct(SizeRepositoryInterface $sizeRepository)
    {
        $this->sizeRepository = $sizeRepository;
    }

    /**
     * @param CommandInterface|StoreSize $command
     *
     * @return Size
     */
    public function handle(CommandInterface $command): Model
    {
        $size = new Size();

        $size->size = $command->getSize();

        return $this->sizeRepository->store($size);
    }
}