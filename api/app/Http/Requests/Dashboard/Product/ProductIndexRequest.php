<?php

declare(strict_types=1);

namespace App\Http\Requests\Dashboard\Product;

use App\Http\Requests\FormRequest;

/**
 * Class ProductIndexRequest
 * @package App\Http\Requests\Dashboard\Product
 */
class ProductIndexRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'page' => 'nullable|string',
            'perPage' => 'nullable|string',
            'field' => 'nullable|string',
            'direction' => 'nullable|string',

            'minPrice' => 'nullable|integer',
            'maxPrice' => 'nullable|integer',
            'brands' => 'nullable|array',
            'types' => 'nullable|array',
            'seasons' => 'nullable|array',
            'sizes' => 'nullable|array',
            'colors' => 'nullable|array',
            'genders' => 'nullable|array',
        ];
    }
}