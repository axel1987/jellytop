<?php

declare(strict_types=1);

namespace App\Http\Requests\Dashboard\StatisPage;

use App\Http\Requests\FormRequest;

/**
 * Class StaticPageIndexRequest
 * @package App\Http\Requests\Dashboard\StatisPage
 */
class StaticPageIndexRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'page' => 'nullable|string',
            'perPage' => 'nullable|string',
            'field' => 'nullable|string',
            'direction' => 'nullable|string',
        ];
    }
}