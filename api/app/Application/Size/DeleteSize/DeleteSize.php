<?php

declare(strict_types=1);

namespace App\Application\Size\DeleteSize;

use App\Contract\Core\CommandInterface;
use App\Domain\Size\Size;

/**
 * Class DeleteSize
 * @package App\Application\Size\DeleteSize
 */
class DeleteSize implements CommandInterface
{
    /** @var Size $size */
    private Size $size;

    /**
     * DeleteSize constructor
     *
     * @param Size $size
     */
    public function __construct(Size $size)
    {
        $this->size = $size;
    }

    /**
     * @return Size
     */
    public function getSize(): Size
    {
        return $this->size;
    }
}