<?php

declare(strict_types=1);

namespace App\Infrastructure\DatabaseRepository;

use App\Contract\Core\DatabaseRepositoryInterface;
use App\Contract\Core\FilterInterface;
use App\Contract\Core\SortingInterface;
use App\Domain\ProductImage\ProductImage;
use App\Domain\ProductImage\ProductImageFilter;
use App\Domain\ProductImage\ProductImageRepositoryInterface;

/**
 * Class ProductImageImageRepository
 * @package App\Infrastructure\DatabaseRepository
 */
class ProductImageRepository extends DatabaseRepository implements ProductImageRepositoryInterface
{
    /**
     * ProductImageRepository constructor
     */
    public function __construct()
    {
        $this->model = new ProductImage();
    }

    /**
     * @param FilterInterface|ProductImageFilter $filter
     *
     * @return $this
     */
    protected function filter(FilterInterface $filter): DatabaseRepositoryInterface
    {
        $this->builder = $this->builder ?? $this->model->newQuery();

        return $this;
    }

    /**
     * @param SortingInterface $sorting
     *
     * @return $this
     */
    protected function sorting(SortingInterface $sorting): DatabaseRepositoryInterface
    {
        return $this;
    }
}