<?php

declare(strict_types=1);

namespace App\Domain\Product;

use App\Domain\Brand\Brand;
use App\Domain\Color\Color;
use App\Domain\Delivery\Delivery;
use App\Domain\Gender\Gender;
use App\Domain\PayType\PayType;
use App\Domain\ProductImage\ProductImage;
use App\Domain\Season\Season;
use App\Domain\Size\Size;
use App\Domain\Type\Type;
use Barryvdh\LaravelIdeHelper\Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;

/**
 * Class Product
 *
 * @package App\Domain\Product
 * @property int $id
 * @property mixed $title
 * @property mixed $description
 * @property mixed $meta_key
 * @property mixed $meta_description
 * @property float $purchase_price
 * @property float $sale_price
 * @property string $slug
 * @property int $in_stock
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Collection|Brand[] $brands
 * @property-read int|null $brands_count
 * @property-read Collection|Color[] $colors
 * @property-read int|null $colors_count
 * @property-read Collection|ProductImage[] $images
 * @property-read int|null $images_count
 * @property-read Collection|Color[] $seasons
 * @property-read int|null $seasons_count
 * @property-read Collection|Color[] $sizes
 * @property-read int|null $sizes_count
 * @property-read Collection|Type[] $types
 * @property-read int|null $types_count
 * @property-read Collection|Delivery[] $delivery
 * @property-read int|null $delivery_count
 * @property-read Collection|PayType[] $payTypes
 * @property-read int|null $pay_types_count
 * @property-read Collection|Gender[] $genders
 * @property-read int|null $genders_count
 * @method static Builder|Product newModelQuery()
 * @method static Builder|Product newQuery()
 * @method static Builder|Product query()
 * @method static Builder|Product whereCreatedAt($value)
 * @method static Builder|Product whereDescription($value)
 * @method static Builder|Product whereId($value)
 * @method static Builder|Product whereInStock($value)
 * @method static Builder|Product whereMetaDescription($value)
 * @method static Builder|Product whereMetaKey($value)
 * @method static Builder|Product wherePurchasePrice($value)
 * @method static Builder|Product whereSalePrice($value)
 * @method static Builder|Product whereTitle($value)
 * @method static Builder|Product whereSlug($value)
 * @method static Builder|Product whereUpdatedAt($value)
 * @mixin Eloquent
 * @property int|null $priority
 * @method static Builder|Product wherePriority($value)
 */
class Product extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
        'meta_key',
        'meta_description',
        'purchase_price',
        'sale_price',
        'in_stock'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * @var string[]
     */
    protected $casts = [
        'title' => 'array',
        'description' => 'array',
        'meta_key' => 'array',
        'meta_description' => 'array',
        'purchase_price' => 'float',
        'sale_price' => 'float',
        'in_stock' => 'boolean',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];

    /**
     * @return HasMany
     */
    public function images(): HasMany
    {
        return $this->hasMany(ProductImage::class);
    }

    /**
     * @return BelongsToMany
     */
    public function brands(): BelongsToMany
    {
        return $this->belongsToMany(
            Brand::class,
            'product_brand',
            'product_id',
            'brand_id',
            'id',
            'id'
        );
    }

    /**
     * @return BelongsToMany
     */
    public function types(): BelongsToMany
    {
        return $this->belongsToMany(
            Type::class,
            'product_types',
            'product_id',
            'type_id',
            'id',
            'id'
        );
    }

    /**
     * @return BelongsToMany
     */
    public function colors(): BelongsToMany
    {
        return $this->belongsToMany(
            Color::class,
            'product_colors',
            'product_id',
            'color_id',
            'id',
            'id'
        );
    }

    /**
     * @return BelongsToMany
     */
    public function sizes(): BelongsToMany
    {
        return $this->belongsToMany(
            Size::class,
            'product_sizes',
            'product_id',
            'size_id',
            'id',
            'id'
        );
    }

    /**
     * @return BelongsToMany
     */
    public function seasons(): BelongsToMany
    {
        return $this->belongsToMany(
            Season::class,
            'product_seasons',
            'product_id',
            'season_id',
            'id',
            'id'
        );
    }

    /**
     * @return BelongsToMany
     */
    public function delivery(): BelongsToMany
    {
        return $this->belongsToMany(
            Delivery::class,
            'product_delivery',
            'product_id',
            'delivery_id',
            'id',
            'id'
        );
    }

    /**
     * @return BelongsToMany
     */
    public function payTypes(): BelongsToMany
    {
        return $this->belongsToMany(
            PayType::class,
            'product_pay_types',
            'product_id',
            'pay_type_id',
            'id',
            'id'
        );
    }

    /**
     * @return BelongsToMany
     */
    public function genders(): BelongsToMany
    {
        return $this->belongsToMany(
            Gender::class,
            'product_genders',
            'product_id',
            'gender_id',
            'id',
            'id'
        );
    }

    /**
     * @return array
     */
    public function makeMetaKey($force = false): array
    {
        $meta = [];
        if (empty($this->meta_key['ru']) || $force) {
            $meta['ru'] = sprintf('%s купить в Украине дешево доставка', $this->title['ru']);
        } else {
            $meta['ru'] = $this->meta_key['ru'];
        }

        if (empty($this->meta_key['ua']) || $force) {
            $meta['ua'] = sprintf(
                '%s придбати в Украині дешево доставка',
                !empty($this->title['ua']) ? $this->title['ua'] : $this->title['ru']
            );
        } else {
            $meta['ua'] = $this->meta_key['ua'];
        }

        return $meta;
    }

    /**
     * @return array
     */
    public function makeMetaDescription($force = false): array
    {
        $meta = [];
        if (empty($this->meta_description['ru']) || $force) {
            $meta['ru'] =
                sprintf('Купить %s в магазине %s по лучшей цене %sгрн. ✈️Отправка в день заказа',
                    $this->title['ru'],
                    'DjeliTop',
                    $this->sale_price
                );
        } else {
            $meta['ru'] = $this->meta_description['ru'];
        }

        if (empty($this->meta_description['ua']) || $force) {
            $meta['ua'] =
                sprintf('Придбати %s умагазині %s за найкращою ціною %sгрн. ✈️Відправка в день замовлення',
                    !empty($this->title['ua']) ? $this->title['ua'] : $this->title['ru'],
                    'DjeliTop',
                    $this->sale_price
                );
        } else {
            $meta['ua'] = $this->meta_description['ua'];
        }

        return $meta;
    }
}
