<?php

declare(strict_types=1);

namespace App\Application\Color\GetColorsList;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\Color\ColorRepositoryInterface;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

/**
 * Class GetColorsListHandler
 * @package App\Application\Color\GetColorsList
 */
class GetColorsListHandler implements HandlerInterface
{
    /** @var ColorRepositoryInterface $colorRepository */
    private ColorRepositoryInterface $colorRepository;

    /**
     * GetColorsListHandler constructor
     *
     * @param ColorRepositoryInterface $colorRepository
     */
    public function __construct(ColorRepositoryInterface $colorRepository)
    {
        $this->colorRepository = $colorRepository;
    }

    /**
     * @param CommandInterface|GetColorsList $command
     *
     * @return LengthAwarePaginator|Collection
     */
    public function handle(CommandInterface $command)
    {
        return $this->colorRepository->all(
            $command->getFilter(),
            $command->getPagination(),
            $command->getSorting()
        );
    }
}