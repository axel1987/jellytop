<?php

declare(strict_types=1);

namespace App\Domain\Season;

use Barryvdh\LaravelIdeHelper\Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class Season
 *
 * @package App\Domain\Season
 * @property int $id
 * @property array $title
 * @property string|null $icon
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|Season newModelQuery()
 * @method static Builder|Season newQuery()
 * @method static Builder|Season query()
 * @method static Builder|Season whereCreatedAt($value)
 * @method static Builder|Season whereIcon($value)
 * @method static Builder|Season whereId($value)
 * @method static Builder|Season whereTitle($value)
 * @method static Builder|Season whereUpdatedAt($value)
 * @mixin Eloquent
 * @property string $slug
 * @method static Builder|Season whereSlug($value)
 */
class Season extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'icon',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * @var string[]
     */
    protected $casts = [
        'title' => 'array',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];
}