<?php

declare(strict_types=1);

namespace App\Application\Cart\DeleteCartItem;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\Cart\CartRepositoryInterface;

/**
 * Class DeleteCartItemHandler
 * @package App\Application\Cart\DeleteCartItem
 */
class DeleteCartItemHandler implements HandlerInterface
{
    /** @var CartRepositoryInterface $cartRepository */
    private CartRepositoryInterface $cartRepository;

    /**
     * DeleteCartItemHandler constructor
     *
     * @param CartRepositoryInterface $cartRepository
     */
    public function __construct(CartRepositoryInterface $cartRepository)
    {
        $this->cartRepository = $cartRepository;
    }

    /**
     * @param CommandInterface|DeleteCartItem $command
     *
     * @return void
     */
    public function handle(CommandInterface $command): void
    {
        $this->cartRepository->delete(
            $command->getCartItem()
        );
    }
}