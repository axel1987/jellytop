<?php

declare(strict_types=1);

namespace App\Application\Product\UpdateProductPriority;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\Product\ProductRepositoryInterface;

/**
 * Class UpdateProductPriorityHandler
 * @package App\Application\Product\UpdateProductPriority
 */
class UpdateProductPriorityHandler implements HandlerInterface
{
    /** @var ProductRepositoryInterface $productRepository */
    private ProductRepositoryInterface $productRepository;

    /**
     * UpdateProductPriorityHandler constructor
     *
     * @param ProductRepositoryInterface $productRepository
     */
    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    /**
     * @param CommandInterface|UpdateProductPriority $command
     *
     * @return mixed|void
     */
    public function handle(CommandInterface $command)
    {
        $product = $command->getProduct();

        $product->priority = $command->getPriority();

        $this->productRepository->store($product);

        return $product;
    }
}