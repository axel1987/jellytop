<?php

declare(strict_types=1);

namespace App\Application\Season\DeleteSeason;

use App\Contract\Core\CommandInterface;
use App\Domain\Season\Season;

/**
 * Class DeleteSeason
 * @package App\Application\Season\DeleteSeason
 */
class DeleteSeason implements CommandInterface
{
    /** @var Season $season */
    private Season $season;

    /**
     * DeleteSeason constructor
     *
     * @param Season $season
     */
    public function __construct(Season $season)
    {
        $this->season = $season;
    }

    /**
     * @return Season
     */
    public function getSeason(): Season
    {
        return $this->season;
    }
}