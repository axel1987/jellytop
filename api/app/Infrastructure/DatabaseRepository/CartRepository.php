<?php

declare(strict_types=1);

namespace App\Infrastructure\DatabaseRepository;

use App\Contract\Core\DatabaseRepositoryInterface;
use App\Contract\Core\FilterInterface;
use App\Contract\Core\SortingInterface;
use App\Domain\Cart\Cart;
use App\Domain\Cart\CartFilter;
use App\Domain\Cart\CartRepositoryInterface;

/**
 * Class CartRepository
 * @package App\Infrastructure\DatabaseRepository
 */
class CartRepository extends DatabaseRepository implements CartRepositoryInterface
{
    /**
     * CartRepository constructor
     */
    public function __construct()
    {
        $this->model = new Cart();
    }

    /**
     * @param FilterInterface|CartFilter $filter
     * @return DatabaseRepositoryInterface
     */
    public function filter(FilterInterface $filter): DatabaseRepositoryInterface
    {
        $this->builder = $this->builder ?? $this->model->newQuery();

        if ($filter->getId()) {
            $this->builder->where('id', '=', $filter->getId());
        }

        if ($filter->getIds()) {
            $this->builder->whereIn('id', $filter->getIds());
        }

        if ($filter->getSessionId()) {
            $this->builder->where('session_id', '=', $filter->getSessionId());
        }

        return $this;
    }

    /**
     * @param SortingInterface $sorting
     * @return DatabaseRepository
     */
    public function sorting(SortingInterface $sorting): DatabaseRepository
    {
        $this->builder = $this->builder ?? $this->model->newQuery();

        $this->builder->orderBy($sorting->getField(), $sorting->getDirection());

        return $this;
    }
}
