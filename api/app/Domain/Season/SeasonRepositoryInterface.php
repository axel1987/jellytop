<?php

declare(strict_types=1);

namespace App\Domain\Season;

use App\Contract\Core\DatabaseRepositoryInterface;

/**
 * Interface SeasonRepositoryInterface
 * @package App\Domain\Season
 */
interface SeasonRepositoryInterface extends DatabaseRepositoryInterface
{

}