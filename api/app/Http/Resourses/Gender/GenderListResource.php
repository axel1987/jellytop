<?php

declare(strict_types=1);

namespace App\Http\Resourses\Gender;

use App\Domain\Gender\Gender;
use App\Http\Resourses\BaseCollectionResource;

/**
 * Class GenderListResource
 * @package App\Http\Resourses\Size
 */
class GenderListResource extends BaseCollectionResource
{
    /**
     * @param Gender $item
     *
     * @return array
     */
    protected function getItemData($item): array
    {
        return [
            'id' => $item->id,
            'type' => 'gender',
            'attributes' => [
                'title' => $item->title,
                'slug' => $item->slug,
                'createdAt' => $item->created_at ? $item->created_at->format('Y-m-d H:i:s') : '',
                'updatedAt' => $item->updated_at ? $item->updated_at->format('Y-m-d H:i:s') : '',
            ]
        ];
    }
}