<?php

declare(strict_types=1);

namespace App\Application\Type\GetTypesList;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\Type\TypeRepositoryInterface;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

/**
 * Class GetTypesListHandler
 * @package App\Application\Type\GetTypesList
 */
class GetTypesListHandler implements HandlerInterface
{
    /** @var TypeRepositoryInterface $typeRepository */
    private TypeRepositoryInterface $typeRepository;

    /**
     * GetTypesListHandler constructor
     *
     * @param TypeRepositoryInterface $typeRepository
     */
    public function __construct(TypeRepositoryInterface $typeRepository)
    {
        $this->typeRepository = $typeRepository;
    }

    /**
     * @param CommandInterface|GetTypesList $command
     *
     * @return LengthAwarePaginator|Collection
     */
    public function handle(CommandInterface $command): mixed
    {
        return $this->typeRepository->all(
            $command->getFilter(),
            $command->getPagination(),
            $command->getSorting()
        );
    }
}