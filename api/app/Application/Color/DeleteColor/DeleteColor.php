<?php

declare(strict_types=1);

namespace App\Application\Color\DeleteColor;

use App\Contract\Core\CommandInterface;
use App\Domain\Color\Color;

/**
 * Class DeleteColor
 * @package App\Application\Color\DeleteColor
 */
class DeleteColor implements CommandInterface
{
    /** @var Color $color */
    private Color $color;

    /**
     * DeleteColor constructor
     *
     * @param Color $color
     */
    public function __construct(Color $color)
    {
        $this->color = $color;
    }

    /**
     * @return Color
     */
    public function getColor(): Color
    {
        return $this->color;
    }
}