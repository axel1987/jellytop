<?php

declare(strict_types=1);

namespace App\Http\Controllers\Admin;

use App\Application\Country\DeleteCountry\DeleteCountry;
use App\Application\Country\GetCountriesList\GetCountriesList;
use App\Application\Country\GetCountryByFilter\GetCountryByFilter;
use App\Application\Country\StoreCountry\StoreCountry;
use App\Application\Country\UpdateCountry\UpdateCountry;
use App\Domain\Country\CountryFilter;
use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Country\CountryIndexRequest;
use App\Http\Requests\Dashboard\Country\CountryRequest;
use App\Http\Resourses\Country\CountriesListResource;
use App\Http\Resourses\Country\CountryResource;
use App\Infrastructure\Core\Pagination;
use App\Infrastructure\Core\Sorting;
use Illuminate\Http\JsonResponse;

/**
 * Class CountryController
 * @package App\Http\Controllers
 */
class CountryController extends Controller
{
    /**
     * @param CountryIndexRequest $request
     *
     * @return JsonResponse
     */
    public function index(CountryIndexRequest $request): JsonResponse
    {
        $filter = CountryFilter::fromRequest($request);
        $pagination = Pagination::fromRequest($request);
        $sorting = Sorting::fromRequest($request);

        $list = $this->execute(new GetCountriesList(
            $filter,
            $pagination,
            $sorting
        ));

        return new JsonResponse(new CountriesListResource($list));
    }

    /**
     * @param CountryIndexRequest $request
     *
     * @return JsonResponse
     */
    public function list(): JsonResponse
    {
        $list = $this->execute(new GetCountriesList(
            new CountryFilter(),
            null,
            new Sorting()
        ));

        return new JsonResponse(new CountriesListResource($list));
    }

    /**
     * @param int $id
     *
     * @return JsonResponse
     */
    public function view(int $id): JsonResponse
    {
        $filter = new CountryFilter();
        $filter->setId($id);
        $country = $this->execute(new GetCountryByFilter($filter));

        return new JsonResponse(new CountryResource($country));
    }

    /**
     * @param CountryRequest $request
     * @param int $id
     *
     * @return JsonResponse
     */
    public function update(CountryRequest $request, int $id): JsonResponse
    {
        $filter = new CountryFilter();
        $filter->setId($id);
        $country = $this->execute(new GetCountryByFilter($filter));
        $country = $this->execute(new UpdateCountry(
            $country,
            $request->get('title'),
            $request->get('slug'),
            $request->get('icon')
        ));

        return new JsonResponse(new CountryResource($country));
    }


    /**
     * @param CountryRequest $request
     *
     * @return JsonResponse
     */
    public function store(CountryRequest $request): JsonResponse
    {
        $country = $this->execute(new StoreCountry(
            $request->get('title'),
            $request->get('slug'),
            $request->get('icon')
        ));

        return new JsonResponse(new CountryResource($country));
    }

    /**
     * @param int $id
     *
     * @return JsonResponse
     */
    public function delete(int $id): JsonResponse
    {
        $filter = new CountryFilter();
        $filter->setId($id);
        $country = $this->execute(new GetCountryByFilter($filter));
        $this->execute(new DeleteCountry($country));

        return new JsonResponse(null, 204);
    }
}