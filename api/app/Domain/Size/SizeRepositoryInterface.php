<?php

declare(strict_types=1);

namespace App\Domain\Size;

use App\Contract\Core\DatabaseRepositoryInterface;

/**
 * Interface SizeRepositoryInterface
 * @package App\Domain\Size
 */
interface SizeRepositoryInterface extends DatabaseRepositoryInterface
{

}