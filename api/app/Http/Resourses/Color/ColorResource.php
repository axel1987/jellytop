<?php

declare(strict_types=1);

namespace App\Http\Resourses\Color;

use App\Domain\Color\Color;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class ColorResource
 * @package App\Http\Resourses\Color
 */
class ColorResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        /** @var Color $this */
        return [
            'id' => $this->id,
            'type' => 'color',
            'attributes' => [
                'title' => $this->title,
                'slug' => $this->slug,
                'hex' => $this->hex,
                'createdAt' => $this->created_at ? $this->created_at->format('Y-m-d H:i:s') : '',
                'updatedAt' => $this->updated_at ? $this->updated_at->format('Y-m-d H:i:s') : '',
            ]
        ];
    }
}