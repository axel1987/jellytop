<?php

declare(strict_types=1);

namespace App\Application\Type\StoreType;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\Type\Type;
use App\Domain\Type\TypeRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * Class StoreTypeHandler
 * @package App\Application\Type\StoreType
 */
class StoreTypeHandler implements HandlerInterface
{
    /** @var TypeRepositoryInterface $typeRepository */
    private TypeRepositoryInterface $typeRepository;

    /**
     * StoreTypeHandler constructor
     *
     * @param TypeRepositoryInterface $typeRepository
     */
    public function __construct(TypeRepositoryInterface $typeRepository)
    {
        $this->typeRepository = $typeRepository;
    }

    /**
     * @param CommandInterface|StoreType $command
     *
     * @return Type
     */
    public function handle(CommandInterface $command): Model
    {
        $type = new Type();

        $type->title = $command->getTitle();
        $type->slug = $command->getSlug();
        $type->description = $command->getDescription();
        $type->icon = $command->getIcon();

        return $this->typeRepository->store($type);
    }
}