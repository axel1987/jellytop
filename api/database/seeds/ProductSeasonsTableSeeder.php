<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * Class ProductSeasonsTableSeeder
 */
class ProductSeasonsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product_seasons')->insert([
            [
                'product_id' => 1,
                'season_id' => 1,
            ],
        ]);
    }
}
