<?php

declare(strict_types=1);

namespace App\Infrastructure\DatabaseRepository;

use App\Contract\Core\DatabaseRepositoryInterface;
use App\Contract\Core\FilterInterface;
use App\Contract\Core\SortingInterface;
use App\Domain\Product\Product;
use App\Domain\Product\ProductFilter;
use App\Domain\Product\ProductRepositoryInterface;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class ProductRepository
 * @package App\Infrastructure\DatabaseRepository
 */
class ProductRepository extends DatabaseRepository implements ProductRepositoryInterface
{
    /**
     * ProductRepository constructor
     */
    public function __construct()
    {
        $this->model = new Product();
    }

    /**
     * @param FilterInterface|ProductFilter $filter
     *
     * @return $this
     */
    protected function filter(FilterInterface $filter): DatabaseRepositoryInterface
    {
        $this->builder = $this->builder ?? $this->model->newQuery();

        if (is_bool($filter->getInStock())) {
            $this->builder->where('in_stock', '=', $filter->getInStock());
        }

        if ($filter->getId()) {
            $this->builder->where('id', '=', $filter->getId());
        }

        if ($filter->getSlug()) {
            $this->builder->where('slug', '=', $filter->getSlug());
        }

        if ($filter->getMinPrice() && !$filter->getMaxPrice()) {
            $this->builder->where('sale_price', '>=', $filter->getMinPrice());
        }

        if ($filter->getMaxPrice() && !$filter->getMinPrice()) {
            $this->builder->where('sale_price', '<=', $filter->getMaxPrice());
        }

        if ($filter->getMaxPrice() && $filter->getMinPrice()) {
            $this->builder->whereBetween('sale_price', [$filter->getMinPrice(), $filter->getMaxPrice()]);
        }

        if ($filter->getBrandIds()) {
            $this->builder->whereHas("brands", function(Builder $q) use ($filter) {
                $q->whereIn('brands.id', $filter->getBrandIds());
            });
        }
        if ($filter->getBrandsSlug()) {
            $this->builder->whereHas("brands", function(Builder $q) use ($filter) {
                $q->whereIn('brands.slug', $filter->getBrandsSlug());
            });
        }

        if ($filter->getTypeIds()) {
            $this->builder->whereHas("types", function(Builder $q) use ($filter) {
                $q->whereIn('types.id', $filter->getTypeIds());
            });
        }
        if ($filter->getTypesSlug()) {
            $this->builder->whereHas("types", function(Builder $q) use ($filter) {
                $q->whereIn('types.slug', $filter->getTypesSlug());
            });
        }

        if ($filter->getColorIds()) {
            $this->builder->whereHas("colors", function(Builder $q) use ($filter) {
                $q->whereIn('colors.id', $filter->getColorIds());
            });
        }
        if ($filter->getColorsSlug()) {
            $this->builder->whereHas("colors", function(Builder $q) use ($filter) {
                $q->whereIn('colors.slug', $filter->getColorsSlug());
            });
        }

        if ($filter->getSizeIds()) {
            $this->builder->whereHas("sizes", function(Builder $q) use ($filter) {
                $q->whereIn('sizes.id', $filter->getSizeIds());
            });
        }
        if ($filter->getSizesSlug()) {
            $this->builder->whereHas("sizes", function(Builder $q) use ($filter) {
                $q->whereIn('sizes.size', $filter->getSizesSlug());
            });
        }

        if ($filter->getSeasonIds()) {
            $this->builder->whereHas("seasons", function(Builder $q) use ($filter) {
                $q->whereIn('seasons.id', $filter->getSeasonIds());
            });
        }
        if ($filter->getSeasonsSlug()) {
            $this->builder->whereHas("seasons", function(Builder $q) use ($filter) {
                $q->whereIn('seasons.slug', $filter->getSeasonsSlug());
            });
        }

        if ($filter->getGendersSlug()) {
            $this->builder->whereHas("genders", function(Builder $q) use ($filter) {
                $q->whereIn('genders.slug', $filter->getGendersSlug());
            });
        }

        return $this;
    }

    /**
     * @param SortingInterface $sorting
     *
     * @return $this
     */
    protected function sorting(SortingInterface $sorting): DatabaseRepositoryInterface
    {
        $this->builder = $this->builder ?? $this->model->newQuery();
        $this->builder->orderBy($sorting->getField(), $sorting->getDirection());

        return $this;
    }
}