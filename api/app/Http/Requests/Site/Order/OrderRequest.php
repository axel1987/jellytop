<?php

declare(strict_types=1);

namespace App\Http\Requests\Site\Order;

use App\Http\Requests\FormRequest;

/**
 * Class OrderRequest
 * @package App\Http\Requests\Site\Order
 */
class OrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cartItemsIds' => 'required|array',
            'cartItemsIds.*' => 'required|exists:cart,id',
            'delivery' => 'required|exists:delivery,id',
            'payType' => 'required|exists:pay_types,id',
            'address' => 'required_if:delivery,2,3|nullable|string',
            'department' => 'required_if:delivery,==,3|nullable|integer',
            'zip' => 'required_if:delivery,==,2|nullable|integer',
            'name' => 'required|string',
            'phone' => 'required|string',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'delivery.required' => 'Выберите способ доставки',
            'payType.required'  => 'Выберите способ оплаты',
            'address.required_if'  => 'Поле "Адрес" обязательно',
            'department.required'  => 'Поле "№ ОТделения" обязательно',
            'department.integer'  => 'Поле "№ ОТделения" должно быть числом',
            'zip.required'  => 'Поле "Индекс" обязательно',
            'zip.integer'  => 'Поле "Индекс" должно быть числом',
            'name.required'  => 'Поле "Имя получателя" обязательно',
            'phone.required'  => 'Поле "Телефон" обязательно',
        ];
    }
}