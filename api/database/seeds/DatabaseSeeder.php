<?php

use Illuminate\Database\Seeder;

/**
 * Class DatabaseSeeder
 */
class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(CountriesTableSeeder::class);
        $this->call(BrandsTableSeeder::class);
        $this->call(ColorsTableSeeder::class);
        $this->call(GendersTableSeeder::class);
        $this->call(SeasonsTableSeeder::class);
        $this->call(SizesTableSeeder::class);
        $this->call(TypesTableSeeder::class);
        $this->call(ProductBrandTableSeeder::class);
        $this->call(ProductColorTableSeeder::class);
        $this->call(ProductSeasonsTableSeeder::class);
        $this->call(ProductSizesTableSeeder::class);
        $this->call(ProductTypesTableSeeder::class);
        $this->call(DeliveryTableSeeder::class);
        $this->call(PayTypeTableSeeder::class);
        $this->call(ProductTableSeeder::class);
        $this->call(OrderStatusesTableSeeder::class);
        $this->call(StaticPagesTableSeeder::class);
        $this->call(SliderTableSeeder::class);
    }
}
