<?php

declare(strict_types=1);

namespace App\Domain\Type;

use Barryvdh\LaravelIdeHelper\Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class Type
 *
 * @package App\Domain\Type
 * @property int $id
 * @property array $title
 * @property array|null $description
 * @property mixed|null $icon
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|Type newModelQuery()
 * @method static Builder|Type newQuery()
 * @method static Builder|Type query()
 * @method static Builder|Type whereCreatedAt($value)
 * @method static Builder|Type whereDescription($value)
 * @method static Builder|Type whereIcon($value)
 * @method static Builder|Type whereId($value)
 * @method static Builder|Type whereTitle($value)
 * @method static Builder|Type whereUpdatedAt($value)
 * @mixin Eloquent
 * @property string $slug
 * @method static Builder|Type whereSlug($value)
 */
class Type extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
        'icon',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * @var string[]
     */
    protected $casts = [
        'title' => 'array',
        'description' => 'array',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];
}