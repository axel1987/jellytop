<?php

declare(strict_types=1);

namespace App\Application\Cart\DeleteCartItem;

use App\Contract\Core\CommandInterface;
use App\Domain\Cart\Cart;

/**
 * Class DeleteCartItem
 * @package App\Application\Cart\DeleteCartItem
 */
class DeleteCartItem implements CommandInterface
{
    /** @var Cart $cartItem */
    private Cart $cartItem;

    /**
     * DeleteCartItem constructor
     *
     * @param Cart $cartItem
     */
    public function __construct(Cart $cartItem)
    {
        $this->cartItem = $cartItem;
    }

    /**
     * @return Cart
     */
    public function getCartItem(): Cart
    {
        return $this->cartItem;
    }
}