export default class Color {
    /**
     * @param data
     */
    constructor(data = {}) {
        this.id = data.id ? data.id : null;
        this.title = data.attributes && data.attributes.title ?
            data.attributes.title :
            null;
        this.slug = data.attributes && data.attributes.slug ?
            data.attributes.slug :
            null;
        this.hex = data.attributes && data.attributes.hex
            ? data.attributes.hex
            : null;
        this.createdAt = data.attributes && data.attributes.createdAt ?
            data.attributes.createdAt :
            null;
        this.updatedAt = data.attributes && data.attributes.updatedAt ?
            data.attributes.updatedAt :
            null;
    }
}