<?php

declare(strict_types=1);

namespace App\Application\Product\GetProductByFilter;

use App\Contract\Core\CommandInterface;
use App\Domain\Product\ProductFilter;

/**
 * Class GetProductByFilter
 * @package App\Application\Product\GetProductByFilter
 */
class GetProductByFilter implements CommandInterface
{
    /** @var ProductFilter $productFilter */
    private ProductFilter $productFilter;

    /**
     * GetProductByFilter constructor
     *
     * @param ProductFilter $productFilter
     */
    public function __construct(ProductFilter $productFilter)
    {
        $this->productFilter = $productFilter;
    }

    /**
     * @return ProductFilter
     */
    public function getProductFilter(): ProductFilter
    {
        return $this->productFilter;
    }
}
