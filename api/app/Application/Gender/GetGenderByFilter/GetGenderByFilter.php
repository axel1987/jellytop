<?php

declare(strict_types=1);

namespace App\Application\Gender\GetGenderByFilter;

use App\Contract\Core\CommandInterface;
use App\Domain\Gender\GenderFilter;

/**
 * Class GetGenderByFilter
 * @package App\Application\Gender\GetGenderByFilter
 */
class GetGenderByFilter implements CommandInterface
{
    /** @var GenderFilter $filter */
    private GenderFilter $filter;

    /**
     * GetGenderByFilter constructor
     *
     * @param GenderFilter $filter
     */
    public function __construct(GenderFilter $filter)
    {
        $this->filter = $filter;
    }

    /**
     * @return GenderFilter
     */
    public function getFilter(): GenderFilter
    {
        return $this->filter;
    }
}