import OrderStatus from "../dto/entities/OrderStatus";
import Pagination from "../dto/entities/Pagination";

/**
 * @returns {{
 * pagination: null,
 * orderStatusList: *[],
 * orderStatus: null
 * }}
 */
export const state = () => ({
    orderStatus: null,
    orderStatusList: [],
    orderStatusSelectList: [],
    pagination: null
});

/**
 * @type {{
 * getOrderStatusList(*): [],
 * getOrderStatus(*): null,
 * getOrderStatusPagination(*): null
 * }}
 */
export const getters = {
    getOrderStatus(state) {
        return state.orderStatus;
    },
    getOrderStatusList(state) {
        return state.orderStatusList;
    },
    getOrderStatusSelectList(state) {
        return state.orderStatusSelectList;
    },
    getOrderStatusPagination(state) {
        return state.pagination;
    }
};

/**
 * @type {{
 * SET_ORDER_STATUSES_LIST(*, *): void, 
 * SET_ORDER_STATUS_PAGINATION(*, *): void, 
 * SET_ORDER_STATUS(*, *): void, 
 * SET_ORDER_STATUSES_SELECT_LIST(*, *): void
 * }}
 */
export const mutations = {
    SET_ORDER_STATUS(state, value) {
        state.orderStatus = value
    },
    SET_ORDER_STATUSES_LIST(state, value) {
        state.orderStatusList = value
    },
    SET_ORDER_STATUSES_SELECT_LIST(state, value) {
        state.orderStatusSelectList = value
    },
    SET_ORDER_STATUS_PAGINATION(state, value) {
        state.pagination = value
    },
};

/**
 * @type {{
 * deleteOrderStatus: ((function({commit: *}, *=): Promise<void>)|*),
 * updateOrderStatus: ((function({commit: *}, *=): Promise<void>)|*),
 * fetchOrderStatusList: ((function({commit: *}, *=): Promise<void>)|*),
 * registerOrderStatus: ((function({commit: *}, *=): Promise<void>)|*),
 * fetchOrderStatusSelectList: ((function({commit: *}, *=): Promise<void>)|*),
 * fetchOrderStatus: ((function({commit: *}, *=): Promise<void>)|*)
 * }}
 */
export const actions = {
    fetchOrderStatusList: async function ({commit}, params = {}) {
        let {data} = await this.$axios.get('/order-statuses', {params: params});

        const list = data.data.map((orderStatusData) => {
            return new OrderStatus(orderStatusData)
        });
        const pagination = data.pagination ? new Pagination(data.pagination) : null;

        commit('SET_ORDER_STATUSES_LIST', list)
        commit('SET_ORDER_STATUS_PAGINATION', pagination)
    },
    fetchOrderStatusSelectList: async function ({commit}, params = {}) {
        let {data} = await this.$axios.get('/order-statuses/list', {params: params});

        const list = data.data.map((orderStatusData) => {
            return new OrderStatus(orderStatusData)
        });

        commit('SET_ORDER_STATUSES_SELECT_LIST', list)
    },
    fetchOrderStatus: async function ({commit}, params = {}) {
        if (params.id) {
            let {data} = await this.$axios.get(`/order-statuses/${params.id}`, params);
            commit('SET_ORDER_STATUS', new OrderStatus(data))
        } else {
            commit('SET_ORDER_STATUS', new OrderStatus(null))
        }
    },
    registerOrderStatus: async function ({commit}, params = {}) {
        await this.$axios.post(`/order-statuses`, params).then((res) => {
            if (res) {
                commit('SET_ORDER_STATUS', new OrderStatus(res.data))
            }
        });
    },
    updateOrderStatus: async function ({commit}, params = {}) {
        await this.$axios.put(`/order-statuses/${params.id}`, params).then((res) => {
            if (res) {
                commit('SET_ORDER_STATUS', new OrderStatus(res.data))
            }
        });
    },
    deleteOrderStatus: async function ({commit}, params = {}) {
        await this.$axios.delete(`/order-statuses/${params.id}`, params);
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations,
};
