<?php

declare(strict_types=1);

namespace App\Http\Resourses\Season;

use App\Domain\Season\Season;
use App\Http\Resourses\BaseCollectionResource;

/**
 * Class SeasonListResource
 * @package App\Http\Resourses\Season
 */
class SeasonListResource extends BaseCollectionResource
{
    /**
     * @param Season $item
     *
     * @return array
     */
    protected function getItemData($item): array
    {
        return [
            'id' => $item->id,
            'type' => 'season',
            'attributes' => [
                'title' => $item->title,
                'slug' => $item->slug,
                'icon' => $item->icon,
                'createdAt' => $item->created_at ? $item->created_at->format('Y-m-d H:i:s') : '',
                'updatedAt' => $item->updated_at ? $item->updated_at->format('Y-m-d H:i:s') : '',
            ]
        ];
    }
}