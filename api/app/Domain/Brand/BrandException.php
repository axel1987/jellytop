<?php

declare(strict_types=1);

namespace App\Domain\Brand;

use Exception;

/**
 * Class BrandException
 * @package App\Domain\Brand
 */
class BrandException extends Exception
{
    /**
     * @param int $id
     * @throws BrandException
     */
    public static function notFound(int $id)
    {
        throw new BrandException(sprintf('Brand with id %d not found', $id));
    }
}
