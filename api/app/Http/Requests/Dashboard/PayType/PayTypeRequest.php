<?php

declare(strict_types=1);

namespace App\Http\Requests\Dashboard\PayType;

use App\Http\Requests\FormRequest;

/**
 * Class PayTypeRequest
 * @package App\Http\Requests\Dashboard\PayType
 */
class PayTypeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|array',
            'title.ua' => 'nullable|string',
            'title.ru' => 'nullable|string',
            'description' => 'required|array',
            'description.ua' => 'nullable|string',
            'description.ru' => 'nullable|string',
            'icon' => 'nullable|string',
        ];
    }
}