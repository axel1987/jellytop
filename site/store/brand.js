import Brand from "../dto/entities/Brand";
import Pagination from "../dto/entities/Pagination";

/**
 * @returns {{
 * pagination: null,
 * brandsList: *[],
 * brand: null
 * }}
 */
export const state = () => ({
    brand: null,
    brandsList: [],
    brandsSelectList: [],
    pagination: null
});

/**
 * @type {{
 * getBrandsList(*): [],
 * getBrand(*): null,
 * getBrandsPagination(*): null
 * }}
 */
export const getters = {
    getBrand(state) {
        return state.brand;
    },
    getBrandsList(state) {
        return state.brandsList;
    },
    getBrandsSelectList(state) {
        return state.brandsSelectList;
    },
    getBrandsPagination(state) {
        return state.pagination;
    }
};

/**
 * @type {{
 * SET_BRANDS_LIST(*, *): void,
 * SET_BRAND(*, *): void
 * }}
 */
export const mutations = {
    SET_BRAND(state, value) {
        state.brand = value
    },
    SET_BRANDS_LIST(state, value) {
        state.brandsList = value
    },
    SET_BRANDS_SELECT_LIST(state, value) {
        state.brandsSelectList = value
    },
    SET_BRANDS_PAGINATION(state, value) {
        state.pagination = value
    },
};

/**
 * @type {{
 * fetchBrandsList: ((function({commit: *}, *=): Promise<void>)|*),
 * fetchBrandsSelectList: ((function({commit: *}, *=): Promise<void>)|*),
 * fetchBrand: ((function({commit: *}, *=): Promise<void>)|*)
 * }}
 */
export const actions = {
    fetchBrandsList: async function ({commit}, params = {}) {
        let {data} = await this.$axios.get('/brands', {params: params});

        const list = data.data.map((brandData) => {
            return new Brand(brandData)
        });
        const pagination = data.pagination ? new Pagination(data.pagination) : null;

        commit('SET_BRANDS_LIST', list)
        commit('SET_BRANDS_PAGINATION', pagination)
    },
    fetchBrandsSelectList: async function ({commit}, params = {}) {
        let res = await this.$axios.get('/brands/list', {params: params});

        if (res && res.data) {
            const list = res.data.data.map((brandData) => {
                return new Brand(brandData)
            });

            commit('SET_BRANDS_SELECT_LIST', list)
        }

        return res;
    },
    fetchBrand: async function ({commit}, params = {}) {
        if (params.id) {
            let {data} = await this.$axios.get(`/brands/${params.id}`, params);
            commit('SET_BRAND', new Brand(data))
        } else {
            commit('SET_BRAND', new Brand(null))
        }
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations,
};
