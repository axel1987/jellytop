<?php

declare(strict_types=1);

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

/**
 * Class TypesTableSeader
 */
class TypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now();

        DB::table('types')->insert([
            [
                'title' => json_encode([
                    'ua' => 'Кросiвки',
                    'ru' => 'Кросовки'
                ]),
                'slug' => 'sneakers',
                'created_at' => $now,
                'updated_at' => $now,
            ],
            [
                'title' => json_encode([
                    'ua' => 'Тапочки',
                    'ru' => 'Тапочки'
                ]),
                'slug' => 'slippers',
                'created_at' => $now,
                'updated_at' => $now,
            ],
            [
                'title' => json_encode([
                    'ua' => 'Черевики',
                    'ru' => 'Ботинки'
                ]),
                'slug' => 'bots',
                'created_at' => $now,
                'updated_at' => $now,
            ],
        ]);
    }
}