<?php

declare(strict_types=1);

namespace App\Domain\PayType;

use App\Contract\Core\FilterInterface;
use Illuminate\Http\Request;

/**
 * Class PayTypeFilter
 * @package App\Domain\PayType
 */
class PayTypeFilter implements FilterInterface
{
    /** @var int|null $id */
    private ?int $id = null;


    /**
     * @param Request $request
     *
     * @return FilterInterface|PayTypeFilter
     */
    public static function fromRequest(Request $request): FilterInterface
    {
        $filter = new self();

        return $filter;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id): void
    {
        $this->id = $id;
    }
}