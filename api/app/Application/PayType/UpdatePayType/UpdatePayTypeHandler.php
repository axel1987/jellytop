<?php

declare(strict_types=1);

namespace App\Application\PayType\UpdatePayType;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\PayType\PayTypeRepositoryInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

/**
 * Class UpdatePayTypeHandler
 * @package App\Application\PayType\UpdatePayType
 */
class UpdatePayTypeHandler implements HandlerInterface
{
    /** @var PayTypeRepositoryInterface $payTypeRepository */
    private PayTypeRepositoryInterface $payTypeRepository;

    /**
     * UpdatePayTypeHandler constructor
     *
     * @param PayTypeRepositoryInterface $payTypeRepository
     */
    public function __construct(PayTypeRepositoryInterface $payTypeRepository)
    {
        $this->payTypeRepository = $payTypeRepository;
    }

    /**
     * @param UpdatePayType $command
     *
     * @return Model
     */
    public function handle(CommandInterface $command): Model
    {
        $oldIcon = null;
        $payType = $command->getPayType();

        if ($payType->icon != $command->getIcon()) {
            $oldIcon = $payType->icon;
        }

        $payType->title = $command->getTitle();
        $payType->description = $command->getDescription();
        $payType->icon = $command->getIcon();

        $this->payTypeRepository->store($payType);

        if ($oldIcon) {
            Storage::disk('public')
                ->delete(str_replace('/storage', '', $oldIcon));
        }

        return $payType;
    }
}