<?php

declare(strict_types=1);

namespace App\Http\Resourses\Slider;

use App\Domain\Slider\Slider;
use App\Http\Resourses\BaseCollectionResource;

/**
 * Class SliderListResource
 * @package App\Http\Resourses\Slider
 */
class SliderListResource extends BaseCollectionResource
{
    /**
     * @param Slider $item
     *
     * @return array
     */
    protected function getItemData($item): array
    {
        return [
            'id' => $item->id,
            'type' => 'slider',
            'attributes' => [
                'link' => $item->link,
                'text' => $item->text,
                'createdAt' => $item->created_at ? $item->created_at->format('Y-m-d H:i:s') : '',
                'updatedAt' => $item->updated_at ? $item->updated_at->format('Y-m-d H:i:s') : '',
            ]
        ];
    }
}