<?php

declare(strict_types=1);

namespace App\Application\PayType\DeletePayType;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\PayType\PayTypeRepositoryInterface;
use Illuminate\Support\Facades\Storage;

/**
 * Class DeletePayTypeHandler
 * @package App\Application\PayType\DeletePayType
 */
class DeletePayTypeHandler implements HandlerInterface
{
    /** @var PayTypeRepositoryInterface $payTypeRepository */
    private PayTypeRepositoryInterface $payTypeRepository;

    /**
     * DeletePayTypeHandler constructor
     *
     * @param PayTypeRepositoryInterface $payTypeRepository
     */
    public function __construct(PayTypeRepositoryInterface $payTypeRepository)
    {
        $this->payTypeRepository = $payTypeRepository;
    }

    /**
     * @param DeletePayType $command
     *
     * @return void
     */
    public function handle(CommandInterface $command)
    {
        $payType = $command->getPayType();

        if ($payType->icon) {
            Storage::disk('public')
                ->delete(str_replace('/storage', '', $payType->icon));
        }

        $this->payTypeRepository->delete($payType);
    }
}