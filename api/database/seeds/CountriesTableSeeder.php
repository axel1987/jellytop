<?php

declare(strict_types=1);

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

/**
 * Class CountriesTableSeeder
 */
class CountriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now();

        DB::table('countries')->insert([
            [
                'title' => json_encode([
                    'ua' => 'Украiна',
                    'ru' => 'Украина'
                ]),
                'slug' => 'ukraine',
                'created_at' => $now,
                'updated_at' => $now,
            ],
            [
                'title' => json_encode([
                    'ua' => 'Китай',
                    'ru' => 'Китай'
                ]),
                'slug' => 'china',
                'created_at' => $now,
                'updated_at' => $now,
            ]
        ]);
    }
}