<?php

declare(strict_types=1);

namespace App\Http\Resourses\OrderItem;

use App\Domain\OrderItem\OrderItem;
use App\Http\Resourses\BaseCollectionResource;
use App\Http\Resourses\Product\ProductNotRelationResource;
use App\Http\Resourses\Size\SizeResource;

/**
 * Class OrderItemListResource
 * @package App\Http\Resourses\OrderItem
 */
class OrderItemListResource extends BaseCollectionResource
{
    /**
     * @param OrderItem $item
     *
     * @return array
     */
    protected function getItemData($item): array
    {
        return [
            'id' => $item->id,
            'type' => 'order-item',
            'attributes' => [
                'count' => $item->count,
                'price' => $item->price,
                'createdAt' => $item->created_at ? $item->created_at->format('Y-m-d H:i:s') : '',
                'updatedAt' => $item->updated_at ? $item->updated_at->format('Y-m-d H:i:s') : '',
            ],
            'relationships' => [
                'product' => new ProductNotRelationResource($item->product),
                'size' => new SizeResource($item->size)
            ]
        ];
    }
}