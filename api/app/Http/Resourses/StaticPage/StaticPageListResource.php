<?php

declare(strict_types=1);

namespace App\Http\Resourses\StaticPage;

use App\Domain\StaticPage\StaticPage;
use App\Http\Resourses\BaseCollectionResource;

/**
 * Class StaticPageListResource
 * @package App\Http\Resourses\StaticPage
 */
class StaticPageListResource extends BaseCollectionResource
{
    /**
     * @param StaticPage $item
     *
     * @return array
     */
    protected function getItemData($item): array
    {
        return [
            'id' => $item->id,
            'type' => 'static-page',
            'attributes' => [
                'page' => $item->page,
                'pageName' => StaticPage::STATIC_PAGES_LIST[$item->page],
                'createdAt' => $item->created_at ? $item->created_at->format('Y-m-d H:i:s') : '',
                'updatedAt' => $item->updated_at ? $item->updated_at->format('Y-m-d H:i:s') : '',
            ]
        ];
    }
}