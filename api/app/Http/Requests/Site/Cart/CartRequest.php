<?php

declare(strict_types=1);

namespace App\Http\Requests\Site\Cart;

use App\Http\Requests\FormRequest;

/**
 * Class CartRequest
 * @package App\Http\Requests\Site\Cart
 */
class CartRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'productId' => 'required|numeric|exists:products,id',
            'sizeId' => 'required|numeric|exists:sizes,id',
            'count' => 'required|numeric',
        ];
    }
}