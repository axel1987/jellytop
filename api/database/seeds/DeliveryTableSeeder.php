<?php

declare(strict_types=1);

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

/**
 * Class GendersTableSeeder
 */
class DeliveryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now();

        DB::table('delivery')->insert([
            [
                'title' => json_encode([
                    'ua' => 'Самовивіз',
                    'ru' => 'Самовывоз'
                ]),
                'min_price' => 0,
                'created_at' => $now,
                'updated_at' => $now,
            ],
            [
                'title' => json_encode([
                    'ua' => 'Укр. Пошта',
                    'ru' => 'Укр. Почта'
                ]),
                'min_price' => 15,
                'created_at' => $now,
                'updated_at' => $now,
            ],
            [
                'title' => json_encode([
                    'ua' => 'Нова Пошта',
                    'ru' => 'Новая Почта'
                ]),
                'min_price' => 25,
                'created_at' => $now,
                'updated_at' => $now,
            ],
        ]);
    }
}