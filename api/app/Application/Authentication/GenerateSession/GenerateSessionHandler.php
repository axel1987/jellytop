<?php

declare(strict_types=1);

namespace App\Application\Authentication\GenerateSession;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Contract\Services\JwtServiceInterface;

/**
 * Class GenerateSessionHandler
 * @package App\Application\Authentication\GenerateSession
 */
class GenerateSessionHandler implements HandlerInterface
{
    /**
     * @param CommandInterface $command
     *
     * @return mixed|void
     */
    public function handle(CommandInterface $command)
    {
        $token = uniqid(). random_bytes(16);

        return [
            'tokenType' => 'session',
            'sessionToken' => $token,
        ];
    }
}