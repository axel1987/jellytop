<?php

declare(strict_types=1);

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

/**
 * Class GendersTableSeeder
 */
class GendersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now();

        DB::table('genders')->insert([
            [
                'title' => json_encode([
                    'ua' => 'Хлопчик',
                    'ru' => 'Мальчик'
                ]),
                'slug' => 'boy',
                'created_at' => $now,
                'updated_at' => $now,
            ],
            [
                'title' => json_encode([
                    'ua' => 'Дiвчинка',
                    'ru' => 'Девочка'
                ]),
                'slug' => 'girl',
                'created_at' => $now,
                'updated_at' => $now,
            ],
        ]);
    }
}