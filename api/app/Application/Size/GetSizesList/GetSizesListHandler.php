<?php

declare(strict_types=1);

namespace App\Application\Size\GetSizesList;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\Size\SizeRepositoryInterface;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

/**
 * Class GetSizesListHandler
 * @package App\Application\Size\GetSizesList
 */
class GetSizesListHandler implements HandlerInterface
{
    /** @var SizeRepositoryInterface $sizeRepository */
    private SizeRepositoryInterface $sizeRepository;

    /**
     * GetSizesListHandler constructor
     *
     * @param SizeRepositoryInterface $sizeRepository
     */
    public function __construct(SizeRepositoryInterface $sizeRepository)
    {
        $this->sizeRepository = $sizeRepository;
    }

    /**
     * @param CommandInterface|GetSizesList $command
     *
     * @return LengthAwarePaginator|Collection
     */
    public function handle(CommandInterface $command): mixed
    {
        return $this->sizeRepository->all(
            $command->getFilter(),
            $command->getPagination(),
            $command->getSorting()
        );
    }
}