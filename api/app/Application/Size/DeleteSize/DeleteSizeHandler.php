<?php

declare(strict_types=1);

namespace App\Application\Size\DeleteSize;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\Size\SizeRepositoryInterface;

/**
 * Class DeleteSizeHandler
 * @package App\Application\Size\DeleteSize
 */
class DeleteSizeHandler implements HandlerInterface
{
    /** @var SizeRepositoryInterface $sizeRepository */
    private SizeRepositoryInterface $sizeRepository;

    /**
     * DeleteSizeHandler constructor
     *
     * @param SizeRepositoryInterface $sizeRepository
     */
    public function __construct(SizeRepositoryInterface $sizeRepository)
    {
        $this->sizeRepository = $sizeRepository;
    }

    /**
     * @param CommandInterface|DeleteSize $command
     *
     * @return mixed|void
     */
    public function handle(CommandInterface $command): void
    {
        $this->sizeRepository->delete(
            $command->getSize()
        );
    }
}