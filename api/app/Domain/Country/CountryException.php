<?php

declare(strict_types=1);

namespace App\Domain\Country;

use Exception;

/**
 * Class CountryException
 * @package App\Domain\Country
 */
class CountryException extends Exception
{
    /**
     * @param int $id
     * @throws CountryException
     */
    public static function notFound(int $id)
    {
        throw new CountryException(sprintf('Color with id %d not found', $id));
    }
}