<?php

declare(strict_types=1);

namespace App\Application\Delivery\UpdateDelivery;

use App\Contract\Core\CommandInterface;
use App\Domain\Delivery\Delivery;

/**
 * Class UpdateDelivery
 * @package App\Application\Delivery\UpdateDelivery
 */
class UpdateDelivery implements CommandInterface
{
    /** @var Delivery $delivery */
    private Delivery $delivery;

    /** @var array $title */
    private array $title;

    /** @var array $description */
    private array $description;

    /** @var string|null $icon */
    private ?string $icon;

    /** @var float|null $icon */
    private ?float $minPrice;

    /**
     * UpdateDelivery constructor
     *
     * @param Delivery $delivery
     * @param array $title
     * @param array $description
     * @param string|null $icon
     * @param float|null $minPrice
     */
    public function __construct(
        Delivery $delivery,
        array $title,
        array $description,
        ?string $icon = null,
        ?float $minPrice = null
    ) {
        $this->delivery = $delivery;
        $this->title = $title;
        $this->description = $description;
        $this->icon = $icon;
        $this->minPrice = $minPrice;
    }


    /**
     * @return Delivery
     */
    public function getDelivery(): Delivery
    {
        return $this->delivery;
    }

    /**
     * @return array
     */
    public function getTitle(): array
    {
        return $this->title;
    }

    /**
     * @return array
     */
    public function getDescription(): array
    {
        return $this->description;
    }

    /**
     * @return string|null
     */
    public function getIcon(): ?string
    {
        return $this->icon;
    }

    /**
     * @return float|null
     */
    public function getMinPrice(): ?float
    {
        return $this->minPrice;
    }
}