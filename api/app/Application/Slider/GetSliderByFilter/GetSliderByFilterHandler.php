<?php

declare(strict_types=1);

namespace App\Application\Slider\GetSliderByFilter;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\Slider\Slider;
use App\Domain\Slider\SliderRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * Class GetSliderByFilterHandler
 * @package App\Application\Slider\GetSliderByFilter
 */
class GetSliderByFilterHandler implements HandlerInterface
{
    /** @var SliderRepositoryInterface $sliderRepository */
    private SliderRepositoryInterface $sliderRepository;

    /**
     * GetSliderByFilterHandler constructor
     *
     * @param SliderRepositoryInterface $sliderRepository
     */
    public function __construct(SliderRepositoryInterface $sliderRepository)
    {
        $this->sliderRepository = $sliderRepository;
    }

    /**
     * @param CommandInterface|GetSliderByFilter $command
     *
     * @return Slider|null
     */
    public function handle(CommandInterface $command): ?Model
    {
        return $this->sliderRepository->one(
            $command->getFilter()
        );
    }
}