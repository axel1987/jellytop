<?php

declare(strict_types=1);

namespace App\Application\Color\GetColorByFilter;

use App\Contract\Core\CommandInterface;
use App\Domain\Color\ColorFilter;

/**
 * Class GetColorByFilter
 * @package App\Application\Color\GetColorByFilter
 */
class GetColorByFilter implements CommandInterface
{
    /** @var ColorFilter $filter */
    private ColorFilter $filter;

    /**
     * GetColorByFilter constructor
     *
     * @param ColorFilter $filter
     */
    public function __construct(ColorFilter $filter)
    {
        $this->filter = $filter;
    }

    /**
     * @return ColorFilter
     */
    public function getFilter(): ColorFilter
    {
        return $this->filter;
    }
}