<?php

declare(strict_types=1);

namespace App\Application\Brand\DeleteBrand;

use App\Contract\Core\CommandInterface;
use App\Domain\Brand\Brand;

/**
 * Class DeleteBrand
 * @package App\Application\Brand\DeleteBrand
 */
class DeleteBrand implements CommandInterface
{
    /** @var Brand $brand */
    private Brand $brand;

    /**
     * DeleteBrand constructor
     *
     * @param Brand $brand
     */
    public function __construct(Brand $brand)
    {
        $this->brand = $brand;
    }

    /**
     * @return Brand
     */
    public function getBrand(): Brand
    {
        return $this->brand;
    }
}