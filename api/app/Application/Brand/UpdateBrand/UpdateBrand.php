<?php

declare(strict_types=1);

namespace App\Application\Brand\UpdateBrand;

use App\Contract\Core\CommandInterface;
use App\Domain\Brand\Brand;
use App\Domain\Country\Country;

/**
 * Class UpdateBrand
 * @package App\Application\Brand\UpdateBrand
 */
class UpdateBrand implements CommandInterface
{
    /** @var Brand $brand */
    private Brand $brand;

    /** @var Country $country */
    private Country $country;

    /** @var array $title */
    private array $title;

    /** @var array|null $description */
    private ?array $description;

    /** @var string|null $logoUrl */
    private ?string $logoUrl;

    /** @var string $slug */
    private  string $slug;

    /**
     * UpdateBrand constructor
     *
     * @param Brand $brand
     * @param Country $country
     * @param array $title
     * @param string $slug,
     * @param array|null $description
     * @param string|null $logoUrl
     */
    public function __construct(
        Brand $brand,
        Country $country,
        array $title,
        string $slug,
        ?array $description = null,
        ?string $logoUrl = null
    ) {
        $this->brand = $brand;
        $this->country = $country;
        $this->title = $title;
        $this->description = $description;
        $this->logoUrl = $logoUrl;
        $this->slug = $slug;
    }

    /**
     * @return Brand
     */
    public function getBrand(): Brand
    {
        return $this->brand;
    }

    /**
     * @return Country
     */
    public function getCountry(): Country
    {
        return $this->country;
    }

    /**
     * @return array
     */
    public function getTitle(): array
    {
        return $this->title;
    }

    /**
     * @return ?array
     */
    public function getDescription(): ?array
    {
        return $this->description;
    }

    /**
     * @return string|null
     */
    public function getLogoUrl(): ?string
    {
        return $this->logoUrl;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }
}