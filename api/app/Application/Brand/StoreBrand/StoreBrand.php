<?php

declare(strict_types=1);

namespace App\Application\Brand\StoreBrand;

use App\Contract\Core\CommandInterface;
use App\Domain\Country\Country;

/**
 * Class StoreBrand
 * @package App\Application\Brand\StoreBrand
 */
class StoreBrand implements CommandInterface
{
    /** @var Country $country */
    private Country $country;

    /** @var array $title */
    private array $title;

    /** @var array|null $description */
    private ?array $description;

    /** @var string|null $logoUrl */
    private ?string $logoUrl;

    /** @var string $slug */
    private string $slug;

    /**
     * StoreBrand constructor
     *
     * @param Country $country
     * @param array $title
     * @param string $slug
     * @param array|null $description
     * @param string|null $logoUrl
     */
    public function __construct(
        Country $country,
        array $title,
        string $slug,
        ?array $description = null,
        ?string $logoUrl = null,
    ) {
        $this->country = $country;
        $this->title = $title;
        $this->slug = $slug;
        $this->description = $description;
        $this->logoUrl = $logoUrl;
    }

    /**
     * @return Country
     */
    public function getCountry(): Country
    {
        return $this->country;
    }

    /**
     * @return array
     */
    public function getTitle(): array
    {
        return $this->title;
    }

    /**
     * @return array
     */
    public function getDescription(): array
    {
        return $this->description;
    }

    /**
     * @return string|null
     */
    public function getLogoUrl(): ?string
    {
        return $this->logoUrl;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }
}