export default class PayType {
    /**
     * @param data
     */
    constructor(data = {}) {
        this.id = data.id ? data.id : null;
        this.title = data.attributes && data.attributes.title ?
            data.attributes.title :
            null;
        this.description = data.attributes && data.attributes.description ?
            data.attributes.description :
            null;
        this.icon = data.attributes && data.attributes.icon
            ? data.attributes.icon
            : null;
        this.createdAt = data.attributes && data.attributes.createdAt ?
            data.attributes.createdAt :
            null;
        this.updatedAt = data.attributes && data.attributes.updatedAt ?
            data.attributes.updatedAt :
            null;
    }
}