<?php

declare(strict_types=1);

namespace App\Http\Controllers\Admin;

use App\Application\Product\CreateProduct\CreateProduct;
use App\Application\Product\DeleteProduct\DeleteProduct;
use App\Application\Product\GetProductByFilter\GetProductByFilter;
use App\Application\Product\GetProductsList\GetProductsList;
use App\Application\Product\SyncProductImages\SyncProductImages;
use App\Application\Product\UpdateProduct\UpdateProduct;
use App\Application\Product\UpdateProductPriority\UpdateProductPriority;
use App\Application\Product\UpdateProductStock\UpdateProductStock;
use App\Domain\Product\Product;
use App\Domain\Product\ProductFilter;
use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Product\ProductIndexRequest;
use App\Http\Requests\Dashboard\Product\ProductRequest;
use App\Http\Resourses\Product\ProductListResource;
use App\Http\Resourses\Product\ProductResource;
use App\Infrastructure\Core\Pagination;
use App\Infrastructure\Core\Sorting;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * Class ProductController
 * @package App\Http\Controllers
 */
class ProductController extends Controller
{
    /**
     * @param ProductIndexRequest $request
     *
     * @return JsonResponse
     */
    public function index(ProductIndexRequest $request): JsonResponse
    {
        $filter = ProductFilter::fromRequest($request);
        $pagination = Pagination::fromRequest($request);
        $sorting = new Sorting('priority', Sorting::DIRECTION_DESC);

        $list = $this->execute(new GetProductsList(
            $filter,
            $pagination,
            $sorting
        ));

        return new JsonResponse(new ProductListResource($list));
    }

    /**
     * @param ProductIndexRequest $request
     *
     * @return JsonResponse
     */
    public function list(ProductIndexRequest $request): JsonResponse
    {
        $filter = ProductFilter::fromRequest($request);
        $sorting = new Sorting('priority', Sorting::DIRECTION_DESC);

        $list = $this->execute(new GetProductsList(
            $filter,
            null,
            $sorting
        ));

        return new JsonResponse(new ProductListResource($list));
    }

    /**
     * @param int $id
     *
     * @return JsonResponse
     */
    public function view(int $id): JsonResponse
    {
        $filter = new ProductFilter();
        $filter->setId($id);

        $product = $this->execute(new GetProductByFilter($filter));

        return new JsonResponse(new ProductResource($product));
    }

    /**
     * @param ProductRequest $request
     *
     * @return JsonResponse
     */
    public function store(ProductRequest $request): JsonResponse
    {
        /** @var Product $product */
        $product = $this->execute(new CreateProduct(
            [$request->get('brandId')],
            [$request->get('typeId')],
            $request->get('colorsIds'),
            $request->get('seasonsIds'),
            $request->get('sizesIds'),
            $request->get('payTypesIds'),
            $request->get('deliveryIds'),
            $request->get('gendersIds'),
            $request->get('title'),
            $request->get('description'),
            $request->get('metaKey'),
            $request->get('metaDescription'),
            (float)$request->get('salePrice'),
            (float)$request->get('purchasePrice'),
            $request->get('slug'),
            (bool)$request->get('inStock', false),
            (int)$request->get('priority'),
        ));

        $this->execute(new SyncProductImages(
            $product,
            $request->get('images')
        ));

        $filter = new ProductFilter();
        $filter->setId($product->id);
        $product = $this->execute(new GetProductByFilter($filter));

        return new JsonResponse(new ProductResource($product));
    }

    /**
     * @param ProductRequest $request
     * @param int $id
     *
     * @return JsonResponse
     */
    public function update(ProductRequest $request, int $id): JsonResponse
    {
        $filter = new ProductFilter();
        $filter->setId($id);
        $product = $this->execute(new GetProductByFilter($filter));

        $product = $this->execute(new UpdateProduct(
            $product,
            [$request->get('brandId')],
            [$request->get('typeId')],
            $request->get('colorsIds'),
            $request->get('seasonsIds'),
            $request->get('sizesIds'),
            $request->get('payTypesIds'),
            $request->get('deliveryIds'),
            $request->get('gendersIds'),
            $request->get('title'),
            $request->get('description'),
            $request->get('metaKey'),
            $request->get('metaDescription'),
            (float)$request->get('salePrice'),
            (float)$request->get('purchasePrice'),
            $request->get('slug'),
            (bool)$request->get('inStock', false),
            (int)$request->get('priority'),
        ));

        $this->execute(new SyncProductImages(
            $product,
            $request->get('images')
        ));

        $filter = new ProductFilter();
        $filter->setId($id);
        $product = $this->execute(new GetProductByFilter($filter));

        return new JsonResponse(new ProductResource($product));
    }

    /**
     * @param Request $request
     * @param int $id
     *
     * @return JsonResponse
     */
    public function updatePriority(Request $request, int $id): JsonResponse
    {
        $filter = new ProductFilter();
        $filter->setId($id);
        $product = $this->execute(new GetProductByFilter($filter));
        $product = $this->execute(new UpdateProductPriority(
            $product,
            !is_null($request->get('priority')) ? (int)$request->get('priority') : null
        ));

        return new JsonResponse(new ProductResource($product));
    }

    /**
     * @param Request $request
     * @param int $id
     *
     * @return JsonResponse
     */
    public function updateStock(Request $request, int $id): JsonResponse
    {
        $filter = new ProductFilter();
        $filter->setId($id);
        $product = $this->execute(new GetProductByFilter($filter));
        $product = $this->execute(new UpdateProductStock(
            $product,
            (bool)$request->get('inStock', false)
        ));

        return new JsonResponse(new ProductResource($product));
    }

    /**
     * @param int $id
     *
     * @return JsonResponse
     */
    public function delete(int $id): JsonResponse
    {
        $filter = new ProductFilter();
        $filter->setId($id);
        $product = $this->execute(new GetProductByFilter($filter));
        $this->execute(new DeleteProduct($product));

        return new JsonResponse(null, 204);
    }
}