<?php

declare(strict_types=1);

namespace App\Application\Country\GetCountryByFilter;

use App\Contract\Core\CommandInterface;
use App\Domain\Country\CountryFilter;

/**
 * Class GetCountryByFilter
 * @package App\Application\Country\GetCountryByFilter
 */
class GetCountryByFilter implements CommandInterface
{
    /** @var CountryFilter $filter */
    private CountryFilter $filter;

    /**
     * GetCountryByFilter constructor
     *
     * @param CountryFilter $filter
     */
    public function __construct(CountryFilter $filter)
    {
        $this->filter = $filter;
    }

    /**
     * @return CountryFilter
     */
    public function getFilter(): CountryFilter
    {
        return $this->filter;
    }
}