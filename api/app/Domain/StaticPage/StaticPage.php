<?php

declare(strict_types=1);

namespace App\Domain\StaticPage;

use Barryvdh\LaravelIdeHelper\Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class StaticPage
 *
 * @package App\Domain\StaticPage
 * @property int $id
 * @property string $page
 * @property mixed $content
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|StaticPage newModelQuery()
 * @method static Builder|StaticPage newQuery()
 * @method static Builder|StaticPage query()
 * @method static Builder|StaticPage whereContent($value)
 * @method static Builder|StaticPage whereCreatedAt($value)
 * @method static Builder|StaticPage whereId($value)
 * @method static Builder|StaticPage wherePage($value)
 * @method static Builder|StaticPage whereUpdatedAt($value)
 * @mixin Eloquent
 */
class StaticPage extends Model
{
    /** @var string $table */
    protected $table = 'static_pages';

    const STATIC_PAGES_LIST = [
        'delivery' => 'Доставка',
        'payments' => 'Оплата',
        'contacts' => 'Контакты',
        'actions' => 'Акции',

    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'page',
        'content',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * @var string[]
     */
    protected $casts = [
        'content' => 'array',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];
}