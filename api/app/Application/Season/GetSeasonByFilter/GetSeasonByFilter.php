<?php

declare(strict_types=1);

namespace App\Application\Season\GetSeasonByFilter;

use App\Contract\Core\CommandInterface;
use App\Domain\Season\SeasonFilter;

/**
 * Class GetSeasonByFilter
 * @package App\Application\Season\GetSeasonByFilter
 */
class GetSeasonByFilter implements CommandInterface
{
    /** @var SeasonFilter $filter */
    private SeasonFilter $filter;

    /**
     * GetSeasonByFilter constructor
     *
     * @param SeasonFilter $filter
     */
    public function __construct(SeasonFilter $filter)
    {
        $this->filter = $filter;
    }

    /**
     * @return SeasonFilter
     */
    public function getFilter(): SeasonFilter
    {
        return $this->filter;
    }
}