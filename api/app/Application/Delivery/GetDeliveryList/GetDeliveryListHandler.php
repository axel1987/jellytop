<?php

declare(strict_types=1);

namespace App\Application\Delivery\GetDeliveryList;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\Delivery\DeliveryRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Class GetDeliveryListHandler
 * @package App\Application\Delivery\GetDeliveryList
 */
class GetDeliveryListHandler implements HandlerInterface
{
    /** @var DeliveryRepositoryInterface $deliveryRepository */
    private DeliveryRepositoryInterface $deliveryRepository;

    /**
     * GetDeliveryListHandler constructor
     *
     * @param DeliveryRepositoryInterface $deliveryRepository
     */
    public function __construct(DeliveryRepositoryInterface $deliveryRepository)
    {
        $this->deliveryRepository = $deliveryRepository;
    }

    /**
     * @param GetDeliveryList $command
     *
     * @return LengthAwarePaginator|Collection
     */
    public function handle(CommandInterface $command)
    {
        return $this->deliveryRepository->all(
            $command->getFilter(),
            $command->getPagination(),
            $command->getSorting()
        );
    }
}