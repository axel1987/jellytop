<?php

declare(strict_types=1);

namespace App\Infrastructure\DatabaseRepository;

use App\Contract\Core\DatabaseRepositoryInterface;
use App\Contract\Core\FilterInterface;
use App\Contract\Core\SortingInterface;
use App\Domain\Season\Season;
use App\Domain\Season\SeasonFilter;
use App\Domain\Season\SeasonRepositoryInterface;

/**
 * Class SeasonRepository
 * @package App\Infrastructure\DatabaseRepository
 */
class SeasonRepository extends DatabaseRepository implements SeasonRepositoryInterface
{
    /**
     * SeasonRepository constructor
     */
    public function __construct()
    {
        $this->model = new Season();
    }

    /**
     * @param FilterInterface|SeasonFilter $filter
     *
     * @return $this
     */
    protected function filter(FilterInterface $filter): DatabaseRepositoryInterface
    {
        $this->builder = $this->builder ?? $this->model->newQuery();

        if ($filter->getId()) {
            $this->builder->where('id', '=', $filter->getId());
        }

        if ($filter->getIds()) {
            $this->builder->whereIn('id', $filter->getIds());
        }

        return $this;
    }

    /**
     * @param SortingInterface $sorting
     *
     * @return $this
     */
    protected function sorting(SortingInterface $sorting): DatabaseRepositoryInterface
    {
        return $this;
    }
}