<?php

declare(strict_types=1);

namespace App\Domain\Type;

use App\Contract\Core\DatabaseRepositoryInterface;

/**
 * Interface TypeRepositoryInterface
 * @package App\Domain\Type
 */
interface TypeRepositoryInterface extends DatabaseRepositoryInterface
{

}