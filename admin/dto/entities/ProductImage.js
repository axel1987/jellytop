export default class ProductImage {
    /**
     * @param data
     */
    constructor(data = {}) {
        this.url = data.attributes && data.attributes.url ?
            data.attributes.url :
            null;
    }
}