<?php

declare(strict_types=1);

namespace App\Http\Controllers\Admin;

use App\Application\StaticPage\DeleteStatiPage\DeleteStaticPage;
use App\Application\StaticPage\GetStaticPageByFilter\GetStaticPageByFilter;
use App\Application\StaticPage\GetStaticPagesList\GetStaticPagesList;
use App\Application\StaticPage\StoreStaticPage\StoreStaticPage;
use App\Application\StaticPage\UpdateStaticPage\UpdateStaticPage;
use App\Domain\StaticPage\StaticPageFilter;
use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\StatisPage\StaticPageIndexRequest;
use App\Http\Requests\Dashboard\StatisPage\StaticPageRequest;
use App\Http\Resourses\StaticPage\StaticPageListResource;
use App\Http\Resourses\StaticPage\StaticPageResource;
use App\Infrastructure\Core\Pagination;
use App\Infrastructure\Core\Sorting;
use Illuminate\Http\JsonResponse;

/**
 * Class StaticPageController
 * @package App\Http\Controllers\Admin
 */
class StaticPageController extends Controller
{
    /**
     * @param StaticPageIndexRequest $request
     *
     * @return JsonResponse
     */
    public function index(StaticPageIndexRequest $request): JsonResponse
    {
        $filter = StaticPageFilter::fromRequest($request);
        $pagination = Pagination::fromRequest($request);
        $sorting = Sorting::fromRequest($request);

        $list = $this->execute(new GetStaticPagesList(
            $filter,
            $pagination,
            $sorting
        ));

        return new JsonResponse(new StaticPageListResource($list));
    }

    /**
     * @param int $id
     *
     * @return JsonResponse
     */
    public function view(int $id): JsonResponse
    {
        $filter = new StaticPageFilter();
        $filter->setId((int)$id);
        $staticPage = $this->execute(new GetStaticPageByFilter($filter));

        return new JsonResponse(new StaticPageResource($staticPage));
    }

    /**
     * @param StaticPageRequest $request
     *
     * @return JsonResponse
     */
    public function store(StaticPageRequest $request): JsonResponse
    {
        $staticPage = $this->execute(new StoreStaticPage(
            $request->get('pageName'),
            $request->get('content')
        ));

        return new JsonResponse(new StaticPageResource($staticPage));
    }

    /**
     * @param StaticPageRequest $request
     * @param int $id
     *
     * @return JsonResponse
     */
    public function update(StaticPageRequest $request, $id): JsonResponse
    {
        $filter = new StaticPageFilter();
        $filter->setId((int)$id);
        $staticPage = $this->execute(new GetStaticPageByFilter($filter));

        $staticPage = $this->execute(new UpdateStaticPage(
            $staticPage,
            $request->get('content')
        ));

        return new JsonResponse(new StaticPageResource($staticPage));
    }

    /**
     * @param int $id
     *
     * @return JsonResponse
     */
    public function delete(int $id): JsonResponse
    {
        $filter = new StaticPageFilter();
        $filter->setId((int)$id);
        $staticPage = $this->execute(new GetStaticPageByFilter($filter));

        $this->execute(new DeleteStaticPage($staticPage));

        return new JsonResponse(null, 204);
    }
}