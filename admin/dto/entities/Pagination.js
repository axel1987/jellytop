export default class Pagination {
    /**
     * @param data
     */
    constructor(data = {}) {
        this.currentPage = data.currentPage ? data.currentPage : null;
        this.lastPage = data.lastPage ? data.lastPage : null;
        this.perPage = data.perPage ? data.perPage : 20;
        this.total = data.total ? data.total : null;
    }
}