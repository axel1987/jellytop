<?php

declare(strict_types=1);

namespace App\Application\Slider\StoreSlider;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\Slider\Slider;
use App\Domain\Slider\SliderRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * Class StoreSliderHandler
 * @package App\Application\Slider\StoreSlider
 */
class StoreSliderHandler implements HandlerInterface
{
    /** @var SliderRepositoryInterface $sliderRepository */
    private SliderRepositoryInterface $sliderRepository;

    /**
     * StoreSliderHandler constructor
     *
     * @param SliderRepositoryInterface $sliderRepository
     */
    public function __construct(SliderRepositoryInterface $sliderRepository)
    {
        $this->sliderRepository = $sliderRepository;
    }

    /**
     * @param CommandInterface|StoreSlider $command
     *
     * @return Slider
     */
    public function handle(CommandInterface $command): Model
    {
        $slider = new Slider();
        $slider->link = $command->getLink();
        $slider->text = $command->getText();

        $this->sliderRepository->store($slider);

        return $slider;
    }
}