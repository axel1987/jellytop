<?php

declare(strict_types=1);

namespace App\Http\Requests\Dashboard\Order;

use App\Http\Requests\FormRequest;

/**
 * Class OrderIndexRequest
 * @package App\Http\Requests\Dashboard\Brand
 */
class OrderIndexRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'page' => 'nullable|string',
            'perPage' => 'nullable|string',
            'field' => 'nullable|string',
            'direction' => 'nullable|string',
            'search' => 'nullable|string',
            'statusId' => 'nullable|exists:order_statuses,id',
            'deliveryId' => 'nullable|exists:delivery,id',
            'payTypeId' => 'nullable|exists:pay_types,id',
        ];
    }
}