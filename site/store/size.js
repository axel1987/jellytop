import Size from "../dto/entities/Size";
import Pagination from "../dto/entities/Pagination";

/**
 * @returns {{
 * size: null,
 * sizesList: *[],
 * sizesSelectList: *[],
 * pagination: null,
 * }}
 */
export const state = () => ({
    size: null,
    sizesList: [],
    sizesSelectList: [],
    pagination: null
});

/**
 *
 * @type {{
 * getSize(*): null,
 * getSizesList(*): [],
 * getSizesSelectList(*): [],
 * getSizePagination(*): null
 * }}
 */
export const getters = {
    getSize(state) {
        return state.size;
    },
    getSizesList(state) {
        return state.sizesList;
    },
    getSizesSelectList(state) {
        return state.sizesSelectList;
    },
    getSizePagination(state) {
        return state.pagination;
    }
};

/**
 * @type {{
 * SET_SIZES_LIST(*, *): void,
 * SET_SIZES_SELECT_LIST(*, *): void,
 * SET_SIZE(*, *): void,
 * SET_SIZES_PAGINATION(*, *): void,
 * }}
 */
export const mutations = {
    SET_SIZE(state, value) {
        state.size = value
    },
    SET_SIZES_LIST(state, value) {
        state.sizesList = value
    },
    SET_SIZES_SELECT_LIST(state, value) {
        state.sizesSelectList = value
    },
    SET_SIZES_PAGINATION(state, value) {
        state.pagination = value
    },
};

/**
 * @type {{
 * registerSize: ((function({commit: *}, *=): Promise<void>)|*),
 * updateSize: ((function({commit: *}, *=): Promise<void>)|*),
 * fetchSize: ((function({commit: *}, *=): Promise<void>)|*),
 * fetchSizesList: ((function({commit: *}, *=): Promise<void>)|*),
 * fetchSizesSelectList: ((function({commit: *}, *=): Promise<void>)|*),
 * deleteSize: ((function({commit: *}, *=): Promise<void>)|*)
 * }}
 */
export const actions = {
    fetchSizesList: async function ({commit}, params = {}) {
        return await this.$axios.get('/sizes', {params: params}).then((res) => {
            if (res.data) {
                const list = res.data.data.map((sizeData) => {
                    return new Size(sizeData)
                });
                const pagination = res.data.pagination ? new Pagination(res.data.pagination) : null;

                commit('SET_SIZES_LIST', list);
                commit('SET_SIZES_PAGINATION', pagination);

                return res;
            }
        });
    },
    fetchSizesSelectList: async function ({commit}, params = {}) {
        return await this.$axios.get('/sizes/list', params).then((res) => {
            if (res && res.data) {
                const list = res.data.data.map((sizeData) => {
                    return new Size(sizeData)
                });

                commit('SET_SIZES_SELECT_LIST', list);
            }

            return res;
        });
    },
    fetchSize: async function ({commit}, params = {}) {
        if (params.id) {
            let {data} = await this.$axios.get(`/sizes/${params.id}`, params);
            commit('SET_SIZE', new Size(data))
        } else {
            commit('SET_SIZE', new Size(null))
        }
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations,
};
