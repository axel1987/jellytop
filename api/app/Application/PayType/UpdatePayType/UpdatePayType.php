<?php

declare(strict_types=1);

namespace App\Application\PayType\UpdatePayType;

use App\Contract\Core\CommandInterface;
use App\Domain\PayType\PayType;

/**
 * Class UpdatePayType
 * @package App\Application\PayType\UpdatePayType
 */
class UpdatePayType implements CommandInterface
{
    /** @var PayType $payType */
    private PayType $payType;

    /** @var array $title */
    private array $title;

    /** @var array $description */
    private array $description;

    /** @var string|null $icon */
    private ?string $icon;

    /**
     * UpdatePayType constructor
     *
     * @param PayType $payType
     * @param array $title
     * @param array $description
     * @param string|null $icon
     */
    public function __construct(
        PayType $payType,
        array $title,
        array $description,
        ?string $icon
    ) {
        $this->payType = $payType;
        $this->title = $title;
        $this->description = $description;
        $this->icon = $icon;
    }

    /**
     * @return PayType
     */
    public function getPayType(): PayType
    {
        return $this->payType;
    }

    /**
     * @return array
     */
    public function getTitle(): array
    {
        return $this->title;
    }

    /**
     * @return array
     */
    public function getDescription(): array
    {
        return $this->description;
    }

    /**
     * @return string|null
     */
    public function getIcon(): ?string
    {
        return $this->icon;
    }
}