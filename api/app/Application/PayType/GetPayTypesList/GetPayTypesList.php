<?php

declare(strict_types=1);

namespace App\Application\PayType\GetPayTypesList;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\PaginationInterface;
use App\Contract\Core\SortingInterface;
use App\Domain\PayType\PayTypeFilter;

/**
 * Class GetPayTypesList
 * @package App\Application\PayType\GetPayTypesList
 */
class GetPayTypesList implements CommandInterface
{
    /** @var PayTypeFilter $filter */
    private PayTypeFilter $filter;

    /** @var PaginationInterface|null $pagination */
    private ?PaginationInterface $pagination;

    /** @var SortingInterface|null $sorting */
    private ?SortingInterface $sorting;

    /**
     * GetPayTypesList constructor
     *
     * @param PayTypeFilter $filter
     * @param PaginationInterface|null $pagination
     * @param SortingInterface|null $sorting
     */
    public function __construct(
        PayTypeFilter $filter,
        ?PaginationInterface $pagination = null,
        ?SortingInterface $sorting = null
    ) {
        $this->filter = $filter;
        $this->pagination = $pagination;
        $this->sorting = $sorting;
    }

    /**
     * @return PayTypeFilter
     */
    public function getFilter(): PayTypeFilter
    {
        return $this->filter;
    }

    /**
     * @return PaginationInterface|null
     */
    public function getPagination(): ?PaginationInterface
    {
        return $this->pagination;
    }

    /**
     * @return SortingInterface|null
     */
    public function getSorting(): ?SortingInterface
    {
        return $this->sorting;
    }
}