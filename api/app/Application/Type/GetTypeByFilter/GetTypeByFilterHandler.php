<?php

declare(strict_types=1);

namespace App\Application\Type\GetTypeByFilter;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\Type\Type;
use App\Domain\Type\TypeRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * Class GetTypeByFilterHandler
 * @package App\Application\Type\GetTypeByFilter
 */
class GetTypeByFilterHandler implements HandlerInterface
{
    /** @var TypeRepositoryInterface $typeRepository */
    private TypeRepositoryInterface $typeRepository;

    /**
     * GetTypeByFilterHandler constructor
     *
     * @param TypeRepositoryInterface $typeRepository
     */
    public function __construct(TypeRepositoryInterface $typeRepository)
    {
        $this->typeRepository = $typeRepository;
    }

    /**
     * @param CommandInterface|GetTypeByFilter $command
     *
     * @return Type|null
     */
    public function handle(CommandInterface $command): ?Model
    {
        return $this->typeRepository->one(
            $command->getFilter()
        );
    }
}