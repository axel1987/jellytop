import File from "../dto/entities/File";

export const actions = {
    uploadFile: async function ({commit}, params = {}) {
        var bodyFormData = new FormData();
        bodyFormData.append('file', params.file);
        bodyFormData.append('group', params.group);
        bodyFormData.append('id', params.id);

        let res = await this.$axios({
                method: "post",
                url: "/file",
                data: bodyFormData,
                headers: {"Content-Type": "multipart/form-data"},
            })
        ;
        if (res && res.data) {
            return new File(res.data)
        }
    },
    removeFile: async function ({commit}, params = {}) {
        return await this.$axios.delete(`/file`, { data: params });
    }
}


export default {
    actions,
};