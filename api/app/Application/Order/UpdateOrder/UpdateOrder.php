<?php

declare(strict_types=1);

namespace App\Application\Order\UpdateOrder;

use App\Contract\Core\CommandInterface;
use App\Domain\Order\Order;
use App\Domain\OrderStatus\OrderStatus;

/**
 * Class UpdateOrder
 * @package App\Application\Order\UpdateOrder
 */
class UpdateOrder implements CommandInterface
{
    /** @var Order $order */
    private Order $order;

    /** @var OrderStatus $orderStatus */
    private OrderStatus $orderStatus;

    /** @var string|null $note */
    private ?string $note;

    /**
     * UpdateOrder constructor
     *
     * @param Order $order
     * @param OrderStatus $orderStatus
     * @param string|null $note
     */
    public function __construct(
        Order $order,
        OrderStatus $orderStatus,
        ?string $note
    ) {
        $this->order = $order;
        $this->orderStatus = $orderStatus;
        $this->note = $note;
    }

    /**
     * @return Order
     */
    public function getOrder(): Order
    {
        return $this->order;
    }

    /**
     * @return OrderStatus
     */
    public function getOrderStatus(): OrderStatus
    {
        return $this->orderStatus;
    }

    /**
     * @return string|null
     */
    public function getNote(): ?string
    {
        return $this->note;
    }
}