<?php

declare(strict_types=1);

namespace App\Http\Resourses\Product;

use App\Domain\Product\Product;
use App\Http\Resourses\Brand\BrandResource;
use App\Http\Resourses\Color\ColorListResource;
use App\Http\Resourses\Delivery\DeliveryListResource;
use App\Http\Resourses\Gender\GenderListResource;
use App\Http\Resourses\PayType\PayTypeListResource;
use App\Http\Resourses\ProductImage\ProductImagesListResource;
use App\Http\Resourses\Season\SeasonListResource;
use App\Http\Resourses\Size\SizeListResource;
use App\Http\Resourses\Type\TypeResource;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class ProductNotRelationResource
 * @package App\Http\Resourses\Product
 */
class ProductNotRelationResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        /** @var Product $product */
        $product = $this->resource;
        if (!$product){
            return [];
        }

        return [
            'id' => $product->id,
            'type' => 'product',
            'attributes' => [
                'title' => $product->title,
                'description' => $product->description,
                'slug' => $product->slug,
                'metaKey' => $product->meta_key,
                'metaDescription' => $product->meta_description,
                'purchasePrice' => number_format($product->purchase_price, 2),
                'salePrice' =>  number_format($product->sale_price, 2),
                'inStock' => $product->in_stock,
                'priority' => $this->priority,

                'createdAt' => $product->created_at ? $product->created_at->format('Y-m-d H:i:s') : '',
                'updatedAt' => $product->updated_at ? $product->updated_at->format('Y-m-d H:i:s') : '',
            ],
            'relationships' => [
                'images' => new ProductImagesListResource($product->images),
                'delivery' => new DeliveryListResource($product->delivery),
                'payTypes' => new PayTypeListResource($product->payTypes),
            ]
        ];
    }
}