export default class OrderForm {

    /**
     * @return <void>
     */
    constructor(data) {
        this.cartItemsIds = data.cartItemsIds ? data.cartItemsIds : [];
        this.delivery = data.delivery ? data.delivery : null;
        this.payType = data.payType ? data.payType : null;
        this.name = data.name ? data.name : null;
        this.phone = data.phone ? data.phone : null;
        this.address = data.address ? data.address : null;
        this.zip = data.zip ? data.zip : null;
        this.department = data.department ? data.department : null;
    }

    /**
     * @return <void>
     */
    async setCartItemIds(cartList) {
        this.cartItemsIds = []
        cartList.forEach(item => {
            this.cartItemsIds.push(item.id)
        })
    }

    /**
     * @returns {{required: (function(*=))}}
     */
    rules() {
        return {
            required: value => !!value || 'Это поле обязательно',
        }
    }
}
