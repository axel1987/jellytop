<?php

declare(strict_types=1);

namespace App\Application\Delivery\DeleteDelivery;

use App\Contract\Core\CommandInterface;
use App\Domain\Delivery\Delivery;

/**
 * Class DeleteDelivery
 * @package App\Application\Delivery\DeleteDelivery
 */
class DeleteDelivery implements CommandInterface
{
    /** @var Delivery $delivery */
    private Delivery $delivery;

    /**
     * DeleteDelivery constructor
     *
     * @param Delivery $delivery
     */
    public function __construct(Delivery $delivery)
    {
        $this->delivery = $delivery;
    }

    /**
     * @return Delivery
     */
    public function getDelivery(): Delivery
    {
        return $this->delivery;
    }
}