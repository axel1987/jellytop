<?php

declare(strict_types=1);

namespace App\Http\Middleware;

use App\Contract\Services\JwtServiceInterface;
use Closure;
use Illuminate\Validation\UnauthorizedException;
use Laravel\Lumen\Http\Request;

/**
 * Class AuthAdminMiddleware
 * @package App\Http\Middleware
 */
class AuthAdminMiddleware
{
    /** @var JwtServiceInterface $JwtService */
    protected $JwtService;

    /**
     * AuthDashboard constructor.
     *
     * @param JwtServiceInterface $JwtService
     */
    public function __construct(JwtServiceInterface $JwtService)
    {
        $this->JwtService = $JwtService;
    }

    /**
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
       if ($request->user() && $request->user()->is_admin){
           return $next($request);
       }

       throw new UnauthorizedException('Not authorized');
    }
}
