import PayType from "../dto/entities/PayType";
import Pagination from "../dto/entities/Pagination";

/**
 * @returns {{
 * payPayType: null,
 * payPayTypesList: *[],
 * payPayTypesSelectList: *[],
 * pagination: null,
 * }}
 */
export const state = () => ({
    payPayType: null,
    payPayTypesList: [],
    payPayTypesSelectList: [],
    pagination: null
});

/**
 * @payPayType {{
 * getPayType(*): null,
 * getPayTypesList(*): [],
 * getPayTypesSelectList(*): [],
 * getPayTypePagination(*): null
 * }}
 */
export const getters = {
    getPayType(state) {
        return state.payPayType;
    },
    getPayTypesList(state) {
        return state.payPayTypesList;
    },
    getPayTypesSelectList(state) {
        return state.payPayTypesSelectList;
    },
    getPayTypePagination(state) {
        return state.pagination;
    }
};

/**
 * @payPayType {{
 * SET_PAY_TYPES_LIST(*, *): void,
 * SET_PAY_TYPES_SELECT_LIST(*, *): void,
 * SET_PAY_TYPE(*, *): void,
 * SET_PAY_TYPES_PAGINATION(*, *): void,
 * }}
 */
export const mutations = {
    SET_PAY_TYPE(state, value) {
        state.payPayType = value
    },
    SET_PAY_TYPES_LIST(state, value) {
        state.payPayTypesList = value
    },
    SET_PAY_TYPES_SELECT_LIST(state, value) {
        state.payPayTypesSelectList = value
    },
    SET_PAY_TYPES_PAGINATION(state, value) {
        state.pagination = value
    },
};

/**
 * @payPayType {{
 * registerPayType: ((function({commit: *}, *=): Promise<void>)|*),
 * updatePayType: ((function({commit: *}, *=): Promise<void>)|*),
 * fetchPayType: ((function({commit: *}, *=): Promise<void>)|*),
 * fetchPayTypesList: ((function({commit: *}, *=): Promise<void>)|*),
 * fetchPayTypesSelectList: ((function({commit: *}, *=): Promise<void>)|*),
 * deletePayType: ((function({commit: *}, *=): Promise<void>)|*)
 * }}
 */
export const actions = {
    fetchPayTypesList: async function ({commit}, params = {}) {
        let {data} = await this.$axios.get('/pay-type', {params: params});

        const list = data.data.map((payPayTypeData) => {
            return new PayType(payPayTypeData)
        });
        const pagination = data.pagination ? new Pagination(data.pagination) : null;

        commit('SET_PAY_TYPES_LIST', list)
        commit('SET_PAY_TYPES_PAGINATION', pagination)
    },
    fetchPayTypesSelectList: async function ({commit}, params = {}) {
        let {data} = await this.$axios.get('/pay-type/list', params);

        const list = data.data.map((payPayTypeData) => {
            return new PayType(payPayTypeData)
        });

        commit('SET_PAY_TYPES_SELECT_LIST', list)
    },
    fetchPayType: async function ({commit}, params = {}) {
        if (params.id) {
            let {data} = await this.$axios.get(`/pay-type/${params.id}`, params);
            commit('SET_PAY_TYPE', new PayType(data))
        } else {
            commit('SET_PAY_TYPE', new PayType(null))
        }
    },
    registerPayType: async function ({commit}, params = {}) {
        await this.$axios.post(`/pay-type`, params).then((res) => {
            if (res) {
                commit('SET_PAY_TYPE', new PayType(res.data))
            }
        });
    },
    updatePayType: async function ({commit}, params = {}) {
        await this.$axios.put(`/pay-type/${params.id}`, params).then((res) => {
            if (res) {
                commit('SET_PAY_TYPE', new PayType(res.data))
            }
        });
    },
    deletePayType: async function ({commit}, params = {}) {
        await this.$axios.delete(`/pay-type/${params.id}`, params);
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations,
};
