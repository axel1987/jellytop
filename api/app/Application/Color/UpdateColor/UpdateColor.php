<?php

declare(strict_types=1);

namespace App\Application\Color\UpdateColor;

use App\Contract\Core\CommandInterface;
use App\Domain\Color\Color;

/**
 * Class UpdateColor
 * @package App\Application\Color\UpdateColor
 */
class UpdateColor implements CommandInterface
{
    /** @var Color $color */
    private Color $color;

    /** @var array $title */
    private array $title;

    /** @var string $slug */
    private string $slug;

    /** @var string|null $hex */
    private ?string $hex;

    /**
     * UpdateColor constructor
     *
     * @param Color $color
     * @param array $title
     * @param string|null $hex
     */
    public function __construct(
        Color $color,
        array $title,
        string $slug,
        ?string $hex = null
    ) {
        $this->color = $color;
        $this->title = $title;
        $this->slug = $slug;
        $this->hex = $hex;
    }

    /**
     * @return Color
     */
    public function getColor(): Color
    {
        return $this->color;
    }

    /**
     * @return array
     */
    public function getTitle(): array
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @return string|null
     */
    public function getHex(): ?string
    {
        return $this->hex;
    }
}