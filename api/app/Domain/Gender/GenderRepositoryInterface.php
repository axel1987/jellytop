<?php

declare(strict_types=1);

namespace App\Domain\Gender;

use App\Contract\Core\DatabaseRepositoryInterface;

/**
 * Interface GenderRepositoryInterface
 * @package App\Domain\Gender
 */
interface GenderRepositoryInterface extends DatabaseRepositoryInterface
{

}