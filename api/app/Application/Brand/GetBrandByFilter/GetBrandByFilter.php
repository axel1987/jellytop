<?php

declare(strict_types=1);

namespace App\Application\Brand\GetBrandByFilter;

use App\Contract\Core\CommandInterface;
use App\Domain\Brand\BrandFilter;

/**
 * Class GetBrandByFilter
 * @package App\Application\Brand\GetBrandByFilter
 */
class GetBrandByFilter implements CommandInterface
{
    /** @var BrandFilter $filter */
    private BrandFilter $filter;

    /**
     * GetBrandByFilter constructor
     *
     * @param BrandFilter $filter
     */
    public function __construct(BrandFilter $filter)
    {
        $this->filter = $filter;
    }

    /**
     * @return BrandFilter
     */
    public function getFilter(): BrandFilter
    {
        return $this->filter;
    }
}