<?php

declare(strict_types=1);

namespace App\Http\Resourses\Product;

use App\Domain\Product\Product;
use App\Http\Resourses\BaseCollectionResource;
use App\Http\Resourses\ProductImage\ProductImagesListResource;
use App\Http\Resourses\Size\SizeListResource;

/**
 * Class SiteProductListResource
 * @package App\Http\Resourses\Product
 */
class SiteProductListResource extends BaseCollectionResource
{
    /**
     * @param Product $item
     *
     * @return array
     */
    protected function getItemData($item): array
    {
        return [
            'id' => $item->id,
            'type' => 'product',
            'attributes' => [
                'title' => $item->title,
                'slug' => $item->slug,
                'salePrice' =>  $item->sale_price,
                'createdAt' => $item->created_at ? $item->created_at->format('Y-m-d H:i:s') : '',
                'updatedAt' => $item->updated_at ? $item->updated_at->format('Y-m-d H:i:s') : '',
            ],
            'relationships' => [
                'images' => new ProductImagesListResource($item->images),
                'sizes' => new SizeListResource($item->sizes()->orderBy('size')->get()),
            ]
        ];
    }
}