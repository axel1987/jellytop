<?php

declare(strict_types=1);

namespace App\Application\Color\StoreColor;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\Color\Color;
use App\Domain\Color\ColorRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * Class StoreColorHandler
 * @package App\Application\Color\StoreColor
 */
class StoreColorHandler implements HandlerInterface
{
    /** @var ColorRepositoryInterface $colorRepository */
    private ColorRepositoryInterface $colorRepository;

    /**
     * StoreColorHandler constructor
     *
     * @param ColorRepositoryInterface $colorRepository
     */
    public function __construct(ColorRepositoryInterface $colorRepository)
    {
        $this->colorRepository = $colorRepository;
    }

    /**
     * @param CommandInterface|StoreColor $command
     *
     * @return Color
     */
    public function handle(CommandInterface $command): Model
    {
        $color = new Color();

        $color->title = $command->getTitle();
        $color->slug = $command->getSlug();
        $color->hex = $command->getHex();

        return $this->colorRepository->store($color);
    }
}