<?php

declare(strict_types=1);

namespace App\Domain\Product;

use App\Contract\Core\DatabaseRepositoryInterface;

/**
 * Interface ProductRepositoryInterface
 * @package App\Domain\Product
 */
interface ProductRepositoryInterface extends DatabaseRepositoryInterface
{

}