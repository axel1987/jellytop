export default class ColorForm {

    /**
     * @return <void>
     */
    constructor(data) {
        this.id = data.id;
        this.title = {
            ua: data.title && data.title.ua ? data.title.ua : null,
            ru: data.title && data.title.ru ? data.title.ru : null
        };
        this.slug = data.slug;
        this.hex = data.hex;
        this.pickerData = {hex: `#${data.hex}`};
    }

    /**
     * @returns {{required: (function(*=))}}
     */
    rules() {
        return {
            required: value => !!value || 'Это поле обязательно',
        }
    }
}
