<?php

declare(strict_types=1);

namespace App\Application\Season\GetSeasonsList;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\Season\SeasonRepositoryInterface;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

/**
 * Class GetSeasonsListHandler
 * @package App\Application\Season\GetSeasonsList
 */
class GetSeasonsListHandler implements HandlerInterface
{
    /** @var SeasonRepositoryInterface $seasonRepository */
    private SeasonRepositoryInterface $seasonRepository;

    /**
     * GetSeasonsListHandler constructor
     *
     * @param SeasonRepositoryInterface $seasonRepository
     */
    public function __construct(SeasonRepositoryInterface $seasonRepository)
    {
        $this->seasonRepository = $seasonRepository;
    }

    /**
     * @param CommandInterface|GetSeasonsList $command
     *
     * @return LengthAwarePaginator|Collection
     */
    public function handle(CommandInterface $command): mixed
    {
        return $this->seasonRepository->all(
            $command->getFilter(),
            $command->getPagination(),
            $command->getSorting()
        );
    }
}