<?php

declare(strict_types=1);

namespace App\Domain\OrderItem;

use Exception;

/**
 * Class OrderItemExceptions
 * @package App\Domain\OrderItem
 */
class OrderItemExceptions extends Exception
{
    /**
     * @param int $id
     *
     * @throws OrderItemExceptions
     */
    public static function notFound(int $id)
    {
        throw new OrderItemExceptions(sprintf('Order item with id %d not found', $id));
    }
}