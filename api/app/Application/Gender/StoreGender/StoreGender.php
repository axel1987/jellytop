<?php

declare(strict_types=1);

namespace App\Application\Gender\StoreGender;

use App\Contract\Core\CommandInterface;

/**
 * Class StoreGender
 * @package App\Application\Gender\StoreGender
 */
class StoreGender implements CommandInterface
{
    /** @var array $title */
    private array $title;

    /** @var string|null $icon */
    private ?string $icon;

    /**
     * StoreGender constructor
     *
     * @param array $title
     * @param string|null $icon
     */
    public function __construct(
        array $title,
        ?string $icon = null
    ) {
        $this->title = $title;
        $this->icon = $icon;
    }

    /**
     * @return array
     */
    public function getTitle(): array
    {
        return $this->title;
    }

    /**
     * @return string|null
     */
    public function getIcon(): ?string
    {
        return $this->icon;
    }
}