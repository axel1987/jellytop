<?php

declare(strict_types=1);

namespace App\Domain\Cart;

use App\Contract\Core\FilterInterface;
use Illuminate\Http\Request;

/**
 * Class CartFilter
 * @package App\Domain\Cart
 */
class CartFilter implements FilterInterface
{
    /** @var int|null $id */
    private ?int $id = null;

    /** @var int[]|null $ids */
    private ?array $ids = null;

    /** @var string|null $session_id */
    private ?string $session_id = null;

    /**
     * @param Request $request
     *
     * @return FilterInterface|CartFilter
     */
    public static function fromRequest(Request $request): FilterInterface
    {
        $filter = new self();
        $filter->setSessionId($request->header('access-session'));

        return $filter;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int[]|null
     */
    public function getIds(): ?array
    {
        return $this->ids;
    }

    /**
     * @param int[]|null $ids
     */
    public function setIds(?array $ids): void
    {
        $this->ids = $ids;
    }

    /**
     * @return string|null
     */
    public function getSessionId(): ?string
    {
        return $this->session_id;
    }

    /**
     * @param string|null $session_id
     */
    public function setSessionId(?string $session_id): void
    {
        $this->session_id = $session_id;
    }
}