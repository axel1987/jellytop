<?php

declare(strict_types=1);

namespace App\Application\Slider\UpdateSlider;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\Slider\Slider;
use App\Domain\Slider\SliderRepositoryInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

/**
 * Class UpdateSliderHandler
 * @package App\Application\Slider\UpdateSlider
 */
class UpdateSliderHandler implements HandlerInterface
{
    /** @var SliderRepositoryInterface $sliderRepository */
    private SliderRepositoryInterface $sliderRepository;

    /**
     * UpdateSliderHandler constructor
     *
     * @param SliderRepositoryInterface $sliderRepository
     */
    public function __construct(SliderRepositoryInterface $sliderRepository)
    {
        $this->sliderRepository = $sliderRepository;
    }

    /**
     * @param CommandInterface|UpdateSlider $command
     *
     * @return Slider
     */
    public function handle(CommandInterface $command): Model
    {
        $slider = $command->getSlider();

        try {
            if ($slider->link != $command->getLink()) {
                Storage::disk('public')->delete(str_replace('/storage/', '', $slider->link));
            }
        } catch (\Exception $exception) {
            Log::error($exception->getMessage());
        }

        $slider->link = $command->getLink();
        $slider->text = $command->getText();

        $this->sliderRepository->store($slider);

        return $slider;
    }
}