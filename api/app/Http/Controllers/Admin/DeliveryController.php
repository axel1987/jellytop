<?php

declare(strict_types=1);

namespace App\Http\Controllers\Admin;

use App\Application\Delivery\DeleteDelivery\DeleteDelivery;
use App\Application\Delivery\GetDeliveryByFilter\GetDeliveryByFilter;
use App\Application\Delivery\GetDeliveryList\GetDeliveryList;
use App\Application\Delivery\StoreDelivery\StoreDelivery;
use App\Application\Delivery\UpdateDelivery\UpdateDelivery;
use App\Domain\Delivery\DeliveryFilter;
use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Delivery\DeliveryListRequest;
use App\Http\Requests\Dashboard\Delivery\DeliveryRequest;
use App\Http\Resourses\Delivery\DeliveryListResource;
use App\Http\Resourses\Delivery\DeliveryResource;
use App\Infrastructure\Core\Pagination;
use App\Infrastructure\Core\Sorting;
use Illuminate\Http\JsonResponse;

/**
 * Class DeliveryController
 * @package App\Http\Controllers
 */
class DeliveryController extends Controller
{
    /**
     * @param DeliveryListRequest $request
     *
     * @return JsonResponse
     */
    public function index(DeliveryListRequest $request): JsonResponse
    {
        $filter = DeliveryFilter::fromRequest($request);
        $pagination = Pagination::fromRequest($request);
        $sorting = Sorting::fromRequest($request);

        $list = $this->execute(new GetDeliveryList(
            $filter,
            $pagination,
            $sorting
        ));

        return new JsonResponse(new DeliveryListResource($list));
    }

    /**
     * @param DeliveryListRequest $request
     *
     * @return JsonResponse
     */
    public function list(DeliveryListRequest $request): JsonResponse
    {
        $filter = DeliveryFilter::fromRequest($request);
        $sorting = Sorting::fromRequest($request);

        $list = $this->execute(new GetDeliveryList(
            $filter,
            null,
            $sorting
        ));

        return new JsonResponse(new DeliveryListResource($list));
    }

    /**
     * @param int $id
     *
     * @return JsonResponse
     */
    public function view(int $id): JsonResponse
    {
        $filter = new DeliveryFilter();
        $filter->setId($id);

        $type = $this->execute(new GetDeliveryByFilter($filter));

        return new JsonResponse(new DeliveryResource($type));
    }

    /**
     * @param DeliveryRequest $request
     *
     * @return JsonResponse
     */
    public function store(DeliveryRequest $request): JsonResponse
    {
        $type = $this->execute(new StoreDelivery(
            $request->get('title'),
            $request->get('description'),
            $request->get('icon'),
            (float)$request->get('minPrice'),
        ));

        return new JsonResponse(new DeliveryResource($type));
    }

    /**
     * @param DeliveryRequest $request
     * @param int $id
     *
     * @return JsonResponse
     */
    public function update(DeliveryRequest $request, int $id): JsonResponse
    {
        $filter = new DeliveryFilter();
        $filter->setId($id);

        $type = $this->execute(new GetDeliveryByFilter($filter));
        $type = $this->execute(new UpdateDelivery(
            $type,
            $request->get('title'),
            $request->get('description'),
            $request->get('icon'),
            (float)$request->get('minPrice'),
        ));

        return new JsonResponse(new DeliveryResource($type));
    }

    /**
     * @param int $id
     *
     * @return JsonResponse
     */
    public function delete(int $id): JsonResponse
    {
        $filter = new DeliveryFilter();
        $filter->setId($id);

        $type = $this->execute(new GetDeliveryByFilter($filter));
        $this->execute(new DeleteDelivery($type));

        return new JsonResponse(null, 204);
    }
}