<?php

declare(strict_types=1);

namespace App\Jobs;

use App\Domain\Order\Order;
use Illuminate\Support\Facades\Mail;

class NewOrderEmailJob extends Job
{
    /** @var Order $order */
    private $order;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $orderLink = 'https://djelitop.com/desc/manage-orders/' . $this->order->id;
        Mail::raw($orderLink, function ($message) {
            $message->to('tatyana.djel@gmail.com')
                ->subject("НОВЫЙ ЗАКАЗ НА САЙТЕ!!!!!");
        });
    }
}
