<?php

declare(strict_types=1);

namespace App\Domain\Slider;

use App\Contract\Core\DatabaseRepositoryInterface;

/**
 * interface SliderRepositoryInterface
 * @package App\Domain\Slider
 */
interface SliderRepositoryInterface extends DatabaseRepositoryInterface
{

}