<?php

declare(strict_types=1);

namespace App\Exceptions;

use Exception;
use FastRoute\BadRouteException;
use GuzzleHttp\Exception\ClientException;
use Throwable;
use Firebase\JWT\ExpiredException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Collection;
use Illuminate\Validation\UnauthorizedException;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class Handler
 * @package App\Exceptions
 */
class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param Throwable $throwable
     * @return void
     * @throws Exception
     */
    public function report(Throwable $throwable)
    {
        parent::report($throwable);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param Request $request
     * @param Throwable $throwable
     * @return Response|JsonResponse
     */
    public function render($request, Throwable $throwable)
    {
        if ($throwable instanceof ValidationException) {
            /** @var ValidationException $exception */
            $errors = [];
            foreach ($throwable->errors() as $key => $error) {
                Collection::make($error)->each(function (string $message) use ($key, &$errors) {
                    $errors[] = [
                        'status' => Response::HTTP_UNPROCESSABLE_ENTITY,
                        'detail' => $message,
                        'source' => [
                            'parameter' => $key
                        ]
                    ];
                });
            };
            return (new JsonResponse($errors, Response::HTTP_UNPROCESSABLE_ENTITY));
        }

        if ($throwable instanceof NotFoundHttpException
            || $throwable instanceof ModelNotFoundException
        ) {
            /** @var NotFoundHttpException $throwable */

            return new JsonResponse([
                'status' => Response::HTTP_NOT_FOUND,
                'detail' => 'Resource not found',
            ], Response::HTTP_NOT_FOUND);
        }

        if ($throwable instanceof UnauthorizedException) {
            /** @var UnauthorizedException $throwable */

            return new JsonResponse([
                'status' => Response::HTTP_UNAUTHORIZED,
                'detail' => $throwable->getMessage() ?: 'Unauthorized',
            ], Response::HTTP_UNAUTHORIZED);
        }

        if ($throwable instanceof BadRouteException) {
            /** @var BadRouteException $throwable */

            return new JsonResponse([
                'status' => Response::HTTP_NOT_FOUND,
                'detail' => $throwable->getMessage() ?: 'Route not found',
            ], Response::HTTP_NOT_FOUND);
        }

        if ($throwable instanceof ClientException) {
            /** @var BadRouteException $throwable */

            return new JsonResponse([
                'status' => Response::HTTP_BAD_REQUEST,
                'detail' => $throwable->getMessage() ?: 'HTTP Exception',
            ], Response::HTTP_BAD_REQUEST);
        }

        if ($throwable instanceof ExpiredException) {
            /** @var ExpiredException $exception */

            return new JsonResponse([
                'status' => 440,
                'detail' => $throwable->getMessage() ?: 'Unauthorized',
            ], 440);
        }

        return parent::render($request, $throwable);
    }
}
