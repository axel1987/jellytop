<?php

declare(strict_types=1);

namespace App\Application\Size\UpdateSize;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\Size\Size;
use App\Domain\Size\SizeRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * Class UpdateSizeHandler
 * @package App\Application\Size\UpdateSize
 */
class UpdateSizeHandler implements HandlerInterface
{
    /** @var SizeRepositoryInterface $sizeRepository */
    private SizeRepositoryInterface $sizeRepository;

    /**
     * UpdateSizeHandler constructor
     *
     * @param SizeRepositoryInterface $sizeRepository
     */
    public function __construct(SizeRepositoryInterface $sizeRepository)
    {
        $this->sizeRepository = $sizeRepository;
    }

    /**
     * @param CommandInterface|UpdateSize $command
     *
     * @return Size
     */
    public function handle(CommandInterface $command):Model
    {
        $size = $command->getSize();

        $size->size = $command->getSize();

        return $this->sizeRepository->store($size);
    }
}