<?php

declare(strict_types=1);

namespace App\Http\Controllers\Admin;

use App\Application\Type\DeleteType\DeleteType;
use App\Application\Type\GetTypeByFilter\GetTypeByFilter;
use App\Application\Type\GetTypesList\GetTypesList;
use App\Application\Type\StoreType\StoreType;
use App\Application\Type\UpdateType\UpdateType;
use App\Domain\Type\TypeFilter;
use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Type\TypeListRequest;
use App\Http\Requests\Dashboard\Type\TypeRequest;
use App\Http\Resourses\Type\TypeListResource;
use App\Http\Resourses\Type\TypeResource;
use App\Infrastructure\Core\Pagination;
use App\Infrastructure\Core\Sorting;
use Illuminate\Http\JsonResponse;

/**
 * Class TypeController
 * @package App\Http\Controllers
 */
class TypeController extends Controller
{
    /**
     * @param TypeListRequest $request
     *
     * @return JsonResponse
     */
    public function index(TypeListRequest $request): JsonResponse
    {
        $filter = TypeFilter::fromRequest($request);
        $pagination = Pagination::fromRequest($request);
        $sorting = Sorting::fromRequest($request);

        $list = $this->execute(new GetTypesList(
            $filter,
            $pagination,
            $sorting
        ));

        return new JsonResponse(new TypeListResource($list));
    }

    /**
     * @param TypeListRequest $request
     *
     * @return JsonResponse
     */
    public function list(TypeListRequest $request): JsonResponse
    {
        $filter = TypeFilter::fromRequest($request);
        $sorting = Sorting::fromRequest($request);

        $list = $this->execute(new GetTypesList(
            $filter,
            null,
            $sorting
        ));

        return new JsonResponse(new TypeListResource($list));
    }

    /**
     * @param int $id
     *
     * @return JsonResponse
     */
    public function view(int $id): JsonResponse
    {
        $filter = new TypeFilter();
        $filter->setId($id);

        $type = $this->execute(new GetTypeByFilter($filter));

        return new JsonResponse(new TypeResource($type));
    }

    /**
     * @param TypeRequest $request
     *
     * @return JsonResponse
     */
    public function store(TypeRequest $request): JsonResponse
    {
        $type = $this->execute(new StoreType(
            $request->get('title'),
            $request->get('slug'),
            $request->get('description'),
            $request->get('icon'),
        ));

        return new JsonResponse(new TypeResource($type));
    }

    /**
     * @param TypeRequest $request
     * @param int $id
     *
     * @return JsonResponse
     */
    public function update(TypeRequest $request, int $id): JsonResponse
    {
        $filter = new TypeFilter();
        $filter->setId($id);

        $type = $this->execute(new GetTypeByFilter($filter));
        $type = $this->execute(new UpdateType(
            $type,
            $request->get('title'),
            $request->get('slug'),
            $request->get('description'),
            $request->get('icon'),
        ));

        return new JsonResponse(new TypeResource($type));
    }

    /**
     * @param int $id
     *
     * @return JsonResponse
     */
    public function delete(int $id): JsonResponse
    {
        $filter = new TypeFilter();
        $filter->setId($id);

        $type = $this->execute(new GetTypeByFilter($filter));
        $this->execute(new DeleteType($type));

        return new JsonResponse(null, 204);
    }
}