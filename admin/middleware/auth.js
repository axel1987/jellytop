/**
 * @param store
 * @param redirect
 * @returns {*}
 */
export default function ({ store, redirect }) {
  if (typeof window !== 'undefined') {
    const token =  localStorage.getItem('token');

    // If the user is not authenticated
    if (token) {
      store.commit('authorization/SET_IS_AUTHORIZED', true)
    }
  }
}
