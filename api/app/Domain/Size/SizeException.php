<?php

declare(strict_types=1);

namespace App\Domain\Size;

use Exception;

/**
 * Class SizeException
 * @package App\Domain\Size
 */
class SizeException extends Exception
{
    /**
     * @param int $id
     *
     * @throws SizeException
     */
    public static function notFound(int $id)
    {
        throw new SizeException(sprintf('Color with id %d not found', $id));
    }
}