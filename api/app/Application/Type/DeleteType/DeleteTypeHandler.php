<?php

declare(strict_types=1);

namespace App\Application\Type\DeleteType;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\Type\TypeRepositoryInterface;
use Illuminate\Support\Facades\Storage;

/**
 * Class DeleteTypeHandler
 * @package App\Application\Type\DeleteType
 */
class DeleteTypeHandler implements HandlerInterface
{
    /** @var TypeRepositoryInterface $typeRepository */
    private TypeRepositoryInterface $typeRepository;

    /**
     * DeleteTypeHandler constructor
     *
     * @param TypeRepositoryInterface $typeRepository
     */
    public function __construct(TypeRepositoryInterface $typeRepository)
    {
        $this->typeRepository = $typeRepository;
    }

    /**
     * @param DeleteType $command
     *
     * @return void
     */
    public function handle(CommandInterface $command): void
    {
        $type = $command->getType();

        if ($type->icon) {
            Storage::disk('public')
                ->delete(str_replace('/storage', '', $type->icon));
        }

        $this->typeRepository->delete($type);
    }
}