<?php

declare(strict_types=1);

namespace App\Application\StaticPage\DeleteStatiPage;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\StaticPage\StaticPageRepositoryInterface;

/**
 * Class DeleteStaticPageHandler
 * @package App\Application\StaticPage\DeleteStatiPage
 */
class DeleteStaticPageHandler implements HandlerInterface
{
    /** @var StaticPageRepositoryInterface $staticPageRepository */
    private StaticPageRepositoryInterface $staticPageRepository;

    /**
     * DeleteStaticPageHandler constructor
     *
     * @param StaticPageRepositoryInterface $staticPageRepository
     */
    public function __construct(StaticPageRepositoryInterface $staticPageRepository)
    {
        $this->staticPageRepository = $staticPageRepository;
    }

    /**
     * @param CommandInterface|DeleteStaticPage $command
     *
     * @return void
     */
    public function handle(CommandInterface $command): void
    {
        $this->staticPageRepository->delete(
            $command->getStaticPage()
        );
    }
}