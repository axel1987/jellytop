<?php

declare(strict_types=1);

namespace App\Application\Country\StoreCountry;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\Country\Country;
use App\Domain\Country\CountryRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * Class StoreCountryHandler
 * @package App\Application\Country\StoreCountry
 */
class StoreCountryHandler implements HandlerInterface
{
    /** @var CountryRepositoryInterface $countryRepository */
    private CountryRepositoryInterface $countryRepository;

    /**
     * StoreCountryHandler constructor
     *
     * @param CountryRepositoryInterface $countryRepository
     */
    public function __construct(CountryRepositoryInterface $countryRepository)
    {
        $this->countryRepository = $countryRepository;
    }

    /**
     * @param StoreCountry $command
     *
     * @return Country
     */
    public function handle(CommandInterface $command): Model
    {
        $country = new Country();

        $country->title = $command->getTitle();
        $country->slug = $command->getSlug();
        $country->icon = $command->getIcon();

        return $this->countryRepository->store($country);
    }
}