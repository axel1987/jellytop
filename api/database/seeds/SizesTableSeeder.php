<?php

declare(strict_types=1);

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

/**
 * Class SizesTableSeeder
 */
class SizesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [];
        $now = Carbon::now();

        for ($i = 18; $i < 46; $i++) {
            $data[] = [
                'size' => $i,
                'created_at' => $now,
                'updated_at' => $now,
            ];
        }

        DB::table('sizes')->insert($data);
    }
}