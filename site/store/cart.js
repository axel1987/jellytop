import Cart from "../dto/entities/Cart";

/**
 * @returns {{
 * Cart: null,
 * CartsList: *[],
 * CartsSelectList: *[],
 * pagination: null,
 * }}
 */
export const state = () => ({
    Cart: null,
    CartsList: []
});

/**
 * @type {{
 * getCart(*): null,
 * getCartsList(*): []
 * }}
 */
export const getters = {
    getCart(state) {
        return state.Cart;
    },
    getCartsList(state) {
        return state.CartsList;
    }
};

/**
 * @type {{
 * SET_CartS_LIST(*, *): void,
 * SET_CartS_SELECT_LIST(*, *): void,
 * SET_Cart(*, *): void,
 * SET_CartS_PAGINATION(*, *): void,
 * }}
 */
export const mutations = {
    SET_CART(state, value) {
        state.Cart = value
    },
    SET_CARTS_LIST(state, value) {
        state.CartsList = value
    },
};

/**
 * @type {{
 * fetchCart: ((function({commit: *}, *=): Promise<void>)|*),
 * fetchCartsList: ((function({commit: *}, *=): Promise<void>)|*),
 * }}
 */
export const actions = {
    fetchCartsList: async function ({commit}, params = {}) {
        let res = await this.$axios.get('/cart', params);

        let list = []
        if (res && res.data) {
            const data = res.data
            list = data.data.map((cartData) => {
                return new Cart(cartData)
            });
        }

        commit('SET_CARTS_LIST', list)
    },
    fetchCart: async function ({commit}, params = {}) {
        if (params.id) {
            let {data} = await this.$axios.get(`/carts/${params.id}`, params);
            commit('SET_CART', new Cart(data))
        } else {
            commit('SET_CART', new Cart(null))
        }
    },
    registerCart: async function (context, params = {}) {
        await this.$axios.post(`/cart`, params).then(async (res) => {
            if (res) {
                await context.dispatch('fetchCartsList')
            }
        });
    },
    deleteCart: async function (context, params = {}) {
        await this.$axios.delete(`/cart/${params.id}`);
        await context.dispatch('fetchCartsList')
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations,
};
