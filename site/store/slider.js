import Slider from "../dto/entities/Slider";
import Pagination from "../dto/entities/Pagination";

/**
 * @returns {{
 * slider: null,
 * slidersList: *[],
 * slidersSelectList: *[],
 * }}
 */
export const state = () => ({
    slider: null,
    slidersList: [],
    slidersSelectList: [],
});

/**
 *
 * @type {{
 * getSlider(*): null,
 * getSlidersList(*): [],
 * getSlidersSelectList(*): [],
 * }}
 */
export const getters = {
    getSlider(state) {
        return state.slider;
    },
    getSlidersList(state) {
        return state.slidersList;
    },
    getSlidersSelectList(state) {
        return state.slidersSelectList;
    },
};

/**
 * @type {{
 * SET_SLIDER(*, *): void,
 * SET_SLIDER_LIST(*, *): void,
 * SET_SLIDER_SELECT_LIST(*, *): void
 * }}
 */
export const mutations = {
    SET_SLIDER(state, value) {
        state.slider = value
    },
    SET_SLIDER_LIST(state, value) {
        state.slidersList = value
    },
    SET_SLIDER_SELECT_LIST(state, value) {
        state.slidersSelectList = value
    },
};

/**
 * @type {{
 * fetchSlider: ((function({commit: *}, *=): Promise<void>)|*),
 * fetchSlidersList: ((function({commit: *}, *=): Promise<void>)|*),
 * fetchSlidersSelectList: ((function({commit: *}, *=): Promise<void>)|*),
 * }}
 */
export const actions = {
    fetchSlidersList: async function ({commit}, params = {}) {
        let {data} = await this.$axios.get('/sliders', {params: params});

        const list = data.data.map((sliderData) => {
            return new Slider(sliderData)
        });

        commit('SET_SLIDER_LIST', list)
    },
    fetchSlidersSelectList: async function ({commit}, params = {}) {
        let res = await this.$axios.get('/sliders/list', params);
        if (res && res.data) {
            const list = res.data.data.map((sliderData) => {
                return new Slider(sliderData)
            });

            commit('SET_SLIDER_SELECT_LIST', list)
        }
    },
    fetchSlider: async function ({commit}, params = {}) {
        let {data} = await this.$axios.get(`/sliders/${params.id}`, params);
        commit('SET_SLIDER', new Slider(data))
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations,
};
