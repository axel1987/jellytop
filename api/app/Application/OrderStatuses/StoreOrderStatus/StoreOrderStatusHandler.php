<?php

declare(strict_types=1);

namespace App\Application\OrderStatuses\StoreOrderStatus;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\OrderStatus\OrderStatus;
use App\Infrastructure\DatabaseRepository\OrderStatusRepository;
use Illuminate\Database\Eloquent\Model;

/**
 * Class StoreOrderStatusHandler
 * @package App\Application\OrderStatuses\StoreOrderStatus
 */
class StoreOrderStatusHandler implements HandlerInterface
{
    /** @var OrderStatusRepository $orderStatusRepository */
    private OrderStatusRepository $orderStatusRepository;

    /**
     * UpdateOrderStatusHandler constructor
     *
     * @param OrderStatusRepository $orderStatusRepository
     */
    public function __construct(OrderStatusRepository $orderStatusRepository)
    {
        $this->orderStatusRepository = $orderStatusRepository;
    }

    /**
     * @param CommandInterface|StoreOrderStatus $command
     *
     * @return OrderStatus
     */
    public function handle(CommandInterface $command): Model
    {
        $orderStatus = new OrderStatus();
        $orderStatus->title = $command->getTitle();

        $this->orderStatusRepository->store($orderStatus);

        return $orderStatus;
    }
}