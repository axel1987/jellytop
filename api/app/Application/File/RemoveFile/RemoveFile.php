<?php

declare(strict_types=1);

namespace App\Application\File\RemoveFile;

use App\Contract\Core\CommandInterface;

/**
 * Class RemoveFile
 * @package App\Application\File\RemoveFile
 */
class RemoveFile implements CommandInterface
{
    /** @var string $url */
    private string $url;

    /** @var string|null $group */
    private ?string $group;

    /** @var int|null $id */
    private ?int $id;

    /**
     * RemoveFile constructor
     *
     * @param string $url
     * @param string|null $group
     * @param int|null $id
     */
    public function __construct(
        string $url,
        ?string $group = null,
        ?int $id = null
    ) {
        $this->url = $url;
        $this->group = $group;
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @return string|null
     */
    public function getGroup(): ?string
    {
        return $this->group;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }
}