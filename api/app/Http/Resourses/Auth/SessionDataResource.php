<?php

declare(strict_types=1);

namespace App\Http\Resourses\Auth;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class SessionDataResource
 * @package App\Http\Resourses\Auth
 */
class SessionDataResource extends JsonResource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => [
                'type' => 'session',
                'attributes' => [
                    'tokenType' => $this['tokenType'],
                    'sessionToken' => $this['sessionToken'],
                ]
            ]
        ];
    }
}
