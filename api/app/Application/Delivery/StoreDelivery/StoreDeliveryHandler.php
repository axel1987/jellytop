<?php

declare(strict_types=1);

namespace App\Application\Delivery\StoreDelivery;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\Delivery\Delivery;
use App\Domain\Delivery\DeliveryRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * Class StoreDeliveryHandler
 * @package App\Application\Delivery\StoreDelivery
 */
class StoreDeliveryHandler implements HandlerInterface
{
    /** @var DeliveryRepositoryInterface $deliveryRepository */
    private DeliveryRepositoryInterface $deliveryRepository;

    /**
     * StoreDeliveryHandler constructor
     *
     * @param DeliveryRepositoryInterface $deliveryRepository
     */
    public function __construct(DeliveryRepositoryInterface $deliveryRepository)
    {
        $this->deliveryRepository = $deliveryRepository;
    }

    /**
     * @param StoreDelivery $command
     *
     * @return Model
     */
    public function handle(CommandInterface $command): Model
    {
        $delivery = new Delivery();

        $delivery->title = $command->getTitle();
        $delivery->description = $command->getDescription();
        $delivery->icon = $command->getIcon();
        $delivery->min_price = $command->getMinPrice();

        return $this->deliveryRepository->store($delivery);
    }
}