<?php

declare(strict_types=1);

namespace App\Application\Country\UpdateCountry;

use App\Contract\Core\CommandInterface;
use App\Domain\Country\Country;

/**
 * Class UpdateCountry
 * @package App\Application\Country\UpdateCountry
 */
class UpdateCountry implements CommandInterface
{
    /** @var Country $country */
    private Country $country;

    /** @var array $title */
    private array $title;

    /** @var string $slug */
    private string $slug;

    /** @var string|null $icon */
    private ?string $icon;

    /**
     * UpdateCountry constructor
     *
     * @param Country $country
     * @param array $title
     * @param string $slug
     * @param string|null $icon
     */
    public function __construct(
        Country $country,
        array $title,
        string $slug,
        ?string $icon = null
    ) {
        $this->country = $country;
        $this->title = $title;
        $this->slug = $slug;
        $this->icon = $icon;
    }

    /**
     * @return Country
     */
    public function getCountry(): Country
    {
        return $this->country;
    }

    /**
     * @return array
     */
    public function getTitle(): array
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @return string|null
     */
    public function getIcon(): ?string
    {
        return $this->icon;
    }
}