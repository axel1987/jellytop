/**
 * @returns {{errors: {}}}
 */
export const state = () => ({
  errors: {}
})

/**
 * @type {{errors(*): {}}}
 */
export const getters = {
  errors (state) {
    return state.errors
  }
}

/**
 * @type {{SET_VALIDATION_ERRORS(*, *): void}}
 */
export const mutations = {
  SET_VALIDATION_ERRORS (state, errors) {
    state.errors = errors
  }
}

/**
 * @type {{
 * clearErrors({commit: *}): void,
 * setErrors({commit: *}, *=): void
 * }}
 */
export const actions = {
  setErrors ({ commit }, errors) {
    commit('SET_VALIDATION_ERRORS', errors)
  },
  clearErrors ({ commit }) {
    commit('SET_VALIDATION_ERRORS', {})
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
