<?php

declare(strict_types=1);

namespace App\Http\Resourses\Size;

use App\Domain\Size\Size;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class SizeResource
 * @package App\Http\Resourses\Size
 */
class SizeResource extends JsonResource
{
    /**
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        /** @var Size $this */
        return [
            'id' => $this->id,
            'type' => 'size',
            'attributes' => [
                'size' => round($this->size),
                'createdAt' => $this->created_at ? $this->created_at->format('Y-m-d H:i:s') : '',
                'updatedAt' => $this->updated_at ? $this->updated_at->format('Y-m-d H:i:s') : '',
            ]
        ];
    }
}