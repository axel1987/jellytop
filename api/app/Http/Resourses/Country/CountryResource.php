<?php

declare(strict_types=1);

namespace App\Http\Resourses\Country;

use App\Domain\Country\Country;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class CountryResource
 * @package App\Http\Resourses\Country
 */
class CountryResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        /** @var Country $this */
        return [
            'id' => $this->id,
            'type' => 'country',
            'attributes' => [
                'title' => $this->title,
                'slug' => $this->slug,
                'icon' => $this->icon,
                'createdAt' => $this->created_at ? $this->created_at->format('Y-m-d H:i:s') : '',
                'updatedAt' => $this->updated_at ? $this->updated_at->format('Y-m-d H:i:s') : '',
            ]
        ];
    }
}