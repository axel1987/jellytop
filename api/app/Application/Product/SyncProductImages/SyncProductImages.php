<?php

declare(strict_types=1);

namespace App\Application\Product\SyncProductImages;

use App\Contract\Core\CommandInterface;
use App\Domain\Product\Product;

/**
 * Class SyncProductImages
 * @package App\Application\Product\SyncProductImages
 */
class SyncProductImages implements CommandInterface
{
    /** @var Product $product */
    private Product $product;

    /** @var array $images */
    private array $images;

    /**
     * SyncProductImages constructor
     *
     * @param Product $product
     * @param array $images
     */
    public function __construct(
        Product $product,
        array $images
    ) {
        $this->product = $product;
        $this->images = $images;
    }

    /**
     * @return Product
     */
    public function getProduct(): Product
    {
        return $this->product;
    }

    /**
     * @return array
     */
    public function getImages(): array
    {
        return $this->images;
    }
}