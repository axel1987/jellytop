<?php

declare(strict_types=1);

namespace App\Application\Brand\StoreBrand;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\Brand\Brand;
use App\Domain\Brand\BrandRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * Class StoreBrandHandler
 * @package App\Application\Brand\StoreBrand
 */
class StoreBrandHandler implements HandlerInterface
{
    /** @var BrandRepositoryInterface $brandRepository */
    private BrandRepositoryInterface $brandRepository;

    /**
     * StoreBrandHandler constructor
     *
     * @param BrandRepositoryInterface $brandRepository
     */
    public function __construct(BrandRepositoryInterface $brandRepository)
    {
        $this->brandRepository = $brandRepository;
    }

    /**
     * @param CommandInterface|StoreBrand $command
     *
     * @return Brand
     */
    public function handle(CommandInterface $command): Model
    {
        $brand = new Brand();

        $brand->country()->associate($command->getCountry());
        $brand->title = $command->getTitle();
        $brand->description = $command->getDescription();
        $brand->logo = $command->getLogoUrl();
        $brand->slug = $command->getSlug();

        return $this->brandRepository->store($brand);
    }
}