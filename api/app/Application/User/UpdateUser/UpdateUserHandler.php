<?php

declare(strict_types=1);

namespace App\Application\User\UpdateUser;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\User\User;
use App\Domain\User\UserRepositoryInterface;

/**
 * Class UpdateUserHandler
 * @package App\Application\User\UpdateUser
 */
class UpdateUserHandler implements HandlerInterface
{
    /** @var UserRepositoryInterface $userRepository */
    private $userRepository;

    /**
     * UpdateUserHandler constructor.
     *
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param CommandInterface|UpdateUser $command
     *
     * @return User
     */
    public function handle(CommandInterface $command): User
    {
        $user = $command->getUser();

        $user->email = $command->getEmail();
        $user->name = $command->getName();
        $user->is_admin = $command->getIsAdmin();

        $this->userRepository->store($user);

        return $user;
    }
}
