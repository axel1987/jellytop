<?php

declare(strict_types=1);

namespace App\Http\Requests\Dashboard\File;

use App\Http\Requests\FormRequest;

/**
 * Class RemoveFileRequest
 * @package App\Http\Requests\Dashboard\File
 */
class RemoveFileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'url' => 'required|string',
        ];
    }
}