<?php

declare(strict_types=1);

namespace App\Application\File\UploadFile;

use App\Contract\Core\CommandInterface;
use Illuminate\Http\UploadedFile;

/**
 * Class UploadFile
 * @package App\Application\File\UploadFile
 */
class UploadFile implements CommandInterface
{
    /** @var UploadedFile $file */
    private UploadedFile $file;

    /** @var string $group */
    private string $group;

    /** @var string|null $id */
    private ?string $id;

    /**
     * UploadFile constructor
     *
     * @param UploadedFile $file
     * @param string $group
     * @param string|null $id
     */
    public function __construct(
        UploadedFile $file,
        string $group,
        ?string $id = null
    ) {
        $this->file = $file;
        $this->group = $group;
        $this->id = $id;
    }

    /**
     * @return UploadedFile
     */
    public function getFile(): UploadedFile
    {
        return $this->file;
    }

    /**
     * @return string
     */
    public function getGroup(): string
    {
        return $this->group;
    }

    /**
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->id;
    }
}