import Brand from "./Brand";
import Type from "./Type";
import Color from "./Color";
import Size from "./Size";
import Season from "./Season";
import ProductImage from "./ProductImage";
import Delivery from "./Delivery";
import PayType from "./PayType";
import Gender from "./Gender";

export default class Product {
    /**
     * @param data
     */
    constructor(data = {}) {
        this.id = data?.id ? data.id : null;
        this.title = data?.attributes && data?.attributes?.title ?
            data.attributes.title :
            null;
        this.description = data?.attributes && data?.attributes?.description ?
            data.attributes.description :
            null;
        this.slug = data?.attributes && data?.attributes?.slug ?
            data.attributes.slug :
            null;
        this.metaKey = data.attributes && data.attributes.metaKey ?
            data.attributes.metaKey :
            null;
        this.metaDescription = data.attributes && data.attributes.metaDescription ?
            data.attributes.metaDescription :
            null;
        this.purchasePrice = data.attributes && data.attributes.purchasePrice ?
            data.attributes.purchasePrice :
            null;
        this.salePrice = data.attributes && data.attributes.salePrice ?
            data.attributes.salePrice :
            null;
        this.inStock = data.attributes && data.attributes.inStock ?
            !!data.attributes.inStock :
            null;
        this.priority = data.attributes && data.attributes.priority ?
            data.attributes.priority :
            null;

        this.brand = data.relationships && data.relationships.brand ?
            new Brand(data.relationships.brand) :
            null;
        this.type = data.relationships && data.relationships.type ?
            new Type(data.relationships.type) :
            null;
        this.colors = data.relationships && data.relationships.colors ?
            data.relationships.colors.data.map((item) => {return new Color(item)}) :
            null;
        this.sizes = data.relationships && data.relationships.sizes ?
            data.relationships.sizes.data.map((item) => {return new Size(item)}) :
            null;
        this.seasons = data.relationships && data.relationships.seasons ?
            data.relationships.seasons.data.map((item) => {return new Season(item)}) :
            null;
        this.delivery = data.relationships && data.relationships.delivery ?
            data.relationships.delivery.data.map((item) => {return new Delivery(item)}) :
            null;
        this.gender = data.relationships && data.relationships.gender ?
            data.relationships.gender.data.map((item) => {return new Gender(item)}) :
            null;
        this.payTypes = data.relationships && data.relationships.payTypes ?
            data.relationships.payTypes.data.map((item) => {return new PayType(item)}) :
            null;
        this.images = data.relationships && data.relationships.images ?
            data.relationships.images.data.map((item) => {return new ProductImage(item)}) :
            null;

        this.createdAt = data.attributes && data.attributes.createdAt ?
            data.attributes.createdAt :
            null;
        this.updatedAt = data.attributes && data.attributes.updatedAt ?
            data.attributes.updatedAt :
            null;
    }
}