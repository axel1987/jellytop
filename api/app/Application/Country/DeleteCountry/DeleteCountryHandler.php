<?php

declare(strict_types=1);

namespace App\Application\Country\DeleteCountry;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\Country\CountryRepositoryInterface;
use Illuminate\Support\Facades\Storage;

/**
 * Class DeleteCountryHandler
 * @package App\Application\Country\DeleteCountry
 */
class DeleteCountryHandler implements HandlerInterface
{
    /** @var CountryRepositoryInterface $countryRepository */
    private CountryRepositoryInterface $countryRepository;

    /**
     * DeleteCountryHandler constructor
     *
     * @param CountryRepositoryInterface $countryRepository
     */
    public function __construct(CountryRepositoryInterface $countryRepository)
    {
        $this->countryRepository = $countryRepository;
    }

    /**
     * @param DeleteCountry $command
     *
     * @return void
     */
    public function handle(CommandInterface $command): void
    {
        $country = $command->getCountry();

        if ($country->icon) {
            Storage::disk('public')
                ->delete(str_replace('/storage', '', $country->icon));
        }

        $this->countryRepository->delete($country);
    }
}