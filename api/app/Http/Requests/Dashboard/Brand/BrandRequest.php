<?php

declare(strict_types=1);

namespace App\Http\Requests\Dashboard\Brand;

use App\Http\Requests\FormRequest;

/**
 * Class BrandRequest
 * @package App\Http\Requests\Dashboard\Brand
 */
class BrandRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'countryId' => 'required|numeric|exists:countries,id',
            'title' => 'required|array',
            'title.ua' => 'nullable|string',
            'title.ru' => 'nullable|string',
            'description' => 'nullable|array',
            'description.ua' => 'nullable|string',
            'description.ru' => 'nullable|string',
            'logo' => 'nullable|string',
        ];
    }
}