<?php

declare(strict_types=1);

namespace App\Http\Controllers\Admin;

use App\Application\Slider\DeleteSlider\DeleteSlider;
use App\Application\Slider\GetSliderByFilter\GetSliderByFilter;
use App\Application\Slider\GetSlidersList\GetSlidersList;
use App\Application\Slider\StoreSlider\StoreSlider;
use App\Application\Slider\UpdateSlider\UpdateSlider;
use App\Domain\Slider\SliderFilter;
use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Slider\SliderIndexRequest;
use App\Http\Requests\Dashboard\Slider\SliderRequest;
use App\Http\Resourses\Slider\SliderListResource;
use App\Http\Resourses\Slider\SliderResource;
use App\Infrastructure\Core\Pagination;
use App\Infrastructure\Core\Sorting;
use Illuminate\Http\JsonResponse;

/**
 * Class SliderController
 * @package App\Http\Controllers\Admin
 */
class SliderController extends Controller
{
    /**
     * @param SliderIndexRequest $request
     *
     * @return JsonResponse
     */
    public function index(SliderIndexRequest $request): JsonResponse
    {
        $filter = SliderFilter::fromRequest($request);
        $pagination = Pagination::fromRequest($request);
        $sorting = Sorting::fromRequest($request);

        $list = $this->execute(new GetSlidersList(
            $filter,
            $pagination,
            $sorting
        ));

        return new JsonResponse(new SliderListResource($list));
    }

    /**
     * @param int $id
     *
     * @return JsonResponse
     */
    public function view(int $id): JsonResponse
    {
        $filter = new SliderFilter();
        $filter->setId((int)$id);
        $slider = $this->execute(new GetSliderByFilter($filter));

        return new JsonResponse(new SliderResource($slider));
    }

    /**
     * @param SliderRequest $request
     *
     * @return JsonResponse
     */
    public function store(SliderRequest $request): JsonResponse
    {
        $slider = $this->execute(new StoreSlider(
            $request->get('link'),
            $request->get('text'),
        ));

        return new JsonResponse(new SliderResource($slider));
    }

    /**
     * @param SliderRequest $request
     * @param int $id
     *
     * @return JsonResponse
     */
    public function update(SliderRequest $request, int $id): JsonResponse
    {
        $filter = new SliderFilter();
        $filter->setId((int)$id);
        $slider = $this->execute(new GetSliderByFilter($filter));

        $slider = $this->execute(new UpdateSlider(
            $slider,
            $request->get('link'),
            $request->get('text'),
        ));

        return new JsonResponse(new SliderResource($slider));
    }

    /**
     * @param int $id
     *
     * @return JsonResponse
     */
    public function delete(int $id): JsonResponse
    {
        $filter = new SliderFilter();
        $filter->setId((int)$id);
        $slider = $this->execute(new GetSliderByFilter($filter));

        $this->execute(new DeleteSlider($slider));

        return new JsonResponse(null, 204);
    }
}