<?php

declare(strict_types=1);

namespace App\Http\Resourses\Auth;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class AuthDataResource
 * @package App\Http\Resources\AuthController
 */
class AuthDataResource extends JsonResource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => [
                'type' => 'authData',
                'attributes' => [
                    'tokenType' => $this['tokenType'],
                    'expiresIn' => $this['expiresIn'],
                    'accessToken' => $this['accessToken'],
                ]
            ]
        ];
    }
}
