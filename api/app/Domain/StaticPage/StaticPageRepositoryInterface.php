<?php

declare(strict_types=1);

namespace App\Domain\StaticPage;

use App\Contract\Core\DatabaseRepositoryInterface;

/**
 * Interface StaticPageRepositoryInterface
 * @package App\Domain\StaticPage
 */
interface StaticPageRepositoryInterface extends DatabaseRepositoryInterface
{

}