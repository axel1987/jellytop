<?php

declare(strict_types=1);

namespace App\Application\Brand\DeleteBrand;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\Brand\BrandRepositoryInterface;
use Illuminate\Support\Facades\Storage;

/**
 * Class DeleteBrandHandler
 * @package App\Application\Brand\DeleteBrand
 */
class DeleteBrandHandler implements HandlerInterface
{
    /** @var BrandRepositoryInterface $brandRepository */
    private BrandRepositoryInterface $brandRepository;

    /**
     * DeleteBrandHandler constructor
     *
     * @param BrandRepositoryInterface $brandRepository
     */
    public function __construct(BrandRepositoryInterface $brandRepository)
    {
        $this->brandRepository = $brandRepository;
    }

    /**
     * @param CommandInterface|DeleteBrand $command
     *
     * @return mixed|void
     */
    public function handle(CommandInterface $command): void
    {
        $brand = $command->getBrand();

        if ($brand->logo) {
            Storage::disk('public')
                ->delete(str_replace('/storage', '', $brand->logo));
        }

        $this->brandRepository->delete($brand);
    }
}