<?php

declare(strict_types=1);

namespace App\Console\Commands;

use App\Domain\Product\Product;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

/**
 * Class GenerateDefaultSeo
 * @package App\Console\Commands
 */
class GenerateDefaultSeo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'seo:generate-default';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate a default meta tags';

    /**
     * @return void
     */
    public function handle()
    {
        foreach (Product::query()->get() as $product) {
            /** @var Product $product */
            $product->meta_key = $product->makeMetaKey(true);
            $product->meta_description = $product->makeMetaDescription(true);

            $product->save();
        };

    }
}