<?php

declare(strict_types=1);

namespace App\Application\Product\UpdateProduct;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\Product\Product;
use App\Domain\Product\ProductRepositoryInterface;

/**
 * Class UpdateProductHandler
 * @package App\Application\Product\UpdateProduct
 */
class UpdateProductHandler implements HandlerInterface
{
    /** @var ProductRepositoryInterface $productRepository */
    private ProductRepositoryInterface $productRepository;

    /**
     * UpdateProductHandler constructor
     *
     * @param ProductRepositoryInterface $productRepository
     */
    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    /**
     * @param UpdateProduct $command
     *
     * @return Product
     */
    public function handle(CommandInterface $command): Product
    {
        $product = $command->getProduct();

        $product->title = $command->getTitle();
        $product->description = $command->getDescription();
        $product->meta_key = $command->getMetaKey();
        $product->meta_description = $command->getMetaDescription();

        $product->in_stock = $command->isInStock();
        $product->priority = $command->getPriority();
        $product->sale_price = $command->getSalePrice();
        $product->purchase_price = $command->getPurchasePrice();

        $this->productRepository->store($product);

        $product->brands()->sync($command->getBrandIds());
        $product->types()->sync($command->getTypeIds());
        $product->colors()->sync($command->getColorsIds());
        $product->sizes()->sync($command->getSizesIds());
        $product->seasons()->sync($command->getSeasonsIds());
        $product->delivery()->sync($command->getDeliveryIds());
        $product->genders()->sync($command->getGendersIds());
        $product->payTypes()->sync($command->getPayTypesIds());

        $product->meta_key = $product->makeMetaKey();
        $product->meta_description = $product->makeMetaDescription();

        $this->productRepository->store($product);

        return $product;
    }
}