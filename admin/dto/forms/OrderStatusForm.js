export default class OrderStatusForm {

    /**
     * @return <void>
     */
    constructor(data) {
        this.id = data.id;
        this.title = data.title ? data.title : null
    }

    /**
     * @returns {{required: (function(*=))}}
     */
    rules() {
        return {
            required: value => !!value || 'Это поле обязательно',
        }
    }
}
