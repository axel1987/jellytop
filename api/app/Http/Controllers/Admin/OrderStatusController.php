<?php

declare(strict_types=1);

namespace App\Http\Controllers\Admin;

use App\Application\OrderStatuses\DeleteOrderStatus\DeleteOrderStatus;
use App\Application\OrderStatuses\GetOrderStatusByFilter\GetOrderStatusByFilter;
use App\Application\OrderStatuses\GetOrderStatusesList\GetOrderStatusesList;
use App\Application\OrderStatuses\StoreOrderStatus\StoreOrderStatus;
use App\Application\OrderStatuses\UpdateOrderStatus\UpdateOrderStatus;
use App\Domain\OrderStatus\OrderStatusFilter;
use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\OrderStatus\OrderStatusIndexRequest;
use App\Http\Requests\Dashboard\OrderStatus\OrderStatusRequest;
use App\Http\Resourses\OrderStatuses\OrderStatusesListResource;
use App\Http\Resourses\OrderStatuses\OrderStatusResource;
use App\Infrastructure\Core\Pagination;
use App\Infrastructure\Core\Sorting;
use Illuminate\Http\JsonResponse;

/**
 * Class OrderStatusController
 * @package App\Http\Controllers\Admin
 */
class OrderStatusController extends Controller
{
    /**
     * @param OrderStatusIndexRequest $request
     *
     * @return JsonResponse
     */
    public function index(OrderStatusIndexRequest $request): JsonResponse
    {
        $filter = OrderStatusFilter::fromRequest($request);
        $pagination = Pagination::fromRequest($request);
        $sorting = Sorting::fromRequest($request);

        $list = $this->execute(new GetOrderStatusesList(
            $filter,
            $pagination,
            $sorting
        ));

        return new JsonResponse(new OrderStatusesListResource($list));
    }

    /**
     * @param OrderStatusIndexRequest $request
     *
     * @return JsonResponse
     */
    public function list(OrderStatusIndexRequest $request): JsonResponse
    {
        $filter = OrderStatusFilter::fromRequest($request);

        $list = $this->execute(new GetOrderStatusesList(
            $filter
        ));

        return new JsonResponse(new OrderStatusesListResource($list));
    }

    /**
     * @param int $id
     *
     * @return JsonResponse
     */
    public function view(int $id): JsonResponse
    {
        $filter= new OrderStatusFilter();
        $filter->setId($id);

        $orderStatus = $this->execute(new GetOrderStatusByFilter($filter));

        return new JsonResponse(new OrderStatusResource($orderStatus));
    }

    /**
     * @param OrderStatusRequest $request
     *
     * @return JsonResponse
     */
    public function store(OrderStatusRequest $request): JsonResponse
    {
        $orderStatus = $this->execute(new StoreOrderStatus(
            $request->get('title')
        ));
        
        return new JsonResponse(new OrderStatusResource($orderStatus));
    }

    /**
     * @param OrderStatusRequest $request
     * @param int $id
     *
     * @return JsonResponse
     */
    public function update(OrderStatusRequest $request, int $id): JsonResponse
    {
        $filter= new OrderStatusFilter();
        $filter->setId($id);

        $orderStatus = $this->execute(new GetOrderStatusByFilter($filter));
        
        $orderStatus = $this->execute(new UpdateOrderStatus(
            $orderStatus,
            $request->get('title')
        ));

        return new JsonResponse(new OrderStatusResource($orderStatus));
    }

    /**
     * @param int $id
     *
     * @return JsonResponse
     */
    public function delete(int $id): JsonResponse
    {
        $filter= new OrderStatusFilter();
        $filter->setId($id);

        $orderStatus = $this->execute(new GetOrderStatusByFilter($filter));
        $this->execute(new DeleteOrderStatus($orderStatus));

        return new JsonResponse(null, 204);
    }
}