import Pagination from "../dto/entities/Pagination";
import Product from "../dto/entities/Product";

/**
 * @returns {{
 * pagination: null,
 * productsList: *[],
 * product: null
 * }}
 */
export const state = () => ({
    product: null,
    productsList: [],
    pagination: null
});

/**
 * @type {{
 * getProductsList(*): [],
 * getProduct(*): null,
 * getProductsPagination(*): null
 * }}
 */
export const getters = {
    getProduct(state) {
        return state.product;
    },
    getProductsList(state) {
        return state.productsList;
    },
    getProductsPagination(state) {
        return state.pagination;
    }
};

/**
 * @type {{}}
 */

/**
 * @type {{
 * SET_PRODUCTS_PAGINATION(*, *): void,
 * SET_PRODUCT(*, *): void,
 * SET_PRODUCTS_LIST(*, *): void
 * }}
 */
export const mutations = {
    SET_PRODUCT(state, value) {
        state.product = value
    },
    SET_PRODUCTS_LIST(state, value) {
        state.productsList = value
    },
    SET_PRODUCTS_PAGINATION(state, value) {
        state.pagination = value
    },
};

/**
 * @type {{
 * fetchProductBySlug: ((function({commit: *}, *=): Promise<void>)|*),
 * fetchProduct: ((function({commit: *}, *=): Promise<void>)|*),
 * fetchProductsList: (function({commit: *}, *=): Promise<any>)
 * }}
 */
export const actions = {
    fetchProductsList: async function ({commit}, params = {}) {
        let res = await this.$axios.get('/products', {params: params});

        if (res && res.data) {
            const data = res.data;
            const list = data.data.map((productData) => {
                return new Product(productData)
            });
            const pagination = data.pagination ? new Pagination(data.pagination) : null;

            commit('SET_PRODUCTS_LIST', list)
            commit('SET_PRODUCTS_PAGINATION', pagination)
        }

        return res;
    },
    fetchProduct: async function ({commit}, params = {}) {
        if (params.id) {
            let {data} = await this.$axios.get(`/products/${params.id}`, params);
            commit('SET_PRODUCT', new Product(data))
        } else {
            commit('SET_PRODUCT', new Product({}))
        }
    },
    fetchProductBySlug: async function ({commit}, params = {}) {
        if (params.slug) {
            return await this.$axios.get(`/products/${params.slug}`, params).then((res) => {
                if (res && res.data) {
                    commit('SET_PRODUCT', new Product(res.data))
                }
                return res
            })
        } else {
            commit('SET_PRODUCT', new Product({}))
        }
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations,
};
