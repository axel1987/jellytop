<?php

declare(strict_types=1);

namespace App\Http\Resourses\ProductImage;

use App\Domain\ProductImage\ProductImage;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class ProductImageResource
 * @package App\Http\Resourses\ProductImage
 */
class ProductImageResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        /** @var ProductImage $this */
        return [
            'type' => 'product-image',
            'attributes' => [
                'url' => $this->url,
                'isMain' => $this->is_main,
            ]
        ];
    }
}