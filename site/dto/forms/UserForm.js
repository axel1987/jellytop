import {required, email} from 'vuelidate/lib/validators';

export default class UserForm {

    /**
     * @return <void>
     */
    constructor(data = {}) {
        this.id = data.id ? data.id : null;
        this.email = data.email ? data.email : null;
        this.name = data.name ? data.name : null;
        this.isAdmin = data.isAdmin ? data.isAdmin : false;
    }

    /**
     * @returns {{
     *  password: null,
     *  email: null
     * }}
     */
    model() {
        return {
            id: null,
            email: null,
            name: null,
            isAdmin: null,
        };
    }

    /**
     * @returns {{form: {password: {required}, email: {required, email}}}}
     */
    validation() {
        return {
            form: {
                email: {
                    required,
                    email,
                },
                name: {
                    required,
                },
                isAdmin: {
                    required,
                },
            },
        };
    }

    /**
     * @return <void>
     */
    reset(data = {}) {
        this.id = data.id ? data.id : null;
        this.email = data.email ? data.email : null;
        this.name = data.name ? data.name : null;
        this.isAdmin = data.isAdmin ? data.link : null;
    }
}
