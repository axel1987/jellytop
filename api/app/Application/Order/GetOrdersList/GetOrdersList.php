<?php

declare(strict_types=1);

namespace App\Application\Order\GetOrdersList;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\PaginationInterface;
use App\Contract\Core\SortingInterface;
use App\Domain\Order\OrderFilter;

/**
 * Class GetOrdersList
 * @package App\Application\Order\GetOrdersList
 */
class GetOrdersList implements CommandInterface
{
    /** @var OrderFilter $filter */
    private OrderFilter $filter;

    /** @var PaginationInterface|null $pagination */
    private ?PaginationInterface $pagination;

    /** @var SortingInterface|null $sorting */
    private ?SortingInterface $sorting;

    /**
     * GetOrdersList constructor
     *
     * @param OrderFilter $filter
     * @param PaginationInterface|null $pagination
     * @param SortingInterface|null $sorting
     */
    public function __construct(
        OrderFilter $filter,
        ?PaginationInterface $pagination = null,
        ?SortingInterface $sorting = null
    ) {
        $this->filter = $filter;
        $this->pagination = $pagination;
        $this->sorting = $sorting;
    }

    /**
     * @return OrderFilter
     */
    public function getFilter(): OrderFilter
    {
        return $this->filter;
    }

    /**
     * @return PaginationInterface|null
     */
    public function getPagination(): ?PaginationInterface
    {
        return $this->pagination;
    }

    /**
     * @return SortingInterface|null
     */
    public function getSorting(): ?SortingInterface
    {
        return $this->sorting;
    }
}