export default class StaticPageForm {

    /**
     * @return <void>
     */
    constructor(data) {
        this.id = data.id;
        this.pageName = data.pageName ? data.pageName : null;
        this.content = {
            ua: data.content && data.content.ua ? data.content.ua : null,
            ru: data.content && data.content.ru ? data.content.ru : null
        };
    }

    /**
     * @returns {{required: (function(*=))}}
     */
    rules() {
        return {
            required: value => !!value || 'Это поле обязательно',
        }
    }
}
