<?php

declare(strict_types=1);

namespace App\Application\Delivery\GetDeliveryByFilter;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\Delivery\DeliveryRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * Class GetDeliveryByFilterHandler
 * @package App\Application\Delivery\GetDeliveryByFilter
 */
class GetDeliveryByFilterHandler implements HandlerInterface
{
    /** @var DeliveryRepositoryInterface $deliveryRepository */
    private DeliveryRepositoryInterface $deliveryRepository;

    /**
     * GetDeliveryByFilterHandler constructor
     *
     * @param DeliveryRepositoryInterface $deliveryRepository
     */
    public function __construct(DeliveryRepositoryInterface $deliveryRepository)
    {
        $this->deliveryRepository = $deliveryRepository;
    }

    /**
     * @param GetDeliveryByFilter $command
     *
     * @return Model|null
     */
    public function handle(CommandInterface $command): ?Model
    {
        return $this->deliveryRepository->one(
            $command->getFilter()
        );
    }
}