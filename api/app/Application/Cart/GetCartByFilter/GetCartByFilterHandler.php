<?php

declare(strict_types=1);

namespace App\Application\Cart\GetCartByFilter;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\Cart\CartRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * Class GetCartByFilterHandler
 * @package App\Application\Cart\GetCartByFilter
 */
class GetCartByFilterHandler implements HandlerInterface
{
    /** @var CartRepositoryInterface $cartRepository */
    private CartRepositoryInterface $cartRepository;

    /**
     * GetCartByFilterHandler constructor
     *
     * @param CartRepositoryInterface $cartRepository
     */
    public function __construct(CartRepositoryInterface $cartRepository)
    {
        $this->cartRepository = $cartRepository;
    }

    /**
     * @param CommandInterface|GetCartByFilter $command
     *
     * @return Model|null
     */
    public function handle(CommandInterface $command): ?Model
    {
        return $this->cartRepository->one(
            $command->getFilter()
        );
    }
}