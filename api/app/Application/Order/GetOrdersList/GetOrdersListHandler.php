<?php

declare(strict_types=1);

namespace App\Application\Order\GetOrdersList;

use App\Application\Order\GetOrderByFilter\GetOrderByFilter;
use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\Order\OrderRepositoryInterface;

/**
 * Class GetOrdersListHandler
 * @package App\Application\Order\GetOrdersList
 */
class GetOrdersListHandler implements HandlerInterface
{
    /** @var OrderRepositoryInterface $orderRepository */
    private OrderRepositoryInterface $orderRepository;

    /**
     * GetOrdersListHandler constructor
     *
     * @param OrderRepositoryInterface $orderRepository
     */
    public function __construct(OrderRepositoryInterface $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    /**
     * @param CommandInterface|GetOrdersList $command
     *
     * @return mixed|void
     */
    public function handle(CommandInterface $command)
    {
        return $this->orderRepository->all(
            $command->getFilter(),
            $command->getPagination(),
            $command->getSorting()
        );
    }
}