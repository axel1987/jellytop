<?php

declare(strict_types=1);

namespace App\Application\Order\GetOrderByFilter;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\Order\Order;
use App\Domain\Order\OrderRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * Class GetOrderByFilterHandler
 * @package App\Application\Order\GetOrderByFilter
 */
class GetOrderByFilterHandler implements HandlerInterface
{
    /** @var OrderRepositoryInterface $orderRepository */
    private OrderRepositoryInterface $orderRepository;

    /**
     * GetOrderByFilterHandler constructor
     *
     * @param OrderRepositoryInterface $orderRepository
     */
    public function __construct(OrderRepositoryInterface $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    /**
     * @param CommandInterface|GetOrderByFilter $command
     *
     * @return Order|null
     */
    public function handle(CommandInterface $command): ?Model
    {
        return $this->orderRepository->one(
            $command->getFilter()
        );
    }
}