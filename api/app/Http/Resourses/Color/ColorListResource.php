<?php

declare(strict_types=1);

namespace App\Http\Resourses\Color;

use App\Domain\Color\Color;
use App\Http\Resourses\BaseCollectionResource;

/**
 * Class ColorListResource
 * @package App\Http\Resourses\Color
 */
class ColorListResource extends BaseCollectionResource
{
    /**
     * @param Color $item
     *
     * @return array
     */
    protected function getItemData($item): array
    {
        return [
            'id' => $item->id,
            'type' => 'color',
            'attributes' => [
                'title' => $item->title,
                'slug' => $item->slug,
                'hex' => $item->hex,
                'createdAt' => $item->created_at ? $item->created_at->format('Y-m-d H:i:s') : '',
                'updatedAt' => $item->updated_at ? $item->updated_at->format('Y-m-d H:i:s') : '',
            ]
        ];
    }
}