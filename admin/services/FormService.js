export const FormService = {
  /**
   * @param form
   * @param httpErrors
   * @param fieldName
   * @returns {{'md-invalid': ((function(): *)|*|(function(): (boolean|*))|(function(): (boolean|*)))}|{'md-invalid': boolean}}
   */
  getValidationClass (form, httpErrors, fieldName) {
    const field = form[fieldName];
    const hasHttpError = (httpErrors.fields && httpErrors.fields[fieldName]) || httpErrors.message;

    if (hasHttpError) {
      return {
        'md-invalid': true,
      };
    }
    if (field) {
      return {
        'md-invalid': field.$invalid && field.$dirty,
      };
    }
  }

}
