<?php

declare(strict_types=1);

namespace App\Domain\Slider;

use App\Contract\Core\FilterInterface;
use Illuminate\Http\Request;

/**
 * Class SliderFilter
 * @package App\Domain\Slider
 */
class SliderFilter implements FilterInterface
{
    /** @var int|null $id */
    private ?int $id = null;

    /**
     * @param Request $request
     *
     * @return FilterInterface|SliderFilter
     */
    public static function fromRequest(Request $request): FilterInterface
    {
        $filter = new self();

        return $filter;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id): void
    {
        $this->id = $id;
    }
}