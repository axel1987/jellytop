<?php

declare(strict_types=1);

namespace App\Http\Resourses\Slider;

use App\Domain\Slider\Slider;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class SliderResource
 * @package App\Http\Resourses\Slider
 */
class SliderResource extends JsonResource
{
    /**
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        /** @var Slider $this */
        return [
            'id' => $this->id,
            'type' => 'slider',
            'attributes' => [
                'link' => $this->link,
                'text' => $this->text,
                'createdAt' => $this->created_at ? $this->created_at->format('Y-m-d H:i:s') : '',
                'updatedAt' => $this->updated_at ? $this->updated_at->format('Y-m-d H:i:s') : '',
            ]
        ];
    }
}