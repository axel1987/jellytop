<?php

declare(strict_types=1);

namespace App\Http\Resourses\OrderStatuses;

use App\Domain\OrderStatus\OrderStatus;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class OrderStatusResource
 * @package App\Http\Resourses\OrderStatuses
 */
class OrderStatusResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        /** @var OrderStatus $this */
        return [
            'id' => $this->id,
            'type' => 'order-status',
            'attributes' => [
                'title' => $this->title,
                'createdAt' => $this->created_at ? $this->created_at->format('Y-m-d H:i:s') : '',
                'updatedAt' => $this->updated_at ? $this->updated_at->format('Y-m-d H:i:s') : '',
            ]
        ];
    }
}