<?php

declare(strict_types=1);

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

/**
 * Class SeasonsTableSeeder
 */
class SeasonsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now();

        DB::table('seasons')->insert([
            [
                'title' => json_encode([
                    'ua' => 'Зима',
                    'ru' => 'Зима'
                ]),
                'slug' => 'winter',
                'created_at' => $now,
                'updated_at' => $now,
            ],
            [
                'title' => json_encode([
                    'ua' => 'Весна',
                    'ru' => 'Весна'
                ]),
                'slug' => 'spring',
                'created_at' => $now,
                'updated_at' => $now,
            ],
            [
                'title' => json_encode([
                    'ua' => 'Лiто',
                    'ru' => 'Лето'
                ]),
                'slug' => 'summer',
                'created_at' => $now,
                'updated_at' => $now,
            ],
            [
                'title' => json_encode([
                    'ua' => 'Осiнь',
                    'ru' => 'Осень'
                ]),
                'slug' => 'autumn',
                'created_at' => $now,
                'updated_at' => $now,
            ],
        ]);
    }
}