<?php

declare(strict_types=1);

namespace App\Application\Gender\DeleteGender;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\Gender\GenderRepositoryInterface;
use Illuminate\Support\Facades\Storage;

/**
 * Class DeleteGenderHandler
 * @package App\Application\Gender\DeleteGender
 */
class DeleteGenderHandler implements HandlerInterface
{
    /** @var GenderRepositoryInterface $genderRepository */
    private GenderRepositoryInterface $genderRepository;

    /**
     * DeleteGenderHandler constructor
     *
     * @param GenderRepositoryInterface $genderRepository
     */
    public function __construct(GenderRepositoryInterface $genderRepository)
    {
        $this->genderRepository = $genderRepository;
    }

    /**
     * @param DeleteGender $command
     *
     * @return void
     */
    public function handle(CommandInterface $command): void
    {
        $gender = $command->getGender();

        Storage::disk('public')
            ->delete(str_replace('/storage', '', $gender->icon));

        $this->genderRepository->delete($gender);
    }
}