import Season from "../dto/entities/Season";
import Pagination from "../dto/entities/Pagination";

/**
 * @returns {{
 * season: null,
 * seasonsList: *[],
 * seasonsSelectList: *[],
 * pagination: null,
 * }}
 */
export const state = () => ({
    season: null,
    seasonsList: [],
    seasonsSelectList: [],
    pagination: null
});

/**
 *
 * @type {{
 * getSeason(*): null,
 * getSeasonsList(*): [],
 * getSeasonsSelectList(*): [],
 * getSeasonPagination(*): null
 * }}
 */
export const getters = {
    getSeason(state) {
        return state.season;
    },
    getSeasonsList(state) {
        return state.seasonsList;
    },
    getSeasonsSelectList(state) {
        return state.seasonsSelectList;
    },
    getSeasonPagination(state) {
        return state.pagination;
    }
};

/**
 * @type {{
 * SET_SEASONS_LIST(*, *): void,
 * SET_SEASONS_SELECT_LIST(*, *): void,
 * SET_SEASON(*, *): void,
 * SET_SEASONS_PAGINATION(*, *): void,
 * }}
 */
export const mutations = {
    SET_SEASON(state, value) {
        state.season = value
    },
    SET_SEASONS_LIST(state, value) {
        state.seasonsList = value
    },
    SET_SEASONS_SELECT_LIST(state, value) {
        state.seasonsSelectList = value
    },
    SET_SEASONS_PAGINATION(state, value) {
        state.pagination = value
    },
};

/**
 * @type {{
 * fetchSeason: ((function({commit: *}, *=): Promise<void>)|*),
 * fetchSeasonsList: ((function({commit: *}, *=): Promise<void>)|*),
 * fetchSeasonsSelectList: ((function({commit: *}, *=): Promise<void>)|*),
 * }}
 */
export const actions = {
    fetchSeasonsList: async function ({commit}, params = {}) {
        let {data} = await this.$axios.get('/seasons', {params: params});

        const list = data.data.map((seasonData) => {
            return new Season(seasonData)
        });
        const pagination = data.pagination ? new Pagination(data.pagination) : null;

        commit('SET_SEASONS_LIST', list)
        commit('SET_SEASONS_PAGINATION', pagination)
    },
    fetchSeasonsSelectList: async function ({commit}, params = {}) {
        let res = await this.$axios.get('/seasons/list', params);

        if (res && res.data) {
            const list = res.data.data.map((seasonData) => {
                return new Season(seasonData)
            });

            commit('SET_SEASONS_SELECT_LIST', list)
        }

        return res;
    },
    fetchSeason: async function ({commit}, params = {}) {
        if (params.id) {
            let {data} = await this.$axios.get(`/seasons/${params.id}`, params);
            commit('SET_SEASON', new Season(data))
        } else {
            commit('SET_SEASON', new Season(null))
        }
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations,
};
