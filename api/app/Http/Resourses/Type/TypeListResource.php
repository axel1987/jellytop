<?php

declare(strict_types=1);

namespace App\Http\Resourses\Type;

use App\Domain\Brand\Brand;
use App\Domain\Type\Type;
use App\Http\Resourses\BaseCollectionResource;
use App\Http\Resourses\Country\CountryResource;

/**
 * Class TypeListResource
 * @package App\Http\Resourses\Type
 */
class TypeListResource extends BaseCollectionResource
{
    /**
     * @param Type $item
     *
     * @return array
     */
    protected function getItemData($item): array
    {
        return [
            'id' => $item->id,
            'type' => 'type',
            'attributes' => [
                'title' => $item->title,
                'slug' => $item->slug,
                'description' => $item->description,
                'icon' => $item->icon,
                'createdAt' => $item->created_at ? $item->created_at->format('Y-m-d H:i:s') : '',
                'updatedAt' => $item->updated_at ? $item->updated_at->format('Y-m-d H:i:s') : '',
            ]
        ];
    }
}