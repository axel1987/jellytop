import Gender from "../dto/entities/Gender";
import Pagination from "../dto/entities/Pagination";

/**
 * @returns {{
 * gender: null,
 * gendersList: *[],
 * gendersSelectList: *[],
 * pagination: null,
 * }}
 */
export const state = () => ({
    gender: null,
    gendersList: [],
    gendersSelectList: [],
    pagination: null
});

/**
 *
 * @type {{
 * getGendersList(*): [],
 * getGendersSelectList(*): [],
 * }}
 */
export const getters = {
    getGendersList(state) {
        return state.gendersList;
    },
    getGendersSelectList(state) {
        return state.gendersSelectList;
    }
};

/**
 * @type {{
 * SET_GENDERS_LIST(*, *): void,
 * SET_GENDERS_SELECT_LIST(*, *): void,
 * }}
 */
export const mutations = {
    SET_GENDERS_LIST(state, value) {
        state.gendersList = value
    },
    SET_GENDERS_SELECT_LIST(state, value) {
        state.gendersSelectList = value
    },
};

/**
 * @type {{
 * fetchGendersList: ((function({commit: *}, *=): Promise<void>)|*),
 * fetchGendersSelectList: ((function({commit: *}, *=): Promise<void>)|*),
 * }}
 */
export const actions = {
    fetchGendersList: async function ({commit}, params = {}) {
        let {data} = await this.$axios.get('/genders', params);

        const list = data.data.map((genderData) => {
            return new Gender(genderData)
        });
        const pagination = data.pagination ? new Pagination(data.pagination) : null;

        commit('SET_GENDERS_LIST', list)
        commit('SET_GENDERS_PAGINATION', pagination)
    },
    fetchGendersSelectList: async function ({commit}, params = {}) {
        let res = await this.$axios.get('/genders/list', {params: params});

        if (res && res.data) {
            const data = res.data
            const list = data.data.map((genderData) => {
                return new Gender(genderData)
            });

            commit('SET_GENDERS_SELECT_LIST', list)
        }
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations,
};
