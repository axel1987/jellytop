<?php

declare(strict_types=1);

namespace App\Application\OrderStatuses\GetOrderStatusByFilter;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\OrderStatus\OrderStatus;
use App\Infrastructure\DatabaseRepository\OrderStatusRepository;
use Illuminate\Database\Eloquent\Model;

/**
 * Class GetOrderStatusByFilterHandler
 * @package App\Application\OrderStatuses\GetOrderStatusByFilter
 */
class GetOrderStatusByFilterHandler implements HandlerInterface
{
    /** @var OrderStatusRepository $orderStatusRepository */
    private OrderStatusRepository $orderStatusRepository;

    /**
     * UpdateOrderStatusHandler constructor
     *
     * @param OrderStatusRepository $orderStatusRepository
     */
    public function __construct(OrderStatusRepository $orderStatusRepository)
    {
        $this->orderStatusRepository = $orderStatusRepository;
    }

    /**
     * @param CommandInterface|GetOrderStatusByFilter $command
     *
     * @return OrderStatus|null
     */
    public function handle(CommandInterface $command): ?Model
    {
        return $this->orderStatusRepository->one(
            $command->getFilter()
        );
    }
}