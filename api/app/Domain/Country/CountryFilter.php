<?php

declare(strict_types=1);

namespace App\Domain\Country;

use App\Contract\Core\FilterInterface;
use Illuminate\Http\Request;

/**
 * Class CountryFilter
 * @package App\Domain\Country
 */
class CountryFilter implements FilterInterface
{
    /** @var int|null $id */
    private ?int $id = null;

    /**
     * @param Request $request
     *
     * @return CountryFilter|FilterInterface
     */
    public static function fromRequest(Request $request): FilterInterface
    {
        $filter = new self();

        return $filter;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id): void
    {
        $this->id = $id;
    }
}