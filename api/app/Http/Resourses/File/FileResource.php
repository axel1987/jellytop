<?php

declare(strict_types=1);

namespace App\Http\Resourses\File;

use App\Domain\Country\Country;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class FileResource
 * @package App\Http\Resourses\File
 */
class FileResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'type' => 'file',
            'attributes' => [
                'url' => $this->resource,
            ]
        ];
    }
}