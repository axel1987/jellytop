<?php

declare(strict_types=1);

namespace App\Http\Resourses\StaticPage;

use App\Domain\StaticPage\StaticPage;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class StaticPageResoutce
 * @package App\Http\Resourses\StaticPage
 */
class StaticPageResource extends JsonResource
{
    /**
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        /** @var StaticPage $this */
        return [
            'id' => $this->id,
            'type' => 'static-page',
            'attributes' => [
                'page' => $this->page,
                'pageName' => StaticPage::STATIC_PAGES_LIST[$this->page],
                'content' => $this->content,
                'createdAt' => $this->created_at ? $this->created_at->format('Y-m-d H:i:s') : '',
                'updatedAt' => $this->updated_at ? $this->updated_at->format('Y-m-d H:i:s') : '',
            ]
        ];
    }
}