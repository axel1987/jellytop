import ProductFilter from "@/dto/forms/ProductFilter";

/**
 * @returns {{
 * productFilter: {sizeId: null, search: null, seasonId: null, colorId: null, brandId: null, genderId: null, typeId: null}
 * }}
 */
export const state = () => ({
    productFilter : new ProductFilter({})
});

/**
 * @type {{
 * getProductFilter(*): {sizeId: null, search: null, seasonId: null, colorId: null, brandId: null, genderId: null, typeId: null},
 * getOrderFilter(*): {search: null, deliveryId: null, statusId: null, payTypeId: null}
 * }}
 */
export const getters = {
    getProductFilter(state) {
        return state.productFilter;
    },
};

/**
 * @type {{
 * SET_PRODUCT_FILTER(*, *): void
 * }}
 */
export const mutations = {
    SET_PRODUCT_FILTER(state, value) {
        state.productFilter = value
    },
};