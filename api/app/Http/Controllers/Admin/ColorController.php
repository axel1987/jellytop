<?php

declare(strict_types=1);

namespace App\Http\Controllers\Admin;

use App\Application\Color\DeleteColor\DeleteColor;
use App\Application\Color\GetColorByFilter\GetColorByFilter;
use App\Application\Color\GetColorsList\GetColorsList;
use App\Application\Color\StoreColor\StoreColor;
use App\Application\Color\UpdateColor\UpdateColor;
use App\Domain\Color\ColorFilter;
use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Color\ColorIndexRequest;
use App\Http\Requests\Dashboard\Color\ColorRequest;
use App\Http\Resourses\Color\ColorListResource;
use App\Http\Resourses\Color\ColorResource;
use App\Infrastructure\Core\Pagination;
use App\Infrastructure\Core\Sorting;
use Illuminate\Http\JsonResponse;

/**
 * Class ColorController
 * @package App\Http\Controllers
 */
class ColorController extends Controller
{
    /**
     * @param ColorIndexRequest $request
     *
     * @return JsonResponse
     */
    public function index(ColorIndexRequest $request): JsonResponse
    {
        $filter = ColorFilter::fromRequest($request);
        $pagination = Pagination::fromRequest($request);
        $sorting = Sorting::fromRequest($request);

        $list = $this->execute(new GetColorsList(
            $filter,
            $pagination,
            $sorting
        ));

        return new JsonResponse(new ColorListResource($list));
    }

    /**
     * @param ColorIndexRequest $request
     *
     * @return JsonResponse
     */
    public function list(ColorIndexRequest $request): JsonResponse
    {
        $filter = ColorFilter::fromRequest($request);
        $sorting = Sorting::fromRequest($request);

        $list = $this->execute(new GetColorsList(
            $filter,
            null,
            $sorting
        ));

        return new JsonResponse(new ColorListResource($list));
    }

    /**
     * @param int $id
     *
     * @return JsonResponse
     */
    public function view(int $id): JsonResponse
    {
        $filter = new ColorFilter();
        $filter->setId($id);

        $color = $this->execute(new GetColorByFilter($filter));

        return new JsonResponse(new ColorResource($color));
    }

    /**
     * @param ColorRequest $request
     *
     * @return JsonResponse
     */
    public function store(ColorRequest $request): JsonResponse
    {
        $color = $this->execute(new StoreColor(
            $request->get('title'),
            $request->get('slug'),
            $request->get('hex')
        ));

        return new JsonResponse(new ColorResource($color));
    }

    /**
     * @param ColorRequest $request
     * @param int $id
     *
     * @return JsonResponse
     */
    public function update(ColorRequest $request, int $id): JsonResponse
    {
        $filter = new ColorFilter();
        $filter->setId($id);

        $color = $this->execute(new GetColorByFilter($filter));
        $color = $this->execute(new UpdateColor(
            $color,
            $request->get('title'),
            $request->get('slug'),
            $request->get('hex')
        ));

        return new JsonResponse(new ColorResource($color));
    }

    /**
     * @param int $id
     *
     * @return JsonResponse
     */
    public function delete(int $id): JsonResponse
    {
        $filter = new ColorFilter();
        $filter->setId($id);

        $color = $this->execute(new GetColorByFilter($filter));
        $this->execute(new DeleteColor($color));

        return new JsonResponse(null, 204);
    }
}