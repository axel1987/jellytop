<?php

declare(strict_types=1);

namespace App\Application\Season\UpdateSeason;

use App\Contract\Core\CommandInterface;
use App\Domain\Season\Season;

/**
 * Class UpdateSeason
 * @package App\Application\Season\UpdateSeason
 */
class UpdateSeason implements CommandInterface
{
    /** @var Season $season */
    private Season $season;

    /** @var array $title */
    private array $title;

    /** @var string $slug */
    private string $slug;

    /** @var string|null $icon */
    private ?string $icon;

    /**
     * UpdateSeason constructor
     *
     * @param Season $season
     * @param array $title
     * @param string $slug
     * @param string|null $icon
     */
    public function __construct(
        Season $season,
        array $title,
        string $slug,
        ?string $icon
    ) {
        $this->season = $season;
        $this->title = $title;
        $this->slug = $slug;
        $this->icon = $icon;
    }

    /**
     * @return Season
     */
    public function getSeason(): Season
    {
        return $this->season;
    }

    /**
     * @return array
     */
    public function getTitle(): array
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @return string|null
     */
    public function getIcon(): ?string
    {
        return $this->icon;
    }
}