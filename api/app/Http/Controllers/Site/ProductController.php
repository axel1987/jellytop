<?php

declare(strict_types=1);

namespace App\Http\Controllers\Site;

use App\Application\Product\CreateProduct\CreateProduct;
use App\Application\Product\DeleteProduct\DeleteProduct;
use App\Application\Product\GetProductByFilter\GetProductByFilter;
use App\Application\Product\GetProductsList\GetProductsList;
use App\Application\Product\SyncProductImages\SyncProductImages;
use App\Application\Product\UpdateProduct\UpdateProduct;
use App\Domain\Product\Product;
use App\Domain\Product\ProductFilter;
use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Product\ProductIndexRequest;
use App\Http\Requests\Dashboard\Product\ProductRequest;
use App\Http\Resourses\Product\ProductListResource;
use App\Http\Resourses\Product\ProductResource;
use App\Http\Resourses\Product\SiteProductListResource;
use App\Http\Resourses\Product\SiteProductResource;
use App\Infrastructure\Core\Pagination;
use App\Infrastructure\Core\Sorting;
use Illuminate\Http\JsonResponse;

/**
 * Class ProductController
 * @package App\Http\Controllers
 */
class ProductController extends Controller
{
    /**
     * @param ProductIndexRequest $request
     *
     * @return JsonResponse
     */
    public function index(ProductIndexRequest $request): JsonResponse
    {
        $filter = ProductFilter::fromRequest($request);
        $filter->setInStock(true);
        $pagination = Pagination::fromRequest($request);
        $sorting = new Sorting('priority', Sorting::DIRECTION_DESC);

        $list = $this->execute(new GetProductsList(
            $filter,
            $pagination,
            $sorting
        ));

        return new JsonResponse(new SiteProductListResource($list));
    }

    /**
     * @param ProductIndexRequest $request
     *
     * @return JsonResponse
     */
    public function list(ProductIndexRequest $request): JsonResponse
    {
        $filter = ProductFilter::fromRequest($request);
        $sorting = Sorting::fromRequest($request);

        $list = $this->execute(new GetProductsList(
            $filter,
            null,
            $sorting
        ));

        return new JsonResponse(new SiteProductListResource($list));
    }

    /**
     * @param int $id
     *
     * @return JsonResponse
     */
    public function view(int $id): JsonResponse
    {
        $filter = new ProductFilter();
        $filter->setId($id);

        $product = $this->execute(new GetProductByFilter($filter));

        return new JsonResponse(new SiteProductResource($product));
    }

    /**
     * @param string $slug
     *
     * @return JsonResponse
     */
    public function slug(string $slug): JsonResponse
    {
        $filter = new ProductFilter();
        $filter->setSlug($slug);

        $product = $this->execute(new GetProductByFilter($filter));

        return new JsonResponse(new SiteProductResource($product));
    }
}