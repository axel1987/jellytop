<?php

declare(strict_types=1);

namespace App\Application\Season\StoreSeason;

use App\Contract\Core\CommandInterface;

/**
 * Class StoreSeason
 * @package App\Application\Season\StoreSeason
 */
class StoreSeason implements CommandInterface
{
    /** @var array $title */
    private array $title;

    /** @var string $slug */
    private string $slug;

    /** @var string|null $icon */
    private ?string $icon;

    /**
     * StoreSeason constructor
     *
     * @param array $title
     * @param string $slug
     * @param string|null $icon
     */
    public function __construct(
        array $title,
        string $slug,
        ?string $icon
    ) {
        $this->title = $title;
        $this->slug = $slug;
        $this->icon = $icon;
    }

    /**
     * @return array
     */
    public function getTitle(): array
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @return string|null
     */
    public function getIcon(): ?string
    {
        return $this->icon;
    }
}