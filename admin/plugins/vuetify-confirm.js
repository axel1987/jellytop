import Vue from 'vue'
import VuetifyConfirm from 'vuetify-confirm'

export default ({app}) => {
    Vue.use(VuetifyConfirm, {
        vuetify: app.vuetify,
        buttonTrueText: 'ДА',
        buttonFalseText: 'НЕТ',
        color: 'red',
        icon: 'mdi-alert',
        title: 'ВНИМАНИЕ!!!!',
        width: 350,
        property: '$confirm'
    })
}