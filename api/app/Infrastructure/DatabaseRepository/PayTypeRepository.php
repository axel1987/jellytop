<?php

declare(strict_types=1);

namespace App\Infrastructure\DatabaseRepository;

use App\Contract\Core\DatabaseRepositoryInterface;
use App\Contract\Core\FilterInterface;
use App\Contract\Core\SortingInterface;
use App\Domain\PayType\PayType;
use App\Domain\PayType\PayTypeRepositoryInterface;

/**
 * Class PayTypeRepository
 * @package App\Infrastructure\DatabaseRepository
 */
class PayTypeRepository extends DatabaseRepository implements PayTypeRepositoryInterface
{
    /**
     * PayTypeRepository constructor
     */
    public function __construct()
    {
        $this->model = new PayType();
    }

    /**
     * @param FilterInterface $filter
     *
     * @return $this
     */
    protected function filter(FilterInterface $filter): DatabaseRepositoryInterface
    {
        $this->builder = $this->builder ?? $this->model->newQuery();

        if ($filter->getId()) {
            $this->builder->where('id', '=', $filter->getId());
        }

        return $this;
    }

    /**
     * @param SortingInterface $sorting
     *
     * @return $this
     */
    protected function sorting(SortingInterface $sorting): DatabaseRepositoryInterface
    {
        return $this;
    }
}