<?php

declare(strict_types=1);

namespace App\Application\OrderStatuses\UpdateOrderStatus;

use App\Contract\Core\CommandInterface;
use App\Domain\OrderStatus\OrderStatus;

/**
 * Class UpdateOrderStatus
 * @package App\Application\OrderStatuses\UpdateOrderStatus
 */
class UpdateOrderStatus implements CommandInterface
{
    /** @var OrderStatus $orderStatus */
    private OrderStatus $orderStatus;

    /** @var string $title */
    private string $title;

    /**
     * UpdateOrderStatus constructor
     *
     * @param OrderStatus $orderStatus
     * @param string $title
     */
    public function __construct(
        OrderStatus $orderStatus,
        string $title
    ) {
        $this->orderStatus = $orderStatus;
        $this->title = $title;
    }

    /**
     * @return OrderStatus
     */
    public function getOrderStatus(): OrderStatus
    {
        return $this->orderStatus;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }
}