<?php

declare(strict_types=1);

namespace App\Application\Type\UpdateType;

use App\Contract\Core\CommandInterface;
use App\Domain\Type\Type;

/**
 * Class UpdateType
 * @package App\Application\Type\UpdateType
 */
class UpdateType implements CommandInterface
{
    /** @var Type $type */
    private Type $type;

    /** @var array $title */
    private array $title;

    /** @var string $slug */
    private string $slug;

    /** @var array|null $description */
    private ?array $description;

    /** @var string|null $icon */
    private ?string $icon;

    /**
     * UpdateType constructor
     *
     * @param Type $type
     * @param array $title
     * @param string $slug
     * @param array|null $description
     * @param string|null $icon
     */
    public function __construct(
        Type $type,
        array $title,
        string $slug,
        array $description = null,
        ?string $icon = null
    ) {
        $this->type = $type;
        $this->title = $title;
        $this->slug = $slug;
        $this->description = $description;
        $this->icon = $icon;
    }

    /**
     * @return Type
     */
    public function getType(): Type
    {
        return $this->type;
    }

    /**
     * @return array
     */
    public function getTitle(): array
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @return array|null
     */
    public function getDescription(): ?array
    {
        return $this->description;
    }

    /**
     * @return string|null
     */
    public function getIcon(): ?string
    {
        return $this->icon;
    }

}