<?php

declare(strict_types=1);

namespace App\Application\StaticPage\StoreStaticPage;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\StaticPage\StaticPage;
use App\Domain\StaticPage\StaticPageRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * Class StoreStaticPageHandler
 * @package App\Application\StaticPage\StoreStaticPage
 */
class StoreStaticPageHandler implements HandlerInterface
{
    /** @var StaticPageRepositoryInterface $staticPageRepository */
    private StaticPageRepositoryInterface $staticPageRepository;

    /**
     * StoreStaticPageHandler constructor
     *
     * @param StaticPageRepositoryInterface $staticPageRepository
     */
    public function __construct(StaticPageRepositoryInterface $staticPageRepository)
    {
        $this->staticPageRepository = $staticPageRepository;
    }

    /**
     * @param CommandInterface|StoreStaticPage $command
     *
     * @return StaticPage
     */
    public function handle(CommandInterface $command): Model
    {
        $staticPage = new StaticPage();
        $staticPage->page = $command->getPageName();
        $staticPage->content = $command->getContent();

        $this->staticPageRepository->store($staticPage);

        return $staticPage;
    }
}