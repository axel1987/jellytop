<?php

declare(strict_types=1);

namespace App\Infrastructure\DatabaseRepository;

use App\Contract\Core\DatabaseRepositoryInterface;
use App\Contract\Core\FilterInterface;
use App\Contract\Core\SortingInterface;
use App\Domain\Country\Country;
use App\Domain\Country\CountryFilter;
use App\Domain\Country\CountryRepositoryInterface;

/**
 * Class CountryRepository
 * @package App\Infrastructure\DatabaseRepository
 */
class CountryRepository extends DatabaseRepository implements CountryRepositoryInterface
{
    /**
     * CountryRepository constructor
     */
    public function __construct()
    {
        $this->model = new Country();
    }

    /**
     * @param CountryFilter|FilterInterface $filter
     *
     * @return $this
     */
    protected function filter(FilterInterface $filter): DatabaseRepositoryInterface
    {
        $this->builder = $this->builder ?? $this->model->newQuery();

        if ($filter->getId()) {
            $this->builder->where('id', '=', $filter->getId());
        }

        return $this;
    }

    /**
     * @param SortingInterface $sorting
     *
     * @return $this
     */
    protected function sorting(SortingInterface $sorting): DatabaseRepositoryInterface
    {
        return $this;
    }
}