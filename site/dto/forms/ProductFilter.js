export default class ProductFilter {

    /**
     * @return <void>
     */
    constructor(data = {}) {
        this.minPrice = data.minPrice ? data.minPrice : '';
        this.maxPrice = data.maxPrice ? data.maxPrice : '';
        this.brands = data.brands && data.brands.length
            ? (Array.isArray(data.brands) ? data.brands : [data.brands])
            : [];
        this.types = data.types && data.types.length
            ? (Array.isArray(data.types) ? data.types : [data.types])
            : [];
        this.seasons = data.seasons && data.seasons.length
            ? (Array.isArray(data.seasons) ? data.seasons : [data.seasons])
            : [];
        this.sizes = data.sizes && data.sizes.length
            ? (Array.isArray(data.sizes) ? data.sizes : [data.sizes])
            : [];
        this.colors = data.colors && data.colors.length
            ? (Array.isArray(data.colors) ? data.colors : [data.colors])
            : [];
        this.genders = data.genders && data.genders.length
            ? (Array.isArray(data.genders) ? data.genders : [data.genders])
            : [];
        this.page = data.page ? data.page : 1;
        this.perPage = data.perPage ? data.perPage : 12;
    }

    /**
     * @returns {{required: (function(*=))}}
     */
    rules() {
        return {
            required: value => !!value || 'Это поле обязательно',
        }
    }
}
