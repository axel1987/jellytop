<?php

declare(strict_types=1);

namespace App\Application\File\UploadFile;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use Illuminate\Support\Facades\Storage;

/**
 * Class UploadFileHandler
 * @package App\Application\File\UploadFile
 */
class UploadFileHandler implements HandlerInterface
{
    /**
     * @param UploadFile $command
     *
     * @return string
     */
    public function handle(CommandInterface $command): string
    {
        $filename = uniqid() . '.' . $command->getFile()->getClientOriginalExtension();

        if (!is_null($command->getId()) && !empty($command->getId())) {
            $storagePath = sprintf('/public/img/%s/%s',
                $command->getGroup(),
                $command->getId()
            );
            $filePath = sprintf('/img/%s/%s/%s',
                $command->getGroup(),
                $command->getId(),
                $filename
            );
        } else {
            $storagePath = sprintf('/public/img/%s',
                $command->getGroup()
            );
            $filePath = sprintf('/img/%s/%s',
                $command->getGroup(),
                $filename
            );
        }

        $command->getFile()->storeAs($storagePath, $filename);

        return Storage::disk('public')->url($filePath);
    }
}