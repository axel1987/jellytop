<?php

declare(strict_types=1);

namespace App\Application\StaticPage\GetStaticPagesList;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\PaginationInterface;
use App\Contract\Core\SortingInterface;
use App\Domain\StaticPage\StaticPageFilter;

/**
 * Class GetStaticPagesList
 * @package App\Application\StaticPage\GetStaticPagesList
 */
class GetStaticPagesList implements CommandInterface
{
    /** @var StaticPageFilter $filter */
    private StaticPageFilter $filter;

    /** @var PaginationInterface|null $pagination */
    private ?PaginationInterface $pagination;

    /** @var SortingInterface|null $sorting */
    private ?SortingInterface $sorting;

    /**
     * GetStaticPagesList constructor
     *
     * @param StaticPageFilter $filter
     * @param PaginationInterface|null $pagination
     * @param SortingInterface|null $sorting
     */
    public function __construct(
        StaticPageFilter $filter,
        ?PaginationInterface $pagination = null,
        ?SortingInterface $sorting = null
    ) {
        $this->filter = $filter;
        $this->pagination = $pagination;
        $this->sorting = $sorting;
    }

    /**
     * @return StaticPageFilter
     */
    public function getFilter(): StaticPageFilter
    {
        return $this->filter;
    }

    /**
     * @return PaginationInterface|null
     */
    public function getPagination(): ?PaginationInterface
    {
        return $this->pagination;
    }

    /**
     * @return SortingInterface|null
     */
    public function getSorting(): ?SortingInterface
    {
        return $this->sorting;
    }
}