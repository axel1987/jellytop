<?php

declare(strict_types=1);

namespace App\Infrastructure\DatabaseRepository;

use App\Contract\Core\DatabaseRepositoryInterface;
use App\Contract\Core\FilterInterface;
use App\Contract\Core\SortingInterface;
use App\Domain\Order\Order;
use App\Domain\Order\OrderFilter;
use App\Domain\Order\OrderRepositoryInterface;

/**
 * Class OrderRepository
 * @package App\Infrastructure\DatabaseRepository
 */
class OrderRepository extends DatabaseRepository implements OrderRepositoryInterface
{
    /**
     * OrderRepository constructor
     */
    public function __construct()
    {
        $this->model = new Order();
    }

    /**
     * @param FilterInterface|OrderFilter $filter
     *
     * @return $this
     */
    protected function filter(FilterInterface $filter): DatabaseRepositoryInterface
    {
        $this->builder = $this->builder ?? $this->model->newQuery();

        if ($filter->getId()) {
            $this->builder->where('id', '=', $filter->getId());
        }

        if ($filter->getStatusId()) {
            $this->builder->whereHas("status", function($q) use ($filter) {
                $q->where('order_statuses.id', '=', $filter->getStatusId());
            });
        }

        if ($filter->getDeliveryId()) {
            $this->builder->whereHas("delivery", function($q) use ($filter) {
                $q->where('delivery.id', '=', $filter->getDeliveryId());
            });
        }

        if ($filter->getPayTypeId()) {
            $this->builder->whereHas("payType", function($q) use ($filter) {
                $q->where('pay_types.id', '=', $filter->getPayTypeId());
            });
        }

        if ($filter->getSearch()) {
            $this->builder->where(function($q) use ($filter) {
                $q->where('name', 'like', "%{$filter->getSearch()}%")
                    ->orWhere('phone', 'like', "%{$filter->getSearch()}%");
            });
        }

        return $this;
    }

    /**
     * @param SortingInterface $sorting
     *
     * @return $this
     */
    protected function sorting(SortingInterface $sorting): DatabaseRepositoryInterface
    {
        $this->builder = $this->builder ?? $this->model->newQuery();
        $this->builder->orderBy($sorting->getField(), $sorting->getDirection());

        return $this;
    }
}