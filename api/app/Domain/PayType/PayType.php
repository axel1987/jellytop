<?php

declare(strict_types=1);

namespace App\Domain\PayType;

use Barryvdh\LaravelIdeHelper\Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class PayType
 *
 * @package App\Domain\PayType
 * @property int $id
 * @property array $title
 * @property array|null $description
 * @property string|null $icon
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|PayType newModelQuery()
 * @method static Builder|PayType newQuery()
 * @method static Builder|PayType query()
 * @method static Builder|PayType whereCreatedAt($value)
 * @method static Builder|PayType whereIcon($value)
 * @method static Builder|PayType whereId($value)
 * @method static Builder|PayType whereTitle($value)
 * @method static Builder|PayType whereDescription($value)
 * @method static Builder|PayType whereUpdatedAt($value)
 * @mixin Eloquent
 */
class PayType extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
        'icon',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * @var string[]
     */
    protected $casts = [
        'title' => 'array',
        'description' => 'array',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];
}