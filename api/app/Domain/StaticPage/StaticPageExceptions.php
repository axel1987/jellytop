<?php

declare(strict_types=1);

namespace App\Domain\StaticPage;

use Exception;

/**
 * Class StaticPageExceptions
 * @package App\Domain\StaticPage
 */
class StaticPageExceptions extends Exception
{
    /**
     * @param int $id
     *
     * @throws StaticPageExceptions
     */
    public static function notFound(int $id)
    {
        throw new StaticPageExceptions(sprintf('Statis page with id %d not found', $id));
    }
}