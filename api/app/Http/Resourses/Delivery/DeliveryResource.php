<?php

declare(strict_types=1);

namespace App\Http\Resourses\Delivery;

use App\Domain\Delivery\Delivery;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class DeliveryResource
 * @package App\Http\Resourses\Delivery
 */
class DeliveryResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        /** @var Delivery $this */
        return [
            'id' => $this->id,
            'type' => 'type',
            'attributes' => [
                'title' => $this->title,
                'slug' => $this->slug,
                'description' => $this->description,
                'icon' => $this->icon,
                'minPrice' => $this->min_price,
                'createdAt' => $this->created_at ? $this->created_at->format('Y-m-d H:i:s') : '',
                'updatedAt' => $this->updated_at ? $this->updated_at->format('Y-m-d H:i:s') : '',
            ]
        ];
    }
}