<?php

declare(strict_types=1);

namespace App\Application\Color\StoreColor;

use App\Contract\Core\CommandInterface;

/**
 * Class StoreColor
 * @package App\Application\Color\StoreColor
 */
class StoreColor implements CommandInterface
{
    /** @var array $title */
    private array $title;

    /** @var string $slug */
    private string $slug;

    /** @var string|null $hex */
    private ?string $hex;

    /**
     * StoreColor constructor
     *
     * @param array $title
     * @param string|null $hex
     */
    public function __construct(
        array $title,
        string $slug,
        ?string $hex = null
    ) {
        $this->title = $title;
        $this->slug = $slug;
        $this->hex = $hex;
    }

    /**
     * @return array
     */
    public function getTitle(): array
    {
        return $this->title;
    }

    /**
     * @param array $title
     */
    public function setTitle(array $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @return string|null
     */
    public function getHex(): ?string
    {
        return $this->hex;
    }

    /**
     * @param string|null $hex
     */
    public function setHex(?string $hex): void
    {
        $this->hex = $hex;
    }
}