<?php

declare(strict_types=1);

namespace App\Application\Type\GetTypeByFilter;

use App\Contract\Core\CommandInterface;
use App\Domain\Type\TypeFilter;

/**
 * Class GetTypeByFilter
 * @package App\Application\Type\GetTypeByFilter
 */
class GetTypeByFilter implements CommandInterface
{
    /** @var TypeFilter $filter */
    private TypeFilter $filter;

    /**
     * GetTypeByFilter constructor
     *
     * @param TypeFilter $filter
     */
    public function __construct(TypeFilter $filter)
    {
        $this->filter = $filter;
    }

    /**
     * @return TypeFilter
     */
    public function getFilter(): TypeFilter
    {
        return $this->filter;
    }
}