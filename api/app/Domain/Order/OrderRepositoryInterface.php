<?php

declare(strict_types=1);

namespace App\Domain\Order;

use App\Contract\Core\DatabaseRepositoryInterface;

/**
 * Interface OrderRepositoryInterface
 * @package App\Domain\Order
 */
interface OrderRepositoryInterface extends DatabaseRepositoryInterface
{

}