export default class PayTypeForm {

    /**
     * @return <void>
     */
    constructor(data) {
        this.id = data.id;
        this.title = {
            ua: data.title && data.title.ua ? data.title.ua : null,
            ru: data.title && data.title.ru ? data.title.ru : null
        };
        this.description = {
            ua: data.description && data.description.ua ? data.description.ua : null,
            ru: data.description && data.description.ru ? data.description.ru : null
        };
        this.icon = data.icon;
    }

    /**
     * @returns {{required: (function(*=))}}
     */
    rules() {
        return {
            required: value => !!value || 'Это поле обязательно',
        }
    }
}
