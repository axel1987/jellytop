<?php

declare(strict_types=1);

namespace App\Http\Resourses\Cart;

use App\Domain\Cart\Cart;
use App\Http\Resourses\BaseCollectionResource;
use App\Http\Resourses\Color\ColorResource;
use App\Http\Resourses\Product\ProductNotRelationResource;
use App\Http\Resourses\Size\SizeResource;

/**
 * Class CartListResource
 * @package App\Http\Resourses\Cart
 */
class CartListResource extends BaseCollectionResource
{
    /**
     * @param Cart $item
     *
     * @return array
     */
    protected function getItemData($item): array
    {
        return [
            'id' => $item->id,
            'type' => 'cart',
            'attributes' => [
                'sessionId' => $item->session_id,
                'storageUntil' => $item->storage_until,
                'count' => $item->count,
                'sum' => $item->product->sale_price * $item->count,
                'createdAt' => $item->created_at ? $item->created_at->format('Y-m-d H:i:s') : '',
                'updatedAt' => $item->updated_at ? $item->updated_at->format('Y-m-d H:i:s') : '',
            ],
            'relationships' => [
                'product' => new ProductNotRelationResource($item->product),
                'size' => new SizeResource($item->size),
            ]
        ];
    }
}