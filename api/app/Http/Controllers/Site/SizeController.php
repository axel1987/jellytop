<?php

declare(strict_types=1);

namespace App\Http\Controllers\Site;

use App\Application\Size\GetSizesList\GetSizesList;
use App\Domain\Size\SizeFilter;
use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Size\SizeIndexRequest;
use App\Http\Resourses\Size\SizeListResource;
use App\Infrastructure\Core\Pagination;
use App\Infrastructure\Core\Sorting;
use Illuminate\Http\JsonResponse;

/**
 * Class SizeController
 * @package App\Http\Controllers
 */
class SizeController extends Controller
{
    /**
     * @param SizeIndexRequest $request
     *
     * @return JsonResponse
     */
    public function index(SizeIndexRequest $request): JsonResponse
    {
        $filter = SizeFilter::fromRequest($request);
        $pagination = Pagination::fromRequest($request);
        $sorting = Sorting::fromRequest($request);

        $list = $this->execute(new GetSizesList(
            $filter,
            $pagination,
            $sorting
        ));

        return new JsonResponse(new SizeListResource($list));
    }

    /**
     * @param SizeIndexRequest $request
     *
     * @return JsonResponse
     */
    public function list(SizeIndexRequest $request): JsonResponse
    {
        $filter = SizeFilter::fromRequest($request);
        $sorting = Sorting::fromRequest($request);

        $list = $this->execute(new GetSizesList(
            $filter,
            null,
            $sorting
        ));

        return new JsonResponse(new SizeListResource($list));
    }
}