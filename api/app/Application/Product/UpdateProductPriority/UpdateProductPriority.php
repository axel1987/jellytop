<?php

declare(strict_types=1);

namespace App\Application\Product\UpdateProductPriority;

use App\Contract\Core\CommandInterface;
use App\Domain\Product\Product;

/**
 * Class UpdateProductPriority
 * @package App\Application\Product\UpdateProductPriority
 */
class UpdateProductPriority implements CommandInterface
{
    /** @var Product $product */
    private Product $product;

    /** @var int|null $priority */
    private ?int $priority;

    /**
     * UpdateProductPriority constructor
     *
     * @param Product $product
     * @param int $priority
     */
    public function __construct(Product $product, ?int $priority)
    {
        $this->product = $product;
        $this->priority = $priority;
    }

    /**
     * @return Product
     */
    public function getProduct(): Product
    {
        return $this->product;
    }

    /**
     * @return int|null
     */
    public function getPriority(): ?int
    {
        return $this->priority;
    }
}