<?php

declare(strict_types=1);

namespace App\Application\Color\UpdateColor;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\Color\Color;
use App\Domain\Color\ColorRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * Class UpdateColorHandler
 * @package App\Application\Color\UpdateColor
 */
class UpdateColorHandler implements HandlerInterface
{
    /** @var ColorRepositoryInterface $colorRepository */
    private ColorRepositoryInterface $colorRepository;

    /**
     * UpdateColorHandler constructor
     *
     * @param ColorRepositoryInterface $colorRepository
     */
    public function __construct(ColorRepositoryInterface $colorRepository)
    {
        $this->colorRepository = $colorRepository;
    }

    /**
     * @param CommandInterface|UpdateColor $command
     *
     * @return Color
     */
    public function handle(CommandInterface $command): Model
    {
        $color = $command->getColor();

        $color->title = $command->getTitle();
        $color->slug = $command->getSlug();
        $color->hex = $command->getHex();

        return $this->colorRepository->store($color);
    }
}