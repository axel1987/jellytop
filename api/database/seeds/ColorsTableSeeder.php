<?php

declare(strict_types=1);

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

/**
 * Class ColorsTableSeeder
 */
class ColorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now();

        DB::table('colors')->insert([
            [
                'title' => json_encode([
                    'ua' => 'Жовтий',
                    'ru' => 'Желтый'
                ]),
                'slug' => 'yellow',
                'created_at' => $now,
                'updated_at' => $now,
            ],
            [
                'title' => json_encode([
                    'ua' => 'Червоний',
                    'ru' => 'Красный'
                ]),
                'slug' => 'red',
                'created_at' => $now,
                'updated_at' => $now,
            ],
            [
                'title' => json_encode([
                    'ua' => 'Чорный',
                    'ru' => 'Чёрный'
                ]),
                'slug' => 'black',
                'created_at' => $now,
                'updated_at' => $now,
            ],
        ]);
    }
}