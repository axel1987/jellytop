<?php

declare(strict_types=1);

namespace App\Application\StaticPage\GetStaticPageByFilter;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\StaticPage\StaticPage;
use App\Domain\StaticPage\StaticPageRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * Class GetStaticPageByFilterHandler
 * @package App\Application\StaticPage\GetStaticPageByFilter
 */
class GetStaticPageByFilterHandler implements HandlerInterface
{
    /** @var StaticPageRepositoryInterface $staticPageRepository */
    private StaticPageRepositoryInterface $staticPageRepository;

    /**
     * GetStaticPageByFilterHandler constructor
     *
     * @param StaticPageRepositoryInterface $staticPageRepository
     */
    public function __construct(StaticPageRepositoryInterface $staticPageRepository)
    {
        $this->staticPageRepository = $staticPageRepository;
    }

    /**
     * @param CommandInterface|GetStaticPageByFilter $command
     *
     * @return StaticPage|null
     */
    public function handle(CommandInterface $command): ?Model
    {
        return $this->staticPageRepository->one(
            $command->getFilter()
        );
    }
}