export default class Slider {
    /**
     * @param data
     */
    constructor(data = {}) {
        this.id = data.id ? data.id : null;
        this.link = data.attributes && data.attributes.link ?
            data.attributes.link :
            null;
        this.text = data.attributes && data.attributes.text ?
            data.attributes.text :
            null;
        this.createdAt = data.attributes && data.attributes.createdAt ?
            data.attributes.createdAt :
            null;
        this.updatedAt = data.attributes && data.attributes.updatedAt ?
            data.attributes.updatedAt :
            null;
    }
}