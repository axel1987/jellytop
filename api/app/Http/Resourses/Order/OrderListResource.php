<?php

declare(strict_types=1);

namespace App\Http\Resourses\Order;

use App\Domain\Order\Order;
use App\Http\Resourses\BaseCollectionResource;
use App\Http\Resourses\Delivery\DeliveryResource;
use App\Http\Resourses\OrderItem\OrderItemListResource;
use App\Http\Resourses\OrderStatuses\OrderStatusResource;
use App\Http\Resourses\PayType\PayTypeResource;
use App\Http\Resourses\Size\SizeListResource;

/**
 * Class OrderListResource
 * @package App\Http\Resourses\Order
 */
class OrderListResource extends BaseCollectionResource
{
    /**
     * @param Order $item
     *
     * @return array
     */
    protected function getItemData($item): array
    {
        return [
            'id' => $item->id,
            'type' => 'order',
            'attributes' => [
                'name' => $item->name,
                'address' => $item->address,
                'sum' => $item->sum,
                'phone' => $item->phone,
                'createdAt' => $item->created_at ? $item->created_at->format('Y-m-d H:i:s') : '',
                'updatedAt' => $item->updated_at ? $item->updated_at->format('Y-m-d H:i:s') : '',
            ],
            'relationships' => [
                'status' => new OrderStatusResource($item->status),
                'delivery' => new DeliveryResource($item->delivery),
                'payType' => new PayTypeResource($item->payType),
                'items' => new OrderItemListResource($item->items)
            ]
        ];
    }
}