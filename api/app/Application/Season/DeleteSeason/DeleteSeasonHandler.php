<?php

declare(strict_types=1);

namespace App\Application\Season\DeleteSeason;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\Season\SeasonRepositoryInterface;
use Illuminate\Support\Facades\Storage;

/**
 * Class DeleteSeasonHandler
 * @package App\Application\Season\DeleteSeason
 */
class DeleteSeasonHandler implements HandlerInterface
{
    /** @var SeasonRepositoryInterface $seasonRepository */
    private SeasonRepositoryInterface $seasonRepository;

    /**
     * DeleteSeasonHandler constructor
     *
     * @param SeasonRepositoryInterface $seasonRepository
     */
    public function __construct(SeasonRepositoryInterface $seasonRepository)
    {
        $this->seasonRepository = $seasonRepository;
    }

    /**
     * @param CommandInterface|DeleteSeason $command
     *
     * @return mixed|void
     */
    public function handle(CommandInterface $command): void
    {
        $season = $command->getSeason();

        if ($season->icon) {
            Storage::disk('public')
                ->delete(str_replace('/storage', '', $season->icon));
        }

        $this->seasonRepository->delete($season);
    }
}