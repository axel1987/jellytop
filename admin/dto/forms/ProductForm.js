export default class ProductForm {

    /**
     * @return <void>
     */
    constructor(data) {
        this.id = data.id;
        this.title = {
            ua: data.title && data.title.ua ? data.title.ua : null,
            ru: data.title && data.title.ru ? data.title.ru : null
        };
        this.description = {
            ua: data.description && data.description.ua ? data.description.ua : null,
            ru: data.description && data.description.ru ? data.description.ru : null
        };
        this.slug = data.slug;
        this.metaKey = {
            ua: data.metaKey && data.metaKey.ua ? data.metaKey.ua : null,
            ru: data.metaKey && data.metaKey.ru ? data.metaKey.ru : null
        };
        this.metaDescription = {
            ua: data.metaDescription && data.metaDescription.ua ? data.metaDescription.ua : null,
            ru: data.metaDescription && data.metaDescription.ru ? data.metaDescription.ru : null
        };
        this.purchasePrice = data.purchasePrice;
        this.salePrice = data.salePrice;
        this.inStock = data.inStock;
        this.priority = data.priority;

        this.brandId = data.brand ? data.brand.id : null;
        this.typeId = data.type ? data.type.id : null;
        this.colorsIds = data.colors && data.colors.length
            ? data.colors.map((item) => {
                return item.id
            })
            : [];
        this.gendersIds = data.gender && data.gender.length
            ? data.gender.map((item) => {
                return item.id
            })
            : [];
        this.sizesIds = data.sizes && data.sizes.length
            ? data.sizes.map((item) => {
                return item.id
            })
            : [];
        this.seasonsIds = data.seasons && data.seasons.length
            ? data.seasons.map((item) => {
                return item.id
            })
            : [];
        this.deliveryIds = data.delivery && data.delivery.length
            ? data.delivery.map((item) => {
                return item.id
            })
            : [];
        this.payTypesIds = data.payTypes && data.payTypes.length
            ? data.payTypes.map((item) => {
                return item.id
            })
            : [];
        this.images = data.images && data.images.length
            ? data.images.map((item) => {
                return item.url
            })
            : [];
    }

    /**
     * @returns {{required: (function(*=))}}
     */
    rules() {
        return {
            required: value => !!value || 'Это поле обязательно',
        }
    }
}
