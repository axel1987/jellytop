<?php

declare(strict_types=1);

namespace App\Domain\Gender;

use App\Contract\Core\FilterInterface;
use Illuminate\Http\Request;

/**
 * Class GenderFilter
 * @package App\Domain\Gender
 */
class GenderFilter implements FilterInterface
{
    /**
     * @param Request $request
     *
     * @return FilterInterface|GenderFilter
     */
    public static function fromRequest(Request $request): FilterInterface
    {
        $filter = new self();

        return $filter;
    }
}