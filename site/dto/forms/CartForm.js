export default class CartForm {

    /**
     * @return <void>
     */
    constructor(data) {
        this.id = data.id;
        this.productId = data.productId;
        this.sizeId = data.sizeId;
        this.count = data.count ? data.count : 1;
    }

    /**
     * @returns {{required: (function(*=))}}
     */
    rules() {
        return {
            required: value => !!value || 'Это поле обязательно',
        }
    }
}
