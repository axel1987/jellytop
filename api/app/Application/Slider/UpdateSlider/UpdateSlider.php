<?php

declare(strict_types=1);

namespace App\Application\Slider\UpdateSlider;

use App\Contract\Core\CommandInterface;
use App\Domain\Slider\Slider;

/**
 * Class UpdateSlider
 * @package App\Application\Slider\UpdateSlider
 */
class UpdateSlider implements CommandInterface
{
    /** @var Slider $slider */
    private Slider $slider;

    /** @var string $link */
    private string $link;

    /** @var array $text */
    private array $text;

    /**
     * UpdateSlider constructor
     *
     * @param Slider $slider
     * @param string $link
     * @param array $text
     */
    public function __construct(
        Slider $slider,
        string $link,
        array $text
    ) {
        $this->slider = $slider;
        $this->link = $link;
        $this->text = $text;
    }

    /**
     * @return Slider
     */
    public function getSlider(): Slider
    {
        return $this->slider;
    }

    /**
     * @return string
     */
    public function getLink(): string
    {
        return $this->link;
    }

    /**
     * @return array
     */
    public function getText(): array
    {
        return $this->text;
    }
}