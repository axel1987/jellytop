<?php

declare(strict_types=1);

namespace App\Application\Brand\UpdateBrand;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\Brand\Brand;
use App\Domain\Brand\BrandRepositoryInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

/**
 * Class UpdateBrandHandler
 * @package App\Application\Brand\UpdateBrand
 */
class UpdateBrandHandler implements HandlerInterface
{
    /** @var BrandRepositoryInterface $brandRepository */
    private BrandRepositoryInterface $brandRepository;

    /**
     * UpdateBrandHandler constructor
     *
     * @param BrandRepositoryInterface $brandRepository
     */
    public function __construct(BrandRepositoryInterface $brandRepository)
    {
        $this->brandRepository = $brandRepository;
    }

    /**
     * @param CommandInterface|UpdateBrand $command
     *
     * @return Brand
     */
    public function handle(CommandInterface $command): Model
    {
        $oldLogo = null;
        $brand = $command->getBrand();

        if ($brand->logo != $command->getLogoUrl()) {
            $oldLogo = $brand->logo;
        }

        $brand->country()->associate($command->getCountry());
        $brand->title = $command->getTitle();
        $brand->description = $command->getDescription();
        $brand->logo = $command->getLogoUrl();
        $brand->slug = $command->getSlug();
        $this->brandRepository->store($brand);

        if ($oldLogo) {
            Storage::disk('public')
                ->delete(str_replace('/storage', '', $oldLogo));
        }

        return $brand;
    }
}