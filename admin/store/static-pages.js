import StaticPage from "../dto/entities/StaticPage";
import Pagination from "../dto/entities/Pagination";
import Vue from "vue";

/**
 * @returns {{
 * staticPage: null,
 * staticPagesList: *[],
 * staticPagesSelectList: *[],
 * pagination: null,
 * }}
 */
export const state = () => ({
    staticPage: null,
    staticPagesList: [],
    staticPagesSelectList: [],
    pagination: null
});

/**
 *
 * @type {{
 * getStaticPage(*): null,
 * getStaticPagesList(*): [],
 * getStaticPagesSelectList(*): [],
 * getStaticPagePagination(*): null
 * }}
 */
export const getters = {
    getStaticPage(state) {
        return state.staticPage;
    },
    getStaticPagesList(state) {
        return state.staticPagesList;
    },
    getStaticPagesSelectList(state) {
        return state.staticPagesSelectList;
    },
    getStaticPagePagination(state) {
        return state.pagination;
    }
};

/**
 * @type {{
 * SET_STATIC_PAGES_LIST(*, *): void,
 * SET_STATIC_PAGES_SELECT_LIST(*, *): void,
 * SET_STATIC_PAGE(*, *): void,
 * SET_STATIC_PAGE_PAGINATION(*, *): void,
 * }}
 */
export const mutations = {
    SET_STATIC_PAGE(state, value) {
        state.staticPage = value
    },
    SET_STATIC_PAGES_LIST(state, value) {
        state.staticPagesList = value
    },
    SET_STATIC_PAGES_SELECT_LIST(state, value) {
        state.staticPagesSelectList = value
    },
    SET_STATIC_PAGES_PAGINATION(state, value) {
        state.pagination = value
    },
};

/**
 * @type {{
 * registerStaticPage: ((function({commit: *}, *=): Promise<void>)|*),
 * updateStaticPage: ((function({commit: *}, *=): Promise<void>)|*),
 * fetchStaticPage: ((function({commit: *}, *=): Promise<void>)|*),
 * fetchStaticPagesList: ((function({commit: *}, *=): Promise<void>)|*),
 * fetchStaticPagesSelectList: ((function({commit: *}, *=): Promise<void>)|*),
 * deleteStaticPage: ((function({commit: *}, *=): Promise<void>)|*)
 * }}
 */
export const actions = {
    fetchStaticPagesList: async function ({commit}, params = {}) {
        let {data} = await this.$axios.get('/static-pages', {params: params});

        const list = data.data.map((staticPageData) => {
            return new StaticPage(staticPageData)
        });
        const pagination = data.pagination ? new Pagination(data.pagination) : null;

        commit('SET_STATIC_PAGES_LIST', list)
        commit('SET_STATIC_PAGES_PAGINATION', pagination)
    },
    fetchStaticPagesSelectList: async function ({commit}, params = {}) {
        let {data} = await this.$axios.get('/static-pages/list', params);

        const list = data.data.map((staticPageData) => {
            return new StaticPage(staticPageData)
        });

        commit('SET_STATIC_PAGES_SELECT_LIST', list)
    },
    fetchStaticPage: async function ({commit}, params = {}) {
        if (params.id) {
            let {data} = await this.$axios.get(`/static-pages/${params.id}`, params);
            commit('SET_STATIC_PAGE', new StaticPage(data))
        } else {
            commit('SET_STATIC_PAGE', new StaticPage(null))
        }
    },
    registerStaticPage: async function ({commit}, params = {}) {
        await this.$axios.post(`/static-pages`, params).then((res) => {
            if (res) {
                commit('SET_STATIC_PAGE', new StaticPage(res.data))
            }
        });
    },
    updateStaticPage: async function ({commit}, params = {}) {
        await this.$axios.put(`/static-pages/${params.id}`, params).then((res) => {
            if (res) {
                commit('SET_STATIC_PAGE', new StaticPage(res.data))

                Vue.notify({
                    group: 'dashboard',
                    title: 'Успешно',
                    type: 'success',
                    text: `Текст обновлен`,
                })
            } else {
                Vue.notify({
                    group: 'dashboard',
                    title: 'Ошибка',
                    type: 'error',
                    text: `неверные данные`,
                });
            }
        });
    },
    deleteStaticPage: async function ({commit}, params = {}) {
        await this.$axios.delete(`/static-pages/${params.id}`, params);
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations,
};
