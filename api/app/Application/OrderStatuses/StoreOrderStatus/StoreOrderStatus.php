<?php

declare(strict_types=1);

namespace App\Application\OrderStatuses\StoreOrderStatus;

use App\Contract\Core\CommandInterface;

/**
 * Class StoreOrderStatus
 * @package App\Application\OrderStatuses\StoreOrderStatus
 */
class StoreOrderStatus implements CommandInterface
{
    /** @var string $title */
    private string $title;

    /**
     * StoreOrderStatus constructor
     *
     * @param string $title
     */
    public function __construct(string $title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }
}