<?php

declare(strict_types=1);

namespace App\Console\Commands;

use App\Domain\ProductImage\ProductImageFilter;
use App\Domain\ProductImage\ProductImageRepositoryInterface;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

/**
 * Class MakeWebpProductImages
 * @package App\Console\Commands
 */
class MakeWebpProductImages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'product:make-webp-images';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make webp images to product';

    /**
     * @param ProductImageRepositoryInterface $imageRepository
     *
     * @return void
     */
    public function handle(ProductImageRepositoryInterface $imageRepository)
    {
        $images = $imageRepository->all(new ProductImageFilter());

        foreach ($images as $image) {
            $img = Storage::disk('public')->get(str_replace('/storage', '', $image->url));
            $newImage = Image::make($img);
            $newImage = $newImage->encode('webp');

            $filepath = explode('.', $image->url)[0] . '.webp';

            Storage::disk('public')->put(str_replace('/storage', '', $filepath), $newImage);
            $image->webp_url = Storage::disk('public')->url(str_replace('/storage', '', $filepath));

            $imageRepository->store($image);
        }
    }
}