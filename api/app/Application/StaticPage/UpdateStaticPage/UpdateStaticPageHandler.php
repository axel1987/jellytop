<?php

declare(strict_types=1);

namespace App\Application\StaticPage\UpdateStaticPage;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\StaticPage\StaticPage;
use App\Domain\StaticPage\StaticPageRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * Class UpdateStaticPageHandler
 * @package App\Application\StaticPage\UpdateStaticPage
 */
class UpdateStaticPageHandler implements HandlerInterface
{
    /** @var StaticPageRepositoryInterface $staticPageRepository */
    private StaticPageRepositoryInterface $staticPageRepository;

    /**
     * UpdateStaticPageHandler constructor
     *
     * @param StaticPageRepositoryInterface $staticPageRepository
     */
    public function __construct(StaticPageRepositoryInterface $staticPageRepository)
    {
        $this->staticPageRepository = $staticPageRepository;
    }

    /**
     * @param CommandInterface|UpdateStaticPage $command
     *
     * @return StaticPage
     */
    public function handle(CommandInterface $command): Model
    {
        $staticPage = $command->getStaticPage();
        $staticPage->content = $command->getContent();

        $this->staticPageRepository->store($staticPage);

        return $staticPage;
    }
}