<?php

declare(strict_types=1);

namespace App\Application\Country\GetCountriesList;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\Country\CountryRepositoryInterface;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

/**
 * Class GetCountriesListHandler
 * @package App\Application\Country\GetCountriesList
 */
class GetCountriesListHandler implements HandlerInterface
{
    /** @var CountryRepositoryInterface $countryRepository */
    private CountryRepositoryInterface $countryRepository;

    /**
     * GetCountriesListHandler constructor
     *
     * @param CountryRepositoryInterface $countryRepository
     */
    public function __construct(CountryRepositoryInterface $countryRepository)
    {
        $this->countryRepository = $countryRepository;
    }

    /**
     * @param CommandInterface|GetCountriesList $command
     *
     * @return LengthAwarePaginator|Collection
     */
    public function handle(CommandInterface $command)
    {
        return $this->countryRepository->all(
            $command->getFilter(),
            $command->getPagination(),
            $command->getSorting()
        );
    }
}