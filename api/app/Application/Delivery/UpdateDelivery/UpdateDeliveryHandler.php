<?php

declare(strict_types=1);

namespace App\Application\Delivery\UpdateDelivery;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\Delivery\DeliveryRepositoryInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

/**
 * Class UpdateDeliveryHandler
 * @package App\Application\Delivery\UpdateDelivery
 */
class UpdateDeliveryHandler implements HandlerInterface
{
    /** @var DeliveryRepositoryInterface $deliveryRepository */
    private DeliveryRepositoryInterface $deliveryRepository;

    /**
     * UpdateDeliveryHandler constructor
     *
     * @param DeliveryRepositoryInterface $deliveryRepository
     */
    public function __construct(DeliveryRepositoryInterface $deliveryRepository)
    {
        $this->deliveryRepository = $deliveryRepository;
    }

    /**
     * @param UpdateDelivery $command
     *
     * @return Model
     */
    public function handle(CommandInterface $command): Model
    {
        $oldIcon = null;
        $delivery = $command->getDelivery();

        if ($delivery->icon != $command->getIcon()) {
            $oldIcon = $delivery->icon;
        }

        $delivery->title = $command->getTitle();
        $delivery->description = $command->getDescription();
        $delivery->icon = $command->getIcon();
        $delivery->min_price = $command->getMinPrice();

        $this->deliveryRepository->store($delivery);

        if ($oldIcon) {
            Storage::disk('public')
                ->delete(str_replace('/storage', '', $oldIcon));
        }

        return $delivery;
    }
}