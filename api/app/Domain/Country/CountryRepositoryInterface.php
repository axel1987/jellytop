<?php

declare(strict_types=1);

namespace App\Domain\Country;

use App\Contract\Core\DatabaseRepositoryInterface;

/**
 * Interface CountryRepositoryInterface
 * @package App\Domain\Country
 */
interface CountryRepositoryInterface extends DatabaseRepositoryInterface
{

}