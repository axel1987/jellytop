<?php

declare(strict_types=1);

namespace App\Http\Controllers\Site;

use App\Application\Cart\GetCartList\GetCartList;
use App\Application\Order\StoreOrder\StoreOrder;
use App\Domain\Cart\CartFilter;
use App\Http\Controllers\Controller;
use App\Http\Requests\Site\Order\OrderRequest;
use Illuminate\Http\JsonResponse;

/**
 * Class OrderController
 * @package App\Http\Controllers\Site
 */
class OrderController extends Controller
{
    /**
     * @param OrderRequest $request
     *
     * @return JsonResponse
     */
    public function store(OrderRequest $request): JsonResponse
    {
        $cartFilter = new CartFilter();
        $cartFilter->setIds($request->get('cartItemsIds'));
        $cartItems = $this->execute(new GetCartList($cartFilter));

        $this->execute(new StoreOrder(
            $cartItems,
            $request->get('name'),
            $request->get('phone'),
            $request->get('delivery'),
            $request->get('payType'),
            $request->get('address', null),
            $request->get('department', null),
            $request->get('zip', null),
        ));

        return response()->json(null, 201);
    }
}