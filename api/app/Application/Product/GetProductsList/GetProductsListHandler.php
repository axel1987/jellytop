<?php

declare(strict_types=1);

namespace App\Application\Product\GetProductsList;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\Product\ProductRepositoryInterface;

/**
 * Class GetProductsListHandler
 * @package App\Application\Product\GetProductsList
 */
class GetProductsListHandler implements HandlerInterface
{
    /** @var ProductRepositoryInterface $productRepository */
    private ProductRepositoryInterface $productRepository;

    /**
     * GetProductsListHandler constructor
     *
     * @param ProductRepositoryInterface $productRepository
     */
    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    /**
     * @param GetProductsList $command
     *
     * @return mixed|void
     */
    public function handle(CommandInterface $command)
    {
        return $this->productRepository->all(
            $command->getProductFilter(),
            $command->getPagination(),
            $command->getSorting()
        );
    }
}