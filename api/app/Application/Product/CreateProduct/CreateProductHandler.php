<?php

declare(strict_types=1);

namespace App\Application\Product\CreateProduct;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\Product\Product;
use App\Domain\Product\ProductRepositoryInterface;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

/**
 * Class CreateProductHandler
 * @package App\Application\Product\CreateProduct
 */
class CreateProductHandler implements HandlerInterface
{
    /** @var ProductRepositoryInterface $productRepository */
    private ProductRepositoryInterface $productRepository;

    /**
     * UpdateProductHandler constructor
     *
     * @param ProductRepositoryInterface $productRepository
     */
    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    /**
     * @param CreateProduct $command
     *
     * @return Product
     */
    public function handle(CommandInterface $command): Product
    {
        $product = new Product();
        $slug = $command->getSlug() ?? Str::slug(Arr::get($command->getTitle(), 'ua'));

        $product->title = $command->getTitle();
        $product->description = $command->getDescription();
        $product->meta_key = $command->getMetaKey();
        $product->meta_description = $command->getMetaDescription();
        $product->slug = trim($slug . '-' . uniqid('', true));
        $product->in_stock = $command->isInStock();
        $product->priority = $command->getPriority();
        $product->sale_price = $command->getSalePrice();
        $product->purchase_price = $command->getPurchasePrice();

        $this->productRepository->store($product);

        $product->brands()->sync($command->getBrandIds());
        $product->types()->sync($command->getTypeIds());
        $product->colors()->sync($command->getColorsIds());
        $product->sizes()->sync($command->getSizesIds());
        $product->seasons()->sync($command->getSeasonsIds());
        $product->delivery()->sync($command->getDeliveryIds());
        $product->payTypes()->sync($command->getPayTypesIds());
        $product->genders()->sync($command->getGendersIds());

        $product->meta_key = $product->makeMetaKey();
        $product->meta_description = $product->makeMetaDescription();

        $this->productRepository->store($product);

        return $product;
    }
}