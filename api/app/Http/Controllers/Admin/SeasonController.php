<?php

declare(strict_types=1);

namespace App\Http\Controllers\Admin;

use App\Application\Season\DeleteSeason\DeleteSeason;
use App\Application\Season\GetSeasonByFilter\GetSeasonByFilter;
use App\Application\Season\GetSeasonsList\GetSeasonsList;
use App\Application\Season\StoreSeason\StoreSeason;
use App\Application\Season\UpdateSeason\UpdateSeason;
use App\Domain\Season\SeasonFilter;
use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Season\SeasonIndexRequest;
use App\Http\Requests\Dashboard\Season\SeasonRequest;
use App\Http\Resourses\Season\SeasonListResource;
use App\Http\Resourses\Season\SeasonResource;
use App\Infrastructure\Core\Pagination;
use App\Infrastructure\Core\Sorting;
use Illuminate\Http\JsonResponse;

/**
 * Class SeasonController
 * @package App\Http\Controllers
 */
class SeasonController extends Controller
{
    /**
     * @param SeasonIndexRequest $request
     *
     * @return JsonResponse
     */
    public function index(SeasonIndexRequest $request): JsonResponse
    {
        $filter = SeasonFilter::fromRequest($request);
        $pagination = Pagination::fromRequest($request);
        $sorting = Sorting::fromRequest($request);

        $list = $this->execute(new GetSeasonsList(
            $filter,
            $pagination,
            $sorting
        ));

        return new JsonResponse(new SeasonListResource($list));
    }

    /**
     * @param SeasonIndexRequest $request
     *
     * @return JsonResponse
     */
    public function list(SeasonIndexRequest $request): JsonResponse
    {
        $filter = SeasonFilter::fromRequest($request);
        $sorting = Sorting::fromRequest($request);

        $list = $this->execute(new GetSeasonsList(
            $filter,
            null,
            $sorting
        ));

        return new JsonResponse(new SeasonListResource($list));
    }

    /**
     * @param int $id
     *
     * @return JsonResponse
     */
    public function view(int $id): JsonResponse
    {
        $filter = new SeasonFilter();
        $filter->setId($id);

        $season = $this->execute(new GetSeasonByFilter($filter));

        return new JsonResponse(new SeasonResource($season));
    }

    /**
     * @param SeasonRequest $request
     *
     * @return JsonResponse
     */
    public function store(SeasonRequest $request): JsonResponse
    {
        $season = $this->execute(new StoreSeason(
            $request->get('title'),
            $request->get('slug'),
            $request->get('icon')
        ));

        return new JsonResponse(new SeasonResource($season));
    }

    /**
     * @param SeasonRequest $request
     * @param int $id
     *
     * @return JsonResponse
     */
    public function update(SeasonRequest $request, int $id): JsonResponse
    {
        $filter = new SeasonFilter();
        $filter->setId($id);

        $season = $this->execute(new GetSeasonByFilter($filter));
        $season = $this->execute(new UpdateSeason(
            $season,
            $request->get('title'),
            $request->get('slug'),
            $request->get('icon')
        ));

        return new JsonResponse(new SeasonResource($season));
    }

    /**
     * @param int $id
     *
     * @return JsonResponse
     */
    public function delete(int $id): JsonResponse
    {
        $filter = new SeasonFilter();
        $filter->setId($id);

        $season = $this->execute(new GetSeasonByFilter($filter));
        $this->execute(new DeleteSeason($season));

        return new JsonResponse(null, 204);
    }
}