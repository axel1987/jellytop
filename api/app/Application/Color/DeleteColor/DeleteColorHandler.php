<?php

declare(strict_types=1);

namespace App\Application\Color\DeleteColor;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\Color\ColorRepositoryInterface;
use Illuminate\Support\Facades\Storage;

/**
 * Class DeleteColorHandler
 * @package App\Application\Color\DeleteColor
 */
class DeleteColorHandler implements HandlerInterface
{
    /** @var ColorRepositoryInterface $colorRepository */
    private ColorRepositoryInterface $colorRepository;

    /**
     * DeleteColorHandler constructor
     *
     * @param ColorRepositoryInterface $colorRepository
     */
    public function __construct(ColorRepositoryInterface $colorRepository)
    {
        $this->colorRepository = $colorRepository;
    }

    /**
     * @param DeleteColor $command
     *
     * @return void
     */
    public function handle(CommandInterface $command): void
    {
        $this->colorRepository->delete(
            $command->getColor()
        );
    }
}