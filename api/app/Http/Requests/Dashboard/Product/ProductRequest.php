<?php

declare(strict_types=1);

namespace App\Http\Requests\Dashboard\Product;

use App\Http\Requests\FormRequest;

/**
 * Class ProductRequest
 * @package App\Http\Requests\Dashboard\Product
 */
class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|array',
            'title.ua' => 'nullable|string',
            'title.ru' => 'nullable|string',
            'description' => 'nullable|array',
            'description.ua' => 'nullable|string',
            'description.ru' => 'nullable|string',
            'metaKey' => 'nullable|array',
            'metaKey.ua' => 'nullable|string',
            'metaKey.ru' => 'nullable|string',
            'metaDescription' => 'nullable|array',
            'metaDescription.ua' => 'nullable|string',
            'metaDescription.ru' => 'nullable|string',
            'salePrice' => 'required|numeric',
            'purchasePrice' => 'required|numeric',
            'slug' => 'nullable|string',
            'inStock' => 'nullable|boolean',

            'brandId' => 'required|exists:brands,id',
            'typeId' => 'required|exists:types,id',
            'colorsIds' => 'required|array',
            'colorsIds.*' => 'required|exists:colors,id',
            'seasonsIds' => 'required|array',
            'seasonsIds.*' => 'required|exists:seasons,id',
            'sizesIds' => 'required|array',
            'sizesIds.*' => 'required|exists:sizes,id',
            'payTypesIds' => 'required|array',
            'payTypesIds.*' => 'required|exists:pay_types,id',
            'deliveryIds' => 'required|array',
            'deliveryIds.*' => 'required|exists:delivery,id',
            'gendersIds' => 'required|array',
            'gendersIds.*' => 'required|exists:genders,id',
            'images' => 'nullable|array',
        ];
    }
}