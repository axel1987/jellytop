<?php

declare(strict_types=1);

namespace App\Http\Requests\Dashboard\Slider;

use App\Http\Requests\FormRequest;

/**
 * Class SliderIndexRequesst
 * @package App\Http\Requests\Dashboard\Slider
 */
class SliderIndexRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'page' => 'nullable|string',
            'perPage' => 'nullable|string',
            'field' => 'nullable|string',
            'direction' => 'nullable|string',
        ];
    }
}