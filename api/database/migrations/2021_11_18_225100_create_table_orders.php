<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateTableOrders
 */
class CreateTableOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();

            $table->string('name');
            $table->string('phone');
            $table->decimal('sum');

            $table->unsignedInteger('status_id');
            $table->unsignedInteger('delivery_id');
            $table->unsignedInteger('pay_type_id');

            $table->string('address')->nullable();
            $table->string('zip')->nullable();
            $table->string('department')->nullable();
            $table->text('note')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
