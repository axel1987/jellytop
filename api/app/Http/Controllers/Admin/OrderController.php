<?php

declare(strict_types=1);

namespace App\Http\Controllers\Admin;

use App\Application\Order\GetOrderByFilter\GetOrderByFilter;
use App\Application\Order\GetOrdersList\GetOrdersList;
use App\Application\Order\UpdateOrder\UpdateOrder;
use App\Application\OrderStatuses\GetOrderStatusByFilter\GetOrderStatusByFilter;
use App\Domain\Order\OrderFilter;
use App\Domain\OrderStatus\OrderStatusFilter;
use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Order\OrderIndexRequest;
use App\Http\Requests\Dashboard\Order\OrderRequest;
use App\Http\Resourses\Order\OrderListResource;
use App\Http\Resourses\Order\OrderResource;
use App\Infrastructure\Core\Pagination;
use App\Infrastructure\Core\Sorting;
use Illuminate\Http\JsonResponse;

/**
 * Class OrderController
 * @package App\Http\Controllers\Admin
 */
class OrderController extends Controller
{
    /**
     * @param OrderIndexRequest $request
     *
     * @return JsonResponse
     */
    public function index(OrderIndexRequest $request): JsonResponse
    {
        $filter = OrderFilter::fromRequest($request);
        $pagination = Pagination::fromRequest($request);
        $sorting = Sorting::fromRequest($request);

        $list = $this->execute(new GetOrdersList(
            $filter,
            $pagination,
            $sorting
        ));

        return new JsonResponse(new OrderListResource($list));
    }

    /**
     * @param $id
     *
     * @return JsonResponse
     */
    public function view($id): JsonResponse
    {
        $filter = new OrderFilter();
        $filter->setId((int)$id);

        $order = $this->execute(new GetOrderByFilter(
            $filter
        ));

        return new JsonResponse(new OrderResource($order));
    }

    /**
     * @param OrderRequest $request
     * @param $id
     *
     * @return JsonResponse
     */
    public function update(OrderRequest $request, $id)
    {
        $statusFilter = new OrderStatusFilter();
        $statusFilter->setId($request->get('statusId'));
        $orderStatus = $this->execute(new GetOrderStatusByFilter($statusFilter));

        $filter = new OrderFilter();
        $filter->setId((int)$id);
        $order = $this->execute(new GetOrderByFilter($filter));

        $order = $this->execute(new UpdateOrder(
            $order,
            $orderStatus,
            $request->get('note')
        ));

        return new JsonResponse(new OrderResource($order));
    }
}