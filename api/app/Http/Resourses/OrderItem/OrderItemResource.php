<?php

declare(strict_types=1);

namespace App\Http\Resourses\OrderItem;

use App\Domain\OrderItem\OrderItem;
use App\Http\Resourses\Product\ProductNotRelationResource;
use App\Http\Resourses\Size\SizeResource;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class OrderItemResource
 * @package App\Http\Resourses\OrderItem
 */
class OrderItemResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        /** @var OrderItem $this */
        return [
            'id' => $item->id,
            'type' => 'order-item',
            'attributes' => [
                'count' => $item->count,
                'price' => $item->price,
                'createdAt' => $item->created_at ? $item->created_at->format('Y-m-d H:i:s') : '',
                'updatedAt' => $item->updated_at ? $item->updated_at->format('Y-m-d H:i:s') : '',
            ],
            'relationships' => [
                'product' => new ProductNotRelationResource($item->product),
                'size' => new SizeResource($item->size)
            ]
        ];
    }
}