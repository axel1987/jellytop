<?php

declare(strict_types=1);

namespace App\Application\Delivery\GetDeliveryList;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\PaginationInterface;
use App\Contract\Core\SortingInterface;
use App\Domain\Delivery\DeliveryFilter;

/**
 * Class GetDeliveryList
 * @package App\Application\Delivery\GetDeliveryList
 */
class GetDeliveryList implements CommandInterface
{
    /** @var DeliveryFilter $filter */
    private DeliveryFilter $filter;

    /** @var PaginationInterface|null $pagination */
    private ?PaginationInterface $pagination;

    /** @var SortingInterface|null $sorting */
    private ?SortingInterface $sorting;

    /**
     * GetDeliveryList constructor
     *
     * @param DeliveryFilter $filter
     * @param PaginationInterface|null $pagination
     * @param SortingInterface|null $sorting
     */
    public function __construct(
        DeliveryFilter $filter,
        ?PaginationInterface $pagination = null,
        ?SortingInterface $sorting = null
    ) {
        $this->filter = $filter;
        $this->pagination = $pagination;
        $this->sorting = $sorting;
    }

    /**
     * @return DeliveryFilter
     */
    public function getFilter(): DeliveryFilter
    {
        return $this->filter;
    }

    /**
     * @return PaginationInterface|null
     */
    public function getPagination(): ?PaginationInterface
    {
        return $this->pagination;
    }

    /**
     * @return SortingInterface|null
     */
    public function getSorting(): ?SortingInterface
    {
        return $this->sorting;
    }
}