import Type from "../dto/entities/Type";
import Pagination from "../dto/entities/Pagination";

/**
 * @returns {{
 * type: null,
 * typesList: *[],
 * typesSelectList: *[],
 * pagination: null,
 * }}
 */
export const state = () => ({
    type: null,
    typesList: [],
    typesSelectList: [],
    pagination: null
});

/**
 *
 * @type {{
 * getType(*): null,
 * getTypesList(*): [],
 * getTypesSelectList(*): [],
 * getTypePagination(*): null
 * }}
 */
export const getters = {
    getType(state) {
        return state.type;
    },
    getTypesList(state) {
        return state.typesList;
    },
    getTypesSelectList(state) {
        return state.typesSelectList;
    },
    getTypePagination(state) {
        return state.pagination;
    }
};

/**
 * @type {{
 * SET_TYPES_LIST(*, *): void,
 * SET_TYPES_SELECT_LIST(*, *): void,
 * SET_TYPE(*, *): void,
 * SET_TYPES_PAGINATION(*, *): void,
 * }}
 */
export const mutations = {
    SET_TYPE(state, value) {
        state.type = value
    },
    SET_TYPES_LIST(state, value) {
        state.typesList = value
    },
    SET_TYPES_SELECT_LIST(state, value) {
        state.typesSelectList = value
    },
    SET_TYPES_PAGINATION(state, value) {
        state.pagination = value
    },
};

/**
 * @type {{
 * fetchType: ((function({commit: *}, *=): Promise<void>)|*),
 * fetchTypesList: ((function({commit: *}, *=): Promise<void>)|*),
 * fetchTypesSelectList: ((function({commit: *}, *=): Promise<void>)|*),
 * }}
 */
export const actions = {
    fetchTypesList: async function ({commit}, params = {}) {
        let res = await this.$axios.get('/types', {params: params});

        if (res.data) {
            const list = res.data.data.map((typeData) => {
                return new Type(typeData)
            });
            const pagination = res.data.pagination ? new Pagination(res.data.pagination) : null;

            commit('SET_TYPES_LIST', list)
            commit('SET_TYPES_PAGINATION', pagination)
        }

        return res;
    },
    fetchTypesSelectList: async function ({commit}, params = {}) {
        let res = await this.$axios.get('/types/list', params);

        if (res && res.data) {
            const list = res.data.data.map((typeData) => {
                return new Type(typeData)
            });

            commit('SET_TYPES_SELECT_LIST', list)
        }

        return res;
    },
    fetchType: async function ({commit}, params = {}) {
        if (params.id) {
            let {data} = await this.$axios.get(`/types/${params.id}`, params);
            commit('SET_TYPE', new Type(data))
        } else {
            commit('SET_TYPE', new Type(null))
        }
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations,
};
