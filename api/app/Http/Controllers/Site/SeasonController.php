<?php

declare(strict_types=1);

namespace App\Http\Controllers\Site;

use App\Application\Season\DeleteSeason\DeleteSeason;
use App\Application\Season\GetSeasonByFilter\GetSeasonByFilter;
use App\Application\Season\GetSeasonsList\GetSeasonsList;
use App\Application\Season\StoreSeason\StoreSeason;
use App\Application\Season\UpdateSeason\UpdateSeason;
use App\Domain\Season\SeasonFilter;
use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Season\SeasonIndexRequest;
use App\Http\Requests\Dashboard\Season\SeasonRequest;
use App\Http\Resourses\Season\SeasonListResource;
use App\Http\Resourses\Season\SeasonResource;
use App\Infrastructure\Core\Pagination;
use App\Infrastructure\Core\Sorting;
use Illuminate\Http\JsonResponse;

/**
 * Class SeasonController
 * @package App\Http\Controllers
 */
class SeasonController extends Controller
{
    /**
     * @param SeasonIndexRequest $request
     *
     * @return JsonResponse
     */
    public function index(SeasonIndexRequest $request): JsonResponse
    {
        $filter = SeasonFilter::fromRequest($request);
        $pagination = Pagination::fromRequest($request);
        $sorting = Sorting::fromRequest($request);

        $list = $this->execute(new GetSeasonsList(
            $filter,
            $pagination,
            $sorting
        ));

        return new JsonResponse(new SeasonListResource($list));
    }

    /**
     * @param SeasonIndexRequest $request
     *
     * @return JsonResponse
     */
    public function list(SeasonIndexRequest $request): JsonResponse
    {
        $filter = SeasonFilter::fromRequest($request);
        $sorting = Sorting::fromRequest($request);

        $list = $this->execute(new GetSeasonsList(
            $filter,
            null,
            $sorting
        ));

        return new JsonResponse(new SeasonListResource($list));
    }

    /**
     * @param int $id
     *
     * @return JsonResponse
     */
    public function view(int $id): JsonResponse
    {
        $filter = new SeasonFilter();
        $filter->setId($id);

        $season = $this->execute(new GetSeasonByFilter($filter));

        return new JsonResponse(new SeasonResource($season));
    }
}