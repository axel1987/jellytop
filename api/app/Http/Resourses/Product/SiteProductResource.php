<?php

declare(strict_types=1);

namespace App\Http\Resourses\Product;

use App\Domain\Product\Product;
use App\Http\Resourses\Brand\BrandResource;
use App\Http\Resourses\Color\ColorListResource;
use App\Http\Resourses\Delivery\DeliveryListResource;
use App\Http\Resourses\Gender\GenderListResource;
use App\Http\Resourses\PayType\PayTypeListResource;
use App\Http\Resourses\ProductImage\ProductImagesListResource;
use App\Http\Resourses\Season\SeasonListResource;
use App\Http\Resourses\Size\SizeListResource;
use App\Http\Resourses\Type\TypeResource;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class SiteProductResource
 * @package App\Http\Resourses\Product
 */
class SiteProductResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        /** @var Product $this */
        return [
            'id' => $this->id,
            'type' => 'product',
            'attributes' => [
                'title' => $this->title,
                'description' => $this->description,
                'slug' => $this->slug,
                'metaKey' => $this->meta_key,
                'metaDescription' => $this->meta_description,
                'salePrice' =>  $this->sale_price,
                'inStock' => $this->in_stock,

                'createdAt' => $this->created_at ? $this->created_at->format('Y-m-d H:i:s') : '',
                'updatedAt' => $this->updated_at ? $this->updated_at->format('Y-m-d H:i:s') : '',
            ],
            'relationships' => [
                'brand' => new BrandResource($this->brands->first()),
                'type' => new TypeResource($this->types->first()),
                'colors' => new ColorListResource($this->colors),
                'sizes' => new SizeListResource($this->sizes),
                'seasons' => new SeasonListResource($this->seasons),
                'delivery' => new DeliveryListResource($this->delivery),
                'payTypes' => new PayTypeListResource($this->payTypes),
                'images' => new ProductImagesListResource($this->images),
                'gender' => new GenderListResource($this->genders),
            ]
        ];
    }
}