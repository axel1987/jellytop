import Color from "../dto/entities/Color";
import Pagination from "../dto/entities/Pagination";

/**
 * @returns {{
 * color: null,
 * colorsList: *[],
 * colorsSelectList: *[],
 * pagination: null,
 * }}
 */
export const state = () => ({
    color: null,
    colorsList: [],
    colorsSelectList: [],
    pagination: null
});

/**
 *
 * @type {{
 * getColor(*): null,
 * getColorsList(*): [],
 * getColorsSelectList(*): [],
 * getColorPagination(*): null
 * }}
 */
export const getters = {
    getColor(state) {
        return state.color;
    },
    getColorsList(state) {
        return state.colorsList;
    },
    getColorsSelectList(state) {
        return state.colorsSelectList;
    },
    getColorPagination(state) {
        return state.pagination;
    }
};

/**
 * @type {{
 * SET_COLORS_LIST(*, *): void,
 * SET_COLORS_SELECT_LIST(*, *): void,
 * SET_COLOR(*, *): void,
 * SET_COLORS_PAGINATION(*, *): void,
 * }}
 */
export const mutations = {
    SET_COLOR(state, value) {
        state.color = value
    },
    SET_COLORS_LIST(state, value) {
        state.colorsList = value
    },
    SET_COLORS_SELECT_LIST(state, value) {
        state.colorsSelectList = value
    },
    SET_COLORS_PAGINATION(state, value) {
        state.pagination = value
    },
};

/**
 * @type {{
 * fetchColor: ((function({commit: *}, *=): Promise<void>)|*),
 * fetchColorsList: ((function({commit: *}, *=): Promise<void>)|*),
 * fetchColorsSelectList: ((function({commit: *}, *=): Promise<void>)|*),
 * }}
 */
export const actions = {
    fetchColorsList: async function ({commit}, params = {}) {
        let {data} = await this.$axios.get('/colors', params);

        const list = data.data.map((colorData) => {
            return new Color(colorData)
        });
        const pagination = data.pagination ? new Pagination(data.pagination) : null;

        commit('SET_COLORS_LIST', list)
        commit('SET_COLORS_PAGINATION', pagination)
    },
    fetchColorsSelectList: async function ({commit}, params = {}) {
        let res = await this.$axios.get('/colors/list', {params: params});

        if (res && res.data) {
            const list = res.data.data.map((colorData) => {
                return new Color(colorData)
            });

            commit('SET_COLORS_SELECT_LIST', list)
        }

        return res;
    },
    fetchColor: async function ({commit}, params = {}) {
        if (params.id) {
            let {data} = await this.$axios.get(`/colors/${params.id}`, params);
            commit('SET_COLOR', new Color(data))
        } else {
            commit('SET_COLOR', new Color(null))
        }
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations,
};
