<?php

declare(strict_types=1);

namespace App\Http\Requests\Dashboard\Order;

use App\Http\Requests\FormRequest;

/**
 * Class OrderRequest
 * @package App\Http\Requests\Dashboard\Order
 */
class OrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'statusId' => 'required|exists:order_statuses,id',
            'note' => 'nullable|string',
        ];
    }
}