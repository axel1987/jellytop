<?php

declare(strict_types=1);

namespace App\Application\Gender\StoreGender;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\Gender\Gender;
use App\Domain\Gender\GenderRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * Class StoreGenderHandler
 * @package App\Application\Gender\StoreGender
 */
class StoreGenderHandler implements HandlerInterface
{
    /** @var GenderRepositoryInterface $genderRepository */
    private GenderRepositoryInterface $genderRepository;

    /**
     * StoreGenderHandler constructor
     *
     * @param GenderRepositoryInterface $genderRepository
     */
    public function __construct(GenderRepositoryInterface $genderRepository)
    {
        $this->genderRepository = $genderRepository;
    }

    /**
     * @param CommandInterface|StoreGender $command
     *
     * @return Gender
     */
    public function handle(CommandInterface $command): Model
    {
        $gender = new Gender();

        $gender->title = $command->getTitle();
        $gender->icon = $command->getIcon();

        return $this->genderRepository->store($gender);
    }
}