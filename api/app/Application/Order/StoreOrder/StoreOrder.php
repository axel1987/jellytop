<?php

declare(strict_types=1);

namespace App\Application\Order\StoreOrder;

use App\Contract\Core\CommandInterface;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class StoreOrder
 * @package App\Application\Order\StoreOrder
 */
class StoreOrder implements CommandInterface
{
    /** @var Collection $cartItems */
    private Collection $cartItems;

    /** @var string $name */
    private string $name;

    /** @var string $phone */
    private string $phone;

    /** @var int $delivery */
    private int $delivery;

    /** @var int $payType */
    private int $payType;

    /** @var string|null $address */
    private ?string $address;

    /** @var string|null $department */
    private ?string $department;

    /** @var string|null $zip */
    private ?string $zip;

    /**
     * StoreOrder constructor
     *
     * @param Collection $cartItems
     * @param string $name
     * @param string $phone
     * @param int $delivery
     * @param int $payType
     * @param string|null $address
     * @param string|null $department
     * @param string|null $zip
     */
    public function __construct(
        Collection $cartItems,
        string $name,
        string $phone,
        int $delivery,
        int $payType,
        ?string $address = null,
        ?string $department = null,
        ?string $zip = null
    ) {
        $this->cartItems = $cartItems;
        $this->name = $name;
        $this->phone = $phone;
        $this->delivery = $delivery;
        $this->payType = $payType;
        $this->address = $address;
        $this->department = $department;
        $this->zip = $zip;
    }

    /**
     * @return Collection
     */
    public function getCartItems(): Collection
    {
        return $this->cartItems;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getPhone(): string
    {
        return $this->phone;
    }

    /**
     * @return int
     */
    public function getDelivery(): int
    {
        return $this->delivery;
    }

    /**
     * @return int
     */
    public function getPayType(): int
    {
        return $this->payType;
    }

    /**
     * @return string|null
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @return string|null
     */
    public function getDepartment(): ?string
    {
        return $this->department;
    }

    /**
     * @return string|null
     */
    public function getZip(): ?string
    {
        return $this->zip;
    }
}