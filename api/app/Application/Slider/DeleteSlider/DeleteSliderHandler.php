<?php

declare(strict_types=1);

namespace App\Application\Slider\DeleteSlider;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\Slider\SliderRepositoryInterface;
use Illuminate\Support\Facades\Storage;

/**
 * Class DeleteSliderHandler
 * @package App\Application\Slider\DeleteSlider
 */
class DeleteSliderHandler implements HandlerInterface
{
    /** @var SliderRepositoryInterface $sliderRepository */
    private SliderRepositoryInterface $sliderRepository;

    /**
     * DeleteSliderHandler constructor
     *
     * @param SliderRepositoryInterface $sliderRepository
     */
    public function __construct(SliderRepositoryInterface $sliderRepository)
    {
        $this->sliderRepository = $sliderRepository;
    }

    /**
     * @param CommandInterface|DeleteSlider $command
     *
     * @return void
     */
    public function handle(CommandInterface $command): void
    {
//        dd($command->getSlider()->link);
        Storage::disk('public')->delete(
            str_replace('/storage/', '', $command->getSlider()->link)
        );
        $this->sliderRepository->delete(
            $command->getSlider()
        );
    }
}