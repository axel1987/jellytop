<?php

declare(strict_types=1);

namespace App\Application\Gender\DeleteGender;

use App\Contract\Core\CommandInterface;
use App\Domain\Gender\Gender;

/**
 * Class DeleteGender
 * @package App\Application\Gender\DeleteGender
 */
class DeleteGender implements CommandInterface
{
    /** @var Gender $gender */
    private Gender $gender;

    /**
     * DeleteGender constructor
     *
     * @param Gender $gender
     */
    public function __construct(Gender $gender)
    {
        $this->gender = $gender;
    }

    /**
     * @return Gender
     */
    public function getGender(): Gender
    {
        return $this->gender;
    }
}