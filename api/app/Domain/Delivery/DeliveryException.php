<?php

declare(strict_types=1);

namespace App\Domain\Delivery;

use Exception;

/**
 * Class DeliveryException
 * @package App\Domain\Delivery
 */
class DeliveryException extends Exception
{
    /**
     * @param int $id
     *
     * @throws DeliveryException
     */
    public static function notFound(int $id)
    {
        throw new DeliveryException(sprintf('Color with id %d not found', $id));
    }
}