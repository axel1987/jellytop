<?php

declare(strict_types=1);

namespace App\Application\Brand\GetBrandsList;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\PaginationInterface;
use App\Contract\Core\SortingInterface;
use App\Domain\Brand\BrandFilter;

/**
 * Class GetBrandsList
 * @package App\Application\Brand\GetBrandsList
 */
class GetBrandsList implements CommandInterface
{
    /** @var BrandFilter $brandFilter */
    private BrandFilter $brandFilter;

    /** @var PaginationInterface|null $pagination */
    private ?PaginationInterface $pagination;

    /** @var SortingInterface|null $sorting */
    protected ?SortingInterface $sorting;

    /**
     * GetBrandsList constructor
     *
     * @param BrandFilter $brandFilter
     * @param PaginationInterface|null $pagination
     * @param SortingInterface|null $sorting
     */
    public function __construct(
        BrandFilter $brandFilter,
        ?PaginationInterface $pagination = null,
        ?SortingInterface $sorting = null
    ) {
        $this->brandFilter = $brandFilter;
        $this->pagination = $pagination;
        $this->sorting = $sorting;
    }

    /**
     * @return BrandFilter
     */
    public function getBrandFilter(): BrandFilter
    {
        return $this->brandFilter;
    }

    /**
     * @return PaginationInterface|null
     */
    public function getPagination(): ?PaginationInterface
    {
        return $this->pagination;
    }

    /**
     * @return SortingInterface|null
     */
    public function getSorting(): ?SortingInterface
    {
        return $this->sorting;
    }
}