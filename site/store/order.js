/**
 * @returns {{}}
 */
export const state = () => ({});

/**
 * @type {{}}
 */
export const getters = {};

/**
 * @type {{}}
 */
export const mutations = {};

/**
 * @type {{
 * storeOrder: (function({commit: *}, *=): any)
 * }}
 */
export const actions = {
    storeOrder: async function ({commit}, params = {}) {
        return await this.$axios.post(`/orders`, params);
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations,
};
