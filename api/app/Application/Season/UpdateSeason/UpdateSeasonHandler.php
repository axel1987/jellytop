<?php

declare(strict_types=1);

namespace App\Application\Season\UpdateSeason;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\Season\Season;
use App\Domain\Season\SeasonRepositoryInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

/**
 * Class UpdateSeasonHandler
 * @package App\Application\Season\UpdateSeason
 */
class UpdateSeasonHandler implements HandlerInterface
{
    /** @var SeasonRepositoryInterface $seasonRepository */
    private SeasonRepositoryInterface $seasonRepository;

    /**
     * UpdateSeasonHandler constructor
     *
     * @param SeasonRepositoryInterface $seasonRepository
     */
    public function __construct(SeasonRepositoryInterface $seasonRepository)
    {
        $this->seasonRepository = $seasonRepository;
    }

    /**
     * @param CommandInterface|UpdateSeason $command
     *
     * @return Season
     */
    public function handle(CommandInterface $command): Model
    {
        $oldIcon = null;
        $season = $command->getSeason();

        if ($season->icon != $command->getIcon()) {
            $oldIcon = $season->icon;
        }

        $season->title = $command->getTitle();
        $season->slug = $command->getSlug();
        $season->icon = $command->getIcon();
        $this->seasonRepository->store($season);

        if ($oldIcon) {
            Storage::disk('public')
                ->delete(str_replace('/storage', '', $oldIcon));
        }

        return $season;
    }
}