<?php

declare(strict_types=1);

namespace App\Application\Product\DeleteProduct;

use App\Contract\Core\CommandInterface;
use App\Contract\Core\HandlerInterface;
use App\Domain\Product\ProductRepositoryInterface;

/**
 * Class DeleteProductHandler
 * @package App\Application\Product\DeleteProduct
 */
class DeleteProductHandler implements HandlerInterface
{
    /** @var ProductRepositoryInterface $productRepository */
    private ProductRepositoryInterface $productRepository;

    /**
     * DeleteProductHandler constructor
     *
     * @param ProductRepositoryInterface $productRepository
     */
    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    /**
     * @param DeleteProduct $command
     *
     * @return void
     */
    public function handle(CommandInterface $command): void
    {
        $product = $command->getProduct();

        $product->images()->delete();
        $product->colors()->detach();
        $product->brands()->detach();
        $product->types()->detach();
        $product->sizes()->detach();
        $product->seasons()->detach();

        $this->productRepository->delete($product);
    }
}