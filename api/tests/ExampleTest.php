<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;
use LoadAnalyser\Config;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        Config::setQueryLog(true, 'resume');
        Config::setRunInformation(true);

        \LoadAnalyser\LoadAnalyser::point();
        $this->post(route('dashboard.login'), [
            'email' => 'test@test.com',
            'password' => 'testpass'
        ])->assertResponseStatus(\Illuminate\Http\Response::HTTP_OK);

        \LoadAnalyser\LoadAnalyser::results();
    }
}
