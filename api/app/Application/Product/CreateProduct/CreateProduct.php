<?php

declare(strict_types=1);

namespace App\Application\Product\CreateProduct;

use App\Contract\Core\CommandInterface;

/**
 * Class CreateProduct
 * @package App\Application\Product\CreateProduct
 * @see CreateProductHandler
 */
class CreateProduct implements CommandInterface
{
    /** @var array $brandIds */
    private array $brandIds;

    /** @var array $typeIds */
    private array $typeIds;

    /** @var array $colorsIds */
    private array $colorsIds;

    /** @var array $seasonsIds */
    private array $seasonsIds;

    /** @var array $sizesIds */
    private array $sizesIds;

    /** @var array $payTypesIds */
    private array $payTypesIds;

    /** @var array $deliveryIds */
    private array $deliveryIds;

    /** @var array $gendersIds */
    private array $gendersIds;

    /** @var array $title */
    private array $title;

    /** @var array $description */
    private array $description;

    /** @var array $metaKey */
    private array $metaKey;

    /** @var array $metaDescription */
    private array $metaDescription;

    /** @var float $salePrice */
    private float $salePrice;

    /** @var float $purchasePrice */
    private float $purchasePrice;

    /** @var string|null $slug */
    private ?string $slug;

    /** @var bool $inStock */
    private bool $inStock;

    /** @var int $priority */
    private int $priority;

    /**
     * CreateProduct constructor
     *
     * @param array $brandIds
     * @param array $typeIds
     * @param array $colorsIds
     * @param array $seasonsIds
     * @param array $sizesIds
     * @param array $payTypesIds
     * @param array $deliveryIds
     * @param array $gendersIds
     * @param array $title
     * @param array $description
     * @param array $metaKey
     * @param array $metaDescription
     * @param float $salePrice
     * @param float $purchasePrice
     * @param string|null $slug
     * @param bool $inStock
     * @param int $priority
     */
    public function __construct(
        array $brandIds,
        array $typeIds,
        array $colorsIds,
        array $seasonsIds,
        array $sizesIds,
        array $payTypesIds,
        array $deliveryIds,
        array $gendersIds,
        array $title,
        array $description,
        array $metaKey,
        array $metaDescription,
        float $salePrice,
        float $purchasePrice,
        ?string $slug,
        bool $inStock,
        int $priority
    ) {
        $this->brandIds = $brandIds;
        $this->typeIds = $typeIds;
        $this->colorsIds = $colorsIds;
        $this->seasonsIds = $seasonsIds;
        $this->sizesIds = $sizesIds;
        $this->payTypesIds = $payTypesIds;
        $this->deliveryIds = $deliveryIds;
        $this->gendersIds = $gendersIds;
        $this->title = $title;
        $this->description = $description;
        $this->metaKey = $metaKey;
        $this->metaDescription = $metaDescription;
        $this->salePrice = $salePrice;
        $this->purchasePrice = $purchasePrice;
        $this->slug = $slug;
        $this->inStock = $inStock;
        $this->priority = $priority;
    }


    /**
     * @return array
     */
    public function getBrandIds(): array
    {
        return $this->brandIds;
    }

    /**
     * @return array
     */
    public function getTypeIds(): array
    {
        return $this->typeIds;
    }

    /**
     * @return array
     */
    public function getColorsIds(): array
    {
        return $this->colorsIds;
    }

    /**
     * @return array
     */
    public function getSeasonsIds(): array
    {
        return $this->seasonsIds;
    }

    /**
     * @return array
     */
    public function getSizesIds(): array
    {
        return $this->sizesIds;
    }

    /**
     * @return array
     */
    public function getPayTypesIds(): array
    {
        return $this->payTypesIds;
    }

    /**
     * @return array
     */
    public function getDeliveryIds(): array
    {
        return $this->deliveryIds;
    }

    /**
     * @return array
     */
    public function getGendersIds(): array
    {
        return $this->gendersIds;
    }

    /**
     * @return array
     */
    public function getTitle(): array
    {
        return $this->title;
    }

    /**
     * @return array
     */
    public function getDescription(): array
    {
        return $this->description;
    }

    /**
     * @return array
     */
    public function getMetaKey(): array
    {
        return $this->metaKey;
    }

    /**
     * @return array
     */
    public function getMetaDescription(): array
    {
        return $this->metaDescription;
    }

    /**
     * @return float
     */
    public function getSalePrice(): float
    {
        return $this->salePrice;
    }

    /**
     * @return float
     */
    public function getPurchasePrice(): float
    {
        return $this->purchasePrice;
    }

    /**
     * @return string|null
     */
    public function getSlug(): ?string
    {
        return $this->slug;
    }

    /**
     * @return bool
     */
    public function isInStock(): bool
    {
        return $this->inStock;
    }

    /**
     * @return int
     */
    public function getPriority(): int
    {
        return $this->priority;
    }
}