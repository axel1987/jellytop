<?php

declare(strict_types=1);

namespace App\Http\Requests\Dashboard\File;

use App\Http\Requests\FormRequest;
use Illuminate\Support\Facades\Storage;

/**
 * Class UploadFileRequest
 * @package App\Http\Requests\Dashboard\File
 */
class UploadFileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'file' => 'image|mimes:jpeg,png,jpg,gif,svg|max:10240',
            'group' => 'required|string',
            'id' => 'nullable|string',
        ];
    }
}