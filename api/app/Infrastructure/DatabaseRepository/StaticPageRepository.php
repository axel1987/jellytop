<?php

declare(strict_types=1);

namespace App\Infrastructure\DatabaseRepository;

use App\Contract\Core\DatabaseRepositoryInterface;
use App\Contract\Core\FilterInterface;
use App\Contract\Core\SortingInterface;
use App\Domain\StaticPage\StaticPage;
use App\Domain\StaticPage\StaticPageFilter;
use App\Domain\StaticPage\StaticPageRepositoryInterface;

/**
 * Class StaticPageRepository
 * @package App\Infrastructure\DatabaseRepository
 */
class StaticPageRepository extends DatabaseRepository implements StaticPageRepositoryInterface
{
    /**
     * StaticPageRepository constructor
     */
    public function __construct()
    {
        $this->model = new StaticPage();
    }

    /**
     * @param FilterInterface|StaticPageFilter $filter
     *
     * @return $this
     */
    protected function filter(FilterInterface $filter): DatabaseRepositoryInterface
    {
        $this->builder = $this->builder ?? $this->model->newQuery();

        if ($filter->getId()) {
            $this->builder->where('id', '=', $filter->getId());
        }
        if ($filter->getName()) {
            $this->builder->where('page', '=', $filter->getName());
        }

        return $this;
    }

    /**
     * @param SortingInterface $sorting
     *
     * @return $this
     */
    protected function sorting(SortingInterface $sorting): DatabaseRepositoryInterface
    {
        return $this;
    }
}