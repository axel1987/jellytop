<?php

declare(strict_types=1);

namespace App\Domain\OrderItem;

use App\Contract\Core\FilterInterface;
use Illuminate\Http\Request;

/**
 * Class OrderItemFilter
 * @package App\Domain\OrderItem
 */
class OrderItemFilter implements FilterInterface
{
    /**
     * @param Request $request
     *
     * @return FilterInterface|OrderItemFilter
     */
    public static function fromRequest(Request $request): FilterInterface
    {
        $filter = new self();

        return $filter;
    }
}