<?php

declare(strict_types=1);

namespace App\Http\Resourses\Brand;

use App\Domain\Brand\Brand;
use App\Http\Resourses\Country\CountryResource;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class BrandResource
 * @package App\Http\Resourses\Brand
 */
class BrandResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        /** @var Brand $this */
        return [
            'id' => $this->id,
            'type' => 'brand',
            'attributes' => [
                'title' => $this->title,
                'description' => $this->description,
                'slug' => $this->slug,
                'logo' => $this->logo,
                'createdAt' => $this->created_at ? $this->created_at->format('Y-m-d H:i:s') : '',
                'updatedAt' => $this->updated_at ? $this->updated_at->format('Y-m-d H:i:s') : '',
            ],
            'relationships' => [
                'country' => new CountryResource($this->country)
            ]
        ];
    }
}