<?php

use Laravel\Lumen\Routing\Router;

/**
 * @var Router $router
 */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->group(['prefix' => 'admin'], function (Router $router) {
    /**
     * Authentications routes
     */
    $router->group(['prefix' => 'auth'], function (Router $router) {
        $router->post('sign-in', ['as' => 'login', 'uses' => 'AuthController@signIn']);
        $router->post('refresh-token', ['as' => 'refreshToken', 'uses' => 'AuthController@refreshToken']);
    });

    $router->group(['middleware' => 'auth-admin'], function (Router $router) {
        $router->group(['prefix' => 'users'], function (Router $router) {
            $router->get('/', 'UserController@index');
            $router->get('/current', 'UserController@current');
            $router->get('/{id:[0-9]+}', 'UserController@view');
            $router->put('/{id:[0-9]+}', 'UserController@update');
            $router->post('/', 'UserController@store');
            $router->delete('/{id:[0-9]+}', 'UserController@delete');
        });
        
        $router->group(['prefix' => 'brands'], function (Router $router) {
            $router->get('/', 'BrandController@index');
            $router->get('/list', 'BrandController@list');
            $router->get('/{id}', 'BrandController@view');
            $router->put('/{id}', 'BrandController@update');
            $router->post('/', 'BrandController@store');
            $router->delete('/{id}', 'BrandController@delete');
        });
        
        $router->group(['prefix' => 'colors'], function (Router $router) {
            $router->get('/', 'ColorController@index');
            $router->get('/list', 'ColorController@list');
            $router->get('/{id}', 'ColorController@view');
            $router->put('/{id}', 'ColorController@update');
            $router->post('/', 'ColorController@store');
            $router->delete('/{id}', 'ColorController@delete');
        });
        
        $router->group(['prefix' => 'countries'], function (Router $router) {
            $router->get('/', 'CountryController@index');
            $router->get('/list', 'CountryController@list');
            $router->get('/{id:[0-9]+}', 'CountryController@view');
            $router->put('/{id:[0-9]+}', 'CountryController@update');
            $router->post('/', 'CountryController@store');
            $router->delete('/{id:[0-9]+}', 'CountryController@delete');
        });
        
        $router->group(['prefix' => 'genders'], function (Router $router) {
            $router->get('/', 'GenderController@index');
            $router->get('/list', 'GenderController@list');
            $router->get('/{id}', 'GenderController@view');
            $router->put('/{id}', 'GenderController@update');
            $router->post('/', 'GenderController@store');
            $router->delete('/{id}', 'GenderController@delete');
        });
        
        $router->group(['prefix' => 'seasons'], function (Router $router) {
            $router->get('/', 'SeasonController@index');
            $router->get('/list', 'SeasonController@list');
            $router->get('/{id}', 'SeasonController@view');
            $router->put('/{id}', 'SeasonController@update');
            $router->post('/', 'SeasonController@store');
            $router->delete('/{id}', 'SeasonController@delete');
        });
        
        $router->group(['prefix' => 'sizes'], function (Router $router) {
            $router->get('/', 'SizeController@index');
            $router->get('/list', 'SizeController@list');
            $router->get('/{id}', 'SizeController@view');
            $router->put('/{id}', 'SizeController@update');
            $router->post('/', 'SizeController@store');
            $router->delete('/{id}', 'SizeController@delete');
        });
        
        $router->group(['prefix' => 'types'], function (Router $router) {
            $router->get('/', 'TypeController@index');
            $router->get('/list', 'TypeController@list');
            $router->get('/{id}', 'TypeController@view');
            $router->put('/{id}', 'TypeController@update');
            $router->post('/', 'TypeController@store');
            $router->delete('/{id}', 'TypeController@delete');
        });

        $router->group(['prefix' => 'delivery'], function (Router $router) {
            $router->get('/', 'DeliveryController@index');
            $router->get('/list', 'DeliveryController@list');
            $router->get('/{id}', 'DeliveryController@view');
            $router->put('/{id}', 'DeliveryController@update');
            $router->post('/', 'DeliveryController@store');
            $router->delete('/{id}', 'DeliveryController@delete');
        });

        $router->group(['prefix' => 'pay-type'], function (Router $router) {
            $router->get('/', 'PayTypeController@index');
            $router->get('/list', 'PayTypeController@list');
            $router->get('/{id}', 'PayTypeController@view');
            $router->put('/{id}', 'PayTypeController@update');
            $router->post('/', 'PayTypeController@store');
            $router->delete('/{id}', 'PayTypeController@delete');
        });

        $router->group(['prefix' => 'sliders'], function (Router $router) {
            $router->get('/', 'SliderController@index');
            $router->get('/{id}', 'SliderController@view');
            $router->put('/{id}', 'SliderController@update');
            $router->post('/', 'SliderController@store');
            $router->delete('/{id}', 'SliderController@delete');
        });

        $router->group(['prefix' => 'static-pages'], function (Router $router) {
            $router->get('/', 'StaticPageController@index');
            $router->get('/{id}', 'StaticPageController@view');
            $router->put('/{id}', 'StaticPageController@update');
            $router->post('/', 'StaticPageController@store');
            $router->delete('/{id}', 'StaticPageController@delete');
        });

        $router->group(['prefix' => 'order-statuses'], function (Router $router) {
            $router->get('/', 'OrderStatusController@index');
            $router->get('/list', 'OrderStatusController@list');
            $router->get('/{id}', 'OrderStatusController@view');
            $router->put('/{id}', 'OrderStatusController@update');
            $router->post('/', 'OrderStatusController@store');
            $router->delete('/{id}', 'OrderStatusController@delete');
        });

        $router->group(['prefix' => 'products'], function (Router $router) {
            $router->get('/', 'ProductController@index');
            $router->get('/list', 'ProductController@list');
            $router->get('/{id}', 'ProductController@view');
            $router->put('/{id}', 'ProductController@update');
            $router->put('/{id}/update-priority', 'ProductController@updatePriority');
            $router->put('/{id}/update-stock', 'ProductController@updateStock');
            $router->post('/', 'ProductController@store');
            $router->delete('/{id}', 'ProductController@delete');
        });

        $router->group(['prefix' => 'orders'], function (Router $router) {
            $router->get('/', 'OrderController@index');
            $router->get('/{id}', 'OrderController@view');
            $router->put('/{id}', 'OrderController@update');
        });

        $router->group(['prefix' => 'file'], function (Router $router) {
            $router->post('/', 'FileController@upload');
            $router->delete('/', 'FileController@remove');
        });
    });
});
