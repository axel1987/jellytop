<?php

declare(strict_types=1);

namespace App\Infrastructure\DatabaseRepository;

use App\Contract\Core\DatabaseRepositoryInterface;
use App\Contract\Core\FilterInterface;
use App\Contract\Core\SortingInterface;
use App\Domain\Gender\Gender;
use App\Domain\Gender\GenderRepositoryInterface;

/**
 * Class GenderRepository
 * @package App\Infrastructure\DatabaseRepository
 */
class GenderRepository extends DatabaseRepository implements GenderRepositoryInterface
{
    /**
     * GenderRepository constructor
     */
    public function __construct()
    {
        $this->model = new Gender();
    }

    /**
     * @param FilterInterface $filter
     *
     * @return $this
     */
    protected function filter(FilterInterface $filter): DatabaseRepositoryInterface
    {
        return $this;
    }

    /**
     * @param SortingInterface $sorting
     *
     * @return $this
     */
    protected function sorting(SortingInterface $sorting): DatabaseRepositoryInterface
    {
        return $this;
    }
}