<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * Class ProductBrandTableSeeder
 */
class ProductBrandTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product_brand')->insert([
            [
                'product_id' => 1,
                'brand_id' => 1,
            ],
        ]);
    }
}
