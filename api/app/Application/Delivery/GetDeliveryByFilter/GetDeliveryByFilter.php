<?php

declare(strict_types=1);

namespace App\Application\Delivery\GetDeliveryByFilter;

use App\Contract\Core\CommandInterface;
use App\Domain\Delivery\DeliveryFilter;

/**
 * Class GetDeliveryByFilter
 * @package App\Application\Delivery\GetDeliveryByFilter
 */
class GetDeliveryByFilter implements CommandInterface
{
    /** @var DeliveryFilter $filter */
    private DeliveryFilter $filter;

    /**
     * GetDeliveryByFilter constructor
     *
     * @param DeliveryFilter $filter
     */
    public function __construct(DeliveryFilter $filter)
    {
        $this->filter = $filter;
    }

    /**
     * @return DeliveryFilter
     */
    public function getFilter(): DeliveryFilter
    {
        return $this->filter;
    }
}