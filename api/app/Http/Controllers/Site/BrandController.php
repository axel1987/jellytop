<?php

declare(strict_types=1);

namespace App\Http\Controllers\Site;

use App\Application\Brand\DeleteBrand\DeleteBrand;
use App\Application\Brand\GetBrandByFilter\GetBrandByFilter;
use App\Application\Brand\GetBrandsList\GetBrandsList;
use App\Application\Brand\StoreBrand\StoreBrand;
use App\Application\Brand\UpdateBrand\UpdateBrand;
use App\Application\Country\GetCountryByFilter\GetCountryByFilter;
use App\Domain\Brand\BrandFilter;
use App\Domain\Country\CountryFilter;
use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Brand\BrandIndexRequest;
use App\Http\Requests\Dashboard\Brand\BrandRequest;
use App\Http\Resourses\Brand\BrandListResource;
use App\Http\Resourses\Brand\BrandResource;
use App\Infrastructure\Core\Pagination;
use App\Infrastructure\Core\Sorting;
use Illuminate\Http\JsonResponse;

/**
 * Class BrandController
 * @package App\Http\Controllers
 */
class BrandController extends Controller
{
    /**
     * @param BrandIndexRequest $request
     *
     * @return JsonResponse
     */
    public function index(BrandIndexRequest $request): JsonResponse
    {
        $filter = BrandFilter::fromRequest($request);
        $pagination = Pagination::fromRequest($request);
        $sorting = Sorting::fromRequest($request);

        $list = $this->execute(new GetBrandsList(
            $filter,
            $pagination,
            $sorting
        ));

        return new JsonResponse(new BrandListResource($list));
    }
    /**
     * @param BrandIndexRequest $request
     *
     * @return JsonResponse
     */
    public function list(BrandIndexRequest $request): JsonResponse
    {
        $filter = BrandFilter::fromRequest($request);
        $sorting = Sorting::fromRequest($request);

        $list = $this->execute(new GetBrandsList(
            $filter,
            null,
            $sorting
        ));

        return new JsonResponse(new BrandListResource($list));
    }

    /**
     * @param int $id
     *
     * @return JsonResponse
     */
    public function view(int $id): JsonResponse
    {
        $filter = new BrandFilter();
        $filter->setId($id);
        $brand = $this->execute(new GetBrandByFilter($filter));

        return new JsonResponse(new BrandResource($brand));
    }

    /**
     * @param BrandRequest $request
     * @param int $id
     *
     * @return JsonResponse
     */
    public function update(BrandRequest $request, int $id): JsonResponse
    {
        $countryFilter = new CountryFilter();
        $countryFilter->setId($request->get('countryId'));
        $country = $this->execute(new GetCountryByFilter($countryFilter));

        $filter = new BrandFilter();
        $filter->setId($id);
        $brand = $this->execute(new GetBrandByFilter($filter));
        $brand = $this->execute(new UpdateBrand(
            $brand,
            $country,
            $request->get('title'),
            $request->get('description'),
            $request->get('logo')
        ));

        return new JsonResponse(new BrandResource($brand));
    }


    /**
     * @param BrandRequest $request
     *
     * @return JsonResponse
     */
    public function store(BrandRequest $request): JsonResponse
    {
        $countryFilter = new CountryFilter();
        $countryFilter->setId($request->get('countryId'));
        $country = $this->execute(new GetCountryByFilter($countryFilter));

        $brand = $this->execute(new StoreBrand(
            $country,
            $request->get('title'),
            $request->get('description'),
            $request->get('logo')
        ));

        return new JsonResponse(new BrandResource($brand));
    }

    /**
     * @param int $id
     *
     * @return JsonResponse
     */
    public function delete(int $id): JsonResponse
    {
        $filter = new BrandFilter();
        $filter->setId($id);

        $brand = $this->execute(new GetBrandByFilter($filter));
        $this->execute(new DeleteBrand($brand));

        return new JsonResponse(null, 204);
    }
}