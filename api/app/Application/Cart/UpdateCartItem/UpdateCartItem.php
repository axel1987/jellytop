<?php

declare(strict_types=1);

namespace App\Application\Cart\UpdateCartItem;

use App\Domain\Cart\CartRepositoryInterface;

/**
 * Class UpdateCartItem
 * @package App\Application\Cart\UpdateCartItem
 */
class UpdateCartItem
{
    /** @var CartRepositoryInterface $cartRepository */
    private CartRepositoryInterface $cartRepository;

}