<?php

declare(strict_types=1);

namespace App\Http\Resourses\Brand;

use App\Domain\Brand\Brand;
use App\Http\Resourses\BaseCollectionResource;
use App\Http\Resourses\Country\CountryResource;

/**
 * Class BrandListResource
 * @package App\Http\Resourses\Brand
 */
class BrandListResource extends BaseCollectionResource
{
    /**
     * @param Brand $item
     *
     * @return array
     */
    protected function getItemData($item): array
    {
        return [
            'id' => $item->id,
            'type' => 'brand',
            'attributes' => [
                'title' => $item->title,
                'description' => $item->description,
                'slug' => $item->slug,
                'logo' => $item->logo,
                'createdAt' => $item->created_at ? $item->created_at->format('Y-m-d H:i:s') : '',
                'updatedAt' => $item->updated_at ? $item->updated_at->format('Y-m-d H:i:s') : '',
            ],
            'relationships' => [
                'country' => new CountryResource($item->country)
            ]
        ];
    }
}