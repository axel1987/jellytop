<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

/**
 * Class ProductTableSeeder
 */
class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now();

        DB::table('products')->insert([
            [
                'title' => json_encode([
                    'ua' => 'Хлопчик',
                    'ru' => 'Мальчик'
                ]),
                'slug' => 'boy-sneakers',
                'description' => json_encode([
                    'ua' => 'Тут якийсь текст по укр',
                    'ru' => 'Тут какой-то текст по рус'
                ]),
                'meta_key' => json_encode([
                    'ua' => 'meta_key Тут якийсь текст по укр',
                    'ru' => 'meta_key Тут какой-то текст по рус'
                ]),
                'meta_description' => json_encode([
                    'ua' => 'meta_description Тут якийсь текст по укр',
                    'ru' => 'meta_description Тут какой-то текст по рус'
                ]),
                'purchase_price' => 25.00,
                'sale_price' => 50.00,
                'in_stock' => true,

                'created_at' => $now,
                'updated_at' => $now,
            ],
        ]);
    }
}
